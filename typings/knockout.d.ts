import 'express';

// Global (non-imported) types
declare global {
  // Custom global variables
  namespace NodeJS {
    interface Global {
      __basedir: string;
    }
  }
  // Extend Request.app
  namespace Express {
    interface Application {
      datasource: any;
    }
  }
}

declare module 'express' {
  // Extend the Express request object
  interface Request {
    ipInfo?: string;
    isLoggedIn?: boolean;
    user?: User;
  }

  /**
   * Authenticated forum user
   */
  interface User {
    id: number;
    username?: string;
    usergroup: number;
    role_id: number;
    avatar_url?: string;
    background_url?: string;
    isBanned?: boolean;
    title?: string;
    createdAt?: Date;
  }
}

declare module 'redis' {
  // Allow custom properties in the redis client object
  interface RedisClient {
    getAsync: (key: string) => Promise<string>;
    mgetAsync: (keys: Array<string>) => Promise<object>;
    setAsync: (key: string, value: any) => Promise<string>;
    smembersAsync: (key: string) => Promise<string[]>;
    sismemberAsync: (key: string, member: string) => Promise<boolean>;
    saddAsync: (key: string, arg1: string | string[]) => Promise<boolean>;
    sremAsync: (key: string, arg1: string | string[]) => Promise<boolean>;
    incrAsync: (key: string) => Promise<number>;
    decrAsync: (key: string) => Promise<number>;
    zcardAsync: (key: string) => Promise<number>;
    zaddAsync: (key: string, score: number, member: string) => Promise<number>;
    zremrangebyrankAsync: (key: string, start: number, stop: number) => Promise<number>;
    zrevrangebyscoreAsync: (
      key: string,
      min: number | string,
      max: number | string,
      withscores: string,
      limit: string,
      offset: number,
      count: number
    ) => Promise<string[]>;
  }
}
