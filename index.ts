/* eslint-disable import/no-extraneous-dependencies */
// FIXME: this shouldn't be in the global scope, especially
// since ES6 imports happen before the current script is run,
// so anything else that's imported from this script that
// runs code in the global scope will see global.__basedir as
// undefined.
import cluster from 'cluster';
import express from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import fs from 'fs';
import helmet from 'helmet';
import compression from 'compression';
import session from 'express-session';
import http from 'http';
import https from 'https';
import limits from 'limits';
import os from 'os';
import { Server, Socket } from 'socket.io';

import 'reflect-metadata';
import { getMetadataArgsStorage, useExpressServer } from 'routing-controllers';
import { routingControllersToSpec } from 'routing-controllers-openapi';
import { validationMetadatasToSchemas } from 'class-validator-jsonschema';

import { createAdapter } from '@socket.io/redis-adapter';
import v2ErrorHandler from './src/middleware/v2ErrorHandler';
import redis from './src/services/redisClient';

import ipInfo from './src/middleware/ip';
import ipBan from './src/middleware/ipBan';
import datasource from './src/models/datasource';
import legacyRoute from './src/routes';
import validOrigins from './config/validOrigins';

import { JWT_SECRET } from './config/server';

import registerThreadHandler from './src/handlers/threadHandler';
import registerEventLogHandler from './src/handlers/eventLogHandler';
import registerThreadPostHandler from './src/handlers/threadPostHandler';
import { authentication } from './src/middleware/auth';

import errorHandler from './src/services/errorHandler';

const { defaultMetadataStorage } = require('class-transformer/cjs/storage');

// eslint-disable-next-line no-underscore-dangle
global.__basedir = process.cwd();

// eslint-disable-next-line no-underscore-dangle
global.__basedir = process.cwd();

const coreCount = os.cpus().length;
const RedisStore = require('connect-redis')(session);
const morgan = require('morgan');
const eiows = require('eiows');
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const timestamp = process.env.NODE_ENV !== 'development' ? require('log-timestamp') : undefined;
const { rateLimiterMiddleware } = require('./src/middleware/rateLimit');

console.log(process.env.MODERATION_WEBHOOK);

let clusterWorkers = parseInt(process.env.CLUSTER_WORKERS, 10);
// eslint-disable-next-line no-restricted-globals
if (!isFinite(clusterWorkers)) {
  // default value
  clusterWorkers = coreCount;
}

const limitsConfig = {
  enable: true,
  file_uploads: true,
  post_max_size: 8000000,
};

const app = express();

app.datasource = datasource();

morgan.token('user-id', (req) => req.user?.id);

app.use(
  morgan(
    '[:date[iso]] :remote-addr - :user-id ":method :url" :status :res[content-length] :response-time'
  )
);

const helmetOptions = {
  contentSecurityPolicy: {
    useDefaults: true,
  },
  crossOriginOpenerPolicy: {
    policy: 'unsafe-none',
  },
  crossOriginResourcePolicy: {
    policy: process.env.NODE_ENV === 'development' ? 'cross-origin' : 'same-origin',
  },
};

app.use(helmet(helmetOptions));

app.use(compression());
app.use(
  session({
    store: new RedisStore({ client: redis }),
    secret: JWT_SECRET,
    resave: false,
    saveUninitialized: false,
  })
);

if (process.env.NODE_ENV !== 'production') {
  validOrigins.push('http://localhost:8080');
  validOrigins.push('http://localhost:8081');
}

const corsOptions = {
  origin: validOrigins,
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  preflightContinue: false,
  optionsSuccessStatus: 204,
  exposedHeaders: 'Content-Range,X-Content-Range,X-Total-Count',
  credentials: true,
};

app.use(cors(corsOptions));

app.use(rateLimiterMiddleware);
app.use(ipInfo);
app.use(ipBan);
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true, parameterLimit: 50000 }));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(cookieParser());
app.use(limits(limitsConfig));

useExpressServer(app, {
  routePrefix: '/v2',
  controllers: [`${__dirname}/src/v2/*Controller.*`],
  middlewares: [v2ErrorHandler],
  cors: corsOptions,
  defaultErrorHandler: false,
});

app.use('/', legacyRoute);

const schemas = validationMetadatasToSchemas({
  refPointerPrefix: '#/components/schemas/',
  classTransformerMetadataStorage: defaultMetadataStorage,
});

const spec = routingControllersToSpec(
  getMetadataArgsStorage(),
  { routePrefix: '/v2' },
  { components: { schemas }, info: { title: 'Knockout! API', version: '2.0.0' } }
);

app.get('/schema', (req, res) => {
  const output = spec;
  // This path only takes multipart/form-data
  output.paths['/v2/users/{id}/profile/background'].put.requestBody.content['application/json'] =
    undefined;
  // Fix for multiple types
  output.components.schemas['Notification']['properties']['data'].$ref = undefined;
  output.components.schemas['Event']['properties']['data'].$ref = undefined;
  return res.json(spec);
});

if (clusterWorkers > 0 && cluster.isMaster) {
  console.log(`Starting ${clusterWorkers} Workers...`);
  // log events
  cluster.on('online', (worker) => {
    console.log(`Worker ${worker.process.pid} is online.`);
  });
  cluster.on('exit', (worker) => {
    console.log(`Worker ${worker.process.pid} died.`);
    cluster.fork();
  });
  // fork processes
  for (let i = 0; i < clusterWorkers; i++) {
    cluster.fork();
  }
} else {
  const setupSockets = (httpServer) => {
    const io: Server = new Server(httpServer, {
      adapter: createAdapter(redis, redis.duplicate()),
      transports: ['websocket'],
      cors: corsOptions,
      wsEngine: eiows.Server,
    });
    const wrap = (middleware) => (socket, next) => middleware(socket.request, {}, next);
    io.use(wrap(authentication.socket));
    io.on('connection', (socket: Socket) => {
      registerThreadHandler(io, socket);
      registerEventLogHandler(socket);
      registerThreadPostHandler(socket);
    });
    app.set('io', io);
  };

  if (process.env.NODE_ENV === 'production') {
    const httpServer = http.createServer(app);
    setupSockets(httpServer);
    httpServer.listen(8002, () => {
      console.log('🚀 HTTP Server running on port 8002');
    });
  }
  if (process.env.NODE_ENV === 'qa') {
    const privateKey = fs.readFileSync(
      `/etc/letsencrypt/live/${process.env.CERT_DOMAIN}/privkey.pem`,
      'utf8'
    );
    const certificate = fs.readFileSync(
      `/etc/letsencrypt/live/${process.env.CERT_DOMAIN}/cert.pem`,
      'utf8'
    );
    const ca = fs.readFileSync(
      `/etc/letsencrypt/live/${process.env.CERT_DOMAIN}/fullchain.pem`,
      'utf8'
    );
    const credentials = {
      key: privateKey,
      cert: certificate,
      ca,
    };
    const httpsServer = https.createServer(credentials, app);
    httpsServer.listen(3000, () => {
      console.log('🚀 HTTPS Server running on port 3000');
    });
  }
  if (process.env.NODE_ENV === 'development') {
    const httpServer = http.createServer(app);
    setupSockets(httpServer);
    httpServer.listen(3000, () => {
      console.log('🚀 HTTP Server running on port 3000');
    });
  }
}

errorHandler.catchHandler(app);

module.exports = app;
