'use strict';

export async function up(queryInterface, Sequelize) {
  await queryInterface.addColumn('Users', 'title', {
    allowNull: true,
    type: Sequelize.STRING,
  });
}

export async function down(queryInterface, Sequelize) {
  return queryInterface.removeColumn('Users', 'title');
}
