'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('Threads', 'subforum_id', {
      allowNull: false,
      type: Sequelize.INTEGER,
      defaultValue: 1
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('Threads', 'subforum_id', {
      allowNull: true,
      type: Sequelize.INTEGER,
    });
  }
};
