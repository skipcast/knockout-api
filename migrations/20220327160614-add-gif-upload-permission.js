'use strict';

import { knex } from '../src/services/knex';
import { RoleCode } from '../src/helpers/permissions';

// Add the 'user-avatar-gif-upload' to gold users and above
export async function up(queryInterface, Sequelize) {
  // insert new permission
  const permissionId = await knex('Permissions').insert({ code: 'user-avatar-gif-upload' });

  // attach permission to roles
  const goldAndUpRoleCodes = [
    RoleCode.GOLD_USER,
    RoleCode.MODERATOR_IN_TRAINING,
    RoleCode.MODERATOR,
    RoleCode.SUPER_MODERATOR,
    RoleCode.ADMIN,
  ];

  const roles = await knex('Roles').select('id').whereIn('code', goldAndUpRoleCodes);
  const roleIds = roles.map((role) => role?.id);

  roleIds.forEach(async (roleId) => {
    return knex('RolePermissions').insert({ role_id: roleId, permission_id: permissionId });
  });
}

export async function down(queryInterface, Sequelize) {
  // noop
}
