'use strict';

const { ROLE_PERMISSION_MAPPINGS } = require("../src/helpers/permissions");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    // do not run if we already have role permissions in the DB
    const rolePermissionCount = await queryInterface.sequelize.query(
      'SELECT COUNT(*) FROM RolePermissions',
      { type: Sequelize.QueryTypes.SELECT }
    ).then((result) => result[0].count);

    if (rolePermissionCount > 0) {
      return true;
    };

    const subforumIds = await queryInterface.sequelize.query(
      `SELECT id FROM Subforums`,
      { type: Sequelize.QueryTypes.SELECT }
    ).then((records) => records.map((record) => record.id));

    return Promise.all(
      ROLE_PERMISSION_MAPPINGS.map(async (rp) => {
        let subforumCodes = subforumIds.map((id) => (
          rp.subforumCodeSuffixes.map((suffix) => (
            `subforum-${id}-${suffix}`
          ))
        ));

        subforumCodes = [].concat(...subforumCodes);

        const codes = [
          ...rp.permissionCodes,
          ...subforumCodes
        ];

        const permissionIds = await queryInterface.sequelize.query(
          `SELECT id FROM Permissions WHERE code IN(:codes)`,
          {
            replacements: { codes },
            type: Sequelize.QueryTypes.SELECT
          }
        ).then((records) => records.map((record) => record.id));

        const roleId = await queryInterface.sequelize.query(
          `SELECT id FROM Roles WHERE code = ? LIMIT 1`,
          {
            replacements: [rp.roleCode],
            type: Sequelize.QueryTypes.SELECT 
          }
        ).then((results) => results[0].id);

        const rolePermissions = permissionIds.map((pid) => (
          { role_id: roleId, permission_id: pid }
        ));

        return queryInterface.bulkInsert('RolePermissions', rolePermissions);
      })
    );
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('RolePermissions', {});
  }
};
