// @ts-check
'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    // Add column
    await queryInterface.addColumn('Threads', 'background_url', {
      allowNull: true,
      type: Sequelize.STRING
    });
    await queryInterface.addColumn('Threads', 'background_type', {
      allowNull: true,
      type: Sequelize.STRING
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('Threads', 'background_url');
    await queryInterface.removeColumn('Threads', 'background_type');
  }
};
