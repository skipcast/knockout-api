'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('IpAddresses', 'post_id', {
      allowNull: true,
      type: Sequelize.INTEGER.UNSIGNED,
      references: {
        model: 'Posts',
        key: 'id'
      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('IpAddresses', 'post_id');
  }
};

