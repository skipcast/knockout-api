'use strict';

export async function up(queryInterface, Sequelize) {
  await queryInterface.addColumn('Users', 'donation_upgrade_expires_at', {
    allowNull: true,
    type: Sequelize.DATE,
  });
}

export async function down(queryInterface, Sequelize) {
  return queryInterface.removeColumn('Users', 'donation_upgrade_expires_at');
}
