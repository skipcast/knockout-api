'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addIndex('Events', 
    {
      fields: [
        {
          attribute: 'created_at',
          order: 'DESC'
        },
      ]
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.removeIndex(
      'Events',
      'events_created_at'
    );
  }
};
