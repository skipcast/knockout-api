'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    // Add column
    return queryInterface.addColumn('Alerts', 'last_post_number', {
      allowNull: true,
      type: Sequelize.INTEGER,
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Alerts', 'last_post_number');
  }
};

