'use strict';

import { knex } from '../src/services/knex';
import { RoleCode } from '../src/helpers/permissions';

// Add permissions related to Calendar Events and associate those permissions with the
// relevant roles
export async function up(queryInterface, Sequelize) {
  // insert new permissions
  const newPermissionCodes = [
    'calendar-event-create',
    'calendar-event-view',
    'calendar-event-update',
    'calendar-event-archive',
  ];

  // if we've already inserted the permissions, bail out
  const permissionsExist =
    (await knex('Permissions').whereIn('code', newPermissionCodes)).length > 0;

  if (permissionsExist) return;

  await knex('Permissions').insert(
    newPermissionCodes.map((code) => {
      return { code };
    })
  );

  const calendarPermissions = await knex('Permissions').whereIn('code', newPermissionCodes);
  const createPermissionId = calendarPermissions.find(
    (perm) => perm.code === 'calendar-event-create'
  ).id;

  const goldRoleId = await knex('Roles')
    .select('id')
    .where('code', RoleCode.GOLD_USER)
    .then((roles) => roles[0].id);

  await knex('RolePermissions').insert({ role_id: goldRoleId, permission_id: createPermissionId });

  // moderators and up can create, update, and archive calendar events
  const modAndUpRolesCodes = [
    RoleCode.MODERATOR_IN_TRAINING,
    RoleCode.MODERATOR,
    RoleCode.SUPER_MODERATOR,
    RoleCode.ADMIN,
  ];

  const roleIds = await knex('Roles')
    .select('id')
    .whereIn('code', modAndUpRolesCodes)
    .then((roles) => roles.map((role) => role.id));

  const calendarPermissionIds = calendarPermissions.map((perm) => perm.id);

  roleIds.forEach(async (roleId) => {
    calendarPermissionIds.forEach(async (permissionId) => {
      return knex('RolePermissions').insert({ role_id: roleId, permission_id: permissionId });
    });
  });
}

export async function down(queryInterface, Sequelize) {
  // noop
}
