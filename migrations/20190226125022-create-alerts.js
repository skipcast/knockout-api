'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Alerts', {
      user_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'Users',
          key: 'id'
        }
      },
      thread_id: {
        allowNull: false,
        type: Sequelize.INTEGER.UNSIGNED,
        references: {
          model: 'Threads',
          key: 'id'
        }
      },
      last_seen: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('NOW')
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('NOW')
      }
    }).then(() => {
      return queryInterface.sequelize.query(
        'ALTER TABLE `Alerts` ADD CONSTRAINT `alertkeys` PRIMARY KEY (`thread_id`, `user_id`)'
      ).then(()=> {
        'ALTER TABLE `Alerts` ADD CONSTRAINT `Alerts_ibfk_1` REFERENCES Users (`user_id`)'
      }).then(()=> {
        'ALTER TABLE `Posts` ADD CONSTRAINT `Alerts_ibfk_2` REFERENCES Posts (`thread_id`)'
      });
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Alerts');
  }
};
