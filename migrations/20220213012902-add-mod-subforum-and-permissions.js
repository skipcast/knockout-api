'use strict';

import { knex } from '../src/services/knex';
import {
  RoleCode,
  ROLE_PERMISSION_MAPPINGS,
  subforumPermissions
} from '../src/helpers/permissions';

// Add the Community Team Chat subforum and the related permissions
export async function up(queryInterface, Sequelize) {
  // insert subforum
  const subforumId = await knex('Subforums').insert({
    name: 'Community Team Chat',
    icon_id: 1,
    icon: 'https://img.icons8.com/fluency/96/000000/fire-element.png',
    nsfw: false,
    description: "Chat with moderators, admins, and the rest of the team!"
  }).then((subforums) => subforums[0]);
  
  // insert new permissions
  const permissionRecords = subforumPermissions([subforumId]);
  await knex('Permissions').insert(permissionRecords);

  // attach new permissions to new roles
  const newSubforumPermissionCodes = ROLE_PERMISSION_MAPPINGS.find((rpmap) => rpmap.roleCode == 'moderator').subforumCodeSuffixes.map(
    (suffix) => `subforum-${subforumId}-${suffix}`
  );

  const permissions = await knex('Permissions').select('id').whereIn('code', newSubforumPermissionCodes);
  const permissionIds = permissions.map(((permission) => permission?.id));

  const teamRoleCodes = [
    RoleCode.MODERATOR_IN_TRAINING,
    RoleCode.MODERATOR,
    RoleCode.SUPER_MODERATOR,
    RoleCode.ADMIN,
  ];

  const roles = await knex('Roles').select('id').whereIn('code', teamRoleCodes);
  const roleIds = roles.map((role) => role?.id);

  roleIds.forEach(async (roleId) => {
    const rolePermissions = permissionIds.map((pid) => ({
      role_id: roleId,
      permission_id: pid,
    }));

    return knex('RolePermissions').insert(rolePermissions);
  })
}

export async function down(queryInterface, Sequelize) {
  // noop
}
