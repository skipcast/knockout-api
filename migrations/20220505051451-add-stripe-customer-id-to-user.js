'use strict';

export async function up(queryInterface, Sequelize) {
  await queryInterface.addColumn('Users', 'stripe_customer_id', {
    allowNull: true,
    type: Sequelize.STRING,
  });
}

export async function down(queryInterface, Sequelize) {
  return queryInterface.removeColumn('Users', 'stripe_customer_id');
}
