'use strict';

import { knex } from '../src/services/knex';
import { RoleCode } from '../src/helpers/permissions';

export async function up(queryInterface, Sequelize) {
  // add Paid Gold User role with the same permissions that the Golde User role has

  // bail out if role already exists
  if ((await knex('Roles').where({ code: RoleCode.PAID_GOLD_USER })).length > 0) {
    return;
  }

  // insert role
  await knex('Roles').insert({
    code: RoleCode.PAID_GOLD_USER,
    description: 'A Gold User who has this role through a Knockout Gold subscription',
  });

  // get permission IDs of Gold User role and create corresponding RolePermissions
  // for the Paid Gold User role
  const goldUserRoleId = (await knex('Roles').select('id').where('code', RoleCode.GOLD_USER))[0].id;
  const goldUserPermissionIds = (
    await knex('RolePermissions').where({ role_id: goldUserRoleId })
  ).map((rp) => rp.permission_id);

  const paidGoldUserRoleId = (
    await knex('Roles').select('id').where('code', RoleCode.PAID_GOLD_USER)
  )[0].id;

  const rolePermissions = goldUserPermissionIds.map((pid) => ({
    role_id: paidGoldUserRoleId,
    permission_id: pid,
  }));

  return knex('RolePermissions').insert(rolePermissions);
}

export async function down(queryInterface, Sequelize) {
  /**
   * Add reverting commands here.
   *
   * Example:
   * await queryInterface.dropTable('users');
   */
}
