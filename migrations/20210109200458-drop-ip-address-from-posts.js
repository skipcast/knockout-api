'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.removeIndex('Posts', 'posts_ip_address');
    return queryInterface.removeColumn('Posts', 'ip_address');
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('Posts', 'ip_address', {
      type: Sequelize.STRING
    });
    return queryInterface.addIndex('Posts', ['ip_address'])
  }
};

