'use strict';

import { knex } from '../src/services/knex';
import {
  RoleCode,
  ROLE_PERMISSION_MAPPINGS,
  subforumPermissions
} from '../src/helpers/permissions';

// Add the Whack-Ass Crystal Prison subforum and the related permissions
export async function up(queryInterface, Sequelize) {
  // insert subforum
  const subforumId = await knex('Subforums').insert({
    name: 'Whack-Ass Crystal Prison',
    icon_id: 1,
    icon: 'https://img.icons8.com/color/344/ruby-gemstone.png',
    nsfw: false,
    description: "Complain about being banned here"
  }).then((subforums) => subforums[0]);

  // insert new permissions
  const permissionRecords = subforumPermissions([subforumId]);
  await knex('Permissions').insert(permissionRecords);

  // attach new permissions to new roles
  const moderatorSubforumPermissionCodes = ROLE_PERMISSION_MAPPINGS.find((rpmap) => rpmap.roleCode == 'moderator').subforumCodeSuffixes.map(
    (suffix) => `subforum-${subforumId}-${suffix}`
  );

  const bannedUserSubforumSuffixes = [
    'view-own-threads',
    'post-create',
    'thread-create'
  ];

  const bannedUserCodes = bannedUserSubforumSuffixes.map(
    (suffix) => `subforum-${subforumId}-${suffix}`
  );

  const moderatorRoleCodes = [
    RoleCode.MODERATOR_IN_TRAINING,
    RoleCode.MODERATOR,
    RoleCode.SUPER_MODERATOR,
    RoleCode.ADMIN,
  ];
  const addNewPermissions = async (permissionCodes, roleCodes) => {
    const permissionIds = (await knex('Permissions').select('id').whereIn('code', permissionCodes)).map((permission) => permission?.id);
    console.log(permissionCodes, permissionIds);

    const roleIds = (await knex('Roles').select('id').whereIn('code', roleCodes)).map((role) => role?.id);
    roleIds.forEach(async (roleId) => {
      const rolePermissions = permissionIds.map((pid) => ({
        role_id: roleId,
        permission_id: pid,
      }));

      return knex('RolePermissions').insert(rolePermissions);
    })
  };

  addNewPermissions(moderatorSubforumPermissionCodes, moderatorRoleCodes);
  addNewPermissions(bannedUserCodes, [RoleCode.BANNED_USER]);
}

export async function down(queryInterface, Sequelize) {
  // noop
}
