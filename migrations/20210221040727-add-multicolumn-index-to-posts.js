'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addIndex('Posts', 
    {
      fields: [
        {
          attribute: 'thread_id'
        },
        {
          attribute: 'thread_post_number'
        },
        {
          attribute: 'created_at',
          order: 'DESC'
        },
      ]
    });
    return queryInterface.removeIndex('Posts', 'posts_thread_id_thread_post_number');
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.addIndex('Threads', ['thread_id', 'thread_post_number']);
    return queryInterface.removeIndex(
      'Posts',
      'posts_thread_id_thread_post_number_created_at'
    );
  }
};
