FROM node:16.15

WORKDIR /usr/src/server
COPY . .
RUN yarn
RUN mv .env.example .env
CMD sleep 20 && yarn sequelize db:migrate && yarn build && node dist/index.js