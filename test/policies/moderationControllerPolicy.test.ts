import { Response } from 'express';
import { ForbiddenError } from 'routing-controllers';
import {
  getIpsByUsername,
  getUsernamesByIp,
  changeThreadStatus,
  removeUserImage,
  removeUserProfile,
  getLatestUsers,
  getDashboardData,
  getFullUserInfo,
  makeBanInvalid,
  getAdminSettings,
  setAdminSettings,
} from '../../src/policies/moderationControllerPolicy';
import createUser from '../factories/user';
import createRole from '../factories/role';
import createSubforum from '../factories/subforum';
import createThread from '../factories/thread';
import { addPermissionCodesToRole } from '../helper/testHelper';

describe('moderationControllerPolicy', () => {
  let mockRequest;
  let role;
  let user;
  const mockResponse = {} as Response;
  mockResponse.status = jest.fn().mockReturnValue(mockResponse);
  mockResponse.json = jest.fn().mockReturnValue(mockResponse);
  let mockNext;

  beforeEach(async () => {
    role = await createRole();
    user = await createUser({ roleId: role.id });
    mockNext = jest.fn();
  });

  describe('getIpsByUsername', () => {
    beforeEach(async () => {
      mockRequest = {
        user: {
          id: user.id,
        },
      };
    });

    test('user with ipAddress-view permission is authorized', async () => {
      await addPermissionCodesToRole(role.id, ['ipAddress-view']);
      await getIpsByUsername(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user without ipAddress-view permission is not authorized', async () => {
      await expect(getIpsByUsername(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
    });
  });

  describe('getUsernamesByIp', () => {
    beforeEach(async () => {
      mockRequest = {
        user: {
          id: user.id,
        },
      };
    });

    test('user with ipAddress-view permission is authorized', async () => {
      await addPermissionCodesToRole(role.id, ['ipAddress-view']);
      await getUsernamesByIp(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user without ipAddress-view permission is not authorized', async () => {
      await expect(getUsernamesByIp(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
    });
  });

  describe('changeThreadStatus', () => {
    let subforum;

    beforeEach(async () => {
      subforum = await createSubforum();
      const threadUser = await createUser();
      const thread = await createThread({ subforum_id: subforum.id, user_id: threadUser.id });
      mockRequest = {
        body: {
          threadId: thread.id,
        },
        user: {
          id: user.id,
        },
      };
    });

    test('user with change-thread status permission for thread subforum is authorized', async () => {
      await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-thread-update`]);
      await changeThreadStatus(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user without change-thread status permission for thread subforum is authorized', async () => {
      await expect(changeThreadStatus(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
    });
  });

  describe('removeUserImage', () => {
    beforeEach(() => {
      mockRequest = {
        user: {
          id: user.id,
        },
      };
    });

    test('user with user-update permission is authorized', async () => {
      await addPermissionCodesToRole(role.id, ['user-update']);
      await removeUserImage(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user without user-update permission is not authorized', async () => {
      await expect(removeUserImage(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
    });
  });

  describe('removeUserProfile', () => {
    beforeEach(() => {
      mockRequest = {
        user: {
          id: user.id,
        },
      };
    });

    test('user with user-archive permission is authorized', async () => {
      await addPermissionCodesToRole(role.id, ['user-archive']);
      await removeUserProfile(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user without user-archive permission is not authorized', async () => {
      await expect(removeUserProfile(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
    });
  });

  describe('getLatestUsers', () => {
    beforeEach(() => {
      mockRequest = {
        user: {
          id: user.id,
        },
      };
    });

    test('user with latest-users-view permission is authorized', async () => {
      await addPermissionCodesToRole(role.id, ['latest-users-view']);
      await getLatestUsers(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user without latest-users-view permission is not authorized', async () => {
      await expect(getLatestUsers(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
    });
  });

  describe('getDashboardData', () => {
    beforeEach(() => {
      mockRequest = {
        user: {
          id: user.id,
        },
      };
    });

    test('user with dashboard-data-view permission is authorized', async () => {
      await addPermissionCodesToRole(role.id, ['dashboard-data-view']);
      await getDashboardData(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user without dashboard-data-view permission is not authorized', async () => {
      await expect(getDashboardData(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
    });
  });

  describe('getFullUserInfo', () => {
    beforeEach(() => {
      mockRequest = {
        user: {
          id: user.id,
        },
      };
    });

    test('user with full-user-info-view permission is authorized', async () => {
      await addPermissionCodesToRole(role.id, ['full-user-info-view']);
      await getFullUserInfo(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user without full-user-info-view permission is not authorized', async () => {
      await expect(getFullUserInfo(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
    });
  });

  describe('makeBanInvalid', () => {
    beforeEach(() => {
      mockRequest = {
        user: {
          id: user.id,
        },
      };
    });

    test('user with ban-archive permission is authorized', async () => {
      await addPermissionCodesToRole(role.id, ['ban-archive']);
      await makeBanInvalid(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user without ban-archive permission is not authorized', async () => {
      await expect(makeBanInvalid(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
    });
  });

  describe('getAdminSettings', () => {
    beforeEach(() => {
      mockRequest = {
        user: {
          id: user.id,
        },
      };
    });

    test('user with admin-settings-view permission is authorized', async () => {
      await addPermissionCodesToRole(role.id, ['admin-settings-view']);
      await getAdminSettings(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user without admin-settings-view permission is not authorized', async () => {
      await expect(getAdminSettings(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
    });
  });

  describe('setAdminSettings', () => {
    beforeEach(() => {
      mockRequest = {
        user: {
          id: user.id,
        },
      };
    });

    test('user with admin-settings-update permission is authorized', async () => {
      await addPermissionCodesToRole(role.id, ['admin-settings-update']);
      await setAdminSettings(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user without admin-settings-update permission is not authorized', async () => {
      await expect(setAdminSettings(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
    });
  });
});
