import { Response } from 'express';
import { ForbiddenError } from 'routing-controllers';
import createUser from '../factories/user';
import createRole from '../factories/role';
import { update } from '../../src/policies/userControllerPolicy';

describe('userControllerPolicy', () => {
  let mockRequest;
  let role;
  let user;
  const mockResponse = {} as Response;
  mockResponse.status = jest.fn().mockReturnValue(mockResponse);
  mockResponse.json = jest.fn().mockReturnValue(mockResponse);
  let mockNext;

  beforeEach(async () => {
    role = await createRole();
    user = await createUser({ roleId: role.id });
    mockNext = jest.fn();
  });

  describe('update', () => {
    beforeEach(() => {
      mockRequest = {
        params: {
          id: user.id,
        },
        user: {
          id: user.id,
        },
      };
    });

    test('user is authorized to update themselves', async () => {
      await update(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user is not authorized to update another user', async () => {
      const otherUser = await createUser();
      mockRequest.user.id = otherUser.id;
      await expect(update(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError()
      );
    });
  });
});
