import { Response } from 'express';
import { ForbiddenError } from 'routing-controllers';
import { store, update, destroy } from '../../src/policies/ruleControllerPolicy';
import createUser from '../factories/user';
import createRole from '../factories/role';
import createRule from '../factories/rule';
import { addPermissionCodesToRole } from '../helper/testHelper';

describe('ruleControllerPolicy', () => {
  let mockRequest;
  let role;
  let user;
  const mockResponse = {} as Response;
  mockResponse.status = jest.fn().mockReturnValue(mockResponse);
  mockResponse.json = jest.fn().mockReturnValue(mockResponse);
  let mockNext;

  beforeEach(async () => {
    role = await createRole();
    user = await createUser({ roleId: role.id });
    mockNext = jest.fn();
  });

  describe('store', () => {
    beforeEach(() => {
      mockRequest = {
        user: {
          id: user.id,
        },
      };
    });

    test('user with rule-create permission is authorized', async () => {
      await addPermissionCodesToRole(role.id, ['rule-create']);
      await store(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user without rule-create permission is not authorized', async () => {
      await expect(store(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
    });
  });

  describe('update', () => {
    beforeEach(async () => {
      const rule = await createRule();

      mockRequest = {
        params: {
          id: rule.id,
        },
        user: {
          id: user.id,
        },
      };
    });

    test('user with rule-update permission is authorized', async () => {
      await addPermissionCodesToRole(role.id, ['rule-update']);
      await update(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user without rule-update permission is not authorized', async () => {
      await expect(update(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
    });
  });

  describe('destroy', () => {
    beforeEach(async () => {
      const rule = await createRule();

      mockRequest = {
        params: {
          id: rule.id,
        },
        user: {
          id: user.id,
        },
      };
    });

    test('user with rule-archive permission is authorized', async () => {
      await addPermissionCodesToRole(role.id, ['rule-archive']);
      await destroy(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user without rule-archive permission is not authorized', async () => {
      await expect(destroy(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
    });
  });
});
