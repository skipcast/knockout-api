import { Response } from 'express';
import { ForbiddenError } from 'routing-controllers';
import {
  update,
  updateBackground,
  updateHeader,
  removeHeader,
  createComment,
  deleteComment,
  wipeAccount,
} from '../../src/policies/userProfileControllerPolicy';
import createUser from '../factories/user';
import createRole from '../factories/role';
import createUserProfile from '../factories/userProfile';
import createProfileComment from '../factories/profileComment';
import { addPermissionCodesToRole } from '../helper/testHelper';

describe('userProfileControllerPolicy', () => {
  let mockRequest;
  let role;
  let user;
  const mockResponse = {} as Response;
  mockResponse.status = jest.fn().mockReturnValue(mockResponse);
  mockResponse.json = jest.fn().mockReturnValue(mockResponse);
  let mockNext;

  beforeEach(async () => {
    role = await createRole();
    user = await createUser({ roleId: role.id });
    mockNext = jest.fn();
  });

  describe('update', () => {
    beforeEach(() => {
      mockRequest = {
        params: {
          id: user.id,
        },
        user: {
          id: user.id,
        },
      };
    });

    test('user is authorized to update their own profile', async () => {
      await update(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user is not authorized to update another users profile', async () => {
      const otherUser = await createUser();
      mockRequest.user.id = otherUser.id;
      await expect(update(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError()
      );
    });
  });

  describe('updateBackground', () => {
    beforeEach(() => {
      mockRequest = {
        params: {
          id: user.id,
        },
        user: {
          id: user.id,
        },
      };
    });

    test('user is authorized to update their own background', async () => {
      await updateBackground(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user is not authorized to update another users background', async () => {
      const otherUser = await createUser();
      mockRequest.user.id = otherUser.id;
      await expect(updateBackground(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError()
      );
    });
  });

  describe('updateHeader', () => {
    beforeEach(() => {
      mockRequest = {
        params: {
          id: user.id,
        },
        user: {
          id: user.id,
        },
      };
    });

    test('user is authorized to update their own header', async () => {
      await updateHeader(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user is not authorized to update another users header', async () => {
      const otherUser = await createUser();
      mockRequest.user.id = otherUser.id;
      await expect(updateHeader(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError()
      );
    });
  });

  describe('removeHeader', () => {
    beforeEach(() => {
      mockRequest = {
        params: {
          id: user.id,
        },
        user: {
          id: user.id,
        },
      };
    });

    test('user is authorized to remove their own header', async () => {
      await removeHeader(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user is authorized to remove another users header with user-update permission', async () => {
      await addPermissionCodesToRole(role.id, ['user-update']);
      const otherUser = await createUser();
      mockRequest.params.id = otherUser.id;
      await removeHeader(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user is not authorized to remove another users header without correct permission', async () => {
      const otherUser = await createUser();
      mockRequest.params.id = otherUser.id;
      await expect(removeHeader(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
    });
  });

  describe('createComment', () => {
    let userProfile;

    beforeEach(async () => {
      userProfile = await createUserProfile({ user_id: user.id });
      mockRequest = {
        params: {
          id: user.id,
        },
        user: {
          id: user.id,
        },
      };
    });

    test('user is not authorized without the correct permission', async () => {
      await expect(createComment(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
    });

    describe('when the user has the correct permission', () => {
      beforeEach(async () => {
        await addPermissionCodesToRole(role.id, ['user-profile-comment-create']);
      });

      test('user is authorized to comment when the user profile does not have comments disabled', async () => {
        await createComment(mockRequest, mockResponse, mockNext);
        expect(mockNext).toHaveBeenCalled();
      });

      test('user is not authorized to comment when the user profile has comments disabled', async () => {
        userProfile.disable_comments = true;
        await userProfile.save();
        await expect(createComment(mockRequest, mockResponse, mockNext)).rejects.toThrow(
          new ForbiddenError("Cannot comment on user's profile.")
        );
      });
    });
  });

  describe('deleteComment', () => {
    let comment;
    let userProfile;

    beforeEach(async () => {
      comment = await createProfileComment({ user_profile: user.id, author: user.id });
      userProfile = await createUserProfile({ user_id: user.id });

      mockRequest = {
        params: {
          id: userProfile.id,
          commentId: comment.id,
        },
        user: {
          id: user.id,
        },
      };
    });

    test('user is authorized to delete their own comment', async () => {
      await deleteComment(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user is authorized to delete their own comment on another users profile', async () => {
      const otherUser = await createUser();
      comment.update({ user_profile: otherUser.id });
      await deleteComment(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user is not authorized to remove another users comment an another users profile without the correct permission', async () => {
      const otherUser = await createUser();
      await comment.update({ user_profile: otherUser.id, author: otherUser.id });
      await expect(deleteComment(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
    });

    test('user is  authorized to remove another users comment an another users profile with the correct permission', async () => {
      await addPermissionCodesToRole(role.id, ['user-profile-comment-archive']);
      const otherUser = await createUser();
      comment.update({ user_profile: otherUser.id, author: otherUser.id });
      await deleteComment(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });
  });

  describe('wipeAccount', () => {
    beforeEach(() => {
      mockRequest = {
        params: {
          id: user.id,
        },
        user: {
          id: user.id,
        },
      };
    });

    test('user is authorized to wipe their own account', async () => {
      await wipeAccount(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user is authorized to wipe another users account with user-archive permission', async () => {
      await addPermissionCodesToRole(role.id, ['user-archive']);
      const otherUser = await createUser();
      mockRequest.params.id = otherUser.id;
      await wipeAccount(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user is not authorized to wipe another users account without correct permission', async () => {
      const otherUser = await createUser();
      mockRequest.params.id = otherUser.id;
      await expect(wipeAccount(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
    });
  });
});
