import { Response } from 'express';
import { ForbiddenError } from 'routing-controllers';
import { store } from '../../src/policies/tagControllerPolicy';
import createUser from '../factories/user';
import createRole from '../factories/role';
import { addPermissionCodesToRole } from '../helper/testHelper';

describe('tagControllerPolicy', () => {
  let mockRequest;
  let user;
  let role;
  const mockResponse = {} as Response;
  mockResponse.status = jest.fn().mockReturnValue(mockResponse);
  mockResponse.json = jest.fn().mockReturnValue(mockResponse);
  let mockNext;

  beforeEach(async () => {
    role = await createRole();
    user = await createUser({ roleId: role.id });
    mockNext = jest.fn();
  });

  describe('store', () => {
    beforeEach(() => {
      mockRequest = {
        user: {
          id: user.id,
        },
      };
    });

    test('user with tag-create permission is authorized', async () => {
      await addPermissionCodesToRole(role.id, ['tag-create']);
      await store(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user without tag-create permission is not authorized', async () => {
      await expect(store(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
    });
  });
});
