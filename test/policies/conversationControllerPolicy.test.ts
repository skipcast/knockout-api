import { Response } from 'express';
import { ForbiddenError } from 'routing-controllers';
import { show } from '../../src/policies/conversationControllerPolicy';
import createUser from '../factories/user';
import createConversation from '../factories/conversation';

describe('conversationControllerPolicy', () => {
  let conversation;
  let mockRequest;
  let user;
  const mockResponse = {} as Response;
  mockResponse.status = jest.fn().mockReturnValue(mockResponse);
  mockResponse.json = jest.fn().mockReturnValue(mockResponse);
  let mockNext;

  beforeEach(async () => {
    user = await createUser();
    conversation = await createConversation([user.id]);
    mockNext = jest.fn();
  });

  describe('show', () => {
    test('user who is a part of the conversation is authorized', async () => {
      mockRequest = {
        params: {
          id: conversation.id,
        },
        user: {
          id: user.id,
        },
      };

      await show(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user who is not a part of the conversation is not authorized', async () => {
      const otherUser = await createUser();
      mockRequest = {
        params: {
          id: conversation.id,
        },
        user: {
          id: otherUser.id,
        },
      };

      await expect(show(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
    });
  });
});
