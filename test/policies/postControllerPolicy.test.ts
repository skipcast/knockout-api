import { Response } from 'express';
import { ForbiddenError } from 'routing-controllers';
import {
  get,
  postRatingStore,
  postStore,
  postUpdate,
} from '../../src/policies/postControllerPolicy';
import createUser from '../factories/user';
import createPost from '../factories/post';
import createRole from '../factories/role';
import createSubforum from '../factories/subforum';
import createThread from '../factories/thread';
import { addPermissionCodesToRole } from '../helper/testHelper';

describe('postControllerPolicy', () => {
  let subforum: { id: number };
  let thread: { id: number; title: string };
  let role;
  let user;
  let mockRequest;
  const mockResponse = {} as Response;
  mockResponse.status = jest.fn().mockReturnValue(mockResponse);
  mockResponse.json = jest.fn().mockReturnValue(mockResponse);
  let mockNext;

  beforeEach(async () => {
    subforum = await createSubforum();
    role = await createRole();
    user = await createUser({ roleId: role.id });
    thread = await createThread({ user_id: user.id, subforum_id: subforum.id });
    mockNext = jest.fn();
  });

  describe('get', () => {
    let post;

    beforeEach(async () => {
      post = await createPost({ user_id: user.id, thread_id: thread.id });

      mockRequest = {
        params: {
          id: post.id,
        },
        user: {
          id: user.id,
        },
      };
    });

    test('user with subforum view permission is authorized', async () => {
      await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-view`]);
      await get(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user without subforum view-deleted-threads permission is not authorized', async () => {
      await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-view`]);
      thread = await createThread({
        user_id: user.id,
        subforum_id: subforum.id,
        deletedAt: new Date(),
      });
      post.thread_id = thread.id;
      await post.save();
      await expect(get(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
    });

    describe('user with subforum view-own-threads permission', () => {
      beforeEach(async () => {
        await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-view-own-threads`]);
      });

      test('is authorized to view post in own thread', async () => {
        await get(mockRequest, mockResponse, mockNext);
        expect(mockNext).toHaveBeenCalled();
      });

      test('is not authorized to view a post in a different users thread in the same subforum', async () => {
        const otherUser = await createUser();
        const otherThread = await createThread({ user_id: otherUser.id, subforum_id: subforum.id });

        post.thread_id = otherThread.id;
        await post.save();
        await expect(get(mockRequest, mockResponse, mockNext)).rejects.toThrow(
          new ForbiddenError('Insufficient user permissions.')
        );
      });
    });
  });

  describe('postStore', () => {
    beforeEach(() => {
      mockRequest = {
        body: {
          content: 'Created post.',
          thread_id: thread.id,
        },
        user: {
          id: user.id,
        },
      };
    });

    test('user with subforum post-create and view permissions is authorized', async () => {
      await addPermissionCodesToRole(role.id, [
        `subforum-${subforum.id}-post-create`,
        `subforum-${subforum.id}-view`,
      ]);
      await postStore(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('thread creator with subforum post-create and view-own-threads permissions is authorized', async () => {
      await addPermissionCodesToRole(role.id, [
        `subforum-${subforum.id}-post-create`,
        `subforum-${subforum.id}-view-own-threads`,
      ]);
      await postStore(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user without subforum view-deleted-threads permission is not authorized', async () => {
      await addPermissionCodesToRole(role.id, [
        `subforum-${subforum.id}-post-create`,
        `subforum-${subforum.id}-view`,
      ]);
      thread = await createThread({
        user_id: user.id,
        subforum_id: subforum.id,
        deletedAt: new Date(),
      });

      mockRequest = { ...mockRequest, body: { ...mockRequest.body, thread_id: thread.id } };
      await expect(postStore(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('You do not have permission to post in this thread.')
      );
    });

    test('user with subforum view-deleted-threads permission is authorized', async () => {
      await addPermissionCodesToRole(role.id, [
        `subforum-${subforum.id}-post-create`,
        `subforum-${subforum.id}-view`,
        `subforum-${subforum.id}-view-deleted-threads`,
      ]);
      thread = await createThread({
        user_id: user.id,
        subforum_id: subforum.id,
        deletedAt: new Date(),
      });

      mockRequest = { ...mockRequest, body: { ...mockRequest.body, thread_id: thread.id } };
      await postStore(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user without subforum view permission is not authorized', async () => {
      await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-post-create`]);
      await expect(postStore(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('You do not have permission to post in this thread.')
      );
    });

    test('user without subforum post-create permission is not authorized', async () => {
      await expect(postStore(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
    });
  });

  describe('postUpdate', () => {
    let post;

    beforeEach(async () => {
      post = await createPost({ user_id: user.id, thread_id: thread.id });

      mockRequest = {
        params: {
          id: post.id,
        },
        body: {
          content: 'Updated post content',
        },
        user: {
          id: user.id,
        },
      };
    });

    test('without the create post permission is not authorized', async () => {
      await expect(postUpdate(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
    });

    describe('with the create post permission', () => {
      beforeEach(async () => {
        await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-post-create`]);
      });

      test('a user updating their own post is authorized', async () => {
        await postUpdate(mockRequest, mockResponse, mockNext);
        expect(mockNext).toHaveBeenCalled();
      });

      describe('a user updating someone elses post', () => {
        let otherUser;

        beforeEach(async () => {
          otherUser = await createUser();
          post.update({ user_id: otherUser.id });
        });

        test('with the subforum post update permission is authorized', async () => {
          await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-post-update`]);
          await postUpdate(mockRequest, mockResponse, mockNext);
          expect(mockNext).toHaveBeenCalled();
        });

        test('without the subforum post update permission is not authorized', async () => {
          await expect(postUpdate(mockRequest, mockResponse, mockNext)).rejects.toThrow(
            new ForbiddenError('Insufficient user permissions.')
          );
        });
      });
    });
  });

  describe('postRatingStore', () => {
    beforeEach(async () => {
      const post = await createPost({ user_id: user.id, thread_id: thread.id });

      mockRequest = {
        params: {
          id: post.id,
        },
        user: {
          id: user.id,
        },
      };
    });

    test('user with subforum post-rating-create permission is authorized', async () => {
      await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-post-rating-create`]);

      await postRatingStore(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user without subforum post-rating-create permission is not authorized', async () => {
      await expect(postRatingStore(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
    });
  });
});
