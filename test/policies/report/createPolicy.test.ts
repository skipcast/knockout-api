import { Response } from 'express';
import { ForbiddenError } from 'routing-controllers';
import { post } from '../../../src/policies/report/createPolicy';
import createUser from '../../factories/user';
import createRole from '../../factories/role';
import createRolePermission from '../../factories/rolePermission';
import datasource from '../../../src/models/datasource';

const { Permission } = datasource().models;

describe('createPolicy', () => {
  let mockRequest;
  const mockResponse = {} as Response;
  mockResponse.status = jest.fn().mockReturnValue(mockResponse);
  mockResponse.json = jest.fn().mockReturnValue(mockResponse);

  describe('post', () => {
    test('user with report-create permission is authorized', async () => {
      const [permission] = await Permission.findOrCreate({ where: { code: 'report-create' } });
      const role = await createRole();
      await createRolePermission({ role_id: role.id, permission_id: permission.id });
      const user = await createUser({ roleId: role.id });
      const mockNext = jest.fn();

      mockRequest = {
        user: {
          id: user.id,
        },
      };

      await post(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user without report-create permission is not authorized', async () => {
      const role = await createRole();
      const user = await createUser({ roleId: role.id });
      const mockNext = jest.fn();

      mockRequest = {
        user: {
          id: user.id,
        },
      };

      await expect(post(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
    });
  });
});
