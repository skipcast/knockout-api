import { Response } from 'express';
import { ForbiddenError } from 'routing-controllers';
import { get } from '../../../src/policies/report/getAllPolicy';
import createUser from '../../factories/user';
import createRole from '../../factories/role';
import createRolePermission from '../../factories/rolePermission';
import datasource from '../../../src/models/datasource';

const { Permission } = datasource().models;

describe('getAllPolicy', () => {
  let mockRequest;
  const mockResponse = {} as Response;
  mockResponse.status = jest.fn().mockReturnValue(mockResponse);
  mockResponse.json = jest.fn().mockReturnValue(mockResponse);

  describe('get', () => {
    test('user with report-view permission is authorized', async () => {
      const [permission] = await Permission.findOrCreate({ where: { code: 'report-view' } });
      const role = await createRole();
      await createRolePermission({ role_id: role.id, permission_id: permission.id });
      const user = await createUser({ roleId: role.id });
      const mockNext = jest.fn();

      mockRequest = {
        user: {
          id: user.id,
        },
      };

      await get(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user without report-view permission is not authorized', async () => {
      const user = await createUser();
      const mockNext = jest.fn();

      mockRequest = {
        user: {
          id: user.id,
        },
      };

      await expect(get(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
    });
  });
});
