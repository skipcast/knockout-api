import { Response } from 'express';
import { ForbiddenError } from 'routing-controllers';
import { create } from '../../src/policies/messageOfTheDayControllerPolicy';
import createUser from '../factories/user';
import createRole from '../factories/role';
import { addPermissionCodesToRole } from '../helper/testHelper';

describe('messageOfTheDayControllerPolicy', () => {
  let mockRequest;
  let role;
  let user;
  const mockResponse = {} as Response;
  mockResponse.status = jest.fn().mockReturnValue(mockResponse);
  mockResponse.json = jest.fn().mockReturnValue(mockResponse);
  let mockNext;

  beforeEach(async () => {
    role = await createRole();
    user = await createUser({ roleId: role.id });
    mockRequest = {
      user: {
        id: user.id,
      },
    };
    mockNext = jest.fn();
  });

  describe('create', () => {
    test('user with motd-create permission is authorized', async () => {
      await addPermissionCodesToRole(role.id, ['messageOfTheDay-create']);
      await create(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user without motd-create permission is not authorized', async () => {
      await expect(create(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
    });
  });
});
