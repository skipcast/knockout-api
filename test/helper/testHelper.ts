import jwt from 'jsonwebtoken';
import request from 'supertest';

import config from '../../config/config';
import { APP_PORT, JWT_SECRET } from '../../config/server';
import datasource from '../../src/models/datasource';

const { Permission, RolePermission } = datasource().models;

const { host } = config[process.env.NODE_ENV];

const api = request(`http://${host}:${APP_PORT}/v2`);

const getUserJwtCookie = (userId: number) => {
  const jwtString = jwt.sign({ id: userId }, JWT_SECRET, {
    algorithm: 'HS256',
    expiresIn: '2 weeks',
  });
  return `knockoutJwt=${jwtString}`;
};

export const apiGet = (path: string, userId: number = null) => {
  if (userId !== null) {
    return api.get(path).set('Cookie', getUserJwtCookie(userId));
  }
  return api.get(path);
};

export const apiPost = (path: string, data: Object, userId: number = null) => {
  if (userId !== null) {
    return api.post(path).send(data).set('Cookie', getUserJwtCookie(userId));
  }
  return api.post(path).send(data);
};

export const apiPut = (path: string, data: Object, userId: number = null) => {
  if (userId !== null) {
    return api.put(path).send(data).set('Cookie', getUserJwtCookie(userId));
  }
  return api.put(path).send(data);
};

export const apiDelete = (path: string, userId: number = null, data: Object = null) => {
  if (userId !== null) {
    return api.delete(path).send(data).set('Cookie', getUserJwtCookie(userId));
  }
  return api.delete(path).send(data);
};

export const addPermissionCodesToRole = async (roleId: number, permissionCodes: string[]) =>
  Promise.all(
    permissionCodes.map(async (code) => {
      const [permission] = await Permission.findOrCreate({ where: { code } });
      await RolePermission.findOrCreate({
        where: { role_id: roleId, permission_id: permission.id },
      });
    })
  );
