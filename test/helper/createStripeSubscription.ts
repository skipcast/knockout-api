import { GoldSubscriptionCheckoutRequest } from 'knockout-schema';
import Stripe from 'stripe';
import { STRIPE_SECRET_KEY, STRIPE_GOLD_MEMBER_PRICE_ID } from '../../config/server';
import { apiPost } from './testHelper';

export default async (user) => {
  const data: GoldSubscriptionCheckoutRequest = {
    successUrl: 'http://localhost:3000/success',
    cancelUrl: 'http://localhost:3000/failure',
  };
  await apiPost(`/gold-subscriptions/checkout-session`, data, user.id);
  await user.reload();

  const { stripeCustomerId } = user;

  const stripe = new Stripe(STRIPE_SECRET_KEY, {
    apiVersion: '2020-08-27',
  });

  const paymentMethod = await stripe.paymentMethods.create({
    type: 'card',
    card: {
      number: '4242424242424242',
      exp_month: 6,
      exp_year: 2023,
      cvc: '314',
    },
  });

  const userPaymentMethod = await stripe.paymentMethods.attach(paymentMethod.id, {
    customer: stripeCustomerId,
  });

  const setupIntent = await stripe.setupIntents.create({
    payment_method_types: ['card'],
    customer: stripeCustomerId,
  });
  await stripe.setupIntents.confirm(setupIntent.id, { payment_method: userPaymentMethod.id });

  await stripe.subscriptions.create({
    payment_behavior: 'allow_incomplete',
    off_session: true,
    customer: stripeCustomerId,
    items: [{ price: STRIPE_GOLD_MEMBER_PRICE_ID }],
    default_payment_method: userPaymentMethod.id,
  });
};
