import datasource from '../../../src/models/datasource';
import createAlert from '../../factories/alert';
import createSubforum from '../../factories/subforum';
import createThread from '../../factories/thread';
import createUser from '../../factories/user';

import deleteAlerts from '../../../src/services/userDeletion/deleteAlerts';

describe('deleteAlerts', () => {
  let user;

  beforeEach(async () => {
    user = await createUser();
    const subforum = await createSubforum();
    const thread = await createThread({ subforum_id: subforum.id, user_id: user.id });
    await createAlert({ user_id: user.id, thread_id: thread.id });
  });

  test('the users alerts are deleted', async () => {
    const { Alert } = datasource().models;

    const alertsCountBefore = (
      await Alert.findAll({
        where: { user_id: user.id },
      })
    ).length;

    expect(alertsCountBefore).toEqual(1);

    await deleteAlerts(user.id);

    const alertsCountAfter = (
      await Alert.findAll({
        where: { user_id: user.id },
      })
    ).length;

    expect(alertsCountAfter).toEqual(0);
  });
});
