import datasource from '../../../src/models/datasource';
import createIpAddress from '../../factories/ipAddress';
import createUser from '../../factories/user';

import deleteIpAddresses from '../../../src/services/userDeletion/deleteIpAddresses';

describe('deleteIpAddresss', () => {
  let user;

  beforeEach(async () => {
    user = await createUser();
    await createIpAddress({ user_id: user.id });
  });

  test('the users alerts are deleted', async () => {
    const { IpAddress } = datasource().models;

    const alertsCountBefore = (
      await IpAddress.findAll({
        where: { user_id: user.id },
      })
    ).length;

    expect(alertsCountBefore).toEqual(1);

    await deleteIpAddresses(user.id);

    const alertsCountAfter = (
      await IpAddress.findAll({
        where: { user_id: user.id },
      })
    ).length;

    expect(alertsCountAfter).toEqual(0);
  });
});
