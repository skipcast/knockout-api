import datasource from '../../../src/models/datasource';
import createUser from '../../factories/user';

import scrubUser from '../../../src/services/userDeletion/scrubUser';
import { RoleCode } from '../../../src/helpers/permissions';

describe('scrubUser', () => {
  let user;

  beforeEach(async () => {
    user = await createUser();
  });

  test('the users info is scrubbed', async () => {
    const { Role, User } = datasource().models;

    await scrubUser(user.id);

    const scrubbedUser = await User.findOne({
      where: { id: user.id },
      include: {
        model: Role,
        attributes: ['code'],
      },
    });
    expect(scrubbedUser.username).toEqual('DELETED_USER');
    expect(scrubbedUser.usergroup).toEqual(1);
    expect(scrubbedUser.email).toBeNull();
    expect(scrubbedUser.avatar_url).toEqual('');
    expect(scrubbedUser.background_url).toEqual('');
    expect(scrubbedUser.external_type).toBeNull();
    expect(scrubbedUser.external_id).toBeNull();
    expect(scrubbedUser.title).toBeNull();
    expect(scrubbedUser.Role.code).toEqual(RoleCode.BANNED_USER);
  });
});
