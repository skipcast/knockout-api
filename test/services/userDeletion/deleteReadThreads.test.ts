import datasource from '../../../src/models/datasource';
import createReadThread from '../../factories/readThread';
import createSubforum from '../../factories/subforum';
import createThread from '../../factories/thread';
import createUser from '../../factories/user';

import deleteReadThreads from '../../../src/services/userDeletion/deleteReadThreads';

describe('deleteReadThreads', () => {
  let user;

  beforeEach(async () => {
    user = await createUser();
    const subforum = await createSubforum();
    const thread = await createThread({ subforum_id: subforum.id, user_id: user.id });
    await createReadThread({ user_id: user.id, thread_id: thread.id });
  });

  test('the users read threads are deleted', async () => {
    const { ReadThread } = datasource().models;

    const rtCountBefore = (
      await ReadThread.findAll({
        where: { user_id: user.id },
      })
    ).length;

    expect(rtCountBefore).toEqual(1);

    await deleteReadThreads(user.id);

    const rtCountAfter = (
      await ReadThread.findAll({
        where: { user_id: user.id },
      })
    ).length;

    expect(rtCountAfter).toEqual(0);
  });
});
