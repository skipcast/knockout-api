import datasource from '../../../src/models/datasource';
import createPost from '../../factories/post';
import createRating from '../../factories/rating';
import createSubforum from '../../factories/subforum';
import createThread from '../../factories/thread';
import createUser from '../../factories/user';

import deleteRatings from '../../../src/services/userDeletion/deleteRatings';

describe('deleteRatings', () => {
  let user;

  beforeEach(async () => {
    user = await createUser();
    const subforum = await createSubforum();
    const thread = await createThread({ subforum_id: subforum.id, user_id: user.id });
    const post = await createPost({ user_id: user.id, thread_id: thread.id });
    await createRating({ user_id: user.id, post_id: post.id });
  });

  test('the users ratings are deleted', async () => {
    const { Rating } = datasource().models;

    const ratingsCountBefore = (
      await Rating.findAll({
        where: { user_id: user.id },
      })
    ).length;

    expect(ratingsCountBefore).toEqual(1);

    await deleteRatings(user.id);

    const ratingsCountAfter = (
      await Rating.findAll({
        where: { user_id: user.id },
      })
    ).length;

    expect(ratingsCountAfter).toEqual(0);
  });
});
