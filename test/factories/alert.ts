import datasource from '../../src/models/datasource';

const { Alert } = datasource().models;

interface CreateAlertData {
  user_id: number;
  thread_id: number;
  lastPostNumber?: number;
  lastSeen?: Date;
  createdAt?: Date;
  updatedAt?: Date;
}

const data = async (props: CreateAlertData) => {
  const defaultData: CreateAlertData = {
    user_id: props.user_id,
    thread_id: props.thread_id,
    lastPostNumber: 1,
    lastSeen: new Date(),
    createdAt: new Date(),
    updatedAt: new Date(),
  };
  return { ...defaultData, ...props };
};

export default async (props: CreateAlertData) => Alert.create(await data(props));
