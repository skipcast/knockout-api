import datasource from '../../src/models/datasource';

const { ReadThread } = datasource().models;

interface CreateReadThreadData {
  user_id: number;
  thread_id: number;
  lastSeen?: Date;
  createdAt?: Date;
  updatedAt?: Date;
}

const data = async (props: CreateReadThreadData) => {
  const defaultData: CreateReadThreadData = {
    user_id: props.user_id,
    thread_id: props.thread_id,
    lastSeen: new Date(),
    createdAt: new Date(),
    updatedAt: new Date(),
  };
  return { ...defaultData, ...props };
};

export default async (props: CreateReadThreadData) => ReadThread.create(await data(props));
