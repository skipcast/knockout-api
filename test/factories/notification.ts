import { NotificationType } from 'knockout-schema';
import datasource from '../../src/models/datasource';

const { Notification } = datasource().models;

interface CreateNotificationData {
  type?: NotificationType;
  dataId: number;
  userId: number;
}

const data = (props: CreateNotificationData) => {
  const defaultData: CreateNotificationData = {
    dataId: props.dataId,
    userId: props.userId,
    type: NotificationType.POST_REPLY,
  };
  return { ...defaultData, ...props };
};

export default async (props: CreateNotificationData) => Notification.create(data(props));
