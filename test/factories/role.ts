import faker from 'faker';
import { v4 as uuidv4 } from 'uuid';
import datasource from '../../src/models/datasource';

const { Role } = datasource().models;

interface CreateRoleData {
  code?: string;
  description?: string;
  createdAt?: Date;
  updatedAt?: Date;
}

const data = async (props: CreateRoleData) => {
  const defaultData: CreateRoleData = {
    code: `${faker.hacker.verb()}-${uuidv4()}`,
    description: faker.hacker.noun(),
    createdAt: new Date(),
    updatedAt: new Date(),
  };
  return { ...defaultData, ...props };
};

export default async (props: CreateRoleData = {}) => Role.create(await data(props));
