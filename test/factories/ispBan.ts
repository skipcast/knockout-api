import datasource from '../../src/models/datasource';

const { IspBan } = datasource().models;

interface CreateIspBanData {
  netname?: string;
  asn?: string;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;
}

const data = async (props: CreateIspBanData) => {
  const defaultData: CreateIspBanData = {
    createdAt: new Date(),
    updatedAt: new Date(),
  };
  return { ...defaultData, ...props };
};

export default async (props: CreateIspBanData = {}) => IspBan.create(await data(props));
