import datasource from '../../src/models/datasource';

const { IpAddress } = datasource().models;

interface CreateIpAddressData {
  user_id: number;
  ip_address?: string;
  post_id?: number;
  createdAt?: Date;
}

const data = async (props: CreateIpAddressData) => {
  const defaultData: CreateIpAddressData = {
    user_id: props.user_id,
    ip_address: props.ip_address || '127.0.0.1',
    post_id: props.post_id || null,
    createdAt: new Date(),
  };
  return { ...defaultData, ...props };
};

export default async (props: CreateIpAddressData) => IpAddress.create(await data(props));
