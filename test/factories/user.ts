import faker from 'faker';
import datasource from '../../src/models/datasource';

const { User } = datasource().models;

interface CreateUserData {
  username?: string;
  email?: string;
  external_type?: string;
  external_id?: string;
  avatar_url?: string;
  background_url?: string;
  usergroup?: number;
  roleId?: number;
  title?: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}

const data = async (props: CreateUserData) => {
  const defaultData: CreateUserData = {
    username: faker.internet.userName(),
    email: faker.internet.email(),
    external_type: 'Google',
    external_id: 'GoogleID',
    avatar_url: 'none.webp',
    background_url: '',
    usergroup: 1,
    roleId: null,
    title: null,
    createdAt: new Date(),
    updatedAt: new Date(),
    deletedAt: null,
  };
  return { ...defaultData, ...props };
};

export default async (props: CreateUserData = {}) => User.create(await data(props));
