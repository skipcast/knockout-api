import faker from 'faker';
import datasource from '../../src/models/datasource';

const { CalendarEvent } = datasource().models;

interface CreateCalendarEventData {
  createdBy: number;
  title?: string;
  description?: string;
  threadId: number;
  startsAt: Date;
  endsAt: Date;
  createdAt?: Date;
  updatedAt?: Date;
}

const data = async (props: CreateCalendarEventData) => {
  const defaultData: CreateCalendarEventData = {
    createdBy: props.createdBy,
    title: faker.lorem.sentence(),
    description: faker.lorem.sentence(),
    threadId: props.threadId,
    startsAt: props.startsAt,
    endsAt: props.endsAt,
    createdAt: new Date(),
    updatedAt: new Date(),
  };
  return { ...defaultData, ...props };
};

export default async (props: CreateCalendarEventData) => CalendarEvent.create(await data(props));
