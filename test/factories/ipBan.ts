import datasource from '../../src/models/datasource';

const { IpBan } = datasource().models;

interface CreateIpBanData {
  address?: string;
  range?: string;
  created_by?: number;
  updated_by?: number;
  created_at?: Date;
  updated_at?: Date;
}

const data = async (props: CreateIpBanData) => {
  const defaultData: CreateIpBanData = {
    address: null,
    range: null,
    created_by: null,
    updated_by: null,
    created_at: new Date(),
    updated_at: new Date(),
  };
  return { ...defaultData, ...props };
};

export default async (props: CreateIpBanData = {}) => IpBan.create(await data(props));
