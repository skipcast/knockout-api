import faker from 'faker';
import datasource from '../../src/models/datasource';

const { Thread } = datasource().models;

interface CreateThreadData {
  subforum_id: number;
  user_id: number;
  title?: string;
  icon_id?: number;
  locked?: boolean;
  pinned?: boolean;
  background_url?: string;
  background_type?: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}

const data = async (props: CreateThreadData) => {
  const defaultData: CreateThreadData = {
    user_id: props.user_id,
    subforum_id: props.subforum_id,
    title: faker.random.words(5),
    icon_id: 1,
    locked: false,
    pinned: false,
    background_url: '',
    background_type: '',
    createdAt: faker.date.past(2),
    updatedAt: faker.date.past(1),
    deletedAt: null,
  };
  return { ...defaultData, ...props };
};

export default async (props: CreateThreadData) => Thread.create(await data(props));
