import faker from 'faker';
import datasource from '../../src/models/datasource';

const { Rule } = datasource().models;

interface CreateRuleData {
  rulableType?: string;
  rulableId?: number;
  category?: string;
  title?: string;
  cardinality?: number;
  description?: string;
  createdAt?: Date;
  updatedAt?: Date;
}

const data = async (props: CreateRuleData) => {
  const defaultData: CreateRuleData = {
    rulableType: null,
    rulableId: null,
    category: faker.hacker.noun(),
    title: faker.hacker.noun(),
    cardinality: 1,
    description: faker.hacker.noun(),
    createdAt: new Date(),
    updatedAt: new Date(),
  };
  return { ...defaultData, ...props };
};

export default async (props: CreateRuleData = {}) => Rule.create(await data(props));
