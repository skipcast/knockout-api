import faker from 'faker';
import datasource from '../../src/models/datasource';

const { Subforum } = datasource().models;

interface CreateSubforumData {
  name?: string;
  icon_id?: number;
  created_at?: Date;
  updated_at?: Date;
}

const data = async (props: CreateSubforumData) => {
  const defaultData: CreateSubforumData = {
    name: faker.lorem.words(3),
    icon_id: 1,
    created_at: new Date(),
    updated_at: new Date(),
  };
  return { ...defaultData, ...props };
};

export default async (props: CreateSubforumData = {}) => Subforum.create(await data(props));
