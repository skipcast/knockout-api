import datasource from '../../src/models/datasource';

const { RolePermission } = datasource().models;

interface CreateRolePermissionData {
  role_id: number;
  permission_id: number;
}

const data = async (props: CreateRolePermissionData) => props;

export default async (props: CreateRolePermissionData) => RolePermission.create(await data(props));
