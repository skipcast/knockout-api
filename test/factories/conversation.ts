import datasource from '../../src/models/datasource';

const { Conversation, ConversationUser } = datasource().models;

export default async (user_ids: number[]) => {
  const conversation = await Conversation.create();
  await Promise.all(
    user_ids.map((user_id) =>
      ConversationUser.create({
        conversation_id: conversation.id,
        user_id,
        createdAt: new Date(),
        updatedAt: new Date(),
      })
    )
  );
  return conversation;
};
