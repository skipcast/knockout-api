import faker from 'faker';
import datasource from '../../src/models/datasource';

const { ProfileComment } = datasource().models;

interface CreateProfileCommentData {
  user_profile: number;
  author: number;
  content?: string;
}

const data = async (props: CreateProfileCommentData) => {
  const defaultData: CreateProfileCommentData = {
    user_profile: props.user_profile,
    author: props.author,
    content: faker.lorem.sentence(),
  };
  return { ...defaultData, ...props };
};

export default async (props: CreateProfileCommentData) => ProfileComment.create(await data(props));
