import { EventType } from 'knockout-schema';
import datasource from '../../src/models/datasource';

const { Event } = datasource().models;

interface CreateEventData {
  type?: EventType;
  content?: string;
  dataId: number;
  creator: number;
}

const data = (props: CreateEventData) => {
  const defaultData: CreateEventData = {
    dataId: props.dataId,
    creator: props.creator,
    type: EventType.THREAD_LOCKED,
    content: '[]',
  };
  return { ...defaultData, ...props };
};

export default async (props: CreateEventData) => Event.create(data(props));
