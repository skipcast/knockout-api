import faker from 'faker';
import { v4 as uuidv4 } from 'uuid';
import datasource from '../../src/models/datasource';

const { Permission } = datasource().models;

interface CreatePermissionData {
  code?: string;
  createdAt?: Date;
  updatedAt?: Date;
}

const data = async (props: CreatePermissionData) => {
  const defaultData: CreatePermissionData = {
    code: `${faker.hacker.verb()}-${uuidv4()}`,
    createdAt: new Date(),
    updatedAt: new Date(),
  };
  return { ...defaultData, ...props };
};

export default async (props: CreatePermissionData = {}) => Permission.create(await data(props));
