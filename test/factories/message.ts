import faker from 'faker';
import datasource from '../../src/models/datasource';

const { Message } = datasource().models;

interface CreateMessageData {
  conversation_id: string;
  user_id: string;
  content?: string;
  created_at?: Date;
  updated_at?: Date;
}

const data = async (props: CreateMessageData) => {
  const defaultData: CreateMessageData = {
    conversation_id: props.conversation_id,
    user_id: props.user_id,
    content: faker.lorem.sentence(),
    created_at: new Date(),
    updated_at: new Date(),
  };
  return { ...defaultData, ...props };
};

export default async (props: CreateMessageData) => Message.create(await data(props));
