import httpStatus from 'http-status';
import {
  UserProfile,
  UpdateUserProfileRequest,
  UserProfileCommentPage,
  UserProfileComment,
  WipeAccountRequest,
  User,
} from 'knockout-schema';
import { addPermissionCodesToRole, apiDelete, apiGet, apiPost, apiPut } from '../helper/testHelper';
import createUser from '../factories/user';
import createUserProfile from '../factories/userProfile';
import createProfileComment from '../factories/profileComment';
import createThread from '../factories/thread';
import createSubforum from '../factories/subforum';
import createRating from '../factories/rating';
import createRole from '../factories/role';
import createPost from '../factories/post';
import knex from '../../src/services/knex';
import UpdateUserRequest from '../../src/v2/schemas/users/updateUserRequest';

describe('/v2/users endpoint', () => {
  let user: User;
  let userRole;
  let user1: User;
  let userProfile;
  let profileComment;

  beforeEach(async () => {
    userRole = await createRole();
    user = await createUser({ roleId: userRole.id });
    user1 = await createUser();
    userProfile = await createUserProfile({ user_id: user.id });
    profileComment = await createProfileComment({ user_profile: user.id, author: user1.id });
    await createProfileComment({ user_profile: user.id, author: user1.id });
  });

  describe('GET /:id/profile', () => {
    test('retrieves a user profile', async () => {
      const expectedResponse: UserProfile = {
        id: userProfile.user_id,
        bio: userProfile.heading_text,
        social: {
          website: userProfile.personal_site,
          steam: {
            url: userProfile.steam,
          },
          discord: userProfile.discord,
          github: userProfile.github,
          youtube: userProfile.youtube,
          twitter: userProfile.twitter,
          twitch: userProfile.twitch,
          gitlab: userProfile.gitlab,
          tumblr: userProfile.tumblr,
        },
        background: {
          url: userProfile.background_url,
          type: userProfile.background_type,
        },
        header: userProfile.header,
        disableComments: false,
      };

      await apiGet(`/users/${user.id}/profile`).expect(httpStatus.OK, expectedResponse);
    });
  });

  describe('PUT /:id/profile', () => {
    test('updates a user profile', async () => {
      const data: UpdateUserProfileRequest = {
        bio: 'many things',
        social: {
          website: 'https://knockout.chat/',
          twitter: 'stuff',
        },
        disableComments: true,
      };

      await apiPut(`/users/${user.id}/profile`, data, user.id).expect(httpStatus.OK);

      const expectedResponse: UserProfile = {
        id: userProfile.user_id,
        bio: data.bio,
        social: {
          website: data.social.website,
          steam: {
            url: userProfile.steam,
          },
          discord: userProfile.discord,
          github: userProfile.github,
          youtube: userProfile.youtube,
          twitter: data.social.twitter,
          twitch: userProfile.twitch,
          gitlab: userProfile.gitlab,
          tumblr: userProfile.tumblr,
        },
        background: {
          url: userProfile.background_url,
          type: userProfile.background_type,
        },
        header: userProfile.header,
        disableComments: data.disableComments,
      };

      await apiGet(`/users/${user.id}/profile`).expect(httpStatus.OK, expectedResponse);
    });

    test('rejects a logged out user', async () => {
      const data: UpdateUserProfileRequest = {
        bio: 'many things',
        social: {
          website: 'https://knockout.chat/',
          twitter: 'stuff',
        },
        disableComments: true,
      };

      await apiPut(`/users/${user.id}/profile`, data).expect(httpStatus.UNAUTHORIZED);
    });

    test('rejects a user who is not the profile owner', async () => {
      const data: UpdateUserProfileRequest = {
        bio: 'many things',
        social: {
          website: 'https://knockout.chat/',
          twitter: 'stuff',
        },
        disableComments: true,
      };

      await apiPut(`/users/${user.id + 1}/profile`, data, user.id).expect(httpStatus.FORBIDDEN);
    });
  });

  describe('GET /:id/profile/comments', () => {
    test("retrieves a user's profile comments", async () => {
      const response: { body: UserProfileCommentPage } = await apiGet(
        `/users/${user.id}/profile/comments`
      ).expect(httpStatus.OK);

      expect(response.body.comments.length).toBe(2);
      expect(response.body.comments[0].author.id).toBe(user1.id);
      expect(response.body.comments[1].author.id).toBe(user1.id);
    });
  });

  describe('POST /:id/profile/comments', () => {
    test("creates a comment on a user's profile with the correct permission", async () => {
      await addPermissionCodesToRole(userRole.id, ['user-profile-comment-create']);
      const response: { body: UserProfileComment } = await apiPost(
        `/users/${user.id}/profile/comments`,
        { content: 'stuff' },
        user.id
      ).expect(httpStatus.CREATED);
      expect(response.body.content).toBe('stuff');
    });

    test('rejects logged out users', async () => {
      await apiPost(`/users/${user.id}/profile/comments`, {
        content: 'stuff',
      }).expect(httpStatus.UNAUTHORIZED);
    });

    test('rejects users without the correct permission', async () => {
      await apiPost(`/users/${user.id}/profile/comments`, { content: 'stuff' }, user.id).expect(
        httpStatus.FORBIDDEN
      );
    });

    test('rejects all comments when profile commenting is disabled', async () => {
      await addPermissionCodesToRole(userRole.id, ['user-profile-comment-create']);
      userProfile.disable_comments = true;
      await userProfile.save();
      await apiPost(`/users/${user.id}/profile/comments`, { content: 'stuff' }, user.id).expect(
        httpStatus.FORBIDDEN
      );
    });
  });

  describe('DELETE /:id/profile/comments/:commentId', () => {
    test("deletes a comment on the user's own profile", async () => {
      await apiDelete(`/users/${user.id}/profile/comments/${profileComment.id}`, user.id).expect(
        httpStatus.OK
      );
    });

    test('deletes a comment the user created', async () => {
      await apiDelete(`/users/${user.id}/profile/comments/${profileComment.id}`, user1.id).expect(
        httpStatus.OK
      );
    });

    test('allows user with the correct permission to delete comments', async () => {
      await addPermissionCodesToRole(userRole.id, ['user-profile-comment-archive']);
      const otherUser = await createUser();
      profileComment.update({ user_profile: otherUser.id, author: otherUser.id });
      await apiDelete(`/users/${user.id}/profile/comments/${profileComment.id}`, user.id).expect(
        httpStatus.OK
      );
    });

    test('rejects users without the correct permission to delete other users comments', async () => {
      const newUser = await createUser();
      await apiDelete(`/users/${user.id}/profile/comments/${profileComment.id}`, newUser.id).expect(
        httpStatus.FORBIDDEN
      );
    });

    test('rejects logged out users', async () => {
      await apiDelete(`/users/${user.id}/profile/comments/${profileComment.id}`).expect(
        httpStatus.UNAUTHORIZED
      );
    });
  });

  describe('DELETE /:id', () => {
    const body: WipeAccountRequest = {
      reason: 'Reason',
    };

    test('a user wipes their own account', async () => {
      const subforum = createSubforum();
      const thread = await createThread({
        user_id: user.id,
        subforum_id: (await subforum).id,
      });
      const post = await createPost({ user_id: user.id, thread_id: thread.id });
      const post1 = await createPost({ user_id: user.id, thread_id: thread.id });
      const rating = await createRating({ user_id: user.id, post_id: post.id });
      const newProfileComment = await createProfileComment({
        user_profile: user1.id,
        author: user.id,
      });
      await apiDelete(`/users/${user.id}?wipe=true`, user.id, body).expect(httpStatus.OK);

      const updatedPost = await knex('Posts').select('content').where({ id: post1.id }).first();
      const updatedRating = await knex('Ratings')
        .select('*')
        .where({ user_id: rating.user_id, post_id: rating.post_id })
        .first();
      const updatedUser = await knex('Users').select('username').where({ id: user.id }).first();

      const updatedProfileComment = await knex('ProfileComments')
        .select('content')
        .where('id', newProfileComment.id)
        .first();
      const updatedUserProfile = await knex('UserProfiles')
        .select('header')
        .where('user_id', userProfile.user_id)
        .first();
      expect(updatedPost.content).toBe("This post's content has been removed by a moderator.");
      expect(updatedRating).toBeFalsy();
      expect(updatedUser.username).toBe('DELETED_USER');
      expect(updatedProfileComment.content).toBe(
        'This profile comment is from a deleted user and is no longer directly available.'
      );
      expect(updatedUserProfile.header).toBeNull();
    });

    test('rejects all other users', async () => {
      const newUser = await createUser();
      await apiDelete(`/users/${user.id}?wipe=true`, newUser.id, body).expect(httpStatus.FORBIDDEN);
    });

    test('rejects logged out users', async () => {
      await apiDelete(`/users/${user.id}?wipe=true`).expect(httpStatus.UNAUTHORIZED);
    });
  });

  describe('PUT /:id', () => {
    test('updates a username', async () => {
      const data: UpdateUserRequest = {
        username: 'newUsername',
      };

      const response: { body: User } = await apiPut(`/users/${user.id}/`, data, user.id).expect(
        httpStatus.OK
      );

      expect(response.body.username).toBe('newUsername');
    });

    test('rejects a logged out user', async () => {
      const data: UpdateUserRequest = {
        username: 'newUsername',
      };

      await apiPut(`/users/${user.id}/`, data).expect(httpStatus.UNAUTHORIZED);
    });

    test('rejects username change if the username was already changed recently', async () => {
      const data: UpdateUserRequest = {
        username: 'newerUsername',
      };

      const response: { body: User } = await apiPut(`/users/${user.id}/`, data, user.id).expect(
        httpStatus.OK
      );

      expect(response.body.username).toBe('newerUsername');

      await apiPut(`/users/${user.id}/`, { username: 'secondUsername' }, user.id).expect(
        httpStatus.FORBIDDEN
      );
    });

    test('rejects invalid usernames', async () => {
      const data: UpdateUserRequest = {
        username: 'ne',
      };

      await apiPut(`/users/${user.id}/`, data, user.id).expect(httpStatus.BAD_REQUEST);

      await apiPut(`/users/${user.id}/`, { username: 'sdf<' }, user.id).expect(
        httpStatus.BAD_REQUEST
      );
    });

    test('rejects username change if the username is already in use', async () => {
      const user2 = await createUser({ username: 'newUsername' });
      const data: UpdateUserRequest = {
        username: user2.username,
      };

      await apiPut(`/users/${user.id}/`, data, user.id).expect(httpStatus.FORBIDDEN);
    });
  });
});
