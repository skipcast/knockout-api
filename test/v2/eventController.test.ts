import httpStatus from 'http-status';
import { EventType, Thread, Event as EventSchema } from 'knockout-schema';
import createUser from '../factories/user';
import createSubforum from '../factories/subforum';
import createThread from '../factories/thread';
import createEvent from '../factories/event';
import createRole from '../factories/role';
import { addPermissionCodesToRole, apiGet } from '../helper/testHelper';
import datasource from '../../src/models/datasource';

const { Event } = datasource().models;

describe('/v2/events endpoint', () => {
  let role;
  let user;
  let user1;
  let subforum;
  let thread;

  beforeAll(async () => {
    await Event.destroy({ where: {} });
  });

  beforeEach(async () => {
    role = await createRole();
    user = await createUser({ roleId: role.id });
    user1 = await createUser();
    subforum = await createSubforum();
    thread = await createThread({ user_id: user.id, subforum_id: subforum.id });
  });

  describe('GET /', () => {
    test('rejects logged out users', async () => {
      await apiGet(`/events`).expect(httpStatus.UNAUTHORIZED);
    });

    test('retrieves events', async () => {
      await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-view`]);
      const event = await createEvent({ creator: user1.id, dataId: thread.id });
      const event1 = await createEvent({
        creator: user.id,
        dataId: user1.id,
        type: EventType.USER_AVATAR_REMOVED,
      });
      const response: { body: EventSchema[] } = await apiGet(`/events`, user.id).expect(
        httpStatus.OK
      );

      expect(response.body.length).toBe(2);

      const avatarEvent = response.body.find((e) => e.id === event1.id);
      const lockEvent = response.body.find((e) => e.id === event.id);

      expect(avatarEvent.creator.id).toBe(user.id);
      expect(avatarEvent.type).toBe(EventType.USER_AVATAR_REMOVED);
      expect(avatarEvent.data.id).toBe(user1.id);

      expect(lockEvent.creator.id).toBe(user1.id);
      expect(lockEvent.type).toBe(EventType.THREAD_LOCKED);
      expect(lockEvent.data.id).toBe(thread.id);
      expect((lockEvent.data as Thread).title).toBe(thread.title);
    });
  });
});
