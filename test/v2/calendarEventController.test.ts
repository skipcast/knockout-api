import httpStatus from 'http-status';
import {
  CalendarEvent,
  CreateCalendarEventRequest,
  UpdateCalendarEventRequest,
} from 'knockout-schema';
import createUser from '../factories/user';

import createRole from '../factories/role';
import createCalendarEvent from '../factories/calendarEvent';
import createSubforum from '../factories/subforum';
import createThread from '../factories/thread';
import { apiGet, apiPost, apiPut, apiDelete, addPermissionCodesToRole } from '../helper/testHelper';
import datasource from '../../src/models/datasource';

describe('/v2/calendarEvents endpoint', () => {
  let role;
  let user;
  let thread;
  let subforum;
  let calendarEvent;

  beforeEach(async () => {
    role = await createRole();
    user = await createUser({ roleId: role.id });
    subforum = await createSubforum();
    thread = await createThread({ subforum_id: subforum.id, user_id: user.id });
    const now = new Date();
    const startsAt = new Date(now.getTime());
    const endsAt = new Date(now.getTime() + 60 * 60 * 1000);
    calendarEvent = await createCalendarEvent({
      createdBy: user.id,
      threadId: thread.id,
      startsAt,
      endsAt,
    });
  });

  describe('GET /', () => {
    test('when the startDate parameter is missing', async () => {
      const endDate = new Date(new Date().setHours(0, 0, 0, 0)).toISOString();
      await apiGet(`/calendarEvents?endDate=${endDate}`, user.id).expect(httpStatus.BAD_REQUEST);
    });

    test('when the endDate parameter is missing', async () => {
      const startDate = new Date(new Date().setHours(0, 0, 0, 0)).toISOString();
      await apiGet(`/calendarEvents?startDate=${startDate}`, user.id).expect(
        httpStatus.BAD_REQUEST
      );
    });

    test('when startDate is after endDate ', async () => {
      const endDate = new Date(new Date().setHours(0, 0, 0, 0)).toISOString();
      const startDate = new Date(new Date().setHours(23, 59, 59, 999)).toISOString();

      await apiGet(`/calendarEvents?startDate=${startDate}&endDate=${endDate}`, user.id).expect(
        httpStatus.BAD_REQUEST
      );
    });

    test('when the range between startsAt and endsAt is valid', async () => {
      const startDate = new Date(new Date().setHours(0, 0, 0, 0)).toISOString();
      const endDate = new Date(new Date().setHours(23, 59, 59, 999)).toISOString();

      const res = await apiGet(
        `/calendarEvents?startDate=${startDate}&endDate=${endDate}`,
        user.id
      ).expect(httpStatus.OK);
      expect(res.body.some((event) => event.id === calendarEvent.id)).toBe(true);
    });
  });

  describe('POST /', () => {
    const startsAt = new Date(new Date().setHours(0, 0, 0, 0)).toISOString();
    const endsAt = new Date(new Date().setHours(1, 0, 0, 0)).toISOString();

    let data: CreateCalendarEventRequest;

    beforeEach(async () => {
      const newThread = await createThread({ subforum_id: subforum.id, user_id: user.id });
      data = {
        title: 'title',
        description: 'description',
        threadId: newThread.id,
        startsAt,
        endsAt,
      };
    });

    test('creates a calendar event when the user has the correct permission', async () => {
      const { CalendarEvent: CalendarEventModel } = datasource().models;

      await addPermissionCodesToRole(role.id, ['calendar-event-create']);

      const expectedResponse: CalendarEvent = {
        id: null,
        createdBy: null,
        title: data.title,
        description: data.description,
        thread: null,
        startsAt: data.startsAt,
        endsAt: data.endsAt,
        createdAt: null,
        updatedAt: null,
      };

      await apiPost('/calendarEvents', data, user.id)
        .expect((res) => {
          expectedResponse.id = res.body.id;
          expectedResponse.thread = res.body.thread;
          expectedResponse.createdBy = res.body.createdBy;
          expectedResponse.createdAt = res.body.createdAt;
          expectedResponse.updatedAt = res.body.updatedAt;
        })
        .expect(httpStatus.CREATED, expectedResponse);

      const latestCalendarEvent = await CalendarEventModel.findOne({
        where: { id: expectedResponse.id },
      });
      expect(latestCalendarEvent.title).toEqual(data.title);
      expect(latestCalendarEvent.description).toEqual(data.description);
      expect(latestCalendarEvent.startsAt.toISOString()).toEqual(data.startsAt);
      expect(latestCalendarEvent.endsAt.toISOString()).toEqual(data.endsAt);
    });

    test('rejects when user does not have the correct permission', async () => {
      await apiPost(`/calendarEvents/`, data, user.id).expect(httpStatus.FORBIDDEN);
    });

    test('rejects a logged out user', async () => {
      await apiPost(`/calendarEvents/`, data).expect(httpStatus.UNAUTHORIZED);
    });
  });

  describe('PUT /', () => {
    const startsAt = new Date(new Date().setHours(2, 0, 0, 0)).toISOString();
    const endsAt = new Date(new Date().setHours(3, 0, 0, 0)).toISOString();

    let data: UpdateCalendarEventRequest;
    let expectedResponse: CalendarEvent;

    beforeEach(async () => {
      data = {
        title: 'title',
        description: 'description',
        startsAt,
        endsAt,
      };

      expectedResponse = {
        id: null,
        createdBy: null,
        title: data.title,
        description: data.description,
        thread: null,
        startsAt: data.startsAt,
        endsAt: data.endsAt,
        createdAt: null,
        updatedAt: null,
      };
    });

    test('user can update their own calendar event', async () => {
      const { CalendarEvent: CalendarEventModel } = datasource().models;

      await apiPut(`/calendarEvents/${calendarEvent.id}`, data, user.id)
        .expect((res) => {
          expectedResponse.id = res.body.id;
          expectedResponse.thread = res.body.thread;
          expectedResponse.createdBy = res.body.createdBy;
          expectedResponse.createdAt = res.body.createdAt;
          expectedResponse.updatedAt = res.body.updatedAt;
        })
        .expect(httpStatus.OK, expectedResponse);

      const updatedEvent = await CalendarEventModel.findOne({ where: { id: calendarEvent.id } });

      expect(updatedEvent.title).toEqual(data.title);
      expect(updatedEvent.description).toEqual(data.description);
      expect(updatedEvent.startsAt.toISOString()).toEqual(data.startsAt);
      expect(updatedEvent.endsAt.toISOString()).toEqual(data.endsAt);
    });

    describe('when the calendar event does not belong to the user', () => {
      beforeEach(async () => {
        const newUser = await createUser();
        calendarEvent.update({ createdBy: newUser.id });
        calendarEvent.save();
      });

      test('updates a calendar event when the user has the correct permission', async () => {
        const { CalendarEvent: CalendarEventModel } = datasource().models;

        await addPermissionCodesToRole(role.id, ['calendar-event-update']);

        await apiPut(`/calendarEvents/${calendarEvent.id}`, data, user.id)
          .expect((res) => {
            expectedResponse.id = res.body.id;
            expectedResponse.thread = res.body.thread;
            expectedResponse.createdBy = res.body.createdBy;
            expectedResponse.createdAt = res.body.createdAt;
            expectedResponse.updatedAt = res.body.updatedAt;
          })
          .expect(httpStatus.OK, expectedResponse);

        const updatedEvent = await CalendarEventModel.findOne({ where: { id: calendarEvent.id } });

        expect(updatedEvent.title).toEqual(data.title);
        expect(updatedEvent.description).toEqual(data.description);
        expect(updatedEvent.startsAt.toISOString()).toEqual(data.startsAt);
        expect(updatedEvent.endsAt.toISOString()).toEqual(data.endsAt);
      });

      test('rejects when user does not have the correct permission', async () => {
        await apiPut(`/calendarEvents/${calendarEvent.id}`, data, user.id).expect(
          httpStatus.FORBIDDEN
        );
      });
    });

    test('rejects a logged out user', async () => {
      await apiPut(`/calendarEvents/${calendarEvent.id}`, data).expect(httpStatus.UNAUTHORIZED);
    });
  });

  describe('DELETE /', () => {
    test('user can delete their own calendar event', async () => {
      const { CalendarEvent: CalendarEventModel } = datasource().models;

      await apiDelete(`/calendarEvents/${calendarEvent.id}`, user.id).expect(httpStatus.OK);

      const updatedEvent = await CalendarEventModel.findOne({ where: { id: calendarEvent.id } });

      expect(updatedEvent).toBeNull();
    });

    describe('when the calendar event does not belong to the user', () => {
      beforeEach(async () => {
        const newUser = await createUser();
        calendarEvent.update({ createdBy: newUser.id });
        calendarEvent.save();
      });

      test('deletes a calendar event when the user has the correct permission', async () => {
        const { CalendarEvent: CalendarEventModel } = datasource().models;

        await addPermissionCodesToRole(role.id, ['calendar-event-archive']);

        await apiDelete(`/calendarEvents/${calendarEvent.id}`, user.id).expect(httpStatus.OK);

        const updatedEvent = await CalendarEventModel.findOne({ where: { id: calendarEvent.id } });

        expect(updatedEvent).toBeNull();
      });

      test('rejects when user does not have the correct permission', async () => {
        await apiDelete(`/calendarEvents/${calendarEvent.id}`, user.id).expect(
          httpStatus.FORBIDDEN
        );
      });
    });

    test('rejects a logged out user', async () => {
      await apiDelete(`/calendarEvents/${calendarEvent.id}`).expect(httpStatus.UNAUTHORIZED);
    });
  });
});
