import httpStatus from 'http-status';
import { Alert, CreateAlertRequest } from 'knockout-schema';
import createUser from '../factories/user';

import createAlert from '../factories/alert';
import createRole from '../factories/role';
import createSubforum from '../factories/subforum';
import createThread from '../factories/thread';
import { apiDelete, apiGet, apiPost } from '../helper/testHelper';
import datasource from '../../src/models/datasource';

describe('/v2/alerts endpoint', () => {
  let user;
  let role;
  let subforum: { id: number };
  let thread;
  let alert;

  beforeEach(async () => {
    role = await createRole();
    user = await createUser({ roleId: role.id });
    subforum = await createSubforum();
    thread = await createThread({ user_id: user.id, subforum_id: subforum.id });
    alert = await createAlert({ user_id: user.id, thread_id: thread.id });
  });

  describe('GET /', () => {
    test('rejects logged out users', async () => {
      await apiGet(`/alerts/1`).expect(httpStatus.UNAUTHORIZED);
    });

    test('retrieve alerts for the user', async () => {
      const res = await apiGet(`/alerts/1`, user.id).expect(httpStatus.OK);
      expect(res.body.totalAlerts).toEqual(1);

      const receivedAlert: Alert = res.body.alerts[0];
      expect(receivedAlert.id).toEqual(alert.thread_id);
    });

    test('does not retrieve alerts in subforums the user cant see', async () => {
      const otherUser = await createUser();
      await thread.update({ user_id: otherUser.id });
      await thread.save();
      const res = await apiGet(`/alerts/1`, user.id).expect(httpStatus.OK);
      expect(res.body.totalAlerts).toEqual(0);
    });
  });

  describe('POST /', () => {
    test('rejects logged out users', async () => {
      await apiPost('/alerts', { threadId: thread.id }).expect(httpStatus.UNAUTHORIZED);
    });

    test('creates an alert for the user', async () => {
      await apiPost('/alerts', { threadId: thread.id }, user.id).expect(httpStatus.CREATED);
    });
  });

  describe('POST /', () => {
    test('rejects logged out users', async () => {
      await apiPost('/alerts', { threadId: thread.id }).expect(httpStatus.UNAUTHORIZED);
    });

    test('creates an alert for the user', async () => {
      await apiPost('/alerts', { threadId: thread.id }, user.id).expect(httpStatus.CREATED);
    });

    test('updates an alert for the user', async () => {
      await apiPost(
        '/alerts',
        { threadId: thread.id, lastPostNumber: 4 } as CreateAlertRequest,
        user.id
      ).expect(httpStatus.CREATED);

      const { Alert: AlertModel } = datasource().models;

      const newAlert = await AlertModel.findOne({
        where: { user_id: user.id, thread_id: thread.id },
      });
      expect(newAlert.dataValues.lastPostNumber).toEqual(4);
    });
  });

  describe('DELETE /:threadId', () => {
    test('rejects logged out users', async () => {
      await apiDelete(`/alerts/${thread.id}`).expect(httpStatus.UNAUTHORIZED);
    });

    test('deletes the alert', async () => {
      await apiDelete(`/alerts/${thread.id}`, user.id).expect(httpStatus.OK);

      const { Alert: AlertModel } = datasource().models;

      const alerts = await AlertModel.findAll({ where: { user_id: user.id } });
      expect(alerts.length).toEqual(0);
    });
  });

  describe('POST /batchDelete', () => {
    test('rejects logged out users', async () => {
      await apiPost('/alerts/batchDelete', { threadIds: [thread.id] }).expect(
        httpStatus.UNAUTHORIZED
      );
    });

    test('deletes the alerts', async () => {
      await apiPost('/alerts/batchDelete', { threadIds: [thread.id] }, user.id).expect(
        httpStatus.OK
      );
      const { Alert: AlertModel } = datasource().models;

      const alerts = await AlertModel.findAll({ where: { user_id: user.id } });
      expect(alerts.length).toEqual(0);
    });
  });
});
