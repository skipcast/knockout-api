import httpStatus from 'http-status';
import { GoldSubscriptionCheckoutRequest, GoldSubscription } from 'knockout-schema';
import createUser from '../factories/user';
import { apiDelete, apiGet, apiPost } from '../helper/testHelper';
import StripeClient from '../../src/services/stripeClient';
import createStripeSubscription from '../helper/createStripeSubscription';

describe('/v2/gold-subscriptions endpoint', () => {
  let user;

  beforeEach(async () => {
    user = await createUser();
  });

  // delete any stripe test customers we have created for our test user
  afterEach(async () => {
    await new StripeClient().deleteStripeCustomer(user.id);
  });

  describe('POST /checkout-session', () => {
    const data: GoldSubscriptionCheckoutRequest = {
      successUrl: 'http://localhost:3000/success',
      cancelUrl: 'http://localhost:3000/failure',
    };

    test('rejects logged out users', async () => {
      await apiPost(`/gold-subscriptions/checkout-session`, data).expect(httpStatus.UNAUTHORIZED);
    });

    test('retrieves a checkout session id and associates a stripe customer with the user', async () => {
      await user.update({ stripeCustomerId: null });
      await user.save();

      expect(user.stripeCustomerId).toBeNull();

      const response = await apiPost(`/gold-subscriptions/checkout-session`, data, user.id).expect(
        httpStatus.CREATED
      );

      expect(response.body.sessionId).not.toBeNull();

      await user.reload();
      expect(user.stripeCustomerId).not.toBeNull();
    });

    test('returns a bad request when a user already has a subscription', async () => {
      // stripe API can sometimes have high latency, set test timeout accordingly
      jest.setTimeout(30000);

      await createStripeSubscription(user);
      await apiPost(`/gold-subscriptions/checkout-session`, data, user.id).expect(
        httpStatus.BAD_REQUEST
      );
      jest.setTimeout(5000);
    });
  });

  describe('GET /', () => {
    test('rejects logged out users', async () => {
      await apiGet(`/gold-subscriptions/`).expect(httpStatus.UNAUTHORIZED);
    });

    test('returns the subscription information for users with a subscription', async () => {
      // stripe API can sometimes have high latency, set test timeout accordingly
      jest.setTimeout(30000);

      await createStripeSubscription(user);

      const expectedResponse: GoldSubscription = { status: 'active' };
      await apiGet('/gold-subscriptions', user.id)
        .expect((res: { body: GoldSubscription }) => {
          expectedResponse.stripeId = res.body.stripeId;
          expectedResponse.startedAt = res.body.startedAt;
          expectedResponse.canceledAt = res.body.canceledAt;
          expectedResponse.nextPaymentAt = res.body.nextPaymentAt;
        })
        .expect(httpStatus.OK, expectedResponse);
      jest.setTimeout(5000);
    });

    test('returns an inactive subscription for users without a knockout gold subscription', async () => {
      const response = await apiGet('/gold-subscriptions', user.id).expect(httpStatus.OK);

      expect(response.body.status).toEqual('inactive');
    });
  });

  describe('DELETE /', () => {
    test('rejects logged out users', async () => {
      await apiDelete(`/gold-subscriptions/`).expect(httpStatus.UNAUTHORIZED);
    });

    test('returns the subscription information for users with a subscription', async () => {
      // stripe API can sometimes have high latency, set test timeout accordingly
      jest.setTimeout(30000);

      await createStripeSubscription(user);

      const expectedResponse: GoldSubscription = { status: 'canceled' };
      await apiDelete('/gold-subscriptions', user.id)
        .expect((res: { body: GoldSubscription }) => {
          expectedResponse.stripeId = res.body.stripeId;
          expectedResponse.startedAt = res.body.startedAt;
          expectedResponse.canceledAt = res.body.canceledAt;
          expectedResponse.nextPaymentAt = res.body.nextPaymentAt;
        })
        .expect(httpStatus.OK, expectedResponse);
      jest.setTimeout(5000);
    });

    test('returns a bad request for users without a gold subscription', async () => {
      await apiDelete('/gold-subscriptions', user.id).expect(httpStatus.BAD_REQUEST);
    });
  });
});
