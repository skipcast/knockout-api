import httpStatus from 'http-status';
import {
  NewThread,
  UpdateThreadRequest,
  UpdateThreadTagsRequest,
  ThreadMetadata,
} from 'knockout-schema';
import { RAID_MODE_KEY } from '../../src/constants/adminSettings';
import { addPermissionCodesToRole, apiGet, apiPost, apiPut } from '../helper/testHelper';
import knex from '../../src/services/knex';
import redis from '../../src/services/redisClient';
import createPost from '../factories/post';
import createUser from '../factories/user';
import createSubforum from '../factories/subforum';
import createThread from '../factories/thread';
import createRole from '../factories/role';
import { getGuestUserRole } from '../../src/helpers/role';

describe('/v2/threads endpoint', () => {
  let user;
  let role;
  let subforum: { id: number; name: string };
  let thread: { id: number; title: string };

  beforeEach(async () => {
    role = await createRole();
    user = await createUser({ roleId: role.id });
    subforum = await createSubforum();
    thread = await createThread({ user_id: user.id, subforum_id: subforum.id });
  });

  describe('POST /', () => {
    let data;

    beforeEach(async () => {
      await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-thread-create`]);

      data = {
        title: 'A Thread',
        content: 'thread post content',
        icon_id: 1,
        subforum_id: subforum.id,
      };
    });

    test('creates a thread', async () => {
      const expectedResponse = {
        id: null,
        title: data.title,
        iconId: data.icon_id,
        userId: user.id,
        subforumId: subforum.id,
        createdAt: null,
        updatedAt: null,
        deletedAt: null,
        locked: false,
        pinned: false,
        backgroundType: null,
        backgroundUrl: null,
      };

      await apiPost('/threads', data, user.id)
        .expect((res: { body: NewThread }) => {
          expectedResponse.id = res.body.id;
          expectedResponse.createdAt = res.body.createdAt;
          expectedResponse.updatedAt = res.body.updatedAt;
        })
        .expect(httpStatus.CREATED, expectedResponse);
    });

    test('rejects an invalid user', async () => {
      await knex('Users').update('username', null).where('id', user.id);
      await apiPost('/threads', data, user.id).expect(httpStatus.UNAUTHORIZED);
    });

    test('rejects a logged out user', async () => {
      await apiPost('/threads', data).expect(httpStatus.UNAUTHORIZED);
    });

    test('rejects invalid thread content', async () => {
      data.content = '';
      await apiPost('/threads', data, user.id).expect(httpStatus.BAD_REQUEST);
    });

    test('rejects invalid thread title', async () => {
      data.title = '';
      await apiPost('/threads', data, user.id).expect(httpStatus.BAD_REQUEST);
    });

    test('blocks users without the correct permission from adding backgrounds', async () => {
      data.background_type = 'cover';
      data.background_url = 'none.webp';
      await apiPost('/threads', data, user.id).expect(httpStatus.FORBIDDEN);
    });

    test('allows users with the correct permission to add backgrounds', async () => {
      await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-thread-background-update`]);

      data.background_type = 'cover';
      data.background_url = 'none.webp';

      const expectedResponse = {
        id: 1,
        title: 'A Thread',
        iconId: 1,
        userId: user.id,
        subforumId: subforum.id,
        createdAt: null,
        updatedAt: null,
        deletedAt: null,
        locked: false,
        pinned: false,
        backgroundType: 'cover',
        backgroundUrl: 'none.webp',
      };

      await apiPost('/threads', data, user.id)
        .expect((res: { body: NewThread }) => {
          expectedResponse.id = res.body.id;
          expectedResponse.createdAt = res.body.createdAt;
          expectedResponse.updatedAt = res.body.updatedAt;
        })
        .expect(httpStatus.CREATED, expectedResponse);
    });

    describe('blocks new users from creating threads', () => {
      test('when raid mode is on', async () => {
        const newUser = await createUser();
        redis.set(RAID_MODE_KEY, 'true');
        await apiPost('/threads', data, newUser.id).expect(httpStatus.FORBIDDEN);
        redis.del(RAID_MODE_KEY);
      });
    });
  });

  describe('GET /:id/page?', () => {
    let post;

    beforeEach(async () => {
      await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-view`]);
      post = await createPost({
        user_id: user.id,
        thread_id: thread.id,
      });
    });

    test('retrieves a thread', async () => {
      const expectedResponse = {
        id: thread.id,
        title: thread.title,
        iconId: 1,
        subforumId: subforum.id,
        deleted: false,
        locked: false,
        pinned: false,
        lastPost: {
          id: post.id,
          thread: thread.id,
          page: 1,
          content: null,
          user: {
            id: user.id,
          },
          ratings: [],
          bans: [],
          threadPostNumber: 1,
          countryName: null,
          appName: 'knockout.chat',
        },
        backgroundUrl: '',
        backgroundType: '',
        user: {
          id: user.id,
          username: user.username,
          usergroup: user.usergroup,
          avatarUrl: 'none.webp',
          backgroundUrl: '',
          banned: false,
          isBanned: false,
          title: null,
        },
        postCount: 1,
        recentPostCount: 0,
        unreadPostCount: 0,
        readThreadUnreadPosts: 0,
        read: false,
        subforum: {
          id: subforum.id,
        },
        tags: [],
        totalPosts: 1,
        currentPage: 1,
        posts: [
          {
            id: post.id,
            thread: thread.id,
            page: 1,
            content: post.content,
            user: {
              id: user.id,
            },
            ratings: [],
            bans: [],
            threadPostNumber: 1,
            countryName: null,
            appName: 'knockout.chat',
          },
        ],
      };

      const response = await apiGet(`/threads/${thread.id}`, user.id).expect(httpStatus.OK);

      expect(response.body).toMatchObject(expectedResponse);
    });

    test('does not retrieve deleted threads', async () => {
      await knex('Threads').update({ deleted_at: new Date() }).where('id', thread.id);
      await apiGet(`/threads/${thread.id}`).expect(httpStatus.FORBIDDEN);
    });

    test('retrieves deleted threads if the user has the correct permission', async () => {
      await knex('Threads').update({ deleted_at: new Date() }).where('id', thread.id);
      await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-view-deleted-threads`]);
      await apiGet(`/threads/${thread.id}`, user.id).expect(httpStatus.OK);
    });
  });

  describe('latest and popular endpoints', () => {
    test('GET /latest retrieves latest threads', async () => {
      await apiGet('/threads/latest').expect(httpStatus.OK);
    });

    test('GET /popular retrieves popular threads', async () => {
      await apiGet('/threads/popular', user.id).expect(httpStatus.OK);
    });
  });

  describe('PUT', () => {
    describe('/:id', () => {
      test('blocks logged out users', async () => {
        const data: UpdateThreadRequest = {
          title: 'New title',
        };

        await apiPut(`/threads/${thread.id}`, data).expect(httpStatus.UNAUTHORIZED);
      });

      describe('the thread creator', () => {
        test('can update the title of a thread with the create permission', async () => {
          await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-thread-create`]);

          const data: UpdateThreadRequest = {
            title: 'New title',
          };

          const response = await apiPut(`/threads/${thread.id}`, data, user.id).expect(
            httpStatus.OK
          );

          expect(response.body.title).toEqual('New title');
        });

        test('cannot update a locked thread', async () => {
          await knex('Threads').update({ locked: true }).where('id', thread.id);

          const data: UpdateThreadRequest = {
            title: 'New title',
          };

          await apiPut(`/threads/${thread.id}`, data, user.id).expect(httpStatus.FORBIDDEN);
        });

        test('cannot move the thread to a different subforum', async () => {
          const data: UpdateThreadRequest = {
            subforum_id: 2,
          };

          await apiPut(`/threads/${thread.id}`, data, user.id).expect(httpStatus.FORBIDDEN);
        });

        test('cannot update the background of the thread', async () => {
          const data: UpdateThreadRequest = {
            background_url: 'none.webp',
            background_type: 'cover',
          };

          await apiPut(`/threads/${thread.id}`, data, user.id).expect(httpStatus.FORBIDDEN);
        });
      });
    });

    describe('/:id/tags', () => {
      test('blocks logged out users', async () => {
        const data: UpdateThreadTagsRequest = {
          tag_ids: [1, 2],
        };

        await apiPut(`/threads/${thread.id}/tags`, data).expect(httpStatus.UNAUTHORIZED);
      });
    });

    describe('GET /:id/metadata', () => {
      beforeEach(async () => {
        const guestRole = await getGuestUserRole();
        await addPermissionCodesToRole(guestRole.id, [`subforum-${subforum.id}-view`]);
        redis.del(`rolePermission-${guestRole.id}`);
      });

      test('retrieves the metadata for a thread', async () => {
        const expectedResponse: ThreadMetadata = {
          title: thread.title,
          iconId: 1,
          subforumName: subforum.name,
          username: user.username,
          createdAt: null,
          updatedAt: null,
        };

        const response = await apiGet(`/threads/${thread.id}/metadata`)
          .expect((res: { body: ThreadMetadata }) => {
            expectedResponse.createdAt = res.body.createdAt;
            expectedResponse.updatedAt = res.body.updatedAt;
          })
          .expect(httpStatus.OK);

        expect(response.body).toMatchObject(expectedResponse);
      });
    });
  });
});
