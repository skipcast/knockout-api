import { ThreadSearchRequest } from 'knockout-schema';
import { addPermissionCodesToRole, apiPost } from '../helper/testHelper';
import createPost from '../factories/post';
import createRole from '../factories/role';
import createUser from '../factories/user';
import createSubforum from '../factories/subforum';
import createThread from '../factories/thread';

describe('/v2/threadsearch endpoint', () => {
  let role;
  let user;
  let subforum;
  let thread;

  beforeEach(async () => {
    role = await createRole();
    user = await createUser({ roleId: role.id });
    subforum = await createSubforum();
    thread = await createThread({
      user_id: user.id,
      subforum_id: subforum.id,
      title: 'abcdefgh',
    });
    await createPost({ user_id: user.id, thread_id: thread.id });
  });

  describe('POST /', () => {
    test('search threads', async () => {
      await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-view`]);

      const data: ThreadSearchRequest = {
        title: thread.title,
        page: 1,
      };

      const expectedThread = {
        id: thread.id,
        title: thread.title,
        iconId: 1,
        subforumId: subforum.id,
        createdAt: null,
        updatedAt: null,
        deletedAt: null,
        deleted: false,
        locked: false,
        pinned: false,
        lastPost: null,
        backgroundUrl: '',
        backgroundType: '',
        user: null,
        postCount: 1,
        recentPostCount: 0,
        unreadPostCount: 0,
        readThreadUnreadPosts: 0,
        read: false,
        hasRead: false,
        hasSeenNoNewPosts: false,
        subforum: null,
        tags: [],
        viewers: null,
      };

      const res = await apiPost('/threadsearch', data, user.id);

      expectedThread.createdAt = res.body.threads[0].createdAt;
      expectedThread.updatedAt = res.body.threads[0].updatedAt;
      expectedThread.subforum = res.body.threads[0].subforum;
      expectedThread.lastPost = res.body.threads[0].lastPost;
      expectedThread.user = res.body.threads[0].user;
      expectedThread.viewers = res.body.threads[0].viewers;
      expect(res.body.threads).toEqual(
        expect.arrayContaining([expect.objectContaining(expectedThread)])
      );
    });
  });

  test('hides deleted threads', async () => {
    await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-view`]);

    const data: ThreadSearchRequest = {
      title: thread.title,
      page: 1,
    };

    thread.deletedAt = new Date();
    await thread.save();
    const res = await apiPost('/threadsearch', data, user.id);

    expect(res.body.threads).toHaveLength(0);
  });
});
