import httpStatus from 'http-status';
import os from 'os';
import {
  CreatePostRequest,
  UpdatePostRequest,
  RatePostRequest,
  RatingValue,
} from 'knockout-schema';
import knex from '../../src/services/knex';
import redis from '../../src/services/redisClient';
import Ripe from '../../src/services/ripe';
import 'reflect-metadata';
import createPost from '../factories/post';
import createSubforum from '../factories/subforum';
import createUser from '../factories/user';
import createThread from '../factories/thread';
import createIpBan from '../factories/ipBan';
import createRole from '../factories/role';
import createIspBan from '../factories/ispBan';
import { apiGet, apiPost, apiPut, apiDelete, addPermissionCodesToRole } from '../helper/testHelper';
import { RAID_MODE_KEY } from '../../src/constants/adminSettings';

describe('/v2/posts endpoint', () => {
  let subforum: { id: number };
  let thread: { id: number; title: string };
  let role;
  let user;
  let post;

  beforeEach(async () => {
    subforum = await createSubforum();
    role = await createRole();
    user = await createUser({ roleId: role.id });
    thread = await createThread({ user_id: user.id, subforum_id: subforum.id });
    post = await createPost({ user_id: user.id, thread_id: thread.id });
  });

  describe('GET /', () => {
    test('rejects logged out users', async () => {
      await apiGet(`/posts/${post.id}`).expect(httpStatus.UNAUTHORIZED);
    });

    test('retrieves posts', async () => {
      await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-view`]);
      await apiGet(`/posts/${post.id}`, user.id).expect(httpStatus.OK);
    });
  });

  describe('POST /', () => {
    let data: CreatePostRequest;

    beforeEach(async () => {
      await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-post-create`]);
      data = {
        content: 'Created post.',
        thread_id: thread.id,
      };
    });

    test('rejects users who cannot view the subforum', async () => {
      await apiPost('/posts/', data, user.id).expect(httpStatus.FORBIDDEN);
    });

    describe('for users who can view the subforum', () => {
      beforeEach(async () => {
        await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-view`]);
      });

      test('creates a post', async () => {
        await apiPost('/posts/', data, user.id).expect(httpStatus.CREATED);
      });

      test('rejects logged out users', async () => {
        await apiPost('/posts/', data).expect(httpStatus.UNAUTHORIZED);
      });

      test('rejects limited users when raid mode is on', async () => {
        await redis.setAsync(RAID_MODE_KEY, 'true');

        const limitedUser = await createUser();
        await apiPost('/posts/', data, limitedUser.id).expect(httpStatus.FORBIDDEN);
        redis.del(RAID_MODE_KEY);
      });

      test('rejects limited users who have posted in the last five minutes', async () => {
        // create post in new thread to ensure we dont merge
        // we can delete this once we have transactional tests
        // set the default async timeout here to 10s because we do two requests that can be
        // slow in the test environment
        const otherThread = await createThread({ user_id: user.id, subforum_id: subforum.id });
        const otherUser = await createUser();
        await addPermissionCodesToRole(otherUser.roleId, [
          `subforum-${subforum.id}-post-create`,
          `subforum-${subforum.id}-view`,
        ]);
        data.thread_id = otherThread.id;
        await apiPost('/posts/', data, otherUser.id).expect(httpStatus.CREATED);
        await apiPost('/posts/', data, otherUser.id).expect(httpStatus.FORBIDDEN);
      }, 10000);

      test('rejects banned ISPs', async () => {
        const ispBan = await createIspBan({
          netname: 'M247-LTD-Frankfurt',
          asn: '9009',
        });

        const ip = '141.98.102.227';
        const isBanned = await new Ripe(ip).isBanned();
        const isParentBanned = await new Ripe(ip).isParentBanned();
        expect(isBanned).toBeTruthy();
        expect(isParentBanned).toBeTruthy();

        await ispBan.destroy();
      });

      test('rejects specifically ip banned users', async () => {
        // figure out which network interfaces are available
        const interfaces = os.networkInterfaces();
        const interfaceNames = Object.keys(interfaces);
        const addresses = interfaceNames.reduce((list, interfaceName) => {
          const netInterface = interfaces[interfaceName];
          list = netInterface.reduce((l, netAddress) => {
            l.push(netAddress.address);
            return l;
          }, list);
          return list;
        }, []);
        // create ipBan records
        const bans = await Promise.all(
          addresses.map((address) =>
            createIpBan({
              address,
            })
          )
        );
        // try to create a post
        await apiPost('/posts/', data, user.id).expect(httpStatus.FORBIDDEN);
        // clear up bans
        await Promise.all(bans.map((ban) => ban.destroy()));
      });

      test('rejects ip range banned users', async () => {
        // figure out which network interfaces are available
        const interfaces = os.networkInterfaces();
        const interfaceNames = Object.keys(interfaces);
        const ranges = interfaceNames.reduce((list, interfaceName) => {
          const netInterface = interfaces[interfaceName];
          list = netInterface.reduce((l, netAddress) => {
            l.push(netAddress.cidr);
            return l;
          }, list);
          return list;
        }, []);
        // create ipBan records
        const bans = await Promise.all(
          ranges.map((range) =>
            createIpBan({
              range,
            })
          )
        );
        // try to create a post
        await apiPost('/posts/', data, user.id).expect(httpStatus.FORBIDDEN);
        // clear up bans
        await Promise.all(bans.map((ban) => ban.destroy()));
      });

      describe('for locked threads', () => {
        beforeEach(async () => {
          await knex('Threads').update({ locked: true }).where('id', thread.id);
        });

        test('blocks users without the subforum post bypass validations permission from posting', async () => {
          await apiPost('/posts/', data, user.id).expect(httpStatus.BAD_REQUEST);
        });

        test('allows users with the subforum post bypass validations permission to post', async () => {
          await addPermissionCodesToRole(role.id, [
            `subforum-${subforum.id}-post-bypass-validations`,
          ]);

          await apiPost('/posts/', data, user.id).expect(httpStatus.CREATED);
        });
      });

      describe('for deleted threads', () => {
        beforeEach(async () => {
          await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-view`]);
          await knex('Threads').update({ deleted_at: new Date() }).where('id', thread.id);
        });

        test('blocks users without the subforum post bypass validations permission from posting', async () => {
          await apiPost('/posts/', data, user.id).expect(httpStatus.FORBIDDEN);
        });

        test('allows users with the correct permissions to post', async () => {
          await addPermissionCodesToRole(role.id, [
            `subforum-${subforum.id}-view-deleted-threads`,
            `subforum-${subforum.id}-post-bypass-validations`,
          ]);
          await apiPost('/posts/', data, user.id).expect(httpStatus.CREATED);
        });
      });
    });
  });

  describe('PUT /:id', () => {
    const data: UpdatePostRequest = {
      content: 'Updated.',
    };

    beforeEach(async () => {
      await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-post-create`]);
    });

    test('updates a post', async () => {
      await apiPut(`/posts/${post.id}`, data, user.id).expect(httpStatus.OK);

      const newPost = await knex('Posts').first('content').where('id', post.id);
      expect(newPost.content).toEqual(data.content);
    });

    test('before the post edit buffer, creates a post edit record for the original post and the edit', async () => {
      await apiPut(`/posts/${post.id}`, data, user.id).expect(httpStatus.OK);

      const postEdits = await knex('PostEdits').where('post_id', post.id).orderBy('id', 'desc');
      expect(postEdits[0]).toBeUndefined();
    });

    test('after the post edit buffer, creates a post edit record for the original post and the edit', async () => {
      post.changed('createdAt', true);
      post.set('createdAt', new Date('2000-01-01'), { raw: true });
      await post.save({ silent: true, fields: ['createdAt'] });

      await apiPut(`/posts/${post.id}`, data, user.id).expect(httpStatus.OK);

      const postEdits = await knex('PostEdits').where('post_id', post.id).orderBy('id', 'desc');
      expect(postEdits[0].content).toEqual(data.content);
      expect(postEdits[1].content).toEqual(post.content);
    });

    test('rejects logged out users', async () => {
      await apiPut(`/posts/${post.id}`, data).expect(httpStatus.UNAUTHORIZED);
    });

    test('does not let non creators without the subforum post-update permission to update posts', async () => {
      const otherUser = await createUser();
      await apiPut(`/posts/${post.id}`, data, otherUser.id).expect(httpStatus.FORBIDDEN);
    });

    test('lets non creators with the subforum post-update permission to update posts', async () => {
      await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-post-update`]);
      await apiPut(`/posts/${post.id}`, data, user.id).expect(httpStatus.OK);

      const newPost = await knex('Posts').first('content').where('id', post.id);
      expect(newPost.content).toEqual(data.content);
    });
  });

  describe('PUT /:id/ratings', () => {
    const data: RatePostRequest = {
      rating: RatingValue.IDEA,
    };
    let otherUser;

    beforeEach(async () => {
      await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-post-rating-create`]);
      otherUser = await createUser({ roleId: role.id });
      await post.update({ user_id: otherUser.id });
    });

    test('rates a post', async () => {
      await apiPut(`/posts/${post.id}/ratings`, data, user.id).expect(httpStatus.OK);
    });

    test('rejects logged out users', async () => {
      await apiPut(`/posts/${post.id}/ratings`, data).expect(httpStatus.UNAUTHORIZED);
    });

    test('does not let users rate their own posts', async () => {
      await apiPut(`/posts/${post.id}/ratings`, data, otherUser.id).expect(httpStatus.BAD_REQUEST);
    });

    test('rejects invalid posts', async () => {
      await apiPut(`/posts/-1/ratings`, data, user.id).expect(httpStatus.INTERNAL_SERVER_ERROR);
    });
  });

  describe('DELETE /:id/ratings', () => {
    test('unrates a post', async () => {
      const otherUser = await createUser();
      await post.update({ user_id: otherUser.id });
      await apiDelete(`/posts/${post.id}/ratings`, user.id).expect(httpStatus.OK);
    });

    test('rejects logged out users', async () => {
      await apiDelete(`/posts/${post.id}/ratings`).expect(httpStatus.UNAUTHORIZED);
    });

    test('does not let users unrate their own posts', async () => {
      await apiDelete(`/posts/${post.id}/ratings`, user.id).expect(httpStatus.BAD_REQUEST);
    });

    test('rejects invalid posts', async () => {
      await apiDelete(`/posts/-1/ratings`, user.id).expect(httpStatus.INTERNAL_SERVER_ERROR);
    });
  });
});
