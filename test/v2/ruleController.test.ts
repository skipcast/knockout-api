import httpStatus from 'http-status';
import { Rule, CreateRuleRequest, UpdateRuleRequest } from 'knockout-schema';
import createUser from '../factories/user';
import createRule from '../factories/rule';
import createRole from '../factories/role';
import { addPermissionCodesToRole, apiDelete, apiGet, apiPost, apiPut } from '../helper/testHelper';

describe('/v2/rules endpoint', () => {
  let user: any;
  let role: any;
  let sitewideRule: Rule;
  let subforumRule: Rule;

  beforeEach(async () => {
    role = await createRole();
    user = await createUser({ roleId: role.id });
    sitewideRule = await createRule();
    subforumRule = await createRule({ rulableType: 'Subforum', rulableId: 1 });
  });

  describe('GET /', () => {
    test('shows all site wide rules when no rule type or id', async () => {
      const expectedRule = {
        id: sitewideRule.id,
        rulableType: null,
        rulableId: null,
        category: sitewideRule.category,
        title: sitewideRule.title,
        cardinality: sitewideRule.cardinality,
        description: sitewideRule.description,
        createdAt: null,
        updatedAt: null,
      };

      const response = await apiGet('/rules');
      const responseRule = response.body.find((rule: Rule) => rule.id === expectedRule.id);
      expectedRule.createdAt = responseRule.createdAt;
      expectedRule.updatedAt = responseRule.updatedAt;
      expect(responseRule).toEqual(expectedRule);
    });

    test('shows all rules for a given rule type and rule id', async () => {
      const expectedRule = {
        id: subforumRule.id,
        rulableType: subforumRule.rulableType,
        rulableId: subforumRule.rulableId,
        category: subforumRule.category,
        title: subforumRule.title,
        cardinality: subforumRule.cardinality,
        description: subforumRule.description,
        createdAt: null,
        updatedAt: null,
      };

      const response = await apiGet(
        `/rules?rulableType=Subforum&rulableId=${expectedRule.rulableId}`
      );
      const responseRule = response.body.find((rule: Rule) => rule.id === expectedRule.id);
      expectedRule.createdAt = responseRule.createdAt;
      expectedRule.updatedAt = responseRule.updatedAt;
      expect(responseRule).toEqual(expectedRule);
    });
  });

  describe('POST /', () => {
    const data: CreateRuleRequest = {
      category: 'Site Wide Rules',
      title: 'Test Rule',
      cardinality: 1,
      description: 'Test rule with description',
    };

    test('create a general rule when the user has the correct permission', async () => {
      await addPermissionCodesToRole(role.id, ['rule-create']);

      const expectedResponse: Rule = {
        id: null,
        rulableType: null,
        rulableId: null,
        category: data.category,
        title: data.title,
        cardinality: data.cardinality,
        description: data.description,
        createdAt: null,
        updatedAt: null,
      };

      await apiPost('/rules', data, user.id)
        .expect((res) => {
          expectedResponse.id = res.body.id;
          expectedResponse.createdAt = res.body.createdAt;
          expectedResponse.updatedAt = res.body.updatedAt;
        })
        .expect(httpStatus.CREATED, expectedResponse);
    });

    test('rejects when user does not have the correct permission', async () => {
      await apiPost(`/rules/`, data, user.id).expect(httpStatus.FORBIDDEN);
    });

    test('rejects a logged out user', async () => {
      await apiPost(`/rules/`, data).expect(httpStatus.UNAUTHORIZED);
    });
  });

  describe('PUT /:id', () => {
    const data: UpdateRuleRequest = {
      description: 'New Description',
    };

    test('updates a rule when user has the correct permission', async () => {
      await addPermissionCodesToRole(role.id, ['rule-update']);

      const expectedResponse: Rule = {
        id: sitewideRule.id,
        rulableType: null,
        rulableId: null,
        category: sitewideRule.category,
        title: sitewideRule.title,
        cardinality: sitewideRule.cardinality,
        description: data.description,
        createdAt: null,
        updatedAt: null,
      };

      expectedResponse.description = data.description;

      await apiPut(`/rules/${sitewideRule.id}`, data, user.id)
        .expect((res) => {
          expectedResponse.createdAt = res.body.createdAt;
          expectedResponse.updatedAt = res.body.updatedAt;
        })
        .expect(httpStatus.CREATED, expectedResponse);
    });

    test('rejects when user does not have the correct permission', async () => {
      await apiPut(`/rules/${sitewideRule.id}`, data, user.id).expect(httpStatus.FORBIDDEN);
    });

    test('rejects a logged out user', async () => {
      await apiPut(`/rules/${sitewideRule.id}`, data).expect(httpStatus.UNAUTHORIZED);
    });
  });

  describe('DELETE /:id', () => {
    test('deletes a rule when user has the correct permission', async () => {
      await addPermissionCodesToRole(role.id, ['rule-archive']);

      const expectedResponse = { message: 'Rule deleted' };

      await apiDelete(`/rules/${sitewideRule.id}`, user.id).expect(httpStatus.OK, expectedResponse);
    });

    test('rejects when user does not have the correct permission', async () => {
      await apiDelete(`/rules/${sitewideRule.id}`, user.id).expect(httpStatus.FORBIDDEN);
    });

    test('rejects a logged out user', async () => {
      await apiDelete(`/rules/${sitewideRule.id}`).expect(httpStatus.UNAUTHORIZED);
    });
  });
});
