import httpStatus from 'http-status';
import { NotificationType, MarkNotificationsAsReadRequest } from 'knockout-schema';
import knex from '../../src/services/knex';
import createUser from '../factories/user';
import createNotification from '../factories/notification';
import createSubforum from '../factories/subforum';
import createThread from '../factories/thread';
import createPost from '../factories/post';
import createConversation from '../factories/conversation';
import createMessage from '../factories/message';
import { apiGet, apiPut } from '../helper/testHelper';
import datasource from '../../src/models/datasource';

const { Notification } = datasource().models;

describe('/v2/notifications endpoint', () => {
  let user;
  let user1;
  let subforum;
  let thread;
  let post;
  let notification;
  let notification1;
  let conversation;
  let message;

  beforeAll(async () => {
    await Notification.destroy({ where: {} });
  });

  beforeEach(async () => {
    user = await createUser();
    user1 = await createUser();
    subforum = await createSubforum();
    thread = await createThread({ user_id: user1.id, subforum_id: subforum.id });
    post = await createPost({ user_id: user1.id, thread_id: thread.id });
    notification = await createNotification({
      userId: user.id,
      type: NotificationType.POST_REPLY,
      dataId: post.id,
    });
    conversation = await createConversation([user.id, user1.id]);
    message = await createMessage({ user_id: user1.id, conversation_id: conversation.id });
    await conversation.update({ latest_message_id: message.id });
    notification1 = await createNotification({
      userId: user.id,
      type: NotificationType.MESSAGE,
      dataId: conversation.id,
    });
  });

  describe('GET /', () => {
    test('rejects logged out users', async () => {
      await apiGet(`/notifications`).expect(httpStatus.UNAUTHORIZED);
    });

    test('retrieves notifications', async () => {
      const response = await apiGet(`/notifications`, user.id).expect(httpStatus.OK);

      expect(response.body.length).toBe(2);
      expect(response.body[0].id).toBe(notification.id);
      expect(response.body[0].type).toBe(NotificationType.POST_REPLY);
      expect(response.body[0].data.id).toBe(post.id);

      expect(response.body[1].id).toBe(notification1.id);
      expect(response.body[1].type).toBe(NotificationType.MESSAGE);
      expect(response.body[1].data.id).toBe(conversation.id);
      expect(response.body[1].data.messages[0].content).toBe(message.content);
    });
  });

  describe('PUT /', () => {
    let data: MarkNotificationsAsReadRequest;

    beforeEach(() => {
      data = { notificationIds: [notification.id] };
    });

    test('rejects logged out users', async () => {
      await apiPut(`/notifications`, data).expect(httpStatus.UNAUTHORIZED);
    });

    test('marks a single notification as read', async () => {
      await apiPut('/notifications', data, user.id).expect(httpStatus.OK);

      const notifications = await knex('Notifications').select('read').where({
        id: notification.id,
        user_id: user.id,
      });

      expect(notifications[0].read).toBeTruthy();
    });

    test('marks multiple notifications as read', async () => {
      await apiPut(
        '/notifications',
        { ...data, notificationIds: [notification.id, notification1.id] },
        user.id
      ).expect(httpStatus.OK);

      const notifications = await knex('Notifications').select().where({
        user_id: user.id,
      });

      expect(notifications.length).toBe(2);
      expect(notifications[0].read).toBeTruthy();
      expect(notifications[1].read).toBeTruthy();
    });
  });

  describe('PUT /all', () => {
    test('rejects logged out users', async () => {
      await apiPut(`/notifications/all`, {}).expect(httpStatus.UNAUTHORIZED);
    });

    test('marks all notifications as read', async () => {
      await apiPut('/notifications/all', {}, user.id).expect(httpStatus.OK);
      const notifications = await knex('Notifications')
        .select()
        .where({
          user_id: user.id,
        })
        .orderBy('id', 'desc');
      expect(notifications.length).toBe(2);
      expect(Boolean(notifications[0].read)).toBe(true);
      expect(Boolean(notifications[1].read)).toBe(true);
    });
  });
});
