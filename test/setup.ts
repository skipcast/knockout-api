import { Sequelize } from 'sequelize';

const env = process.env.NODE_ENV || 'test';
// eslint-disable-next-line import/no-dynamic-require
const config = require('../config/config')[env];

export default async () => {
  const sequelize = new Sequelize(config.database, config.username, config.password, config);
  await sequelize.sync({ force: true });
};
