import createUser from '../factories/user';
import createRole from '../factories/role';

import datasource from '../../src/models/datasource';
import { RoleCode } from '../../src/helpers/permissions';

const { Role, PreviousUserRole } = datasource().models;

describe('User', () => {
  let user;

  beforeEach(async () => {
    user = await createUser();
  });

  test('user without a role is created with limited user role', async () => {
    const role = await Role.findOne({ where: { id: user.roleId } });
    expect(role.code).toEqual(RoleCode.LIMITED_USER);
  });

  test('users previous role is persisted', async () => {
    const previousUserRoleId = user.roleId;
    const newRole = await createRole({});
    user.roleId = newRole.id;
    await user.save();
    const previousRole = await PreviousUserRole.findOne({ where: { user_id: user.id } });
    expect(previousRole.role_id).toEqual(previousUserRoleId);
  });
});
