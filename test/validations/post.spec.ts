import { validateThreadStatus } from '../../src/validations/post';
import createSubforum from '../factories/subforum';
import createThread from '../factories/thread';
import createUser from '../factories/user';

describe('Test thread status validation', () => {
  let subforum;
  let user;

  beforeEach(async () => {
    subforum = await createSubforum();
    user = await createUser();
  });

  test('validation passes for a valid thread', async () => {
    const thread = await createThread({ user_id: user.id, subforum_id: subforum.id });
    const threadStatus = await validateThreadStatus(thread.id);
    expect(threadStatus).toBe(true);
  });

  test('validation fails for a locked thread', async () => {
    const thread = await createThread({ user_id: user.id, subforum_id: subforum.id, locked: true });
    const threadStatus = await validateThreadStatus(thread.id);
    expect(threadStatus).toBe(false);
  });

  test('validation fails for a deleted thread', async () => {
    const thread = await createThread({
      user_id: user.id,
      subforum_id: subforum.id,
      deletedAt: new Date(),
    });
    const threadStatus = await validateThreadStatus(thread.id);
    expect(threadStatus).toBe(false);
  });
});
