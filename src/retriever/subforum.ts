/* eslint-disable import/no-cycle */
import Sequelize from 'sequelize';
import { ThreadWithLastPost } from 'knockout-schema';
import AbstractRetriever from './abstractRetriever';
import knex from '../services/knex';
import datasource from '../models/datasource';
import ThreadRetriever, { ThreadFlag } from './thread';
import { getUserPermissionCodes, userCanViewSubforum } from '../helpers/user';
import nsfwTagName from '../constants/nsfwTagName';

export enum SubforumFlag {
  INCLUDE_TOTAL_THREADS,
  INCLUDE_TOTAL_POSTS,
  INCLUDE_LAST_POST,
  HIDE_NSFW,
  LIMITED_USER,
}

const SELECTED_ATTRIBUTES = [
  'id',
  'name',
  'description',
  'icon_id',
  'icon',
  'created_at',
  'updated_at',
];

export default class SubforumRetriever extends AbstractRetriever {
  protected cachePrefix: string = 'subforum';

  private threadCounterCachePrefix: string = 'subforum-thread-count';

  private threadCounterCacheLifetime = 43200; // 12 hours

  private postCounterCachePrefix: string = 'subforum-post-count';

  protected async query(ids: Array<number>) {
    return datasource().models.Subforum.findAll({
      attributes: SELECTED_ATTRIBUTES,
      where: { id: ids },
    });
  }

  private async getTotalThreads() {
    if (!this.hasFlag(SubforumFlag.INCLUDE_TOTAL_THREADS)) return {};

    const cachedThreadCounts = await this.cacheGet(this.threadCounterCachePrefix, this.ids);
    const uncachedIds = this.filterNullIndices(cachedThreadCounts);

    const threadCounts = await datasource().models.Thread.findAll({
      attributes: ['subforum_id', [Sequelize.fn('COUNT', Sequelize.col('id')), 'totalThreads']],
      where: {
        subforum_id: uncachedIds,
        deleted_at: { [Sequelize.Op.eq]: null },
      },
      group: ['subforum_id'],
    });

    const result = threadCounts.reduce((list, count) => {
      const threadSubforumId = count.subforum_id;
      list[threadSubforumId] = { totalThreads: count.dataValues.totalThreads };
      this.cacheSet(
        this.threadCounterCachePrefix,
        threadSubforumId,
        list[threadSubforumId],
        this.threadCounterCacheLifetime
      );
      return list;
    }, {});

    this.ids.forEach((id) => {
      if (cachedThreadCounts.get(id) !== null) result[id] = cachedThreadCounts.get(id);
    });

    return result;
  }

  private async getTotalPosts() {
    if (!this.hasFlag(SubforumFlag.INCLUDE_TOTAL_POSTS)) return {};

    const cachedPostCounts = await this.cacheGet(this.postCounterCachePrefix, this.ids);
    const uncachedIds = this.filterNullIndices(cachedPostCounts);

    let postCounts = [];
    if (uncachedIds.length > 0) {
      postCounts = await datasource().models.Post.findAll({
        attributes: [[Sequelize.fn('COUNT', Sequelize.col('Post.id')), 'totalPosts']],
        include: [
          {
            model: datasource().models.Thread,
            attributes: ['subforum_id'],
            where: {
              subforum_id: uncachedIds,
              deleted_at: { [Sequelize.Op.eq]: null },
            },
          },
        ],
        group: ['subforum_id'],
      });
    }

    const result = postCounts.reduce((list, count) => {
      const postSubforumId = count.Thread.subforum_id;
      list[postSubforumId] = { totalPosts: count.dataValues.totalPosts };
      this.cacheSet(this.postCounterCachePrefix, postSubforumId, list[postSubforumId]);
      return list;
    }, {});

    this.ids.forEach((id) => {
      if (cachedPostCounts.get(id) !== null) result[id] = cachedPostCounts.get(id);
    });

    return result;
  }

  private async getLastPosts() {
    if (!this.hasFlag(SubforumFlag.INCLUDE_LAST_POST)) return {};

    const hideNsfw = this.hasFlag(SubforumFlag.HIDE_NSFW);

    const lastThreadIds = await knex
      .raw(
        `SELECT
        t.id as threadId
      FROM (
        SELECT MAX(Threads.updated_at) as maxUpdatedAt
        FROM Threads
        LEFT OUTER JOIN ThreadTags tt
          ON Threads.id = tt.thread_id
        LEFT OUTER JOIN Tags tags
          ON tt.tag_id = tags.id
        WHERE Threads.deleted_at IS NULL AND Threads.subforum_id IN (${this.ids})
        ${hideNsfw ? ` AND coalesce(tags.name, '') != '${nsfwTagName}'` : ''}
        GROUP BY subforum_id
      ) AS latest_updated_threads
      INNER JOIN Threads t
        ON t.updated_at = latest_updated_threads.maxUpdatedAt`
      )
      .then((records) => records[0].map((record) => record.threadId));

    const lastThreads = (await new ThreadRetriever(lastThreadIds, [
      ThreadFlag.INCLUDE_LAST_POST,
    ]).getObjectArray()) as ThreadWithLastPost[];

    const result = lastThreads.reduce((list, thread) => {
      const postSubforumId = thread.subforumId;
      list[postSubforumId] = {
        lastPost: { ...thread.lastPost, thread },
      };
      return list;
    }, {});

    return result;
  }

  protected async populateData(subforums: Array<any>) {
    // grab data from related caches
    const lastPosts = await this.getLastPosts();
    const totalThreads = await this.getTotalThreads();
    const totalPosts = await this.getTotalPosts();
    const userPermissionCodes = Object.prototype.hasOwnProperty.call(this.args, 'userId')
      ? await getUserPermissionCodes(this.args['userId'])
      : [];

    // merge related data in
    const populatedSubforums = subforums.reduce((list, subforum) => {
      if (
        Object.prototype.hasOwnProperty.call(this.args, 'userId') &&
        !userCanViewSubforum(userPermissionCodes, subforum.id)
      ) {
        return list;
      }

      subforum.lastPost = lastPosts[subforum.id]?.lastPost || {};
      subforum.totalThreads = totalThreads[subforum.id]?.totalThreads || 0;
      subforum.totalPosts = totalPosts[subforum.id]?.totalPosts || 0;

      return [...list, subforum];
    }, []);
    return super.populateData(populatedSubforums);
  }

  protected format(data): Object {
    return {
      id: data.id,
      name: data.name,
      description: data.description,
      iconId: data.icon_id,
      icon: data.icon,
      createdAt: data.created_at,
      updatedAt: data.updated_at,
    };
  }
}
