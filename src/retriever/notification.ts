import { Notification, NotificationType } from 'knockout-schema';
import AbstractRetriever from './abstractRetriever';
import knex from '../services/knex';
import ConversationRetriever, { ConversationFlag } from './conversation';
import PostRetriever, { PostFlag } from './post';
import ProfileCommentRetriever from './profileComment';

export default class NotificationRetriever extends AbstractRetriever<Notification> {
  protected cacheLifetime = 604800;

  protected cachePrefix: string = 'notification';

  protected async query(ids: Array<number>) {
    return knex('Notifications')
      .select(
        'id',
        'type',
        'data_id as dataId',
        'user_id as userId',
        'read',
        'created_at as createdAt'
      )
      .whereIn('id', ids);
  }

  protected format(data): Notification {
    const { dataId, ...notification } = data;
    return {
      ...notification,
      read: Boolean(notification.read),
      data: { id: dataId },
    };
  }

  private static async getDataObjects(notifications) {
    const objects = {};
    await Promise.all(
      notifications.map((notification) =>
        (async () => {
          let object;
          switch (notification.type) {
            case NotificationType.MESSAGE:
              object = await new ConversationRetriever(notification.data.id, [
                ConversationFlag.RETRIEVE_SHALLOW,
              ]).getSingleObject();
              break;
            case NotificationType.POST_REPLY:
              object = await new PostRetriever(notification.data.id, [
                PostFlag.INCLUDE_THREAD,
                PostFlag.TRUNCATE_CONTENT,
              ]).getSingleObject();
              break;
            case NotificationType.PROFILE_COMMENT:
              object = await new ProfileCommentRetriever(notification.data.id).getSingleObject();
              break;
            case NotificationType.REPORT_RESOLUTION:
              object = {};
              break;
            default:
              break;
          }
          objects[`${notification.type}-${notification.data.id}`] = object;
        })()
      )
    );
    return objects;
  }

  protected async populateData(notifications) {
    const dataObjects = await NotificationRetriever.getDataObjects(notifications);

    const populatedNotifications = notifications.map((notification) => ({
      ...notification,
      data: dataObjects[`${notification.type}-${notification.data.id}`],
    }));

    return super.populateData(populatedNotifications);
  }
}
