/* eslint-disable import/no-cycle */
import Sequelize from 'sequelize';
import { Rating, Thread } from 'knockout-schema';
import AbstractRetriever from './abstractRetriever';
import UserRetriever from './user';
import PostRetriever, { PostFlag } from './post';
import knex from '../services/knex';
import PostRatingRetriever, { PostRatingFlag } from './postRating';
import SubforumRetriever from './subforum';
import { getGuestCountKey, getMemberCountKey, getViewers } from '../handlers/threadHandler';
import redis from '../services/redisClient';

import datasource from '../models/datasource';

export enum ThreadFlag {
  INCLUDE_SUBFORUM,
  RETRIEVE_SHALLOW,
  INCLUDE_USER,
  INCLUDE_LAST_POST,
  INCLUDE_POST_COUNT,
  EXCLUDE_READ_THREADS,
  INCLUDE_RECENT_POSTS,
  INCLUDE_TAGS,
  INCLUDE_VIEWER_USERS,
}

export default class ThreadRetriever extends AbstractRetriever<Thread> {
  protected cachePrefix: string = 'thread';

  private lastPostCachePrefix: string = 'threadPosts';

  protected recentPostCachePrefix: string = 'threadRecentPosts';

  protected postCountCachePrefix: string = 'threadPostCount;';

  protected async query(ids: Array<number>) {
    return knex
      .from('Threads')
      .select(
        'id',
        'title as threadTitle',
        'icon_id as threadIconId',
        'user_id as threadUserId',
        'subforum_id as threadSubforumId',
        'created_at as threadCreatedAt',
        'updated_at as threadUpdatedAt',
        'deleted_at as threadDeletedAt',
        'locked as threadLocked',
        'pinned as threadPinned',
        'background_url as threadBackgroundUrl',
        'background_type as threadBackgroundType'
      )
      .whereIn('id', ids);
  }

  private async getThreadTags(ids: Array<number>) {
    if (this.hasFlag(ThreadFlag.RETRIEVE_SHALLOW) && !this.hasFlag(ThreadFlag.INCLUDE_TAGS))
      return {};
    const threadQuery = await knex
      .from('ThreadTags as tht')
      .select('tht.thread_id as threadId', 'tht.tag_id as tagId', 't.name as tagName')
      .leftJoin('Tags as t', 'tht.tag_id', 't.id')
      .whereIn('tht.thread_id', ids)
      .groupBy('threadId', 'tagId');

    // make into an array of tag {id: name} objects
    return threadQuery.reduce(
      (list, row) => ({
        ...list,
        [row.threadId]: list[row.threadId]
          ? [...list[row.threadId], { [row.tagId]: row.tagName }]
          : [{ [row.tagId]: row.tagName }],
      }),
      {}
    );
  }

  private async getFirstPostRatings(threads: Thread[]) {
    if (this.hasFlag(ThreadFlag.RETRIEVE_SHALLOW)) return {};
    const ids = threads.map((thread) => thread.id);

    const firstPosts = await knex('Posts')
      .select('Posts.id', 'Posts.thread_id')
      .where('Posts.thread_post_number', 1)
      .whereIn('Posts.thread_id', ids);

    const postIds: number[] = firstPosts.map((el) => el.id);

    const postThreadDict = firstPosts.reduce((acc, val) => {
      acc[val.id] = val.thread_id;
      return acc;
    }, {});

    const firstPostRatings = (await new PostRatingRetriever(postIds, [
      PostRatingFlag.EXCLUDE_USERS,
    ]).get()) as Map<number, Rating[]>;

    const threadTopRatings = {};

    postIds.forEach((postId) => {
      const threadId = postThreadDict[postId];
      firstPostRatings.get(postId)?.forEach((rating) => {
        if (
          (!threadTopRatings[threadId] || threadTopRatings[threadId].count < rating.count) &&
          rating.count >= 10
        ) {
          threadTopRatings[threadId] = rating;
        }
      });
    });

    return threadTopRatings;
  }

  private async getSubforums(threads: Thread[]) {
    if (!this.hasFlag(ThreadFlag.INCLUDE_SUBFORUM)) return new Map();
    const subforumIds = threads.map((thread) => thread.subforumId).filter((id) => id !== null);

    return new SubforumRetriever(subforumIds).get();
  }

  private async getPostCounts() {
    if (!this.hasFlag(ThreadFlag.INCLUDE_POST_COUNT)) return {};

    const cachedPostCounts = await this.cacheGet(this.postCountCachePrefix, this.ids);
    const uncachedIds = this.filterNullIndices(cachedPostCounts);

    const postCounts = await datasource().models.Post.findAll({
      attributes: ['thread_id', [Sequelize.fn('COUNT', Sequelize.col('Post.id')), 'postCount']],
      where: { thread_id: uncachedIds },
      group: ['thread_id'],
    });

    const result = postCounts.reduce((list, count) => {
      const threadId = parseInt(count.dataValues.thread_id, 10);
      list[threadId] = { postCount: count.dataValues.postCount };
      this.cacheSet(this.postCountCachePrefix, threadId, list[threadId]);
      return list;
    }, {});

    this.ids.forEach((id) => {
      if (cachedPostCounts.get(id) !== null) result[id] = cachedPostCounts.get(id);
    });

    return result;
  }

  private async getLastPosts() {
    if (this.hasFlag(ThreadFlag.RETRIEVE_SHALLOW) && !this.hasFlag(ThreadFlag.INCLUDE_LAST_POST))
      return {};

    const cachedLastPosts = await this.cacheGet(this.lastPostCachePrefix, this.ids);
    const uncachedIds = this.filterNullIndices(cachedLastPosts);

    let lastPosts = [];

    if (uncachedIds.length > 0) {
      lastPosts = await knex.raw(
        `SELECT
            Posts.thread_id as threadId, Posts.id as postId
          FROM (
            SELECT MAX(Posts.id) maxPostId
            FROM Posts
            GROUP BY Posts.thread_id
          ) AS latest_posts
          INNER JOIN Posts
            ON Posts.id = latest_posts.maxPostId
          WHERE
            Posts.thread_id IN (${uncachedIds})
          GROUP BY Posts.thread_id`
      );

      lastPosts = lastPosts?.length ? lastPosts[0] : [];
    }

    const postIds = lastPosts.map((post) => post?.postId).filter((post) => post !== null);
    const postRetriever = new PostRetriever(postIds, [
      PostFlag.RETRIEVE_SHALLOW,
      PostFlag.INCLUDE_USER,
      PostFlag.TRUNCATE_CONTENT,
    ]);
    const rawPosts = await postRetriever.get();

    const result = lastPosts.reduce((list, post) => {
      list[post.threadId] = {
        lastPost: { ...rawPosts.get(post.postId), thread: post.threadId },
      };
      this.cacheSet(this.lastPostCachePrefix, post.threadId, list[post.threadId]);
      return list;
    }, {});

    this.ids.forEach((id) => {
      if (cachedLastPosts.get(id) !== null) result[id] = cachedLastPosts.get(id);
    });
    return result;
  }

  private async getRecentPosts() {
    if (!this.hasFlag(ThreadFlag.INCLUDE_RECENT_POSTS)) return {};
    const cachedRecentPosts = await this.cacheGet(this.recentPostCachePrefix, this.ids);
    const uncachedIds = this.filterNullIndices(cachedRecentPosts);

    const postCounts = await knex
      .from('Posts as p')
      .select('p.thread_id as id')
      .count('id as threadRecentPostCount')
      .whereIn('p.thread_id', uncachedIds)
      .whereRaw('p.created_at >= DATE_SUB(NOW(), INTERVAL 1 DAY)')
      .groupBy('p.thread_id');

    const result = postCounts.reduce((list, count) => {
      list[count.id] = count.threadRecentPostCount;
      this.cacheSet(this.recentPostCachePrefix, count.id, list[count.id]);
      return list;
    }, {});

    this.ids.forEach((id) => {
      if (cachedRecentPosts.get(id) !== null) result[id] = cachedRecentPosts.get(id);
    });

    return result;
  }

  private async getReadThreads(threads: Thread[], userId: number) {
    if (userId == null || this.hasFlag(ThreadFlag.EXCLUDE_READ_THREADS)) return {};
    const threadIds = threads.map((thread) => thread.id);
    const threadIdentifier = knex.raw('??', ['ReadThreads.thread_id']);
    const lastSeenIdentifier = knex.raw('??', ['ReadThreads.last_seen']);

    const postsSince = knex('Posts')
      .count({ all: '*' })
      .where('Posts.thread_id', threadIdentifier)
      .where('Posts.created_at', '>', lastSeenIdentifier)
      .as('posts_since');

    const readThreads = await knex
      .select(
        'ReadThreads.thread_id',
        'ReadThreads.last_seen',
        postsSince,
        knex.raw(
          '(select id from Posts where Posts.thread_id = ReadThreads.thread_id and Posts.created_at > ReadThreads.last_seen order by Posts.created_at limit 1 ) as firstUnreadId'
        )
      )
      .from('ReadThreads')
      .where('ReadThreads.user_id', userId)
      .whereIn('ReadThreads.thread_id', threadIds);

    return readThreads.reduce((list, readThread) => {
      list[readThread.thread_id] = {
        lastSeen: readThread.last_seen,
        postsSince: readThread.posts_since,
        firstUnreadId: readThread.firstUnreadId,
      };
      return list;
    }, {});
  }

  private async getViewers(ids) {
    const result = {};

    const viewerQueries = ids.map(async (id) => {
      let userObjects;
      if (this.hasFlag(ThreadFlag.INCLUDE_VIEWER_USERS)) {
        const members = await getViewers(id);
        userObjects = await new UserRetriever(members.map(Number)).getObjectArray();
      }
      result[id] = {
        memberCount: Number(await redis.getAsync(getMemberCountKey(id))) || 0,
        guestCount: Number(await redis.getAsync(getGuestCountKey(id))) || 0,
        users: userObjects,
      };
    });

    await Promise.all(viewerQueries);
    return result;
  }

  protected format(data): Object {
    return {
      id: data.id,
      title: data.threadTitle,
      iconId: data.threadIconId,
      subforumId: data.threadSubforumId,
      createdAt: data.threadCreatedAt,
      updatedAt: data.threadUpdatedAt,
      deletedAt: data.threadDeletedAt,
      deleted: Boolean(data.threadDeletedAt != null),
      locked: Boolean(data.threadLocked),
      pinned: Boolean(data.threadPinned),
      lastPost: {},
      backgroundUrl: data.threadBackgroundUrl,
      backgroundType: data.threadBackgroundType,
      user: data.threadUserId,
    };
  }

  protected async populateData(threads) {
    // grab data from related caches
    const users =
      this.hasFlag(ThreadFlag.RETRIEVE_SHALLOW) && !this.hasFlag(ThreadFlag.INCLUDE_USER)
        ? new Map()
        : await new UserRetriever(threads.map((thread) => thread.user)).get();
    const posts = await this.getLastPosts();
    const postCounts = await this.getPostCounts();
    const recentPostCounts = await this.getRecentPosts();
    const firstPostRatings = await this.getFirstPostRatings(threads);
    // eslint-disable-next-line @typescript-eslint/dot-notation
    const readThreads = await this.getReadThreads(threads, this.args['userId'] || null);
    const subforums = await this.getSubforums(threads);
    const tags = await this.getThreadTags(threads.map((thread) => thread.id));
    const viewers = await this.getViewers(threads.map((thread) => thread.id));

    // merge related data in
    const populatedThreads = threads.reduce((list, thread) => {
      const readThread = readThreads[thread.id] || null;
      thread.user = users.get(thread.user) || thread.user;

      thread.lastPost = posts[thread.id] != null ? posts[thread.id].lastPost : {};
      thread.postCount = postCounts[thread.id]?.postCount || 0;
      thread.recentPostCount = recentPostCounts[thread.id] || 0;

      // holy shit, clean this mess up, too tightly coupled to rushed front-end implementation
      // write something that all clients can sanely consume
      thread.unreadPostCount = readThread != null ? readThread.postsSince : 0;
      thread.readThreadUnreadPosts = readThread != null ? readThread.postsSince : 0;
      thread.read = readThread != null;
      thread.hasRead = readThread != null;
      thread.hasSeenNoNewPosts = readThread != null && readThread.postsSince === 0;

      if (readThread) {
        thread.firstUnreadId = readThread.firstUnreadId;
      }

      if (firstPostRatings[thread.id]) {
        thread.firstPostTopRating = firstPostRatings[thread.id];
      }

      thread.subforum = subforums.get(thread.subforumId) || null;
      thread.tags = tags[thread.id] || [];
      thread.viewers = viewers[thread.id];

      return [...list, thread];
    }, []);

    return super.populateData(populatedThreads);
  }

  async invalidatePosts() {
    this.ids.map(async (id) => {
      this.cacheDrop(this.lastPostCachePrefix, id);
      this.cacheDrop(this.recentPostCachePrefix, id);
      this.cacheDrop(this.postCountCachePrefix, id);
    });
  }
}
