/* eslint-disable class-methods-use-this */
import redis from '../services/redisClient';

interface Result {
  id: number;
}

export default abstract class AbstractRetriever<EntityType extends Result = any> {
  protected ids: Array<number>;

  protected flags: Array<number>;

  protected args: Object;

  protected cachePrefix: string;

  protected cacheLifetime = 2592000;

  protected abstract query(ids: Array<number>): Promise<Result[]>;

  protected async getEntities(ids: Array<number>): Promise<Map<number, Result>> {
    const results = await this.query(ids);
    const output = new Map<number, Result>();
    results.forEach((result) => {
      output.set(Number(result.id), result);
    });
    return output;
  }

  // grab a record from the cache
  protected async cacheGet(prefix: string, ids: Array<number>): Promise<Map<number, EntityType>> {
    if (ids.length === 0) return new Map();
    const keys = ids.map((id) => [prefix, id].join('-'));
    const records = await redis.mgetAsync(keys);
    const output = new Map<number, EntityType>();
    ids.forEach((id, index) => {
      if (typeof records[index] === 'string') {
        output.set(id, JSON.parse(records[index]));
      } else {
        output.set(id, null);
      }
    });
    return output;
  }

  // add a record to the cache
  protected async cacheSet(
    prefix: string,
    id: number,
    data: EntityType,
    cacheLifetime = this.cacheLifetime
  ) {
    const json = JSON.stringify(data);
    const key = [prefix, id].join('-');
    redis.setex(key, cacheLifetime, json);
  }

  // empty a cache record
  protected async cacheDrop(prefix: string, id: number) {
    const key = [prefix, id].join('-');
    redis.del(key);
  }

  // return an array of indices containing nulls
  protected filterNullIndices(map: Map<number, EntityType>): Array<number> {
    return this.ids.filter((id) => map.get(id) === null);
  }

  // check if flag exists
  protected hasFlag(flag: number): Boolean {
    return this.flags.indexOf(flag) > -1;
  }

  // pass a list of entity ID's into the constructor and a set of flags
  constructor(ids: Array<number> | number, flags?: Array<number>, args?: Object) {
    this.ids = Array.isArray(ids) ? ids : [ids];
    this.ids = [...new Set(this.ids.filter((id) => id != null).map(Number))];
    this.flags = flags || [];
    this.args = args || {};
  }

  protected abstract format(data: any): any;

  protected async getRawData(): Promise<EntityType[]> {
    // grab canonical data
    const cachedData = await this.cacheGet(this.cachePrefix, this.ids);
    const uncachedIds = this.filterNullIndices(cachedData);
    let uncachedData = new Map<number, Result>();
    if (uncachedIds.length > 0) {
      uncachedData = await this.getEntities(uncachedIds);
    }
    const data = await Promise.all(
      this.ids.map((id) => {
        if (cachedData.get(id) !== null) return cachedData.get(id);
        if (typeof uncachedData.get(id) !== 'undefined') return this.format(uncachedData.get(id));
        return {};
      })
    );

    // write formatted data back to the cache
    if (uncachedIds.length > 0) {
      console.debug(this.cachePrefix, 'cache miss:', uncachedIds);

      await Promise.all(
        data.map((item) =>
          uncachedData.get(item.id)
            ? this.cacheSet(this.cachePrefix, item.id, item)
            : Promise.resolve()
        )
      );
    }

    return data;
  }

  protected async populateData(objects: EntityType[]): Promise<Map<number, EntityType>> {
    const result = new Map<number, EntityType>();
    objects.forEach((object) => {
      result.set(object.id, object);
    });
    return result;
  }

  // get record set
  async get(): Promise<Map<number, EntityType>> {
    const objects = await this.getRawData();
    return this.populateData(objects);
  }

  // invalidate record set
  async invalidate() {
    this.ids.map(async (id) => {
      this.cacheDrop(this.cachePrefix, id);
    });
  }

  getObjectArray = async () => [...(await this.get()).values()];

  getSingleObject = async () => (await this.getObjectArray())[0];
}
