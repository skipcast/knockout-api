import AbstractRetriever from './abstractRetriever';
import knex from '../services/knex';
import UserRetriever from './user';

export default class MentionRetriever extends AbstractRetriever {
  protected cacheLifetime = 604800;

  protected cachePrefix: string = 'mention';

  protected async query(ids: Array<number>) {
    return knex('Mentions')
      .select(
        'id',
        'post_id as postId',
        'content',
        'created_at as createdAt',
        'thread_id as threadId',
        'page as threadPage',
        'thread_title as threadTitle',
        'mentioned_by as mentionedBy'
      )
      .whereIn('id', ids);
  }

  protected format(data) {
    return data;
  }

  protected async populateData(mentions) {
    const userIds: number[] = Array.from(
      new Set(mentions.map((result) => Number(result.mentionedBy)))
    );

    const users = await new UserRetriever(userIds).get();

    const populatedMentions = mentions.map((mention) => ({
      ...mention,
      mentionedBy: users.get(mention.mentionedBy),
    }));

    return super.populateData(populatedMentions);
  }
}
