import SteamAPI from 'steamapi';
import { UserProfile } from 'knockout-schema';
import AbstractRetriever from './abstractRetriever';
import knex from '../services/knex';
import { STEAM_API_KEY } from '../../config/server';

export default class ProfileRetriever extends AbstractRetriever<UserProfile> {
  protected cacheLifetime = 86400;

  protected cachePrefix: string = 'profile';

  protected async query(ids: Array<number>) {
    return knex
      .from('UserProfiles')
      .select(
        'user_id as id',
        'heading_text as headingText',
        'personal_site as personalSite',
        'background_url as backgroundUrl',
        'background_type as backgroundType',
        'steam',
        'discord',
        'github',
        'youtube',
        'twitter',
        'twitch',
        'gitlab',
        'tumblr',
        'header',
        'disable_comments as disableComments',
        'created_at as createdAt',
        'updated_at as updatedAt'
      )
      .whereIn('user_id', ids);
  }

  private static async getSteamNickname(url: string) {
    try {
      const steam = new SteamAPI(STEAM_API_KEY);
      const id = await steam.resolve(url);
      const summary = await steam.getUserSummary(id);
      return summary.nickname;
    } catch (error) {
      // If we get a TypeError, that means the user provided steam URL was incorrect.
      // We don't need to take any action on this, so don't log it. Only log other exceptions
      if (error.name !== 'TypeError') {
        console.error(error);
      }
      return undefined;
    }
  }

  protected async format(data): Promise<UserProfile> {
    return {
      id: data.id,
      bio: data.headingText,
      social: {
        website: data.personalSite || undefined,
        steam: data.steam
          ? {
              name: await ProfileRetriever.getSteamNickname(data.steam),
              url: data.steam,
            }
          : undefined,
        discord: data.discord || undefined,
        github: data.github || undefined,
        youtube: data.youtube || undefined,
        twitter: data.twitter || undefined,
        twitch: data.twitch || undefined,
        gitlab: data.gitlab || undefined,
        tumblr: data.tumblr || undefined,
      },
      background: {
        url: data.backgroundUrl,
        type: data.backgroundType,
      },
      header: data.header || undefined,
      disableComments: Boolean(data.disableComments) || false,
    };
  }
}
