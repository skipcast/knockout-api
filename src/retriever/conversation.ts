import { Conversation } from 'knockout-schema';
import AbstractRetriever from './abstractRetriever';
import knex from '../services/knex';
import MessageRetriever from './message';
import UserRetriever from './user';

export enum ConversationFlag {
  RETRIEVE_SHALLOW,
}

export default class ConversationRetriever extends AbstractRetriever<Conversation> {
  protected cacheLifetime = 86400;

  protected cachePrefix: string = 'conversation';

  protected async query(ids: Array<number>) {
    return knex
      .from('Conversations')
      .select('id', 'created_at as conversationCreatedAt', 'updated_at as conversationUpdatedAt')
      .whereIn('id', ids);
  }

  protected format(data): Object {
    return {
      id: data.id,
      messages: [],
      users: [],
      createdAt: data.conversationCreatedAt,
      updatedAt: data.conversationUpdatedAt,
    };
  }

  private allMessagesQuery = () =>
    knex
      .from('Messages')
      .select('id', 'conversation_id as conversationId')
      .whereIn('conversation_id', this.ids)
      .orderBy('created_at', 'desc');

  private latestMessagesQuery = () =>
    knex
      .from('Conversations as c')
      .select('c.latest_message_id as id', 'c.id as conversationId')
      .whereIn('c.id', this.ids)
      .orderBy('updated_at', 'desc');

  private async getUsers() {
    const conversationUsers = await knex('ConversationUsers')
      .select('user_id', 'conversation_id')
      .whereIn('conversation_id', this.ids);

    const users = await new UserRetriever(conversationUsers.map((item) => item.user_id)).get();

    const convoToUsers = conversationUsers.reduce((output, convoUser) => {
      if (typeof output[convoUser.conversation_id] === 'undefined')
        output[convoUser.conversation_id] = [];
      output[convoUser.conversation_id].push(users.get(convoUser.user_id));
      return output;
    }, {});
    return convoToUsers;
  }

  private async getMessages() {
    let conversationMessages;
    if (this.hasFlag(ConversationFlag.RETRIEVE_SHALLOW)) {
      conversationMessages = await this.latestMessagesQuery();
    } else {
      conversationMessages = await this.allMessagesQuery();
    }

    const messageIds = conversationMessages.map((message) => message.id);
    const messages = await new MessageRetriever(messageIds).get();
    return conversationMessages.reduce((list, message) => {
      if (typeof list[message.conversationId] === 'undefined') list[message.conversationId] = [];
      list[message.conversationId].push(messages.get(message.id));
      return list;
    }, {});
  }

  protected async populateData(conversations) {
    // grab data from related caches
    const users = await this.getUsers();
    const messages = await this.getMessages();

    // merge related data in
    const populatedConversations = conversations.reduce((result, conversation) => {
      if (conversation) {
        conversation.messages = messages[conversation.id] || [];
        conversation.users = users[conversation.id] || [];
        result.push(conversation);
      }
      return result;
    }, []);

    return super.populateData(populatedConversations);
  }
}
