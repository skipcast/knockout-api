/* eslint-disable import/no-cycle */
import { Ban } from 'knockout-schema';
import AbstractRetriever from './abstractRetriever';
import UserRetriever from './user';
import PostRetriever, { PostFlag } from './post';
import ThreadRetriever, { ThreadFlag } from './thread';
import knex from '../services/knex';

export enum BanFlag {
  INCLUDE_POST,
  INCLUDE_THREAD,
}

export default class BanRetriever extends AbstractRetriever<Ban> {
  protected cachePrefix: string = 'ban';

  protected async query(ids: Array<number>) {
    return knex
      .from('Bans')
      .select(
        'id',
        'user_id as banUserId',
        'expires_at as banExpiresAt',
        'created_at as banCreatedAt',
        'updated_at as banUpdatedAt',
        'ban_reason as banReason',
        'banned_by as banBannedBy',
        'post_id as banPostId'
      )
      .whereIn('id', ids);
  }

  private static async getUsers(bans: Array<any>) {
    const userIds = bans.reduce((list, ban) => {
      list.push(ban.bannedBy);
      list.push(ban.user);
      return list;
    }, []);

    return new UserRetriever(userIds, []).get();
  }

  private async getPosts(bans: Array<any>) {
    if (!this.hasFlag(BanFlag.INCLUDE_POST) && !this.hasFlag(BanFlag.INCLUDE_THREAD))
      return new Map();
    const postIds = bans.map((ban) => ban.post);

    return new PostRetriever(postIds, [PostFlag.RETRIEVE_SHALLOW]).get();
  }

  private async getThreads(threadIds: Array<any>) {
    if (!this.hasFlag(BanFlag.INCLUDE_THREAD)) return new Map();
    return new ThreadRetriever(threadIds, [ThreadFlag.RETRIEVE_SHALLOW]).get();
  }

  protected format(data): Object {
    return {
      id: data.id,
      banReason: data.banReason,
      expiresAt: data.banExpiresAt,
      createdAt: data.banCreatedAt,
      updatedAt: data.banUpdatedAt,
      post: data.banPostId,
      user: data.banUserId,
      bannedBy: data.banBannedBy,
    };
  }

  protected async populateData(bans) {
    // grab data from related caches
    const users = await BanRetriever.getUsers(bans);
    const posts = await this.getPosts(bans);
    const threads = await this.getThreads([...posts.values()].map((post) => post.thread));

    // merge related data in
    const populatedBans = bans.map((ban) => ({
      ...ban,
      user: users.get(ban.user),
      bannedBy: users.get(ban.bannedBy),
      post: posts.get(ban.post) || ban.post,
      thread: threads.get(posts.get(ban.post)?.thread) || ban.thread,
    }));

    return super.populateData(populatedBans);
  }
}
