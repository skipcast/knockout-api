import { UserProfileComment } from 'knockout-schema';
import AbstractRetriever from './abstractRetriever';
import knex from '../services/knex';
import UserRetriever from './user';

export default class ProfileCommentRetriever extends AbstractRetriever<UserProfileComment> {
  protected cacheLifetime = 604800;

  protected cachePrefix: string = 'profileComment';

  protected async query(ids: Array<number>) {
    return knex
      .from('ProfileComments')
      .select(
        'id',
        'user_profile as userProfile',
        'author',
        'content',
        'deleted',
        'created_at as createdAt',
        'updated_at as updatedAt'
      )
      .whereIn('id', ids);
  }

  protected format(data) {
    return {
      ...data,
      deleted: Boolean(data.deleted),
    };
  }

  protected async populateData(comments) {
    const users = await new UserRetriever(comments.map((comment) => comment.author)).get();

    const populatedComments = comments.map((comment) => ({
      ...comment,
      author: users.get(comment.author) || comment.author,
    }));

    return super.populateData(populatedComments);
  }
}
