import { Rule } from 'knockout-schema';
import AbstractRetriever from './abstractRetriever';
import knex from '../services/knex';

export default class RuleRetriever extends AbstractRetriever<Rule> {
  protected cacheLifetime = 86400;

  protected cachePrefix: string = 'rule';

  protected async query(ids: Array<number>) {
    return knex
      .from('Rules')
      .select(
        'id',
        'rulable_type as ruleRulableType',
        'rulable_id as ruleRulableId',
        'category as ruleCategory',
        'title as ruleTitle',
        'cardinality as ruleCardinality',
        'description as ruleDescription',
        'created_at as ruleCreatedAt',
        'updated_at as ruleUpdatedAt'
      )
      .whereIn('id', ids);
  }

  protected format(data): Rule {
    return {
      id: data.id,
      rulableId: data.ruleRulableId,
      rulableType: data.ruleRulableType,
      category: data.ruleCategory,
      title: data.ruleTitle,
      cardinality: data.ruleCardinality,
      description: data.ruleDescription,
      createdAt: data.ruleCreatedAt,
      updatedAt: data.ruleUpdatedAt,
    };
  }
}
