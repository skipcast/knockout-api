import { Alert } from 'knockout-schema';
import AbstractRetriever from './abstractRetriever';
import ThreadRetriever, { ThreadFlag } from './thread';
import knex from '../services/knex';

export enum AlertFlag {
  HIDE_NSFW,
}

export default class AlertRetriever extends AbstractRetriever<Alert> {
  protected cachePrefix: string = 'alert';

  private user: number;

  constructor(ids: Array<number>, user: number, flags?: Array<number>, args?: Object) {
    super(ids, flags, args);
    this.user = user;
  }

  protected async query(ids: Array<number>) {
    return knex
      .from('Alerts as al')
      .select(
        'al.thread_id as thread_id',
        'al.last_post_number as last_post_number',
        'al.last_seen as last_seen',
        'Posts.id as firstUnreadId'
      )
      .leftJoin('Posts', function () {
        this.on('Posts.thread_id', '=', 'al.thread_id').andOn(
          'Posts.thread_post_number',
          '=',
          knex.raw('al.last_post_number + 1')
        );
      })
      .where('al.user_id', this.user)
      .whereIn('al.thread_id', ids);
  }

  private async getThreads(alerts: Array<any>) {
    const threads = alerts.map((alert) => alert.thread_id);
    const threadFlags = [
      ThreadFlag.RETRIEVE_SHALLOW,
      ThreadFlag.EXCLUDE_READ_THREADS,
      ThreadFlag.INCLUDE_LAST_POST,
      ThreadFlag.INCLUDE_POST_COUNT,
      ThreadFlag.INCLUDE_USER,
    ];

    if (this.hasFlag(AlertFlag.HIDE_NSFW)) {
      threadFlags.push(ThreadFlag.INCLUDE_TAGS);
    }

    return new ThreadRetriever(threads, threadFlags).get();
  }

  private static async getUnreadCount(data) {
    if (data.last_post_number) {
      return data.thread.postCount - data.last_post_number;
    }
    const result = await knex('Posts')
      .count('id as unreadPosts')
      .first()
      .where('thread_id', data.thread_id)
      .where('created_at', '>', data.last_seen);

    return result.unreadPosts;
  }

  private static async getFirstUnreadId(data) {
    // if we already fetched our first unread ID,
    // OR if the thread has no unread posts (post count is equal to last post number),
    // just return the first unread ID
    if (data.firstUnreadId || data.thread.postCount === data.last_post_number) {
      return data.firstUnreadId || -1;
    }

    const result = await knex('Posts')
      .select('id as firstUnreadId')
      .first()
      .where('thread_id', data.thread_id)
      .where('created_at', '>', data.last_seen)
      .orderBy('created_at')
      .limit(1);

    if (!result) {
      return -1;
    }

    return result.firstUnreadId;
  }

  protected async format(data) {
    return {
      id: data.thread_id,
      thread: data.thread,
      unreadPosts: Number(await AlertRetriever.getUnreadCount(data)),
      firstUnreadId: await AlertRetriever.getFirstUnreadId(data),
    };
  }

  protected async getRawData() {
    const alerts = await this.query(this.ids);
    const threads = await this.getThreads(alerts);

    // merge related data in
    alerts.forEach((alert) => {
      alert.thread = threads.get(alert.thread_id);
    });

    const filteredAlerts = alerts.filter((alert) => alert.thread !== undefined);

    return Promise.all(filteredAlerts.map((alert) => this.format(alert)));
  }

  protected async populateData(alerts: Alert[]) {
    const result = new Map();

    alerts.forEach((alert) => {
      result.set(alert.id, alert);
    });
    return result;
  }
}
