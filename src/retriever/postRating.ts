import { Rating, User } from 'knockout-schema';
import AbstractRetriever from './abstractRetriever';
import knex from '../services/knex';
import UserRetriever from './user';

const ratingList = require('../helpers/ratingList.json');

export enum PostRatingFlag {
  USE_USERNAMES,
  EXCLUDE_USERS,
}

export default class PostRatingRetriever extends AbstractRetriever {
  protected cachePrefix: string = 'post-rating';

  protected async query(ids: Array<number>) {
    const results = await knex
      .from('Ratings as r')
      .select('r.rating_id as ratingId', 'r.post_id as postId', 'r.user_id as userId')
      .whereIn('r.post_id', ids)
      .orderBy('r.rating_id', 'asc')
      .orderBy('r.updated_at', 'desc');

    const postsWithRatings = results.reduce((list, rating) => {
      if (typeof list[rating.postId] === 'undefined') list[rating.postId] = [];
      list[rating.postId].push(rating);
      return list;
    }, {});

    ids.forEach((id) => {
      if (typeof postsWithRatings[id] === 'undefined') {
        postsWithRatings[id] = [];
      }
    });

    return postsWithRatings;
  }

  private async getUsers(userIds: number[]): Promise<Map<number, User>> {
    if (this.hasFlag(PostRatingFlag.EXCLUDE_USERS)) return new Map();
    return new UserRetriever(userIds).get();
  }

  protected async format(datum): Promise<Rating[]> {
    const byRating = datum.reduce((list, rating) => {
      if (typeof list[rating.ratingId] === 'undefined') list[rating.ratingId] = [];
      list[rating.ratingId].push(rating);
      return list;
    }, {});

    return Object.keys(byRating)
      .map((ratingId) => {
        const data = byRating[ratingId];
        return {
          id: data[0].postId,
          rating: ratingList[ratingId].short,
          ratingId: Number(ratingId),
          users: data.map((rating) => rating.userId),
          count: data.length,
        };
      })
      .sort((ratingA, ratingB) => {
        if (ratingA.count > ratingB.count) return -1;
        if (ratingA.count < ratingB.count) return 1;
        return 0;
      });
  }

  protected async getRawData(): Promise<Rating[][]> {
    // grab canonical data
    const cachedRatings = await this.cacheGet(this.cachePrefix, this.ids);
    const uncachedIds = this.filterNullIndices(cachedRatings);
    const uncachedRatings = await this.query(uncachedIds);
    const ratings: Rating[][] = (
      await Promise.all(
        this.ids.map(async (id) => {
          if (cachedRatings.get(id) !== null) return cachedRatings.get(id);
          if (typeof uncachedRatings[id] !== 'undefined') return this.format(uncachedRatings[id]);
          return null;
        })
      )
    ).filter((rating) => rating != null);

    // write formatted data back to the cache
    if (uncachedIds.length > 0) {
      ratings.map(async (rating) => {
        if (rating[0]) {
          await this.cacheSet(this.cachePrefix, rating[0].id, rating);
        }
      });
    }
    return ratings;
  }

  protected async populateData(ratings): Promise<Map<number, Rating[][]>> {
    const userIds: Set<number> = new Set();
    ratings.forEach((ratingItem) => {
      ratingItem.forEach((ratingType) => {
        ratingType.users.forEach((user) => userIds.add(Number(user)));
      });
    });

    const users = await this.getUsers(Array.from(userIds));

    const result = new Map<number, Rating[][]>();
    const populatedRatings = ratings.map((post) =>
      post.map((rating) => ({
        ...rating,
        users: this.hasFlag(PostRatingFlag.EXCLUDE_USERS)
          ? []
          : rating.users.map((userId) =>
              this.hasFlag(PostRatingFlag.USE_USERNAMES)
                ? users.get(userId).username
                : users.get(userId)
            ),
      }))
    );

    populatedRatings.forEach((ratingArray) => {
      if (ratingArray[0]) {
        result.set(ratingArray[0].id, ratingArray);
      }
    });
    return result;
  }
}
