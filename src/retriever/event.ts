import { Event, EventType } from 'knockout-schema';
import AbstractRetriever from './abstractRetriever';
import knex from '../services/knex';
import UserRetriever from './user';
import ThreadRetriever, { ThreadFlag } from './thread';
import BanRetriever, { BanFlag } from './ban';
import PostRetriever, { PostFlag } from './post';
import { getUserPermissionCodes, userCanViewSubforum } from '../helpers/user';

const THREAD_EVENT_TYPES = [
  EventType.THREAD_CREATED,
  EventType.THREAD_BACKGROUND_UPDATED,
  EventType.THREAD_DELETED,
  EventType.THREAD_MOVED,
  EventType.THREAD_PINNED,
  EventType.THREAD_UNPINNED,
  EventType.THREAD_LOCKED,
  EventType.THREAD_UNLOCKED,
  EventType.THREAD_RENAMED,
  EventType.THREAD_RESTORED,
  EventType.THREAD_POST_LIMIT_REACHED,
];

const BAN_EVENT_TYPES = [EventType.USER_BANNED];

export default class EventRetriever extends AbstractRetriever<Event> {
  protected cacheLifetime = 604800;

  protected cachePrefix: string = 'event';

  protected async query(ids: Array<number>) {
    return knex
      .from('Events')
      .select(
        'id',
        'executed_by as creator',
        'type',
        'data_id as dataId',
        'content',
        'created_at as createdAt'
      )
      .whereIn('id', ids);
  }

  protected format(data): Event {
    const { dataId, ...event } = data;
    return {
      ...event,
      content: JSON.parse(event.content),
      data: { id: dataId },
    };
  }

  static formatRawEvent(data) {
    return new EventRetriever([]).format(data);
  }

  static async getDataObjects(events: Event[]) {
    const objects = {};
    await Promise.all(
      events.map((event) =>
        (async () => {
          let object;
          switch (event.type) {
            case EventType.THREAD_CREATED:
            case EventType.THREAD_BACKGROUND_UPDATED:
            case EventType.THREAD_DELETED:
            case EventType.THREAD_MOVED:
            case EventType.THREAD_PINNED:
            case EventType.THREAD_UNPINNED:
            case EventType.THREAD_LOCKED:
            case EventType.THREAD_UNLOCKED:
            case EventType.THREAD_RENAMED:
            case EventType.THREAD_RESTORED:
            case EventType.THREAD_POST_LIMIT_REACHED:
              object = await new ThreadRetriever((event.data as any).id, [
                ThreadFlag.INCLUDE_SUBFORUM,
              ]).getSingleObject();
              break;
            case EventType.USER_AVATAR_REMOVED:
            case EventType.USER_BACKGROUND_REMOVED:
            case EventType.USER_UNBANNED:
            case EventType.USER_PROFILE_REMOVED:
            case EventType.PROFILE_COMMENT_CREATED:
            case EventType.GOLD_EARNED:
            case EventType.GOLD_LOST:
              object = await new UserRetriever((event.data as any).id).getSingleObject();
              break;
            case EventType.USER_BANNED:
            case EventType.USER_WIPED:
              object = await new BanRetriever((event.data as any).id, [
                BanFlag.INCLUDE_THREAD,
              ]).getSingleObject();
              break;
            case EventType.POST_CREATED:
            case EventType.RATING_CREATED:
              object = await new PostRetriever((event.data as any).id, [
                PostFlag.RETRIEVE_SHALLOW,
                PostFlag.INCLUDE_THREAD,
              ]).getSingleObject();
              break;
            default:
              break;
          }
          objects[`${event.type}-${(event.data as any).id}`] = object;
        })()
      )
    );
    return objects;
  }

  protected async populateData(events) {
    const dataObjects = await EventRetriever.getDataObjects(events);
    const users = await new UserRetriever(events.map((event) => event.creator)).get();
    const userPermissionCodes = Object.prototype.hasOwnProperty.call(this.args, 'userId')
      ? await getUserPermissionCodes(this.args['userId'])
      : [];

    const populatedEvents: Event[] = events.reduce((list, event) => {
      const data = dataObjects[`${event.type}-${event.data.id}`];
      let subforumId: number;

      if (THREAD_EVENT_TYPES.includes(event.type)) {
        subforumId = data.subforumId;
      } else if (BAN_EVENT_TYPES.includes(event.type)) {
        subforumId = data.thread?.subforumId;
      }

      // if the event has a subforumId and we're validating against a user,
      // make sure the user can see the event
      if (
        Object.prototype.hasOwnProperty.call(this.args, 'userId') &&
        subforumId &&
        !userCanViewSubforum(userPermissionCodes, subforumId)
      ) {
        return list;
      }

      return [
        ...list,
        {
          ...event,
          data: dataObjects[`${event.type}-${event.data.id}`],
          creator: users.get(event.creator),
        },
      ];
    }, []);

    return super.populateData(populatedEvents);
  }
}
