import AbstractRetriever from './abstractRetriever';
import knex from '../services/knex';
import UserRetriever from './user';

export default class MessageRetriever extends AbstractRetriever {
  protected cacheLifetime = 86400;

  protected cachePrefix: string = 'message';

  protected query(ids: Array<number>) {
    return knex
      .from('Messages')
      .select(
        'id',
        'conversation_id as messageConversationId',
        'user_id as messageUserId',
        'content as messageContent',
        'read_at as messageReadAt',
        'created_at as messageCreatedAt',
        'updated_at as messageUpdatedAt'
      )
      .whereIn('id', ids);
  }

  protected format(data): Object {
    return {
      id: data.id,
      conversationId: data.messageConversationId,
      user: { id: data.messageUserId },
      content: data.messageContent,
      readAt: data.messageReadAt,
      createdAt: data.messageCreatedAt,
      updatedAt: data.messageUpdatedAt,
    };
  }

  protected async populateData(messages) {
    // grab data from related caches
    const userIds = messages.map((message) => message.user.id);
    const users = await new UserRetriever(userIds).get();

    const result = new Map();
    messages.forEach((message) => {
      result.set(message.id, {
        ...message,
        user: users.get(message.user.id),
      });
    });
    return result;
  }
}
