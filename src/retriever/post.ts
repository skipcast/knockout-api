/* eslint-disable import/no-cycle */
import { Post } from 'knockout-schema';
import AbstractRetriever from './abstractRetriever';
import UserRetriever from './user';
import ThreadRetriever, { ThreadFlag } from './thread';
import PostRatingRetriever, { PostRatingFlag } from './postRating';
import BanRetriever from './ban';
import knex from '../services/knex';
import { postsPerPage } from '../constants/pagination';
import { getCountryCode } from '../helpers/geoipLookup';
import { getUserPermissionCodes, userCanViewThread } from '../helpers/user';

export enum PostFlag {
  INCLUDE_THREAD,
  RETRIEVE_SHALLOW,
  HIDE_DELETED,
  HIDE_NSFW,
  RETRIEVE_RATING_USERNAMES,
  INCLUDE_USER,
  TRUNCATE_CONTENT,
}

export default class PostRetriever extends AbstractRetriever<Post> {
  protected cachePrefix: string = 'post';

  protected async query(ids: Array<number>) {
    return knex
      .from('Posts')
      .select(
        'id',
        'content as postContent',
        'created_at as postCreatedAt',
        'updated_at as postUpdatedAt',
        'user_id as postUserId',
        'thread_id as postThreadId',
        'thread_post_number as postThreadPostNumber',
        'country_name as postCountryName',
        'app_name as postAppName'
      )
      .whereIn('id', ids);
  }

  private async getRatings(posts: Array<Post>) {
    if (this.hasFlag(PostFlag.RETRIEVE_SHALLOW)) return new Map();
    const postIds = posts.map((post) => post.id);
    const flags = this.hasFlag(PostFlag.RETRIEVE_RATING_USERNAMES)
      ? [PostRatingFlag.USE_USERNAMES]
      : [];
    return new PostRatingRetriever(postIds, flags).get();
  }

  private async getThreads(posts: Array<Post>) {
    if (!this.hasFlag(PostFlag.INCLUDE_THREAD)) return new Map();
    const threadIds = posts
      .map((post) => post.thread)
      .filter((elem, index, self) => index === self.indexOf(elem));
    const flags = [ThreadFlag.INCLUDE_SUBFORUM, ThreadFlag.RETRIEVE_SHALLOW];

    if (this.hasFlag(PostFlag.HIDE_NSFW)) {
      flags.push(ThreadFlag.INCLUDE_TAGS);
    }

    return new ThreadRetriever(threadIds, flags).get();
  }

  private async getBans(posts: Array<Post>) {
    if (this.hasFlag(PostFlag.RETRIEVE_SHALLOW)) return {};
    const postIds = posts.map((post) => post.id);
    const postBans = await knex.from('Bans').select('id', 'post_id').whereIn('post_id', postIds);
    const banIds = postBans.map((ban) => ban.id);
    const bans = await new BanRetriever(banIds, []).get();
    return postBans.reduce((list, ban) => {
      if (typeof list[ban.post_id] === 'undefined') list[ban.post_id] = [];
      list[ban.post_id].push(bans.get(ban.id));
      return list;
    }, {});
  }

  protected format(data): Post {
    return {
      id: data.id,
      thread: data.postThreadId,
      page: Math.ceil(data.postThreadPostNumber / postsPerPage),
      content: data.postContent,
      createdAt: data.postCreatedAt,
      updatedAt: data.postUpdatedAt,
      user: data.postUserId,
      ratings: [],
      bans: [],
      threadPostNumber: data.postThreadPostNumber,
      countryName: data.postCountryName,
      countryCode: getCountryCode(data.postCountryName),
      appName: data.postAppName,
    };
  }

  protected async populateData(posts) {
    // grab data from related caches
    const users =
      this.hasFlag(PostFlag.RETRIEVE_SHALLOW) && !this.hasFlag(PostFlag.INCLUDE_USER)
        ? new Map()
        : await new UserRetriever(posts.map((post) => post.user)).get();
    const ratings = await this.getRatings(posts);
    const bans = await this.getBans(posts);
    const threads = await this.getThreads(posts);
    const userPermissionCodes = Object.prototype.hasOwnProperty.call(this.args, 'userId')
      ? await getUserPermissionCodes(this.args['userId'])
      : [];

    // merge related data in
    const postList = posts.reduce((list, post) => {
      const postThread = threads.get(post.thread);
      if (this.hasFlag(PostFlag.HIDE_NSFW) && postThread === undefined) {
        return list;
      }

      // user must be able to see thread to see the post
      // only applies when we have the INCLUDE_THREAD flag
      if (
        this.hasFlag(PostFlag.INCLUDE_THREAD) &&
        Object.prototype.hasOwnProperty.call(this.args, 'userId') &&
        !userCanViewThread(this.args['userId'], userPermissionCodes, postThread)
      ) {
        return list;
      }

      const MILLISECONDS_UNTIL_RATINGS_ARE_SHOWN = 300000;
      const postIsOldEnoughForRatings =
        new Date().getTime() - new Date(post.createdAt).getTime() >
        MILLISECONDS_UNTIL_RATINGS_ARE_SHOWN;

      const postContent = !this.hasFlag(PostFlag.TRUNCATE_CONTENT) ? post.content : null;

      return [
        ...list,
        {
          ...post,
          user: users.get(post.user) || post.user,
          ratings: (postIsOldEnoughForRatings && ratings.get(post.id)) || [],
          bans: bans[post.id] || [],
          thread: postThread || post.thread,
          content: postContent,
        },
      ];
    }, []);

    return super.populateData(postList);
  }
}
