import { User } from 'knockout-schema';
import AbstractRetriever from './abstractRetriever';
import knex from '../services/knex';
import RoleRetriever from './role';

export enum UserFlag {
  HIDE_BANNED,
  INCLUDE_STRIPE_CUSTOMER_ID,
  INCLUDE_DONATION_UPGRADE_EXPIRATION,
}

export default class UserRetriever extends AbstractRetriever<User> {
  protected cacheLifetime = 7200;

  protected cachePrefix: string = 'user';

  protected async query(ids: Array<number>) {
    return knex
      .from('Users as u')
      .select(
        'u.id as id',
        'u.username as userUsername',
        'u.usergroup as userUsergroup',
        'u.avatar_url as userAvatarUrl',
        'u.background_url as userBackgroundUrl',
        'u.created_at as userCreatedAt',
        'u.updated_at as userUpdatedAt',
        'u.role_id as userRoleId',
        'u.stripe_customer_id as userStripeCustomerId',
        'u.title as userTitle',
        'u.donation_upgrade_expires_at as userDonationUpgradeExpiresAt',
        knex.raw(
          '(select count(*) from Threads as t where t.user_id = u.id and deleted_at is null) as userThreadCount'
        ),
        knex.raw('(select count(*) from Posts as p where p.user_id = u.id) as userPostCount'),
        knex.raw(
          '(select 1 from Bans as b where b.user_id = u.id and expires_at > now() LIMIT 1) as isBanned'
        )
      )
      .whereIn('u.id', ids);
  }

  protected format(data): Object {
    return {
      id: data.id,
      role: data.userRoleId,
      username: data.userUsername,
      usergroup: data.userUsergroup,
      avatarUrl: data.userAvatarUrl,
      backgroundUrl: data.userBackgroundUrl,
      posts: data.userPostCount,
      threads: data.userThreadCount,
      createdAt: data.userCreatedAt,
      updatedAt: data.userUpdatedAt,
      banned: !!data.isBanned,
      isBanned: !!data.isBanned, // deprecate
      stripeCustomerId: data.userStripeCustomerId,
      title: data.userTitle,
      donationUpgradeExpiresAt: data.userDonationUpgradeExpiresAt,
    };
  }

  async populateData(users) {
    const data = users.filter((user) => !this.hasFlag(UserFlag.HIDE_BANNED) || !user.banned);

    const roles = await new RoleRetriever(data.map((user) => user.role)).get();

    const populatedUsers = data.map((user) => ({
      ...user,
      role: roles.get(user.role) || user.role,
      stripeCustomerId: this.hasFlag(UserFlag.INCLUDE_STRIPE_CUSTOMER_ID)
        ? user.stripeCustomerId
        : undefined,
      donationUpgradeExpiresAt: this.hasFlag(UserFlag.INCLUDE_DONATION_UPGRADE_EXPIRATION)
        ? user.donationUpgradeExpiresAt
        : undefined,
    }));
    return super.populateData(populatedUsers);
  }
}
