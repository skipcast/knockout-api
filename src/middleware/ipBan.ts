import { NextFunction, Request, Response } from 'express';
import ipaddr from 'ipaddr.js';
import httpStatus from 'http-status';
import knex from '../services/knex';

const exactMatch = async (ip: string) => {
  const results = await knex.from('IpBans').pluck('id').where('address', ip);
  return typeof results[0] !== 'undefined';
};

const rangeMatch = async (ip: string) => {
  const results = await knex.from('IpBans').pluck('range').whereNotNull('range');
  let address;

  try {
    address = ipaddr.parse(ip);
  } catch (e) {
    console.error(`[ip-ban] Failed to match IP ${ip} against range: ${e.message}`);
    return false;
  }

  const matches = results
    .map((range) => {
      try {
        return address.match(ipaddr.parseCIDR(range));
      } catch (e) {
        return null;
      }
    })
    .filter((match) => match);
  return matches.length > 0;
};

const parseAddress = (ip: string) => {
  try {
    const address = ipaddr.IPv6.parse(ip);
    const mapped = address.isIPv4MappedAddress();
    return mapped ? address.toIPv4Address().toString() : address.toString();
  } catch (e) {
    return ip;
  }
};

export default async (req: Request, res: Response, next: NextFunction) => {
  if (typeof req.ipInfo !== 'undefined') {
    const ip = parseAddress(req.ipInfo);
    if ((await exactMatch(ip)) || (await rangeMatch(ip))) {
      res.status(httpStatus.FORBIDDEN);
      res.json({ message: 'You are banned, go away' });
      return;
    }
  }

  next();
};
