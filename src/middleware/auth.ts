import { Handler, NextFunction, Request, Response } from 'express';
import httpStatus from 'http-status';
import jwt from 'jsonwebtoken';
import ms from 'ms';
import cookie from 'cookie';
import { JWT_SECRET as jwtSecret } from '../../config/server';
import { generateToken, updateToken } from '../controllers/auth/common';

import { unBanUser } from '../helpers/user';
import { RoleCode } from '../helpers/permissions';
import UserRetriever from '../retriever/user';

function unauthorized(req: Request, res: Response, message?: string) {
  res
    .status(httpStatus.UNAUTHORIZED)
    .send({ message: message || 'Invalid credentials. Please log out and try again.' });
}

function authenticationWithParams({ optional, socket }: { optional: boolean; socket?: boolean }) {
  const authentication = async (req: Request, res: Response, next: NextFunction) => {
    const cookies = socket ? cookie.parse((req.headers.cookie as string) || '') : req.cookies;
    const knockoutJwt = cookies ? cookies.knockoutJwt : undefined;

    let token: { id: number; iat: number; exp: number };

    if (socket) {
      req.session = cookies['connect.sid'];
    }

    if (!knockoutJwt) {
      req.isLoggedIn = false;
      if (!socket) res.clearCookie('knockoutJwt');
      if (optional) {
        return next();
      }
      return unauthorized(req, res, 'Missing credentials.');
    }

    try {
      // @ts-ignore
      token = jwt.verify(knockoutJwt, jwtSecret, { algorithms: ['HS256'] });
    } catch (error) {
      console.error(`Token validation error: ${error}`);
      req.isLoggedIn = false;
      if (socket) {
        return next();
      }
      res.clearCookie('knockoutJwt');
      return unauthorized(req, res);
    }

    try {
      // Load the logged in user
      const user = await new UserRetriever(token.id).getSingleObject();

      // if the user has the banned-user role but their current active ban count is 0, unban them
      if (user.role.code === RoleCode.BANNED_USER && !user.banned) {
        await unBanUser(user.id);
      }

      // Add user info to req.user for later use
      req.user = {
        id: user.id,
        username: user.username,
        usergroup: user.usergroup,
        role_id: user.role.id,
        avatar_url: user.avatarUrl,
        background_url: user.backgroundUrl,
        isBanned: Boolean(user.banned),
        title: user.title,
        createdAt: new Date(user.createdAt),
      };

      // Refresh the token if it's more than one day old
      if (token.exp && Date.now() > token.iat * 1000 + ms('1 day')) {
        const newToken = generateToken(user);
        if (!socket) updateToken(res, newToken);
      }

      // Continue onto next route handler
      req.isLoggedIn = true;
      return next();
    } catch (error) {
      console.error(`User validation error: ${error}`);
      req.isLoggedIn = false;
      if (socket) return next();
      res.clearCookie('knockoutJwt');
      return unauthorized(req, res);
    }
  };
  return authentication;
}

interface AuthMiddleware {
  (req: Request, res: Response, next: NextFunction): Promise<void>;
  required: Handler;
  optional: Handler;
  socket: Handler;
}

function createAuthMiddleware(): AuthMiddleware {
  const auth = authenticationWithParams({ optional: false });
  // TypeScript *really* doesn't like the fact that we have to assign
  // properties to a function after it's already assigned to a variable,
  // so we bypass it with Object.assign, which is not type-checked
  Object.assign(auth, {
    required: authenticationWithParams({ optional: false }),
    optional: authenticationWithParams({ optional: true }),
    socket: authenticationWithParams({ optional: true, socket: true }),
  });
  return auth as AuthMiddleware;
}

// eslint-disable-next-line import/prefer-default-export
export const authentication = createAuthMiddleware();
