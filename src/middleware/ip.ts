/* eslint-disable prefer-destructuring */

import { NextFunction, Request, Response } from 'express';
import redis from '../services/redisClient';

const datasource = require('../models/datasource');

const { IpAddress } = datasource().models;

const USER_IP_LOG_TTL_SECONDS = 1800;

export default (req: Request, res: Response, next: NextFunction) => {
  let xForwardedFor = req.headers['x-forwarded-for'] || '';
  // Un-arrayify the parameter
  if (Array.isArray(xForwardedFor)) {
    xForwardedFor = xForwardedFor[0];
  }
  // Remove the port
  xForwardedFor = xForwardedFor.replace(/:\d+$/, '');
  // Grab the start of the chain
  const xForwardedParts = xForwardedFor.split(',')[0];
  xForwardedFor = xForwardedParts.length > 0 ? xForwardedParts.trim() : xForwardedFor;
  // Get either the proxied address or the real remote address
  const ip = xForwardedFor || req.connection.remoteAddress;
  req.ipInfo = ip;
  next();
};

export const logIpInfo = async (req: Request, res: Response, next: NextFunction) => {
  // log the IP of the user if we have an ip
  if (req.ipInfo && req.user?.id) {
    // if we have a cache entry for the ip log already, bail out
    // the cache entry should have a TTL so that we automatically
    // fetch a new ip if the old one is too old
    const cacheKey = `user-${req.user.id}-ip-log`;
    const cache = await redis.getAsync(cacheKey);
    if (cache === null) {
      redis.setex(cacheKey, USER_IP_LOG_TTL_SECONDS, '1');
      IpAddress.create({
        ip_address: req.ipInfo,
        user_id: req.user.id,
      });
    }
  }
  next();
};
