module.exports = (sequelize, DataTypes) => {
  const CalendarEvent = sequelize.define(
    'CalendarEvent',
    {
      createdBy: {
        type: DataTypes.INTEGER,
        field: 'created_by',
        references: {
          model: 'Users',
          key: 'id',
        },
      },
      title: DataTypes.STRING,
      description: DataTypes.TEXT,
      threadId: {
        field: 'thread_id',
        allowNull: false,
        type: DataTypes.INTEGER.UNSIGNED,
        references: {
          model: 'Threads',
          key: 'id',
        },
      },
      startsAt: { type: DataTypes.DATE, field: 'starts_at' },
      endsAt: { type: DataTypes.DATE, field: 'ends_at' },
      createdAt: { type: DataTypes.DATE, field: 'created_at' },
      updatedAt: { type: DataTypes.DATE, field: 'updated_at' },
    },
    {
      timestamps: true,
    }
  );
  CalendarEvent.associate = function (models) {
    CalendarEvent.User = CalendarEvent.belongsTo(models.User, {
      as: 'user',
      foreignKey: 'created_by',
    });
    CalendarEvent.Thread = CalendarEvent.belongsTo(models.Thread, {
      as: 'thread',
      foreignKey: 'threadId',
    });
  };
  CalendarEvent.initScopes = () => {};
  return CalendarEvent;
};
