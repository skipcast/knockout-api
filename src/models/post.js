const { Op } = require('sequelize');
const { EventType, THREAD_POST_LIMIT } = require('knockout-schema');
const { createEvent } = require('../controllers/eventLogController');
const { getRoleIdsWithPermissions } = require('../helpers/permissions');
const { default: nsfwTagName } = require('../constants/nsfwTagName');

module.exports = (sequelize, DataTypes) => {
  const Post = sequelize.define(
    'Post',
    {
      content: DataTypes.STRING,
      user_id: DataTypes.BIGINT,
      thread_id: DataTypes.BIGINT,
      createdAt: { type: DataTypes.DATE, field: 'created_at' },
      updatedAt: { type: DataTypes.DATE, field: 'updated_at' },
      country_name: DataTypes.STRING,
      app_name: DataTypes.STRING,
      thread_post_number: DataTypes.INTEGER,
    },
    {
      scopes: {
        byUser(userId, userPermissionCodes, hideNsfw) {
          const where = {
            user_id: userId,
          };
          if (hideNsfw) {
            where['$Thread.Tags.name$'] = {
              [Op.or]: [
                null,
                {
                  [Op.ne]: nsfwTagName,
                },
              ],
            };
          }
          return {
            where,
            subquery: false,
            include: [
              {
                model: sequelize.models.Thread.scope({
                  method: ['byUser', userId, userPermissionCodes],
                }),
                attributes: ['id'],
                required: true,
                include: {
                  attributes: ['id'],
                  model: sequelize.models.Tag,
                  as: 'Tags',
                  required: false,
                },
              },
            ],
          };
        },
      },
      hooks: {
        async afterCreate(post, options) {
          try {
            // increment thread post number
            const threadPostCount = await Post.count({
              where: { thread_id: post.thread_id },
              transaction: options.transaction,
            });
            post.thread_post_number = threadPostCount;
            await post.save();
            await post.reload();

            const thread = await sequelize.models.Thread.findOne({
              attributes: ['id', 'subforum_id', 'locked'],
              where: { id: post.thread_id },
            });

            // if the thread is at or above the post limit and needs to be locked, lock it
            const threadReachedPostLimit =
              post.thread_post_number >= THREAD_POST_LIMIT && !thread.locked;

            if (threadReachedPostLimit) {
              const roleIds = await getRoleIdsWithPermissions([
                `subforum-${thread.subforum_id}-view`,
              ]);

              createEvent(
                post.user_id,
                EventType.THREAD_POST_LIMIT_REACHED,
                post.thread_id,
                undefined,
                roleIds
              );

              thread.locked = true;
            }

            thread.updatedAt = new Date();
            thread.changed('updatedAt', true);

            await thread.save();
          } catch (err) {
            console.error(err);
          }
        },
      },
      timestamps: true,
    }
  );
  Post.associate = function (models) {
    // A post belongs to a user
    Post.User = Post.belongsTo(models.User, { as: 'user', foreignKey: 'user_id' });
    // A post can belong to a thread
    Post.Thread = Post.belongsTo(models.Thread, { foreignKey: 'thread_id' });
  };
  Post.initScopes = () => {};
  return Post;
};
