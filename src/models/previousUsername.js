module.exports = (sequelize, DataTypes) => {
  const PreviousUsername = sequelize.define(
    'PreviousUsername',
    {
      user_id: {
        type: DataTypes.INTEGER,
        references: {
          model: 'Users',
          key: 'id',
        },
      },
      username: {
        type: DataTypes.STRING,
      },
      createdAt: { type: DataTypes.DATE, field: 'created_at' },
    },
    {
      timestamps: true,
      updatedAt: false,
    }
  );
  PreviousUsername.associate = (models) => {
    PreviousUsername.User = PreviousUsername.belongsTo(models.User, {
      as: 'user',
      foreignKey: 'user_id',
    });
  };
  PreviousUsername.initScopes = () => {};
  return PreviousUsername;
};
