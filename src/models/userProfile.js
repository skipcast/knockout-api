module.exports = (sequelize, DataTypes) => {
  const UserProfile = sequelize.define(
    'UserProfile',
    {
      user_id: {
        primaryKey: true,
        allowNull: false,
        unique: true,
        type: DataTypes.INTEGER,
        references: {
          model: 'Users',
          key: 'id',
        },
      },
      heading_text: DataTypes.STRING,
      personal_site: DataTypes.STRING,
      background_url: DataTypes.STRING,
      background_type: DataTypes.STRING,
      steam: DataTypes.STRING,
      discord: DataTypes.STRING,
      github: DataTypes.STRING,
      youtube: DataTypes.STRING,
      twitter: DataTypes.STRING,
      twitch: DataTypes.STRING,
      gitlab: DataTypes.STRING,
      tumblr: DataTypes.STRING,
      header: DataTypes.STRING,
      disable_comments: DataTypes.BOOLEAN,
      createdAt: { type: DataTypes.DATE, field: 'created_at' },
      updatedAt: { type: DataTypes.DATE, field: 'updated_at' },
    },
    {
      timestamps: true,
    }
  );
  UserProfile.associate = function (models) {
    UserProfile.User = UserProfile.belongsTo(models.User, { as: 'user', foreignKey: 'user_id' });
  };
  UserProfile.initScopes = () => {};
  return UserProfile;
};
