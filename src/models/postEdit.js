module.exports = (sequelize, DataTypes) => {
  const PostEdit = sequelize.define(
    'PostEdit',
    {
      postId: {
        field: 'post_id',
        type: DataTypes.INTEGER.UNSIGNED,
        references: {
          model: 'Posts',
          key: 'id',
        },
      },
      userId: {
        field: 'user_id',
        type: DataTypes.INTEGER,
        references: {
          model: 'Users',
          key: 'id',
        },
      },
      editReason: { type: DataTypes.STRING, field: 'edit_reason' },
      content: DataTypes.TEXT,
      createdAt: { type: DataTypes.DATE, field: 'created_at' },
    },
    {
      timestamps: true,
      updatedAt: false,
    }
  );
  PostEdit.associate = (models) => {
    PostEdit.Post = PostEdit.belongsTo(models.Post, {
      as: 'post',
      foreignKey: 'post_id',
    });
    PostEdit.User = PostEdit.belongsTo(models.User, {
      as: 'user',
      foreignKey: 'user_id',
    });
  };
  PostEdit.initScopes = () => {};
  return PostEdit;
};
