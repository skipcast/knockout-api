const { Op } = require('sequelize');
const { default: nsfwTagName } = require('../constants/nsfwTagName');

module.exports = (sequelize, DataTypes) => {
  const Alert = sequelize.define(
    'Alert',
    {
      user_id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.INTEGER,
        references: {
          model: 'Users',
          key: 'id',
        },
      },
      thread_id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.INTEGER.UNSIGNED,
        references: {
          model: 'Threads',
          key: 'id',
        },
      },
      lastPostNumber: {
        type: DataTypes.INTEGER,
        field: 'last_post_number',
      },
      lastSeen: {
        type: DataTypes.DATE,
        field: 'last_seen',
      },
      createdAt: {
        type: DataTypes.DATE,
        field: 'created_at',
      },
      updatedAt: {
        type: DataTypes.DATE,
        field: 'created_at',
      },
    },
    {
      timestamps: true,
      scopes: {
        byUser(userId, userPermissionCodes, hideNsfw) {
          const where = {
            user_id: userId,
          };
          if (hideNsfw) {
            where['$Thread.Tags.name$'] = {
              [Op.or]: [
                null,
                {
                  [Op.ne]: nsfwTagName,
                },
              ],
            };
          }
          return {
            where,
            subquery: false,
            include: [
              {
                model: sequelize.models.Thread.scope({
                  method: ['byUser', userId, userPermissionCodes],
                }),
                attributes: ['id'],
                required: true,
                include: {
                  attributes: ['id'],
                  model: sequelize.models.Tag,
                  as: 'Tags',
                  required: false,
                },
              },
            ],
          };
        },
      },
    }
  );
  Alert.associate = function (models) {
    Alert.User = Alert.belongsTo(models.User, { foreignKey: 'user_id' });
    Alert.Thread = Alert.belongsTo(models.Thread, { foreignKey: 'thread_id' });
  };
  Alert.initScopes = () => {};
  return Alert;
};
