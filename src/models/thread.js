const { Op, col, where, fn, literal } = require('sequelize');
const { ResourceAction, Resource, SubforumPermissionSuffixes } = require('knockout-schema');
const { getUserPermissionCodes } = require('../helpers/user');
const { actionableResourceIds } = require('../helpers/permissions');

module.exports = (sequelize, DataTypes) => {
  const Thread = sequelize.define(
    'Thread',
    {
      title: DataTypes.STRING,
      icon_id: DataTypes.INTEGER,
      user_id: DataTypes.INTEGER,
      subforum_id: DataTypes.INTEGER,
      createdAt: { type: DataTypes.DATE, field: 'created_at' },
      updatedAt: { type: DataTypes.DATE, field: 'updated_at' },
      deletedAt: { type: DataTypes.DATE, field: 'deleted_at' },
      locked: DataTypes.BOOLEAN,
      pinned: DataTypes.BOOLEAN,
      background_url: DataTypes.STRING,
      background_type: DataTypes.STRING,
    },
    {
      timestamps: true,
      whereMergeStrategy: 'and',
      scopes: {
        notDeleted: {
          where: {
            deleted: null,
          },
        },
        notLocked: {
          where: {
            locked: false,
          },
        },
        byCreator(userId) {
          return {
            where: { user_id: userId },
          };
        },
        byUser(userId, userPermissionCodes) {
          const subforumIdsCanView = actionableResourceIds(
            ResourceAction.VIEW,
            Resource.SUBFORUM,
            userPermissionCodes
          );
          const subforumIdsCanViewDeletedThreads = actionableResourceIds(
            SubforumPermissionSuffixes.VIEW_DELETED_THREADS,
            Resource.SUBFORUM,
            userPermissionCodes
          );
          return {
            where: {
              [Op.and]: [
                {
                  [Op.or]: [
                    { subforum_id: { [Op.in]: subforumIdsCanViewDeletedThreads } },
                    { deleted_at: null },
                  ],
                },
                {
                  [Op.or]: [
                    { subforum_id: { [Op.in]: subforumIdsCanView } },
                    { user_id: userId || null },
                  ],
                },
              ],
            },
          };
        },
        bySubforum(subforumId) {
          return {
            where: {
              subforum_id: subforumId,
            },
          };
        },
        searchByTitle(title) {
          return {
            where: literal('MATCH (title) AGAINST (:title IN NATURAL LANGUAGE MODE)'),
            replacements: {
              title,
            },
          };
        },
      },
    }
  );
  Thread.associate = function (models) {
    // A user can have many posts
    Thread.Posts = Thread.hasMany(models.Post, { as: 'posts', foreignKey: 'thread_id' });
    Thread.User = Thread.belongsTo(models.User, { as: 'user', foreignKey: 'user_id' });
    Thread.Subforum = Thread.belongsTo(models.Subforum, {
      as: 'subforum',
      foreignKey: 'subforum_id',
    });
    Thread.Tags = Thread.belongsToMany(models.Tag, {
      through: models.ThreadTag,
      foreignKey: 'thread_id',
      otherKey: 'tag_id',
    });
  };
  Thread.initScopes = () => {};
  return Thread;
};
