module.exports = (sequelize, DataTypes) => {
  const Notification = sequelize.define(
    'Notification',
    {
      type: DataTypes.STRING,
      dataId: { type: DataTypes.BIGINT, field: 'data_id' },
      userId: {
        type: DataTypes.BIGINT,
        field: 'user_id',
        references: {
          model: 'Users',
          key: 'id',
        },
      },
      read: DataTypes.BOOLEAN,
      createdAt: { type: DataTypes.DATE, field: 'created_at' },
    },
    {
      timestamps: false,
    }
  );
  Notification.associate = (models) => {
    Notification.User = Notification.belongsTo(models.User, {
      as: 'user',
      foreignKey: 'user_id',
    });
  };
  Notification.initScopes = () => {};
  return Notification;
};
