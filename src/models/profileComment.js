module.exports = (sequelize, DataTypes) => {
  const ProfileComment = sequelize.define(
    'ProfileComment',
    {
      user_profile: {
        type: DataTypes.BIGINT,
        references: {
          model: 'Users',
          key: 'id',
        },
      },
      author: {
        type: DataTypes.BIGINT,
        references: {
          model: 'Users',
          key: 'id',
        },
      },
      content: DataTypes.TEXT,
      deleted: DataTypes.BOOLEAN,
      createdAt: { type: DataTypes.DATE, field: 'created_at' },
      updatedAt: { type: DataTypes.DATE, field: 'updated_at' },
    },
    {
      timestamps: true,
    }
  );
  ProfileComment.associate = function (models) {
    ProfileComment.User = ProfileComment.belongsTo(models.User, {
      as: 'user',
      foreignKey: 'author',
    });
  };
  ProfileComment.initScopes = () => {};
  return ProfileComment;
};
