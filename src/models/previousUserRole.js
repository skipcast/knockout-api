"use strict";

module.exports = (sequelize, DataTypes) => {
  const PreviousUserRole = sequelize.define(
    'PreviousUserRole',
    {
      user_id: {
        type: DataTypes.INTEGER,
        references: {
          model: "Users",
          key: "id"
        }
      },
      role_id: {
        type: DataTypes.BIGINT,
        references: {
          model: 'Roles',
          key: 'id',
        },
      },
      createdAt: {type: DataTypes.DATE, field: 'created_at'},
    },
    {
      timestamps: true,
      updatedAt: false,
    }
  );
  PreviousUserRole.associate = (models) => {
    PreviousUserRole.User = PreviousUserRole.belongsTo(models.User, {
      as: 'user',
      foreignKey: 'user_id',
    });
    PreviousUserRole.Role = PreviousUserRole.belongsTo(models.Role, {
      as: 'role',
      foreignKey: 'role_id',
    });
  };
  PreviousUserRole.initScopes = () => {};
  return PreviousUserRole;
};
