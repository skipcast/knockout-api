const Sequelize = require('sequelize');
const fs = require('fs');
const path = require('path');

const { NODE_ENV } = require('../../config/server');

const env = NODE_ENV;

const basename = path.basename(module.filename);

const config = require('../../config/config')[env];

let datasource = null;

const testConnection = (sequelize) => {
  sequelize
    .authenticate()
    .then(() => {
      console.log('Connection has been established successfully.');
    })
    .catch((err) => {
      console.error('Unable to connect to the database:', err);
    });
};

const loadModels = (sequelize) => {
  const models = [];

  fs
    .readdirSync(__dirname)
    .filter(file => file.indexOf('.') !== 0 && file !== basename && file.slice(-3) === '.js')
    .forEach((file) => {
      const model = require(path.join(__dirname, file))(sequelize, Sequelize.DataTypes);
      
      if (typeof model.options.defaultScope == 'function') {
        console.log('Loading scope for ', model.name)
        model.addScope('defaultScope', model.options.defaultScope(), { override: true })
      }

      models[model.name] = model;
    });
  return models;
};

module.exports = () => {
  if (!datasource) {
    let connection = null;

    console.log('Connection to database: ', config.database);
    console.log('Database host: ', config.host);
    connection = new Sequelize(config.database, config.username, config.password, config);

    testConnection(connection);

    datasource = {
      connection,
      Sequelize,
      models: {},
    };
    datasource.models = loadModels(connection);

    Object.keys(datasource.models).forEach((modelName) => {
      if (datasource.models[modelName].associate) {
        datasource.models[modelName].associate(datasource.models);
        datasource.models[modelName].initScopes(datasource.models);
      }
    });
    return datasource;
  }
  return datasource;
};