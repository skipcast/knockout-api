'use strict';

module.exports = (sequelize, DataTypes) => {
  const ThreadTag = sequelize.define(
    'ThreadTag',
    {
      tag_id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.INTEGER.UNSIGNED,
        references: {
          model: 'Tags',
          key: 'id',
        },
      },
      thread_id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.INTEGER.UNSIGNED,
        references: {
          model: 'Threads',
          key: 'id',
        },
      },
      createdAt: { type: DataTypes.DATE, field: 'created_at' },
      deletedAt: { type: DataTypes.DATE, field: 'deleted_at' },
    },
    {
      timestamps: false,
    }
  );
  ThreadTag.associate = function (models) {
    ThreadTag.Tag = ThreadTag.belongsTo(models.Tag, { foreignKey: 'tag_id' });
    ThreadTag.Thread = ThreadTag.belongsTo(models.Thread, { foreignKey: 'thread_id' });
  };
  ThreadTag.initScopes = () => {};
  return ThreadTag;
};
