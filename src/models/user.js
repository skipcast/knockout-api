const { RoleCode } = require('../helpers/permissions');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    'User',
    {
      username: DataTypes.STRING,
      email: DataTypes.STRING,
      avatar_url: DataTypes.STRING,
      background_url: DataTypes.STRING,
      usergroup: { type: DataTypes.INTEGER, defaultValue: 1 },
      roleId: { type: DataTypes.INTEGER, field: 'role_id' },
      external_type: DataTypes.STRING,
      external_id: DataTypes.STRING,
      stripeCustomerId: { type: DataTypes.STRING, field: 'stripe_customer_id' },
      title: { type: DataTypes.STRING },
      donationUpgradeExpiresAt: { type: DataTypes.DATE, field: 'donation_upgrade_expires_at' },
      createdAt: { type: DataTypes.DATE, field: 'created_at' },
      updatedAt: { type: DataTypes.DATE, field: 'updated_at' },
    },
    {
      timestamps: true,
      hooks: {
        async beforeCreate(user, options) {
          // if user has no role, assign role to limited user role
          if (user.roleId) {
            return;
          }

          // we cannot use our helper method since it is possible
          // that the datasource is not initialized when this hook runs
          const [limitedUserRole] = await sequelize.models.Role.findOrCreate({
            attributes: ['id'],
            where: { code: RoleCode.LIMITED_USER },
            defaults: { description: 'A limited user with limited forum access' },
          });
          user.roleId = limitedUserRole.id;
        },
        // if the user's role_id is not the same as their
        // previous role_id, create a new previous user role record
        async afterSave(user, options) {
          // if the user has no role / previous role
          // or if the user's role was not changed,
          // do not run
          if (user.roleId && user.previous('roleId') && user.previous('roleId') !== user.roleId) {
            // create the previous user role record
            await sequelize.models.PreviousUserRole.create({
              role_id: user.previous('roleId'),
              user_id: user.id,
            });
          }

          if (
            user.username &&
            user.previous('username') &&
            user.previous('username') !== user.username
          ) {
            // create the previous user name record
            await sequelize.models.PreviousUsername.create({
              username: user.previous('username'),
              user_id: user.id,
            });
          }
        },
      },
    }
  );
  User.associate = function (models) {
    // A user can have many posts
    User.hasMany(models.Post, { foreignKey: 'user_id' });
    User.hasMany(models.Thread, { foreignKey: 'user_id' });

    // a user belongs to a role
    User.belongsTo(models.Role, { foreignKey: 'roleId' });
  };

  User.initScopes = () => {};
  return User;
};
