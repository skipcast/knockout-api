'use strict';

const { Op } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  const Tag = sequelize.define(
    'Tag',
    {
      id: {
        type: DataTypes.INTEGER.UNSIGNED,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING,
      },
      createdAt: { type: DataTypes.DATE, field: 'created_at' },
      deletedAt: { type: DataTypes.DATE, field: 'deleted_at' },
    },
    {
      timestamps: false,
    }
  );
  Tag.associate = function (models) {
    Tag.Threads = Tag.belongsToMany(models.Thread, {
      through: models.ThreadTag,
      foreignKey: 'tag_id',
      otherKey: 'thread_id',
      constraints: false,
    });
  };
  Tag.initScopes = () => {};
  return Tag;
};
