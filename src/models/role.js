'use strict';
module.exports = (sequelize, DataTypes) => {
  const Role = sequelize.define('Role', {
    code: DataTypes.STRING,
    description: DataTypes.STRING,
    createdAt: {type: DataTypes.DATE, field: 'created_at'},
    updatedAt: {type: DataTypes.DATE, field: 'updated_at'},
  }, {
    timestamps: true
  });
  Role.associate = function(models) {
    // A role can have many permissions through RolePermissions
    Role.belongsToMany(models.Permission, { through: 'RolePermissions'});
  };
  Role.initScopes = () => {}
  return Role;
};