module.exports = (sequelize, DataTypes) => {
  const RolePermission = sequelize.define(
    'RolePermission',
    {
      role_id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.BIGINT,
        references: {
          model: 'Roles',
          key: 'id',
        },
      },
      permission_id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.BIGINT,
        references: {
          model: 'Permissions',
          key: 'id',
        },
      },
    },
    {
      timestamps: false,
    }
  );
  RolePermission.associate = (models) => {
    RolePermission.Role = RolePermission.belongsTo(models.Role, {
      as: 'role',
      foreignKey: 'role_id',
    });
    RolePermission.Permission = RolePermission.belongsTo(models.Permission, {
      as: 'permission',
      foreignKey: 'permission_id',
    });
  };
  RolePermission.initScopes = () => {};
  return RolePermission;
};
