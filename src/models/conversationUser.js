module.exports = (sequelize, DataTypes) => {
  const ConversationUser = sequelize.define(
    'ConversationUser',
    {
      conversation_id: {
        type: DataTypes.INTEGER.UNSIGNED,
        primaryKey: true,
        references: {
          model: 'Conversations',
          key: 'id',
        },
      },
      user_id: {
        type: DataTypes.INTEGER,
        references: {
          model: 'Users',
          key: 'id',
        },
      },
    },
    {
      timestamps: false,
    }
  );
  ConversationUser.associate = (models) => {
    ConversationUser.Conversation = ConversationUser.belongsTo(models.Conversation, {
      as: 'conversation',
      foreignKey: 'conversation_id',
    });
    ConversationUser.User = ConversationUser.belongsTo(models.User, {
      as: 'user',
      foreignKey: 'user_id',
    });
  };
  ConversationUser.initScopes = () => {};
  return ConversationUser;
};
