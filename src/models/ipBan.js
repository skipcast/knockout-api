module.exports = (sequelize, DataTypes) => {
  const IpBan = sequelize.define(
    'IpBan',
    {
      address: DataTypes.STRING,
      range: DataTypes.STRING,
      createdBy: { type: DataTypes.INTEGER, fields: 'created_by' },
      updatedBy: { type: DataTypes.INTEGER, fields: 'updated_by' },
      createdAt: { type: DataTypes.DATE, field: 'created_at' },
      updatedAt: { type: DataTypes.DATE, field: 'updated_at' },
    },
    {
      timestamps: true,
    }
  );
  IpBan.associate = function (models) {
    // An ipBan can have a creator
    IpBan.User = IpBan.belongsTo(
      models.User, 
      { as: 'user', foreignKey: 'created_by' }
    );
  };
  IpBan.initScopes = () => {};
  return IpBan;
};
