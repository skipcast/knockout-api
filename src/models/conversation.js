module.exports = (sequelize, DataTypes) => {
  const Conversation = sequelize.define(
    'Conversation',
    {
      latest_message_id: {
        type: DataTypes.BIGINT,
        references: {
          model: 'Messages',
          key: 'id',
        },
      },
      createdAt: { type: DataTypes.DATE, field: 'created_at' },
      updatedAt: { type: DataTypes.DATE, field: 'updated_at' },
    },
    {
      timestamps: true,
    }
  );
  Conversation.associate = () => {};
  Conversation.initScopes = () => {};
  return Conversation;
};
