'use strict';
module.exports = (sequelize, DataTypes) => {
  const Subforum = sequelize.define(
    'Subforum',
    {
      name: DataTypes.STRING,
      icon_id: DataTypes.INTEGER,
      icon: DataTypes.STRING,
      nsfw: DataTypes.BOOLEAN,
      createdAt: {
        type: DataTypes.DATE,
        field: 'created_at',
        defaultValue: sequelize.literal('NOW()')
      },
      updatedAt: {
        type: DataTypes.DATE,
        field: 'updated_at',
        defaultValue: sequelize.literal('NOW()')
      },
      description: DataTypes.STRING
    },
    {
      timestamps: true
    }
  );
  Subforum.associate = function(models) {
    Subforum.Threads = Subforum.hasMany(models.Thread, {
      as: 'threads',
      foreignKey: 'subforum_id'
    });
  };
  Subforum.initScopes = () => {};
  return Subforum;
};
