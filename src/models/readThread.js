module.exports = (sequelize, DataTypes) => {
  const ReadThread = sequelize.define(
    'ReadThread',
    {
      user_id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.INTEGER,
        references: {
          model: 'Users',
          key: 'id',
        },
      },
      thread_id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.INTEGER.UNSIGNED,
        references: {
          model: 'Threads',
          key: 'id',
        },
      },
      lastSeen: {
        type: DataTypes.DATE,
        field: 'last_seen',
      },
      createdAt: {
        type: DataTypes.DATE,
        field: 'created_at',
      },
      updatedAt: {
        type: DataTypes.DATE,
        field: 'created_at',
      },
    },
    {
      timestamps: true,
    }
  );
  ReadThread.associate = function (models) {
    ReadThread.User = ReadThread.belongsTo(models.User, { foreignKey: 'user_id' });
    ReadThread.Thread = ReadThread.belongsTo(models.Thread, { foreignKey: 'thread_id' });
  };
  ReadThread.initScopes = () => {};
  return ReadThread;
};
