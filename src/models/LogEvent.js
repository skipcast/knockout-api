module.exports = (sequelize, DataTypes) => {
  const LogEvent = sequelize.define(
    'LogEvent',
    {
      userId: { type: DataTypes.INTEGER, fields: 'user_id' },
      entity: DataTypes.STRING,
      action: DataTypes.STRING,
      success: DataTypes.BOOLEAN,
      message: DataTypes.STRING,
      properties: DataTypes.STRING,
      createdAt: { type: DataTypes.DATE, field: 'created_at' },
    },
    {
      timestamps: true,
    }
  );
  LogEvent.initScopes = () => {};
  return LogEvent;
};
