module.exports = (sequelize, DataTypes) => {
  const Message = sequelize.define(
    'Message',
    {
      conversation_id: {
        type: DataTypes.BIGINT,
        references: {
          model: 'Conversations',
          key: 'id',
        },
      },
      user_id: {
        type: DataTypes.INTEGER,
        references: {
          model: 'Users',
          key: 'id',
        },
      },
      content: {
        type: DataTypes.TEXT,
      },
      read_at: { type: DataTypes.DATE, field: 'read_at' },
      createdAt: { type: DataTypes.DATE, field: 'created_at' },
      updatedAt: { type: DataTypes.DATE, field: 'updated_at' },
    },
    {
      timestamps: true,
    }
  );
  Message.associate = (models) => {
    Message.Conversation = Message.belongsTo(models.Conversation, {
      as: 'conversation',
      foreignKey: 'conversation_id',
    });
    Message.User = Message.belongsTo(models.User, { as: 'user', foreignKey: 'user_id' });
  };
  Message.initScopes = () => {};
  return Message;
};
