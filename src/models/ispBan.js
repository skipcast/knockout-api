module.exports = (sequelize, DataTypes) => {
  const IspBan = sequelize.define(
    'IspBan',
    {
      netname: DataTypes.STRING,
      asn: DataTypes.STRING,
      createdBy: { type: DataTypes.INTEGER, field: 'created_by' },
      updatedBy: { type: DataTypes.INTEGER, field: 'updated_by' },
      createdAt: { type: DataTypes.DATE, field: 'created_at' },
      updatedAt: { type: DataTypes.DATE, field: 'updated_at' },
    },
    {
      timestamps: true,
    }
  );
  IspBan.initScopes = () => {};
  return IspBan;
};
