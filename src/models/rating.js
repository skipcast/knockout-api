const { knex } = require('../services/knex');
const ratingList = require('../helpers/ratingList.json');

module.exports = (sequelize, DataTypes) => {
  function sortRatings(ratings) {
    Object.keys(ratings).forEach((post) => {
      ratings[post].sort((a, b) => b.count - a.count);
    });
    return ratings;
  }

  const Rating = sequelize.define(
    'Rating',
    {
      post_id: {
        type: DataTypes.INTEGER.UNSIGNED,
        primaryKey: true,
        references: {
          model: 'Posts',
          key: 'id',
        },
      },
      user_id: {
        type: DataTypes.INTEGER,
        references: {
          model: 'Users',
          key: 'id',
        },
      },
      rating_id: {
        type: DataTypes.INTEGER.UNSIGNED,
      },
      createdAt: {
        type: DataTypes.DATE,
        field: 'created_at',
      },
      updatedAt: {
        type: DataTypes.DATE,
        field: 'updated_at',
      },
    },
    {
      timestamps: true,
    }
  );
  Rating.associate = function (models) {
    Rating.Post = Rating.belongsTo(models.Post, { as: 'post', foreignKey: 'post_id' });
    Rating.User = Rating.belongsTo(models.User, { as: 'user', foreignKey: 'user_id' });
  };
  Rating.initScopes = () => {};
  Rating.getRatingsForPostsWithUsers = function (postIds) {
    return knex
      .from('Ratings as rating')
      .select(
        'rating.rating_id as ratingId',
        'rating.post_id as ratingPostId',
        'userRatedBy.username as ratedBy'
      )
      .whereIn('rating.post_id', postIds)
      .leftJoin('Users as userRatedBy', 'rating.user_id', 'userRatedBy.id')
      .then((results) => {
        let resultArray = Array.isArray(results) ? results : [results];

        resultArray = resultArray.reduce((ratings, rating) => {
          if (ratings[rating.ratingPostId] === undefined) {
            ratings[rating.ratingPostId] = {};
          }
          if (ratings[rating.ratingPostId][rating.ratingId] === undefined) {
            ratings[rating.ratingPostId][rating.ratingId] = [];
          }
          ratings[rating.ratingPostId][rating.ratingId].push(rating.ratedBy);

          return ratings;
        }, {});

        Object.keys(resultArray).forEach((key) => {
          const ratingArray = [];
          Object.keys(resultArray[key]).forEach((ratingId) => {
            const ratingShorthand = ratingList[ratingId].short;

            ratingArray.push({
              rating: ratingShorthand,
              rating_id: ratingId,
              users: resultArray[key][ratingId],
              count: resultArray[key][ratingId].length,
            });
          });

          resultArray[key] = ratingArray;
        });

        return sortRatings(resultArray);
      });
  };
  Rating.getRatingsForPosts = function (postIds) {
    return knex
      .from('Ratings as rating')
      .select('rating.rating_id as ratingId', 'rating.post_id as ratingPostId')
      .count('rating.rating_id as ratingCount')
      .whereIn('rating.post_id', postIds)
      .groupBy('rating.post_id', 'rating.rating_id')
      .then((results) => {
        const resultArray = Array.isArray(results) ? results : [results];

        return sortRatings(
          resultArray.reduce((ratings, rating) => {
            if (ratings[rating.ratingPostId] === undefined) {
              ratings[rating.ratingPostId] = [];
            }

            const ratingShorthand = ratingList[rating.ratingId].short;
            ratings[rating.ratingPostId].push({
              rating: ratingShorthand,
              rating_id: rating.ratingId,
              count: rating.ratingCount,
            });

            return ratings;
          }, {})
        );
      });
  };
  return Rating;
};
