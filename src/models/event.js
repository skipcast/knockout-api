module.exports = (sequelize, DataTypes) => {
  const Event = sequelize.define(
    'Event',
    {
      type: DataTypes.STRING,
      dataId: { type: DataTypes.BIGINT, field: 'data_id' },
      content: { type: DataTypes.JSON },
      creator: {
        type: DataTypes.BIGINT,
        field: 'executed_by',
        references: {
          model: 'Users',
          key: 'id',
        },
      },
      createdAt: { type: DataTypes.DATE, field: 'created_at' },
    },
    {
      timestamps: false,
    }
  );
  Event.associate = (models) => {
    Event.User = Event.belongsTo(models.User, {
      as: 'user',
      foreignKey: 'creator',
    });
  };
  Event.initScopes = () => {};
  return Event;
};
