'use strict';

module.exports = (sequelize, DataTypes) => {
  const Rule = sequelize.define('Rule', {
    rulableType: { type: DataTypes.STRING, field: 'rulable_type' },
    rulableId: { type: DataTypes.INTEGER, field: 'rulable_id' },
    category: DataTypes.STRING,
    title: DataTypes.STRING,
    cardinality: DataTypes.INTEGER,
    description: DataTypes.STRING,
    createdAt: {type: DataTypes.DATE, field: 'created_at'},
    updatedAt: {type: DataTypes.DATE, field: 'updated_at'},
  }, {
    timestamps: true
  });
  Rule.initScopes = () => {}
  return Rule;
};
