const mountScope = (scope, f, model) => {
  let filter = Object.assign({}, f);
  let scopes = [];

  if (filter) {
    Object.keys(filter).forEach(key => {
      scopes.push({ method: [key, filter[key]] });
    });
  }

  if (scope) {
    scope = Array.isArray(scope) ? scope : [scope];
    scopes = [...scope, ...scopes];
  }

  return model.scope(...scopes);
};

const query = (req, options) => {
  const params = req.params ? req.params : null;
  const q = req.query.q ? req.query.q : null;

  if (!q && !params) return options;

  const where = {};

  if (params) Object.assign(where, params);
  if (q) Object.assign(where, q);

  return Object.assign(options, { where });
};

const attributes = (req, options, blockedFields) => {
  let exclude = req.query.exclude ? req.query.exclude : [];
  let include = req.query.include ? req.query.include : null;

  let attr = {};

  exclude = [...blockedFields, ...exclude];

  Object.assign(attr, { exclude });

  if (include) {
    include = include.split(" ");
    // campos no array exclude não pode ser inseridos
    include = include.filter(el => !exclude.includes(el));

    if (include.length > 0) {
      attr = include;
    }
  }

  return Object.assign(options, { attributes: attr });
};

const order = (req, options) => {
  const q = req.query.order ? req.query.order : null;

  if (!q) return options;

  const o = [];

  Object.keys(q).forEach(key => {
    o.push(key);
    o.push(q[key]);
  });

  return Object.assign(options, { order: [o] });
};

const offset = (req, options) => {
  let q = req.query.offset ? req.query.offset : "0";

  q = parseInt(q, 10);

  return Object.assign(options, { offset: q });
};

const limit = (req, options, logged) => {
  let q = req.query.limit ? req.query.limit : "24";

  q = parseInt(q, 10);

  if (!logged) {
    q = q > 50 ? 50 : q;
  }

  return Object.assign(options, { limit: q });
};

const group = (req, options) => {
  const q = req.query.group ? req.query.group : null;

  if (!q) return options;

  return Object.assign(options, { group: q });
};

const page = (req, options) => {
  const p = req.query.page ? parseInt(req.query.page, 10) : 1;
  const ops = Object.assign({}, options);
  ops.offset = ops.limit * (p - 1);

  return Object.assign(options, ops, { page: p.toString() });
};

const keyCache = req => {
  const keyCache = {
    key: req.params,
    query: req.query,
    orig: req.path,
    sort: req.query.sort,
    range: req.query.range
  };

  return JSON.stringify(keyCache);
};

const options = (req, blockedFields = [], logged = false) => {
  let options: any = {};
  options = query(req, options);
  options = attributes(req, options, blockedFields);
  options = group(req, options);
  options = order(req, options);
  options = limit(req, options, logged);
  options = offset(req, options);
  options = page(req, options);

  return options;
};

const onlyQuery = req => {
  let options = {};
  return query(req, options);
};

const scope = (req, model, options?) =>
  req.query.scope || req.query.filter !== undefined
    ? mountScope(req.query.scope, req.query.filter, model)
    : model;

export default {
  keyCache,
  options,
  onlyQuery,
  scope
};