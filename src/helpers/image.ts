import sharp, { Metadata } from 'sharp';

const GIF_MAXIMUM_WIDTH = 115;
const GIF_MAXIMUM_HEIGHT = 115;
const GIF_MAXIMUM_SIZE = 250000;

export const stripExifTags = (buffer: Buffer) => sharp(buffer).toBuffer();

export const imageMetadata = (buffer: Buffer) => sharp(buffer).metadata();

export const avatarGifToWebp = (buffer: Buffer) =>
  sharp(buffer, { pages: -1 }).toFormat('webp').toBuffer();

export const avatarToWebp = (buffer: Buffer) =>
  sharp(buffer)
    .resize(115, 115, {
      withoutEnlargement: true,
      fit: 'inside',
    })
    .toFormat('webp')
    .toBuffer();

export const backgroundToWebp = (buffer: Buffer) =>
  sharp(buffer)
    .resize(230, 460)
    .webp({ quality: 75, nearLossless: true, lossless: true })
    .toFormat('webp')
    .toBuffer();

export const isValidGif = (metadata: Metadata, avatar = true): boolean => {
  const { format, width, height, size, pages } = metadata;
  return (
    format === 'gif' &&
    pages > 1 &&
    (!avatar ||
      (width <= GIF_MAXIMUM_WIDTH && height <= GIF_MAXIMUM_HEIGHT && size <= GIF_MAXIMUM_SIZE))
  );
};
