import { PermissionCode, Thread } from 'knockout-schema';
import { Op } from 'sequelize';
import datasource from '../models/datasource';
import RoleRetriever, { RoleFlag } from '../retriever/role';
import UserRetriever from '../retriever/user';
import {
  getBasicUserRole,
  getGuestUserRole,
  getLimitedUserRole,
  getPaidGoldUserRole,
  getBannedUserRole,
} from './role';

export const unBanUser = async (userId: number) => {
  const { PreviousUserRole, User } = datasource().models;
  // make user's new role the same as their first unbanned previous role
  const bannedUserRoleId = (await getBannedUserRole()).id;
  const previousUserRole = await PreviousUserRole.findOne({
    attributes: ['id', 'role_id'],
    where: { user_id: userId, role_id: { [Op.not]: bannedUserRoleId } },
    order: [['created_at', 'DESC']],
  });

  let newRoleId = previousUserRole?.role_id;
  // if previous role is not found, make them a limited user
  if (!newRoleId) {
    newRoleId = (await getLimitedUserRole()).id;
  }

  const user = await User.findOne({ where: { id: userId } });
  await user.update({ roleId: newRoleId });
  await user.save();

  await new UserRetriever(userId).invalidate();
};

export const demoteUserFromGold = async (userId: number) => {
  const { PreviousUserRole, User } = datasource().models;
  // make user's new role the same as their first previous role that is not paid-gold or banned
  // this accounts for cases where a previously banned user is demoted from gold after their ban
  // in this case their previous role is the banned user, but we don't want to revert to that, so we
  // find the nearest valid role
  const invalidRoleIds = [await getBannedUserRole(), await getPaidGoldUserRole()].map(
    (role) => role.id
  );
  const previousUserRole = await PreviousUserRole.findOne({
    attributes: ['id', 'role_id'],
    where: { user_id: userId, role_id: { [Op.notIn]: invalidRoleIds } },
    order: [['created_at', 'DESC']],
  });

  let newRoleId = previousUserRole?.role_id;
  // if previous role is not found, make them a basic user
  if (!newRoleId) {
    newRoleId = (await getBasicUserRole()).id;
  }

  const user = await User.findOne({ where: { id: userId } });
  await user.update({ roleId: newRoleId });
  await user.save();

  await new UserRetriever(userId).invalidate();
};

export const promoteUserToGold = async (userId: number) => {
  const goldRoleId = (await getPaidGoldUserRole()).id;

  const { User } = await datasource().models;
  const user = await User.findOne({ where: { id: userId } });

  await user.update({ roleId: goldRoleId });
  await user.save();

  await new UserRetriever(userId).invalidate();
};

export const getUserPermissionCodes = async (userId?: number): Promise<PermissionCode[]> => {
  // if we don't get a user id, return the guest user permission codes
  if (!userId) {
    const guestRole = await getGuestUserRole();
    const { permissionCodes } = await new RoleRetriever(guestRole.id, [
      RoleFlag.INCLUDE_PERMISSION_CODES,
    ]).getSingleObject();
    return permissionCodes;
  }
  const {
    role: { id: userRoleId },
  } = await new UserRetriever(userId).getSingleObject();

  const role = await new RoleRetriever(userRoleId, [
    RoleFlag.INCLUDE_PERMISSION_CODES,
  ]).getSingleObject();

  return role.permissionCodes;
};

export const userHasPermissions = async (
  userId: number,
  permissionCodes: PermissionCode[]
): Promise<boolean> => {
  const userCodes = await getUserPermissionCodes(userId);
  return permissionCodes.every((code) => userCodes.includes(code));
};

export const userCanViewSubforum = (
  userPermissionCodes: PermissionCode[],
  subforumId: number
): boolean =>
  userPermissionCodes.includes(`subforum-${subforumId}-view`) ||
  userPermissionCodes.includes(`subforum-${subforumId}-view-own-threads`);

export const userCanViewThread = (
  userId: number,
  userPermissionCodes: PermissionCode[],
  thread: Thread
): boolean => {
  if (
    thread.deletedAt !== null &&
    !userPermissionCodes.includes(`subforum-${thread.subforumId}-view-deleted-threads`)
  ) {
    return false;
  }

  const threadUserId = thread.user?.id || thread.user;

  if (userId === threadUserId) {
    return userCanViewSubforum(userPermissionCodes, thread.subforumId);
  }
  return userPermissionCodes.includes(`subforum-${thread.subforumId}-view`);
};

export const getViewableSubforumIdsByUser = async (userId?: number): Promise<number[]> => {
  const userCodes = await getUserPermissionCodes(userId);
  const { Subforum } = datasource().models;
  return Subforum.findAll({
    attributes: ['id'],
    order: [['created_at', 'asc']],
  }).then((records) =>
    records.reduce((list, subforum) => {
      if (userCanViewSubforum(userCodes, subforum.id)) {
        list.push(subforum.id);
      }
      return list;
    }, [])
  );
};
