import { CreateRuleRequest, Rule } from 'knockout-schema';
import knex from '../services/knex';
import RuleRetriever from '../retriever/rule';
import datasource from '../models/datasource';

export const storeRule = async ({
  rulable_type = null,
  rulable_id = null,
  category,
  title,
  cardinality,
  description,
}: CreateRuleRequest): Promise<Rule> => {
  const rule = await knex('Rules')
    .insert({
      rulable_type: rulable_type || null,
      rulable_id: rulable_id || null,
      category: category.trim(),
      title: title.trim(),
      cardinality: Number(cardinality) || 1,
      description,
    })
    .then((results) => results[0]);

  return new RuleRetriever(rule).getSingleObject();
};

export const updateRule = async ({
  ruleId,
  category,
  title,
  cardinality,
  description,
}: {
  ruleId: number;
  category?: string;
  title?: string;
  cardinality?: number;
  description?: string;
}) => {
  await knex('Rules').where('id', ruleId).update({ category, title, cardinality, description });

  const ruleRetriever = new RuleRetriever(ruleId);
  await ruleRetriever.invalidate();

  return ruleRetriever.getSingleObject();
};

export const indexRules = async ({
  rulableType = null,
  rulableId = null,
}: {
  rulableType?: string;
  rulableId?: number;
}) => {
  const query = await knex('Rules')
    .select('id')
    .where('rulable_type', rulableType || null)
    .andWhere('rulable_id', rulableId || null);

  return new RuleRetriever(query.map((item) => item.id)).getObjectArray();
};

export const destroyRule = async ({ ruleId }: { ruleId: number }) => {
  const { Rule: RuleModel } = datasource().models;
  await RuleModel.destroy({ where: { id: ruleId } });
  return { message: 'Rule deleted' };
};
