import {
  PermissionCode,
  Resource,
  ResourceAction,
  SubforumPermissionSuffixes,
} from 'knockout-schema';
import knex from '../services/knex';

const AGGREGATE_RECORD_ACTIONS = [
  ResourceAction.CREATE,
  ResourceAction.UPDATE,
  ResourceAction.ARCHIVE,
  ResourceAction.VIEW,
];

const RESOURCES = [
  Resource.BAN,
  Resource.RULE,
  Resource.EVENT,
  Resource.IP_ADDRESS,
  Resource.MESSAGE,
  Resource.MESSAGE_OF_THE_DAY,
  Resource.RATING,
  Resource.REPORT,
  Resource.ROLE,
  Resource.TAG,
  Resource.THREAD_TAG,
  Resource.USER,
  Resource.LATEST_USERS,
  Resource.DASHBOARD_DATA,
  Resource.FULL_USER_INFO,
  Resource.GOLD_PRODUCT,
  Resource.ADMIN_SETTINGS,
  Resource.USER_PROFILE_COMMENT,
  Resource.CALENDAR_EVENT,
];

const SUBFORUM_PERMISSION_CODE_SUFFIXES = [
  SubforumPermissionSuffixes.VIEW,
  SubforumPermissionSuffixes.VIEW_OWN_THREADS,
  SubforumPermissionSuffixes.UPDATE,
  SubforumPermissionSuffixes.ARCHIVE,
  SubforumPermissionSuffixes.POST_CREATE,
  SubforumPermissionSuffixes.POST_UPDATE,
  SubforumPermissionSuffixes.POST_ARCHIVE,
  SubforumPermissionSuffixes.POST_BYPASS_VALIDATIONS,
  SubforumPermissionSuffixes.POST_RATING_CREATE,
  SubforumPermissionSuffixes.POST_REPORT_CREATE,
  SubforumPermissionSuffixes.THREAD_CREATE,
  SubforumPermissionSuffixes.THREAD_UPDATE,
  SubforumPermissionSuffixes.THREAD_LOCK,
  SubforumPermissionSuffixes.THREAD_UNLOCK,
  SubforumPermissionSuffixes.THREAD_PIN,
  SubforumPermissionSuffixes.THREAD_UNPIN,
  SubforumPermissionSuffixes.THREAD_MOVE,
  SubforumPermissionSuffixes.THREAD_DELETE,
  SubforumPermissionSuffixes.VIEW_DELETED_THREADS,
  SubforumPermissionSuffixes.VIEW_THREAD_VIEWERS,
  SubforumPermissionSuffixes.THREAD_BACKGROUND_UPDATE,
  SubforumPermissionSuffixes.RULE_CREATE,
  SubforumPermissionSuffixes.RULE_UPDATE,
  SubforumPermissionSuffixes.RULE_ARCHIVE,
];

const SUBFORUM_GUEST_USER_SUFFIXES = [SubforumPermissionSuffixes.VIEW];

const SUBFORUM_BASIC_USER_SUFFIXES = SUBFORUM_GUEST_USER_SUFFIXES.concat(
  // create a post
  SubforumPermissionSuffixes.POST_CREATE,
  // rate a post
  SubforumPermissionSuffixes.POST_RATING_CREATE,
  // report a post
  SubforumPermissionSuffixes.POST_REPORT_CREATE,
  // create a thread
  SubforumPermissionSuffixes.THREAD_CREATE
);

const SUBFORUM_GOLD_USER_SUFFIXES = SUBFORUM_BASIC_USER_SUFFIXES.concat(
  SubforumPermissionSuffixes.THREAD_BACKGROUND_UPDATE
);

const SUBFORUM_MODERATOR_SUFFIXES = SUBFORUM_GOLD_USER_SUFFIXES.concat(
  // update any post
  SubforumPermissionSuffixes.POST_UPDATE,
  // archive a post
  SubforumPermissionSuffixes.POST_ARCHIVE,
  // bypass post validations
  SubforumPermissionSuffixes.POST_BYPASS_VALIDATIONS,
  // update a thread title
  SubforumPermissionSuffixes.THREAD_UPDATE,
  // lock a thread
  SubforumPermissionSuffixes.THREAD_LOCK,
  // unlock a thread
  SubforumPermissionSuffixes.THREAD_UNLOCK,
  // pin a thread
  SubforumPermissionSuffixes.THREAD_PIN,
  // unpin a thread
  SubforumPermissionSuffixes.THREAD_UNPIN,
  // change the subforum of a thread
  SubforumPermissionSuffixes.THREAD_MOVE,
  // delete a thread
  SubforumPermissionSuffixes.THREAD_DELETE,
  // view deleted threads
  SubforumPermissionSuffixes.VIEW_DELETED_THREADS,
  // view thread viewers
  SubforumPermissionSuffixes.VIEW_THREAD_VIEWERS,
  // create a rule
  SubforumPermissionSuffixes.RULE_CREATE,
  // update a rule
  SubforumPermissionSuffixes.RULE_UPDATE,
  // archive a rule
  SubforumPermissionSuffixes.RULE_ARCHIVE
);

const GUEST_USER_PERMISSIONS: PermissionCode[] = [
  'event-view',
  'messageOfTheDay-view',
  'user-view',
];

const BANNED_USER_PERMISSIONS = GUEST_USER_PERMISSIONS;

const LIMITED_USER_PERMISSIONS = GUEST_USER_PERMISSIONS.concat('message-view', 'report-create');

const BASIC_USER_PERMISSIONS = GUEST_USER_PERMISSIONS.concat(
  'message-create',
  'message-view',
  'report-create',
  'user-profile-comment-create'
);

const GOLD_USER_PERMISSIONS = BASIC_USER_PERMISSIONS.concat(
  'user-avatar-gif-upload',
  'calendar-event-create'
);

const MODERATOR_PERMISSIONS = GOLD_USER_PERMISSIONS.concat(
  'ban-create',
  'ban-update',
  'ban-archive',
  'ipAddress-view',
  'messageOfTheDay-create',
  'messageOfTheDay-update',
  'report-view',
  'report-resolve',
  'tag-create',
  'tag-view',
  'tag-update',
  'tag-archive',
  'threadTag-update',
  'user-archive',
  'user-update',
  'latest-users-view',
  'dashboard-data-view',
  'full-user-info-view',
  'gold-product-create',
  'admin-settings-view',
  'admin-settings-update',
  'rule-create',
  'rule-update',
  'rule-archive',
  'user-profile-comment-archive',
  'calendar-event-update',
  'calendar-event-archive'
);

const SUPER_MODERATOR_PERMISSIONS = MODERATOR_PERMISSIONS.concat(
  'event-archive',
  'role-create',
  'role-update',
  'role-view',
  'subforum-create',
  'subforum-update',
  'subforum-archive'
);

export enum RoleCode {
  GUEST = 'guest',
  BANNED_USER = 'banned-user',
  LIMITED_USER = 'limited-user',
  BASIC_USER = 'basic-user',
  GOLD_USER = 'gold-user',
  PAID_GOLD_USER = 'paid-gold-user',
  MODERATOR_IN_TRAINING = 'moderator-in-training',
  MODERATOR = 'moderator',
  SUPER_MODERATOR = 'super-moderator',
  ADMIN = 'admin',
}

interface RolePermissionMapping {
  roleCode: RoleCode;
  permissionCodes: PermissionCode[];
  subforumCodeSuffixes: SubforumPermissionSuffixes[];
}

export const ROLE_PERMISSION_MAPPINGS: RolePermissionMapping[] = [
  {
    roleCode: RoleCode.GUEST,
    permissionCodes: GUEST_USER_PERMISSIONS,
    subforumCodeSuffixes: SUBFORUM_GUEST_USER_SUFFIXES,
  },
  {
    roleCode: RoleCode.BANNED_USER,
    permissionCodes: BANNED_USER_PERMISSIONS,
    subforumCodeSuffixes: SUBFORUM_GUEST_USER_SUFFIXES,
  },
  {
    roleCode: RoleCode.LIMITED_USER,
    permissionCodes: LIMITED_USER_PERMISSIONS,
    subforumCodeSuffixes: SUBFORUM_BASIC_USER_SUFFIXES,
  },
  {
    roleCode: RoleCode.BASIC_USER,
    permissionCodes: BASIC_USER_PERMISSIONS,
    subforumCodeSuffixes: SUBFORUM_BASIC_USER_SUFFIXES,
  },
  {
    roleCode: RoleCode.GOLD_USER,
    permissionCodes: GOLD_USER_PERMISSIONS,
    subforumCodeSuffixes: SUBFORUM_GOLD_USER_SUFFIXES,
  },
  {
    roleCode: RoleCode.MODERATOR_IN_TRAINING,
    permissionCodes: MODERATOR_PERMISSIONS,
    subforumCodeSuffixes: SUBFORUM_MODERATOR_SUFFIXES,
  },
  {
    roleCode: RoleCode.MODERATOR,
    permissionCodes: MODERATOR_PERMISSIONS,
    subforumCodeSuffixes: SUBFORUM_MODERATOR_SUFFIXES,
  },
  {
    roleCode: RoleCode.SUPER_MODERATOR,
    permissionCodes: SUPER_MODERATOR_PERMISSIONS,
    subforumCodeSuffixes: SUBFORUM_MODERATOR_SUFFIXES,
  },
  {
    roleCode: RoleCode.ADMIN,
    permissionCodes: SUPER_MODERATOR_PERMISSIONS,
    subforumCodeSuffixes: SUBFORUM_MODERATOR_SUFFIXES,
  },
];

export const aggregatePermissions = () => {
  const permissions = RESOURCES.map((recordName: string) =>
    AGGREGATE_RECORD_ACTIONS.map((action) => ({ code: `${recordName}-${action}` }))
  );

  return [].concat(...permissions);
};

export const subforumPermissions = (subforumIds: number[]) => {
  const permissions = subforumIds.map((id: number) =>
    SUBFORUM_PERMISSION_CODE_SUFFIXES.map((suffix) => ({ code: `subforum-${id}-${suffix}` }))
  );

  return [].concat(...permissions);
};

export const addAndAssociateSubforumPermissions = async (
  subforumId: number
): Promise<Promise<number[]>[]> => {
  const permissionCodes = subforumPermissions([subforumId]);

  // create permissions
  await knex('Permissions').insert(permissionCodes);

  // associate permissions to roles based off of their associated suffixes
  return ROLE_PERMISSION_MAPPINGS.map(async (rp) => {
    const subforumCodes = rp.subforumCodeSuffixes.map(
      (suffix) => `subforum-${subforumId}-${suffix}`
    );

    const permissions = await knex('Permissions').select('id').where('code', [subforumCodes]);

    const permissionIds: number[] = permissions.map((record: { id: number }) => record.id);

    const role = await knex('Roles').select('id').where('code', rp.roleCode).first();

    const rolePermissions = permissionIds.map((pid: number) => ({
      role_id: role.id,
      permission_id: pid,
    }));

    return knex('RolePermissions').insert(rolePermissions);
  });
};

export const getRoleIdsWithPermissions = async (
  permissionCodes: PermissionCode[]
): Promise<number[]> =>
  (
    await knex('RolePermissions')
      .select('role_id')
      .join('Permissions as p', 'p.id', 'permission_id')
      .where('p.code', permissionCodes)
  ).map((rp) => rp.role_id);

const resourceAndActionFromPermissionCode = (
  permissionCode: PermissionCode
): { resource: Resource; action: ResourceAction | SubforumPermissionSuffixes } | null => {
  const splitCode = permissionCode.split(/\d+/);
  // return null if our permission code is non resource based
  if (splitCode.length === 1) {
    return null;
  }

  const resourceName = splitCode[0].slice(0, -1) as Resource;
  const actionName = splitCode[1].slice(1) as ResourceAction | SubforumPermissionSuffixes;
  return { resource: resourceName, action: actionName };
};

const idFromPermissionCode = (permissionCode: PermissionCode): number => {
  const id = Number(permissionCode.match(/\d+/)[0]);
  return id || -1;
};

const validId = (id: number | null) => Number(id) > 0;

export const actionableResourceIds = (
  action: ResourceAction | SubforumPermissionSuffixes,
  resource: Resource,
  userPermissionCodes: PermissionCode[]
): number[] =>
  userPermissionCodes
    .map((permissionCode) => {
      const resourceAndAction = resourceAndActionFromPermissionCode(permissionCode);
      if (resourceAndAction?.resource === resource && resourceAndAction?.action === action) {
        return idFromPermissionCode(permissionCode);
      }
      return null;
    })
    .filter(validId);
