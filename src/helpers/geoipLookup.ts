import geoip from 'fast-geoip';
import countryLookup from 'country-code-lookup';

const ipLookup = async (ip: string) => geoip.lookup(ip);

export const getCountryName = async (geoData) => {
  try {
    const data = await ipLookup(geoData);

    if (data) {
      return countryLookup.byIso(data.country).country;
    }
  } catch (error) {
    return null;
  }

  return null;
};

export const getCountryCode = (countryName) => {
  const data = countryLookup.byCountry(countryName);

  if (data) {
    return data.iso2;
  }

  return null;
};
