import { PermissionCode } from 'knockout-schema';
import { ForbiddenError } from 'routing-controllers';
import { getUserPermissionCodes } from './user';

const authorize = async (
  userId: number,
  requiredPermissionCodes: PermissionCode[],
  some: boolean = false
): Promise<void> => {
  const userPermissionCodes = await getUserPermissionCodes(userId);

  const authorized =
    (some && requiredPermissionCodes.some((code) => userPermissionCodes.includes(code))) ||
    requiredPermissionCodes.every((code) => userPermissionCodes.includes(code));

  if (!authorized) {
    throw new ForbiddenError('Insufficient user permissions.');
  }
};

export default authorize;
