import { roundNumber } from '../helpers/utils';
import { Request } from 'express';

interface Options {
  limit: string;
  page: string;
}

const changePageOffset = (url: string, newOffset: any) => url.replace(/page=\d+/g, `page=${newOffset}`);

const addPageAttrOnUrl = (url) => {
  if (!url.includes('page=')) {
    if (!url.includes('?')) {
      return url.concat('?page=1');
    }
    return url.concat('&page=1');
  }
  return url;
};

class Pagination {

  next_page: string;
  previous_page: string;
  total_entries: number;
  per_page: number;
  total_pages: number;
  current_page: number;
  list: any[];

  constructor({
    totalEntries = 0,
    perPage = 0,
    totalPages = 0,
    currentPage = 0,
    nextPage = '',
    previousPage = '',
    list = [{}],
  }) {
    this.next_page = nextPage;
    this.previous_page = previousPage;
    this.total_entries = totalEntries;
    this.per_page = perPage;
    this.total_pages = totalPages;
    this.current_page = currentPage;
    this.list = list;
  }
}

class PaginationBuilder {

  pagination: Pagination;
  dbResult: any;
  options: Options;
  perPage: string;
  totalPages: number;
  currentPage: number;
  url: string;

  constructor(options: Options, req: Request, dbResult: any) {
    this.pagination = new Pagination({ nextPage: '', previousPage: '' });

    this.dbResult = dbResult;
    this.options = options;

    this.perPage = options.limit;
    this.totalPages = options.limit ? dbResult.count / parseInt(this.perPage, 10) : 1;
    this.currentPage = options.page ? parseInt(options.page, 10) - 1 : 1;
    this.url = req.originalUrl;
  }

  buildTotalRows() {
    this.pagination.total_entries = this.dbResult.count;
    return this;
  }

  buildPerPage() {
    this.pagination.per_page = parseInt(this.perPage, 10);
    return this;
  }

  buildTotalPages() {
    this.pagination.total_pages = roundNumber(this.totalPages);
    return this;
  }

  buildCurrenPageNum() {
    this.pagination.current_page = this.currentPage + 1;
    return this;
  }

  buildSidePages() {
    const currentPage = this.currentPage + 1;
    const nextPage = currentPage + 1;
    const prevPage = currentPage - 1;

    if (Math.ceil(this.totalPages) >= nextPage) {
      this.pagination.next_page = changePageOffset(addPageAttrOnUrl(this.url), nextPage);
    } else {
      delete this.pagination.next_page;
    }

    if (prevPage >= 1) {
      this.pagination.previous_page = changePageOffset(addPageAttrOnUrl(this.url), prevPage);
    } else {
      delete this.pagination.previous_page;
    }

    return this;
  }

  buildList() {
    this.pagination.list = this.dbResult.rows;
    return this;
  }

  build() {
    return this.buildTotalRows()
      .buildCurrenPageNum()
      .buildPerPage()
      .buildTotalPages()
      .buildSidePages()
      .buildList().pagination;
  }
}

export default PaginationBuilder;