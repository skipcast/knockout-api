import datasource from '../models/datasource';
import { RoleCode } from './permissions';

export const getBannedUserRole = async () => {
  const { Role } = datasource().models;
  const [role] = await Role.findOrCreate({
    where: { code: RoleCode.BANNED_USER },
    defaults: { description: 'A banned user with restricted forum access' },
  });
  return role;
};

export const getGuestUserRole = async () => {
  const { Role } = datasource().models;
  const [role] = await Role.findOrCreate({
    where: { code: RoleCode.GUEST },
    defaults: { description: 'A guest user who is not logged in to the forum' },
  });
  return role;
};

export const getLimitedUserRole = async () => {
  const { Role } = datasource().models;
  const [role] = await Role.findOrCreate({
    where: { code: RoleCode.LIMITED_USER },
    defaults: { description: 'A limited user with limited forum access' },
  });
  return role;
};

export const getGoldUserRole = async () => {
  const { Role } = datasource().models;
  const [role] = await Role.findOrCreate({
    where: { code: RoleCode.GOLD_USER },
    defaults: { description: 'An elevated user with special forum priveleges' },
  });
  return role;
};

export const getPaidGoldUserRole = async () => {
  const { Role } = datasource().models;
  const [role] = await Role.findOrCreate({
    where: { code: RoleCode.PAID_GOLD_USER },
    defaults: { description: 'A Gold User who has this role through a Knockout Gold subscription' },
  });
  return role;
};

export const getBasicUserRole = async () => {
  const { Role } = datasource().models;
  const [role] = await Role.findOrCreate({
    where: { code: RoleCode.BASIC_USER },
    defaults: { description: 'A basic user with normal forum access' },
  });
  return role;
};
