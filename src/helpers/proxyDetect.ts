import { ForbiddenError } from 'routing-controllers';
import Scamalytics from '../services/scamalytics';
import Ripe from '../services/ripe';

export default async function checkUserIp(ip: string) {
  // are they using a VPN or do they look like they're in a datacenter?
  if (await new Scamalytics(ip).isRisky()) {
    console.log('VPN detected', ip);
    throw new ForbiddenError('You appear to be using a proxy/VPN.');
  }

  // Do we have a ban in place for their netname or parent ASN?
  const ripe = new Ripe(ip);
  if ((await ripe.isBanned()) || (await ripe.isParentBanned())) {
    console.log('Banned ISP detected', ip);
    throw new ForbiddenError('You appear to be using an ISP which we have banned.');
  }
}
