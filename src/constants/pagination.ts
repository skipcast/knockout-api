export const reportsPerPage = 40;
export const postsPerPage = 20;
export const threadsPerPage = 40;
export const alertsPerPage = 20;
