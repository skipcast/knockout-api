import { Request, Response } from 'express';
import ResponseStrategy from '../helpers/responseStrategy';
import knex from '../services/knex';
import redis from '../services/redisClient';

const STATS_TTL = 21600;

// eslint-disable-next-line import/prefer-default-export
export const index = async (req: Request, res: Response) => {
  const cacheKey = 'stats';
  let stats: any = await redis.getAsync(cacheKey);
  if (typeof stats === 'string') {
    stats = JSON.parse(stats);
  } else {
    const query = await knex
      .select(
        knex.raw('(select count("id") from Users) as userCount'),
        knex.raw('(select count("id") from Posts) as postCount'),
        knex.raw('(select count("id") from Threads) as threadCount'),
        knex.raw('(select count("id") from Ratings) as ratingsCount')
      )
      .first();

    const { userCount, postCount, threadCount, ratingsCount } = query as any;

    stats = {
      userCount,
      postCount,
      threadCount,
      ratingsCount,
    };

    redis.setex(cacheKey, STATS_TTL, JSON.stringify(stats));
  }

  return ResponseStrategy.send(res, stats);
};
