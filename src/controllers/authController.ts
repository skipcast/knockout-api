import { Request, Response } from 'express';
import httpStatus from 'http-status';
import google from './auth/google';
import passport from './auth/passport';

export { google, passport };

export function logout(req: Request, res: Response) {
  res.clearCookie('knockoutJwt');
  res.sendStatus(httpStatus.OK);
}

export function finish(req: Request, res: Response) {
  res.status(httpStatus.OK).send(`<script src="/static/scripts/authRedirectHandler.js"></script>`);
}
