import { Request, Response } from 'express';
import httpStatus from 'http-status';
import { NotificationType } from 'knockout-schema';
import knex from '../services/knex';
import ConversationRetriever from '../retriever/conversation';

import onDuplicateUpdate from '../helpers/onDuplicateUpdate';
import errorHandler from '../services/errorHandler';
import validateMessageLength from '../validations/message';
import MessageRetriever from '../retriever/message';
import { createNotification } from './notificationController';

export const store = async (req: Request, res: Response) => {
  try {
    if (!validateMessageLength(req.body.content)) {
      throw new Error('Failed validations.');
    }

    if (!req.body.receivingUserId) {
      throw new Error('Message must have a recipient.');
    }

    let message;
    let conversationId: number;
    await knex.transaction(async (trx) => {
      // if no conversation ID is passed,
      // we assume the client wants a new
      // conversation to be created

      if (req.body.conversationId) {
        conversationId = req.body.conversationId;
      } else {
        const createResult = await trx('Conversations')
          .insert({})
          .then((results) => results[0]);
        conversationId = createResult;

        // if this user is not part of
        // the conversation already, add them to it
        await onDuplicateUpdate(trx, 'ConversationUsers', {
          user_id: req.user.id,
          conversation_id: conversationId,
        });

        // if the receiving user is not part of
        // the conversation already, add them to it
        //
        // note: if we want
        // more than 2 users per conversation,
        // this controller method / block will need to be updated
        // to accept an array of users
        await onDuplicateUpdate(trx, 'ConversationUsers', {
          user_id: req.body.receivingUserId,
          conversation_id: conversationId,
        });
      }

      // create message
      message = await trx('Messages')
        .insert({
          user_id: req.user.id,
          conversation_id: conversationId,
          content: req.body.content,
        })
        .then((results) => results[0]);

      // update conversation
      await trx('Conversations')
        .where('id', conversationId)
        .update({ latest_message_id: message, updated_at: new Date() });

      // invalidate conversation
      new ConversationRetriever(conversationId).invalidate();
    });

    // format message for response
    const formattedMessage = await new MessageRetriever(message).getSingleObject();

    // send notification to message recipient
    await createNotification(NotificationType.MESSAGE, req.body.receivingUserId, conversationId);

    res.status(httpStatus.CREATED);
    res.json(formattedMessage);
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const update = async (req: Request, res: Response) => {
  try {
    await knex('Messages')
      .update({
        read_at: new Date(),
      })
      .where('id', req.params.id);

    const messageRetriever = new MessageRetriever(req.params.id);
    await messageRetriever.invalidate();

    // format message for response
    const updatedMessage = await messageRetriever.getSingleObject();

    res.status(httpStatus.OK);
    res.json(updatedMessage);
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};
