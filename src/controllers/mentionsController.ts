import { Request, Response } from 'express';

import httpStatus from 'http-status';
import MentionRetriever from '../retriever/mention';
import errorHandler from '../services/errorHandler';
import knex from '../services/knex';

export const createMention = async ({
  postId,
  mentionsUser,
  content,
  inactive,
  threadId,
  threadTitle,
  page,
  userId,
}) => {
  try {
    const result = await knex('Mentions').insert({
      post_id: postId,
      mentions_user: mentionsUser,
      content,
      inactive,
      thread_id: threadId,
      thread_title: threadTitle,
      page,
      mentioned_by: userId,
    });

    return result;
  } catch (exception) {
    return new Error(exception);
  }
};

export const markAsRead = async (req: Request, res: Response) => {
  try {
    if (!req.body.postIds) {
      throw new Error('No postIds supplied.');
    }

    const postIds = Array.isArray(req.body.postIds) ? req.body.postIds : [req.body.postIds];

    await knex('Mentions')
      .update({ inactive: true })
      .where({ mentions_user: req.user.id })
      .whereIn('post_id', postIds);

    res.status(httpStatus.OK);
    res.json({ message: 'Posts marked as read!' });
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const getMentionsQuery = async (userId: number) => {
  const mentions: any[] = await knex('Mentions')
    .select('id')
    .where({ mentions_user: userId, inactive: false })
    .orWhere({ mentions_user: userId, inactive: null })
    .limit(20);

  return new MentionRetriever(mentions.map((mention) => Number(mention.id))).getObjectArray();
};

export const index = async (req: Request, res: Response) => {
  try {
    const results = await getMentionsQuery(req.user.id);

    res.status(httpStatus.OK);
    res.json(results);
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};
