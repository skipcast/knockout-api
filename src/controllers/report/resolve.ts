import { Request, Response } from 'express';
import httpStatus from 'http-status';
import { NotificationType } from 'knockout-schema';
import ReportRetriever from '../../retriever/report';
import errorHandler from '../../services/errorHandler';
import knex from '../../services/knex';
import { createNotification } from '../notificationController';

// eslint-disable-next-line import/prefer-default-export
export const post = async (req: Request, res: Response) => {
  try {
    await knex('Reports')
      .where({ id: req.params.id })
      .update({ solved_by: req.user.id, updated_at: knex.fn.now() });

    const reportPostId = (await knex('Reports').select('post_id').where({ id: req.params.id }))[0]
      .post_id;

    // anyone who reported this post will get a notification if action has been taken on this post
    // in this case, "action" means that a Ban record exists on the report's post_id
    const actionTaken =
      (await knex('Bans').select('id').where({ post_id: reportPostId }).limit(1)).length > 0;

    if (actionTaken) {
      // get all users who reported this post and create a notification for them
      const reportingUserIds = (
        await knex('Reports').select('reported_by').where({ post_id: reportPostId })
      ).map((report) => report.reported_by);

      const notifQueries = reportingUserIds.map((userId) =>
        createNotification(NotificationType.REPORT_RESOLUTION, userId, -1)
      );
      await Promise.all(notifQueries);
    }

    new ReportRetriever(req.params.id).invalidate();
    res.status(httpStatus.CREATED);
    res.json({ message: 'Report marked as solved.' });
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};
