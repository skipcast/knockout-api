import { Request, Response } from 'express';
import httpStatus from 'http-status';
import DiscordWebhook from 'discord-webhook-ts';
import knex from '../../services/knex';
import PostRetriever from '../../retriever/post';

const notify = async (postId: number, userId: number, reason: String, reportId: number) => {
  if (process.env.NODE_ENV !== 'production') return;

  const discordClient = new DiscordWebhook(process.env.MODERATION_WEBHOOK);

  const post = await new PostRetriever(postId).getSingleObject();

  const userNameQuery = await knex('Users').select('username').where({ id: userId }).limit(1);

  const { username } = userNameQuery[0];

  const description = `Report: https://knockout.chat/moderate/reports/${reportId}
Post: https://knockout.chat/thread/${post.thread}/${post.page}#post-${postId}
Reason: "${reason}"
Reported by: ${username} (https://knockout.chat/user/${userId})
Go get 'em, <@&551783790188691476>!
https://knockout.chat/moderate/reports/
  `;

  await discordClient.execute({
    embeds: [
      {
        title: 'A new report has been received!',
        description,
        color: 16527151,
      },
    ],
  });
};

// eslint-disable-next-line import/prefer-default-export
export const post = async (req: Request, res: Response) => {
  const result = await knex('Reports').insert(
    {
      post_id: req.body.postId,
      reported_by: req.user.id,
      report_reason: req.body.reportReason,
    },
    ['id']
  );

  await notify(req.body.postId, req.user.id, req.body.reportReason, result[0]);

  res.status(httpStatus.CREATED);
  res.json({ message: `Created a report for post #${req.body.postId}` });
};
