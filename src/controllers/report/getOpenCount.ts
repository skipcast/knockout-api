/* eslint-disable import/prefer-default-export */

import { Request, Response } from 'express';
import httpStatus from 'http-status';
import knex from '../../services/knex';

export const get = async (req: Request, res: Response) => {
  const result = await knex('Reports').count({ count: 'id' }).whereNull('solved_by').first();

  res.status(httpStatus.OK);
  res.json({ openReports: result.count });
};
