import { Request, Response } from 'express';
import httpStatus from 'http-status';
import { AlertsResponse } from 'knockout-schema';
import onDuplicateUpdate from '../helpers/onDuplicateUpdate';
import knex from '../services/knex';
import { alertsPerPage } from '../constants/pagination';
import AlertRetriever from '../retriever/alert';
import errorHandler from '../services/errorHandler';
import datasource from '../models/datasource';
import { getUserPermissionCodes } from '../helpers/user';

export const store = async (req: Request, res: Response) => {
  try {
    onDuplicateUpdate(knex, 'Alerts', {
      user_id: req.user.id,
      thread_id: req.body.threadId,
      last_seen: req.body.lastSeen ? new Date(req.body.lastSeen) : new Date(),
      last_post_number: req.body.lastPostNumber,
    });

    res.status(httpStatus.CREATED);
    res.json({ message: `Created an alert for thread #${req.body.threadId}` });
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const destroy = async (req: Request, res: Response) => {
  try {
    await knex('Alerts').where({ user_id: req.user.id, thread_id: req.body.threadId }).del();

    res.status(httpStatus.OK);
    res.json({ message: `Deleted alert for thread #${req.body.threadId}` });
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const get = async (
  userId: number,
  page: number = 1,
  hideNsfw: boolean = false
): Promise<AlertsResponse> => {
  const { Alert: AlertModel } = datasource().models;
  const permissionCodes = await getUserPermissionCodes(userId);

  const query = await AlertModel.scope({
    method: ['byUser', userId, permissionCodes, hideNsfw],
  }).findAndCountAll({ attributes: ['thread_id'] });
  const alerts = await new AlertRetriever(
    query.rows.map((item) => item.thread_id),
    userId
  ).getObjectArray();

  const ids = alerts.map((alert) => alert.id);
  const result = { alerts, totalAlerts: alerts.length, ids };

  result.alerts.sort((alertA, alertB) => {
    if (alertA.unreadPosts > alertB.unreadPosts) return -1;
    if (alertA.unreadPosts < alertB.unreadPosts) return 1;

    if (alertA.thread.lastPost.createdAt > alertB.thread.lastPost.createdAt) return -1;
    if (alertA.thread.lastPost.createdAt < alertB.thread.lastPost.createdAt) return 1;
    return 0;
  });

  result.alerts = result.alerts.slice((page - 1) * alertsPerPage, page * alertsPerPage);

  return result;
};

export const getAlerts = async (userId: number, page: number = 1, hideNsfw: boolean = false) => {
  const result = await get(userId, page, hideNsfw);

  result.alerts = result.alerts.map((alert) => ({
    ...alert,
    thread: undefined,
    threadId: alert.id,
    icon_id: alert.thread.iconId,
    threadIcon: alert.thread.iconId,
    threadTitle: alert.thread.title,
    threadUser: alert.thread.user.id,
    threadLocked: alert.thread.locked,
    threadCreatedAt: alert.thread.createdAt,
    threadUpdatedAt: alert.thread.updatedAt,
    threadDeletedAt: alert.thread.deleted,
    threadBackgroundUrl: alert.thread.backgroundUrl,
    threadBackgroundType: alert.thread.backgroundType,
    threadUsername: alert.thread.user.username,
    threadUserAvatarUrl: alert.thread.user.avatarUrl,
    threadUserUsergroup: alert.thread.user.usergroup,
    threadUserRoleCode: alert.thread.user.role?.code,
    threadPostCount: alert.thread.postCount,
    lastPost: alert.thread.lastPost,
  }));
  return result;
};

export const index = async (req: Request, res: Response) => {
  try {
    const hideNsfw = req.query.hideNsfw || false;
    const pageParam: number = req.params.page;

    const results = await getAlerts(req.user.id, pageParam, hideNsfw);

    res.status(httpStatus.OK);
    res.json(results);
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};
