/* eslint-disable import/prefer-default-export */

import { Request } from 'express';
import { ThreadSearchRequest } from 'knockout-schema';
import ThreadRetriever, { ThreadFlag } from '../retriever/thread';
import { getUserPermissionCodes } from '../helpers/user';
import datasource from '../models/datasource';

const THREADS_PER_PAGE = 10;

export const search = async (req: Request, body: ThreadSearchRequest) => {
  const {
    title,
    tag_ids: tagIds,
    subforum_id: subforumId,
    user_id: userId,
    sort_by: orderByColumn,
    sort_order: sortOrder,
    page,
  } = body;

  const orderByOrder = sortOrder || 'desc';
  const permissionCodes = await getUserPermissionCodes(req.user?.id);

  let orderBy;
  if (orderByColumn && orderByColumn !== 'relevance') {
    orderBy = [[orderByColumn, orderByOrder]];
  }

  const { Tag: TagModel, Thread: ThreadModel } = datasource().models;

  let include;
  if (tagIds?.length > 0) {
    include = {
      model: TagModel,
      attributes: ['id'],
      required: true,
      where: { id: tagIds },
    };
  }

  const threadScopes = [
    { method: ['byUser', req.user?.id, permissionCodes] },
    { method: ['searchByTitle', title] },
  ];

  if (subforumId) {
    threadScopes.push({ method: ['bySubforum', subforumId] });
  }

  if (userId) {
    threadScopes.push({ method: ['byCreator', userId] });
  }

  const query = await ThreadModel.scope(threadScopes).findAndCountAll({
    attributes: ['id'],
    limit: THREADS_PER_PAGE,
    offset: (page - 1) * THREADS_PER_PAGE,
    include,
    order: orderBy,
  });

  const threadIds = query.rows.map((thread) => thread.id);

  const flags = [
    ThreadFlag.RETRIEVE_SHALLOW,
    ThreadFlag.INCLUDE_LAST_POST,
    ThreadFlag.INCLUDE_POST_COUNT,
    ThreadFlag.INCLUDE_USER,
    ThreadFlag.INCLUDE_TAGS,
    ThreadFlag.INCLUDE_SUBFORUM,
  ];
  const threads = await new ThreadRetriever(threadIds, flags, {
    userId: req.user?.id,
  }).getObjectArray();

  return { totalThreads: query.count, threads };
};
