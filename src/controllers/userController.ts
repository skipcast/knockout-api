import { Request, Response } from 'express';
import httpStatus from 'http-status';
import Sequelize from 'sequelize';
import { Server } from 'socket.io';
import { EventType, UpdateUserRequest, User as UserSchema } from 'knockout-schema';
import ms from 'ms';
import { BadRequestError, ForbiddenError, HttpError } from 'routing-controllers';
import ResponseStrategy from '../helpers/responseStrategy';
import UserRetriever, { UserFlag } from '../retriever/user';
import ThreadRetriever, { ThreadFlag } from '../retriever/thread';
import PostRetriever, { PostFlag } from '../retriever/post';
import BanRetriever, { BanFlag } from '../retriever/ban';

import composer from '../helpers/queryComposer';
import datasource from '../models/datasource';
import knex from '../services/knex';
import redis from '../services/redisClient';
import { getMentionsQuery } from './mentionsController';
import { getAlerts } from './alertController';
import { countOpenReportsQuery } from './moderationController';
import ratingList from '../helpers/ratingList.json';
import errorHandler from '../services/errorHandler';
import { RAID_MODE_KEY } from '../constants/adminSettings';
import deleteUser from '../services/userDeletion/deleteUser';
import { banUser } from './banController';
import { wipePosts } from '../services/userDeletion/scrubPosts';
import scrubMessages from '../services/userDeletion/scrubMessages';
import scrubProfileComments from '../services/userDeletion/scrubProfileComments';
import scrubUserProfiles from '../services/userDeletion/scrubUserProfiles';
import scrubUser from '../services/userDeletion/scrubUser';
import deleteRatings from '../services/userDeletion/deleteRatings';
import RoleRetriever, { RoleFlag } from '../retriever/role';
import { demoteUserFromGold, getUserPermissionCodes, promoteUserToGold } from '../helpers/user';
import { threadsPerPage } from '../constants/pagination';
import StripeClient from '../services/stripeClient';
import { RoleCode } from '../helpers/permissions';
import { wipePostEdits } from '../services/userDeletion/scrubPostEdits';
import invalidateCache from '../services/userDeletion/invalidateCache';
import { createEvent } from './eventLogController';

export const store = async (req: Request, res: Response) => {
  const { User } = datasource().models;

  try {
    const raidModeEnabled = await redis.getAsync(RAID_MODE_KEY);

    if (raidModeEnabled) {
      return res.sendStatus(httpStatus.FORBIDDEN);
    }

    const user = await User.create({ ...req.body, usergroup: 1 });

    const scope = composer.scope(req, User);
    const options = composer.options(req, User.blockedFields);

    const resultingUser = scope.findOne({ ...options, where: { id: user.id } });

    return resultingUser;
  } catch (exception) {
    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    return res.json({ error: 'An error has occurred.' });
  }
};

/**
 * Send the loaded user data to the client
 */
export const index = async (req: Request, res: Response) => {
  try {
    if (req.isLoggedIn) {
      res.status(httpStatus.OK);
      res.json({ user: req.user });
    } else {
      res.status(httpStatus.UNAUTHORIZED);
      res.json({ error: 'You are not logged in.' });
    }
  } catch (exception) {
    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    res.json({ error: 'An error has ocurred. ' });
  }
};

export const checkUsername = async (user, username?: string): Promise<string> => {
  const { PreviousUsername } = datasource().models;
  if (user.username) {
    const previousUsername = await PreviousUsername.findOne({
      where: {
        user_id: user.id,
        createdAt: {
          [Sequelize.Op.gt]: new Date(Date.now() - ms('1 year')),
        },
      },
    });
    if (previousUsername) {
      throw new ForbiddenError('You can only change your username once every year.');
    }
  }

  if (!username) {
    return user.username;
  }

  const tentativeUsername = username.trim();

  if (
    tentativeUsername.length < 3 ||
    !/^[\w\-\_\s\á\é\ó\ú\ã\ê\ô\ç\!\?\$\#\&]+$/.test(tentativeUsername)
  ) {
    throw new BadRequestError('This username is not valid.');
  }

  const existingUserCheck = await knex
    .select('*')
    .from('Users')
    .where({ username: tentativeUsername })
    .first();

  if (existingUserCheck && existingUserCheck.length !== 0) {
    throw new ForbiddenError('This username has already been used.');
  }

  return tentativeUsername;
};

export const update = async (id: number, body: UpdateUserRequest): Promise<UserSchema> => {
  const user = await knex.select('*').from('Users').where({ id }).first();
  const { User } = datasource().models;

  // shitty way of making sure users can only change what we want
  let fields: { username?: string; avatar_url?: string; background_url?: string } = {};
  if (body.username && user.username !== body.username) {
    const username = await checkUsername(user, body.username);
    fields = { ...fields, username };
  }
  if (body.avatarUrl && (body.avatarUrl === `${user.id}.webp` || body.avatarUrl === 'none.webp')) {
    fields = { ...fields, avatar_url: body.avatarUrl };
  }
  if (body.backgroundUrl && body.backgroundUrl === `${user.id}-bg.webp`) {
    fields = { ...fields, background_url: body.backgroundUrl };
  }

  // eslint-disable-next-line @typescript-eslint/naming-convention
  const { username, avatar_url, background_url } = fields;

  if (!username && !avatar_url && !background_url) {
    throw new Error('Nothing changed.');
  }

  const userModel = await User.findOne({ where: { id } });
  await userModel.update({ ...fields });
  await userModel.save();

  const retriever = new UserRetriever(id);
  retriever.invalidate();

  return retriever.getSingleObject();
};

export const updateUser = async (req: Request, res: Response) => {
  try {
    const newUser = await update(req.user.id, req.body);

    res.status(httpStatus.OK);
    return res.json(newUser);
  } catch (err) {
    if (!(err instanceof HttpError)) {
      return errorHandler.respondWithError(httpStatus.BAD_REQUEST, err, res);
    }

    if (err instanceof BadRequestError) {
      res.status(httpStatus.BAD_REQUEST);
    }
    if (err instanceof ForbiddenError) {
      res.status(httpStatus.FORBIDDEN);
    }
    return res.json({ error: err.message });
  }
};

export const authCheck = async (req: Request, res: Response) => {
  try {
    if (!req.isLoggedIn) {
      throw new Error('Bad user login on userController.authCheck');
    }
    if (req.user == null || typeof req.user.id === 'undefined') {
      throw new Error('Invalid user.');
    }

    res.status(httpStatus.OK);
    return res.send('OK');
  } catch (err) {
    return errorHandler.respondWithError(httpStatus.OK, err, res, 'Your authcheck failed');
  }
};

export const getThreads = async (req: Request, res: Response) => {
  const userId = req.params.id;
  const hideNsfw = req.query.hideNsfw || false;

  const userLookup = await knex('Users')
    .select('id')
    .where('id', userId)
    .where('deleted_at', null)
    .first();

  if (typeof userLookup === 'undefined') {
    res.status(httpStatus.NOT_FOUND);
    return res.json({ error: 'Not found.' });
  }

  const { Tag: TagModel, Thread: ThreadModel } = datasource().models;

  const permissionCodes = await getUserPermissionCodes(req.user?.id);
  const threadScopes = [{ method: ['byUser', req.user?.id, permissionCodes] }];
  const whereCondition = {
    user_id: userId,
    '$Tags.id$': {
      [Sequelize.Op.or]: [
        null,
        !hideNsfw,
        {
          [Sequelize.Op.ne]: 1,
        },
      ],
    },
  };

  const offset = Number(req.params.page) * threadsPerPage - threadsPerPage;

  const query = await ThreadModel.scope(threadScopes).findAndCountAll({
    attributes: ['id'],
    limit: threadsPerPage,
    offset,
    subQuery: false,
    where: whereCondition,
    include: {
      attributes: ['id'],
      model: TagModel,
      required: false,
    },
    order: [['created_at', 'desc']],
  });

  const threadIds = query.rows.map((thread) => thread.id);

  const flags = [
    ThreadFlag.RETRIEVE_SHALLOW,
    ThreadFlag.INCLUDE_LAST_POST,
    ThreadFlag.INCLUDE_POST_COUNT,
  ];

  if (hideNsfw) {
    flags.push(ThreadFlag.INCLUDE_TAGS);
  }

  const threads = await new ThreadRetriever(threadIds, flags, {
    userId: req.user?.id,
  }).getObjectArray();

  return ResponseStrategy.send(res, {
    totalThreads: query.count,
    currentPage: Number(req.params.page) || 1,
    threads,
  });
};

export const getPosts = async (req: Request, res: Response) => {
  const postsPerPage = 40;
  const userId = req.params.id;
  const offset = req.params.page ? Number(req.params.page) * postsPerPage - postsPerPage : 0;
  const hideNsfw = req.query.hideNsfw || false;
  const { Post: PostModel } = datasource().models;

  const userLookup = await knex('Users')
    .select('id')
    .where('id', userId)
    .where('deleted_at', null)
    .first();

  if (typeof userLookup === 'undefined') {
    res.status(httpStatus.NOT_FOUND);
    return res.json({ error: 'Not found.' });
  }

  const permissionCodes = await getUserPermissionCodes(req.user?.id);
  const postScopes = [{ method: ['byUser', userId, permissionCodes, hideNsfw] }];

  const query = await PostModel.scope(postScopes).findAndCountAll({
    attributes: ['id'],
    limit: postsPerPage,
    offset,
    subQuery: false,
    order: [['id', 'desc']],
  });

  const postIds = query.rows.map((post) => post.id);
  const allPosts = await knex
    .from('Posts as p')
    .count('p.id as count')
    .where('user_id', userId)
    .first();

  const posts = await new PostRetriever(postIds, [PostFlag.INCLUDE_THREAD], {
    userId: req.user?.id,
  }).getObjectArray();

  return ResponseStrategy.send(res, {
    totalPosts: allPosts['count'],
    currentPage: Number(req.params.page) || 1,
    posts,
  });
};

export const getBans = async (req: Request, res: Response) => {
  const userId = req.params.id;

  const userLookup = await knex('Users')
    .select('id')
    .where('id', userId)
    .where('deleted_at', null)
    .first();

  if (typeof userLookup === 'undefined') {
    res.status(httpStatus.NOT_FOUND);
    return res.json({ error: 'Not found.' });
  }

  const query = await knex
    .from('Bans as b')
    .select('b.id')
    .where('user_id', userId)
    .orderBy('b.id', 'desc');

  const banIds = query.map((ban) => ban.id);

  const bans = await new BanRetriever(banIds, [
    BanFlag.INCLUDE_POST,
    BanFlag.INCLUDE_THREAD,
  ]).getObjectArray();
  return ResponseStrategy.send(res, bans);
};

export const show = async (req: Request, res: Response) => {
  const userId = req.params.id;

  const userLookup = await knex('Users')
    .select('id')
    .where('id', userId)
    .where('deleted_at', null)
    .first();

  if (typeof userLookup === 'undefined') {
    res.status(httpStatus.NOT_FOUND);
    return res.json({ error: 'Not found.' });
  }

  const user = await new UserRetriever(Number(userId), [
    UserFlag.INCLUDE_DONATION_UPGRADE_EXPIRATION,
  ]).getSingleObject();
  return ResponseStrategy.send(res, user);
};

export const syncData = async (req: Request, res: Response) => {
  try {
    if (!req.isLoggedIn) {
      throw new Error('Bad user login for user data sync.');
    }
    if (!req.user) {
      throw new Error('Invalid user.');
    }

    interface UserDataSyncResponse {
      id: number;
      username: string;
      usergroup: number;
      isBanned: boolean;
      createdAt: Date;
      banInfo?: { banMessage: string; expiresAt: Date; threadId: number; postContent: string };
      avatarUrl: string;
      backgroundUrl: string;
      subscriptions?: Array<object>;
      subscriptionIds?: Array<number>;
      mentions?: Array<object>;
      reports?: number;
      role?: object;
    }
    const userDataSyncResponse: UserDataSyncResponse = {
      id: req.user.id,
      username: req.user.username,
      usergroup: req.user.usergroup,
      isBanned: req.user.isBanned,
      avatarUrl: req.user.avatar_url,
      backgroundUrl: req.user.background_url,
      createdAt: req.user.createdAt,
    };

    if (req.user.isBanned) {
      const bans = await knex('Bans')
        .select(
          'ban_reason',
          'expires_at',
          'Posts.thread_id',
          knex.raw('(SUBSTRING(Posts.content, 1, 250)) as postContent')
        )
        .leftJoin('Posts', 'Bans.post_id', 'Posts.id')
        .where({
          'Bans.user_id': req.user.id,
        })
        .orderBy('Bans.created_at', 'desc')
        .limit(1);

      const {
        ban_reason: banMessage,
        expires_at: expiresAt,
        thread_id: threadId,
        postContent,
      } = bans[0];

      if (banMessage) {
        userDataSyncResponse.banInfo = {
          banMessage,
          expiresAt,
          threadId,
          postContent,
        };
      }
    }

    const userRetriever = new UserRetriever(req.user.id, [
      UserFlag.INCLUDE_STRIPE_CUSTOMER_ID,
      UserFlag.INCLUDE_DONATION_UPGRADE_EXPIRATION,
    ]);
    const cachedUser = await userRetriever.getSingleObject();

    //
    // cache invalidation for bans
    //
    if (cachedUser.banned !== req.user.isBanned) {
      userRetriever.invalidate();
    }

    if (!req.user.isBanned) {
      // get mentions
      userDataSyncResponse.mentions = await getMentionsQuery(req.user.id);

      // get alerts
      const alerts = await getAlerts(req.user.id, 1);
      userDataSyncResponse.subscriptions = alerts.alerts;
      userDataSyncResponse.subscriptionIds = alerts.ids;

      // add reports if the user has the correct permission
      const userRole = await new RoleRetriever(cachedUser.role?.id, [
        RoleFlag.INCLUDE_PERMISSION_CODES,
      ]).getSingleObject();
      if (userRole?.permissionCodes?.includes('report-view')) {
        userDataSyncResponse.reports = Number(await countOpenReportsQuery());
      }
    }

    // knockout gold check for users with a stripe customer ID
    // if the user has a promotable role (can be upgraded to gold),
    // or if the user is already gold (via their role)
    // check their active stripe subscription to see if we need to take any action
    if (cachedUser.stripeCustomerId) {
      const promotableRoleCodes: string[] = [RoleCode.LIMITED_USER, RoleCode.BASIC_USER];
      const goldPromotable = promotableRoleCodes.includes(cachedUser.role?.code);
      const userIsGold = cachedUser.role?.code === RoleCode.PAID_GOLD_USER;

      if (goldPromotable || userIsGold) {
        const currentSubscription = await new StripeClient().findGoldSubscription(req.user.id);
        const subscriptionIsActive = currentSubscription?.status === 'active';
        const donationUpgradeExpired = new Date(cachedUser.donationUpgradeExpiresAt) < new Date();

        // if the user has a gold-promotable rule and we got an active subscription, promote them to gold
        if (goldPromotable && subscriptionIsActive) {
          await promoteUserToGold(req.user.id);
          createEvent(req.user.id, EventType.GOLD_EARNED, req.user.id, req.app.get('io'));
          // if the user is already gold but their subscription is not active and any donation upgrade has expired,
          // demote them from gold
        } else if (userIsGold && !subscriptionIsActive && donationUpgradeExpired) {
          await demoteUserFromGold(req.user.id);
          createEvent(req.user.id, EventType.GOLD_LOST, req.user.id, req.app.get('io'));
        }
      }
    }

    // add role to response
    userDataSyncResponse.role = cachedUser.role;

    res.status(httpStatus.OK);
    return res.json(userDataSyncResponse);
  } catch (error) {
    return errorHandler.respondWithError(httpStatus.OK, error, res, 'Your authcheck failed');
  }
};

export const getTopRatings = async (req: Request, res: Response) => {
  try {
    const userId = req.params.id;

    const ratingsHiddenForUser = await knex('Users')
      .select('hide_profile_ratings')
      .where({ id: userId, deleted_at: null });

    if (!ratingsHiddenForUser[0] || ratingsHiddenForUser[0].hide_profile_ratings === 1) {
      res.status(httpStatus.OK);
      return res.json([]);
    }

    const cached = await redis.getAsync(`top-ratings-${userId}`);

    if (cached) {
      res.status(httpStatus.OK);
      return res.send(cached);
    }

    const ratingsLookup = await knex('Posts')
      .select('Ratings.rating_id')
      .count('Ratings.rating_id as count')
      .leftJoin('Threads', 'Posts.thread_id', 'Threads.id')
      .rightJoin('Ratings', 'Ratings.post_id', 'Posts.id')
      .whereRaw(
        `
      Posts.user_id = ?
    `,
        [userId]
      )
      .groupBy('rating_id');

    const ratingsNamed = ratingsLookup.map((val) => {
      if (ratingList[val.rating_id]) {
        return {
          name: ratingList[val.rating_id].short,
          count: val.count,
        };
      }
      return {};
    });

    redis.setex(`top-ratings-${userId}`, 168000, JSON.stringify(ratingsNamed));

    res.status(httpStatus.OK);
    return res.json(ratingsNamed);
  } catch (error) {
    return errorHandler.respondWithError(httpStatus.INTERNAL_SERVER_ERROR, error, res);
  }
};

export const updateProfileRatingsDisplay = async (req: Request, res: Response) => {
  try {
    if (!req.isLoggedIn || !req.user || req.user.isBanned) {
      res.status(httpStatus.FORBIDDEN);
      res.json({ error: 'Forbidden' });
      return;
    }

    const hide = req.body.hideProfileRatings === true;
    const boolValue = hide ? 1 : 0;

    await knex('Users').update({ hide_profile_ratings: boolValue }).where({ id: req.user.id });

    res.status(httpStatus.OK);
    res.json({ message: 'Profile ratings preference saved.' });
  } catch (err) {
    errorHandler.respondWithError(
      httpStatus.BAD_REQUEST,
      err,
      res,
      'You have provided invalid data.'
    );
  }
};

export const getProfileRatingsDisplay = async (req: Request, res: Response) => {
  try {
    if (!req.isLoggedIn || !req.user || req.user.isBanned) {
      res.status(httpStatus.FORBIDDEN);
      res.json({ error: 'Forbidden' });
      return;
    }

    const ratingsHiddenForUser = await knex('Users')
      .select('hide_profile_ratings')
      .where({ id: req.user.id });

    if (!ratingsHiddenForUser[0]) {
      throw new Error('Could not find data for user.');
    }

    res.status(httpStatus.OK);
    res.json({ ratingsHiddenForUser: ratingsHiddenForUser[0].hide_profile_ratings === 1 });
  } catch (err) {
    errorHandler.respondWithError(httpStatus.BAD_REQUEST, err, res, 'Could not find data for user');
  }
};

export const wipeAccount = async (userId: number, reason: string, wipedBy: number, io: Server) => {
  await banUser(0, userId, reason, wipedBy, io, null, true);

  await Promise.all([
    wipePosts(userId),
    wipePostEdits(userId),
    scrubMessages(userId),
    scrubProfileComments(userId),
    scrubUserProfiles(userId),
    scrubUser(userId),
    deleteRatings(userId),
    invalidateCache(userId),
  ]);

  return { message: 'User account data has been wiped.' };
};

export const deleteAccount = async (req: Request, res: Response) => {
  try {
    await deleteUser(req.params.id);

    res.status(httpStatus.OK);
    res.json({ message: 'User account deletion is now in progress.' });
  } catch (err) {
    errorHandler.respondWithError(httpStatus.BAD_REQUEST, err, res, 'Could not find data for user');
  }
};
