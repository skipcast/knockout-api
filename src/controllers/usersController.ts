import { Request, Response } from 'express';
import ResponseStrategy from '../helpers/responseStrategy';
import { userHasPermissions } from '../helpers/user';
import UserRetriever, { UserFlag } from '../retriever/user';
import knex from '../services/knex';

// eslint-disable-next-line import/prefer-default-export
export const search = async (req: Request, res: Response) => {
  const offset = req.params.page ? Number(req.params.page) * 40 - 40 : 0;
  const filter = req.query.filter || null;
  const modTable =
    req.query.modTable && (await userHasPermissions(req.user.id, ['full-user-info-view']));

  const setBuilderParams = (builder) => {
    if (!filter) return;
    builder.where('u.username', 'like', `${filter}%`);
    if (!modTable) builder.andWhereNot('u.id', req.user.id);
  };

  const allUsers = await knex
    .from('Users as u')
    .count('u.id as count')
    .where(setBuilderParams)
    .first();

  const query = await knex
    .from('Users as u')
    .select('u.id')
    .where(setBuilderParams)
    .orderBy('u.id', 'desc')
    .limit(40)
    .offset(offset);

  const userIds = query.map((user) => user.id);

  const users = await new UserRetriever(
    userIds,
    modTable ? [] : [UserFlag.HIDE_BANNED]
  ).getObjectArray();
  return ResponseStrategy.send(res, {
    totalUsers: allUsers['count'],
    currentPage: Number(req.params.page) || 1,
    users,
  });
};
