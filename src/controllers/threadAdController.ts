import { Request, Response } from 'express';
import httpStatus from 'http-status';

import redis from '../services/redisClient';
import knex from '../services/knex';
import errorHandler from '../services/errorHandler';

const THREAD_ADS_LIST_KEY = 'thread-ads-list';
const THREAD_ADS_LIST_TTL = 86400;

export const index = async (req: Request, res: Response) => {
  try {
    const cachedData: string = await redis.getAsync(THREAD_ADS_LIST_KEY);

    if (cachedData) {
      res.status(httpStatus.OK);
      return res.json(JSON.parse(cachedData));
    }

    const result = await knex('ThreadAds').select('id', 'description', 'query', 'imageUrl');

    redis.setex(THREAD_ADS_LIST_KEY, THREAD_ADS_LIST_TTL, JSON.stringify(result));

    res.status(httpStatus.OK);
    return res.json(result);
  } catch (exception) {
    return errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const random = async (req: Request, res: Response) => {
  try {
    const cachedData: string = await redis.getAsync(THREAD_ADS_LIST_KEY);

    if (cachedData) {
      const result = JSON.parse(cachedData);
      const item = result[Math.floor(Math.random() * result.length)];

      res.status(httpStatus.OK);
      return res.json(item);
    }

    const result = await knex('ThreadAds').select('description', 'query', 'imageUrl');

    redis.setex(THREAD_ADS_LIST_KEY, THREAD_ADS_LIST_TTL, JSON.stringify(result));

    res.status(httpStatus.OK);
    return res.json(result);
  } catch (exception) {
    return errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};
