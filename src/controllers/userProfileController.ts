import { Request, Response } from 'express';
import sharp from 'sharp';
import httpStatus from 'http-status';

import { BadRequestError } from 'routing-controllers';
import { Server } from 'socket.io';
import {
  UpdateUserProfileRequest,
  UserProfileCommentPage,
  CreateUserProfileCommentRequest,
  UserProfileComment,
  NotificationType,
  EventType,
} from 'knockout-schema';
import knex from '../services/knex';
import fileStore from '../helpers/fileStore';
import { NODE_ENV } from '../../config/server';
import ProfileRetriever from '../retriever/userProfile';
import errorHandler from '../services/errorHandler';
import ProfileCommentRetriever from '../retriever/profileComment';
import datasource from '../models/datasource';
import validateProfileCommentLength from '../validations/profileComment';
import { createNotification } from './notificationController';
import { createSocketEvent } from './eventLogController';

const COMMENTS_PER_PAGE = 10;

const { ProfileComment } = datasource().models;

export const get = (id: number) => new ProfileRetriever(id).getSingleObject();

export const update = async (id: number, body: UpdateUserProfileRequest) => {
  const {
    bio: headingText,
    social: { website, discord, github, gitlab, youtube, twitch, twitter, tumblr },
    disableComments,
  } = body;

  const steam = body.social?.steam?.url;

  interface UserProfileDatabaseInsertBody {
    user_id: number;
    heading_text?: String;
    personal_site?: String;
    background_url?: String;
    background_type?: 'cover' | 'tiled';
    steam?: String;
    discord?: String;
    github?: String;
    youtube?: String;
    twitter?: String;
    twitch?: String;
    gitlab?: String;
    tumblr?: String;
    disable_comments?: boolean;
  }

  const insertBody: UserProfileDatabaseInsertBody = {
    user_id: id,
    heading_text: headingText,
    personal_site: website,
    steam,
    discord,
    github,
    gitlab,
    youtube,
    twitch,
    twitter,
    tumblr,
    disable_comments: disableComments,
  };

  const currentProfile = await get(id);
  if (!currentProfile.id) {
    await knex('UserProfiles').insert(insertBody);
  } else {
    await knex('UserProfiles').update(insertBody).where({ user_id: id });
  }

  // invalidate cached user profile
  new ProfileRetriever(id).invalidate();

  return { message: 'Profile updated.' };
};

export const updateBackground = async (file, user, type: 'cover' | 'tiled') => {
  try {
    let backgroundImageUrl;
    if (file?.buffer && file?.mimetype?.startsWith('image/')) {
      const { size } = await sharp(file.buffer).metadata();

      if (size < 2000000) {
        const fileName = `${user.id}-profile-background.webp`;
        const filePath = NODE_ENV === 'production' ? `image/${fileName}` : `avatars/${fileName}`;

        const encodedImageBuffer = await sharp(file.buffer).toFormat('webp').toBuffer();

        await fileStore.storeBuffer(encodedImageBuffer, filePath, 'image/webp');

        backgroundImageUrl = fileName;
      } else {
        throw new BadRequestError('Background over 2MB.');
      }
    }
    if (backgroundImageUrl) {
      const currentProfile = await get(user.id);
      if (!currentProfile.id) {
        await knex('UserProfiles').insert({
          user_id: user.id,
          background_url: backgroundImageUrl,
          background_type: type,
        });
      } else {
        await knex('UserProfiles')
          .update({
            background_url: backgroundImageUrl,
            background_type: type,
          })
          .where({ user_id: user.id });
      }

      // invalidate cached user profile
      new ProfileRetriever(user.id).invalidate();
    }
  } catch (error) {
    throw new BadRequestError('Could not upload this image.');
  }

  return { message: 'Background updated.' };
};

export const updateHeader = async (file, id: number) => {
  let header;
  const HEADER_SIZE_LIMIT = 3000000;
  try {
    if (file?.buffer && file?.mimetype?.startsWith('image/')) {
      const { size } = await sharp(file.buffer).metadata();

      if (size < HEADER_SIZE_LIMIT) {
        const fileName = `${id}-profile-header.webp`;
        const filePath = NODE_ENV === 'production' ? `image/${fileName}` : `avatars/${fileName}`;

        const encodedImageBuffer = await sharp(file.buffer).toFormat('webp').toBuffer();

        await fileStore.storeBuffer(encodedImageBuffer, filePath, 'image/webp');

        header = fileName;
      } else {
        throw new BadRequestError('Header over 3MB.');
      }
    }
    if (header) {
      const currentProfile = await get(id);
      if (!currentProfile.id) {
        await knex('UserProfiles').insert({
          user_id: id,
          header,
        });
      } else {
        await knex('UserProfiles')
          .update({
            header,
          })
          .where({ user_id: id });
      }

      // invalidate cached user profile
      new ProfileRetriever(id).invalidate();
    }
  } catch (error) {
    throw new BadRequestError('Could not upload this image.');
  }

  return { header };
};

export const removeHeader = async (id: number) => {
  const fileName = `${id}-profile-header.webp`;
  const filePath = NODE_ENV === 'production' ? `image/${fileName}` : `avatars/${fileName}`;
  await fileStore.deleteFile(filePath);

  await knex('UserProfiles').where({ user_id: id }).update({
    header: null,
  });

  // invalidate cached user profile
  new ProfileRetriever(id).invalidate();

  return { message: 'Profile header removed.' };
};

export const updateProfile = async (req: Request, res: Response) => {
  interface UserProfileUpdateRequestBody {
    headingText?: string;
    personalSite?: string;
    backgroundType?: 'cover' | 'tiled';
    disableComments?: boolean;
  }

  try {
    const requestBody: UserProfileUpdateRequestBody = req.body;

    await update(req.user.id, {
      bio: requestBody.headingText,
      social: { website: requestBody.personalSite },
      disableComments: requestBody.disableComments,
    });

    await updateBackground(req.file, req.user, requestBody.backgroundType);
    res.status(httpStatus.OK);
    return res.json({
      message:
        'Success. User profile updated. Background image changes might take a while to take effect - clear your cache to see the changes.',
    });
  } catch (exception) {
    if (exception instanceof BadRequestError) {
      res.status(httpStatus.UNPROCESSABLE_ENTITY);
      return res.json({ error: exception.message });
    }
    return errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

/**
 * Send the loaded user data to the client
 */
export const show = async (req: Request, res: Response) => {
  interface UserProfileShowRequestBody {
    id: number;
  }

  try {
    const requestBody: UserProfileShowRequestBody = req.params;

    // try to get cached user profile
    const userProfile = await get(requestBody.id);

    const result = userProfile['headingText']
      ? userProfile
      : {
          ...userProfile,
          userId: userProfile.id,
          headingText: userProfile.bio,
          personalSite: userProfile.social?.website,
          backgroundUrl: userProfile.background?.url,
          backgroundType: userProfile.background?.type,
        };
    res.status(httpStatus.OK);
    res.json(result);
  } catch (error) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, error, res);
  }
};

export const getComments = async (id: number, page = 1): Promise<UserProfileCommentPage> => {
  const commentIds = await knex('ProfileComments')
    .select('id')
    .where({ user_profile: id, deleted: false })
    .orderBy('id', 'desc')
    .limit(COMMENTS_PER_PAGE)
    .offset(COMMENTS_PER_PAGE * (page - 1));

  const totalComments = await knex('ProfileComments')
    .count('id as total')
    .where({ user_profile: id, deleted: false })
    .first();

  return {
    totalComments: Number(totalComments.total),
    page,
    comments: await new ProfileCommentRetriever(
      commentIds.map((comment) => comment.id)
    ).getObjectArray(),
  };
};

export const createComment = async (
  id: number,
  body: CreateUserProfileCommentRequest,
  author: number,
  io: Server
): Promise<UserProfileComment> => {
  if (!validateProfileCommentLength(body.content)) {
    throw new BadRequestError('Comment is too long.');
  }
  const comment = await ProfileComment.create({
    user_profile: id,
    author,
    content: body.content,
  });
  if (id !== author) {
    await createNotification(NotificationType.PROFILE_COMMENT, id, comment.id);
    createSocketEvent(author, EventType.PROFILE_COMMENT_CREATED, id, io);
  }
  return new ProfileCommentRetriever(comment.id).getSingleObject();
};

export const deleteComment = async (profileId: number, commentId: number) => {
  await knex('ProfileComments')
    .update({
      deleted: true,
    })
    .where({
      id: commentId,
      user_profile: profileId,
    });

  return { message: 'Comment deleted.' };
};
