import { Request, Response } from 'express';
import httpStatus from 'http-status';

import redis from '../services/redisClient';
import knex from '../services/knex';
import errorHandler from '../services/errorHandler';

const MOTD_KEY = 'motd';

// Create a new MOTD rather than update
// so we can see MOTD histories
export const create = async (req: Request, res: Response) => {
  try {
    const { message, buttonName, buttonLink } = req.body;
    const userId = req.user.id;
    const now = new Date();

    await knex('MessageOfTheDays').insert({
      message: message.trim(),
      buttonName: buttonName.trim(),
      buttonLink: buttonLink.trim(),
      created_by: userId,
      created_at: now,
      updated_at: now,
    });

    redis.del(MOTD_KEY);

    res.status(httpStatus.CREATED);
    res.json({ message: 'MOTD updated.' });
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const latest = async (req: Request, res: Response) => {
  try {
    const cachedData: string = await redis.getAsync(MOTD_KEY);

    if (cachedData) {
      const result = JSON.parse(cachedData);

      res.status(httpStatus.OK);
      return res.json(result);
    }

    const result = await knex('MessageOfTheDays')
      .select('message', 'id', 'buttonName', 'buttonLink')
      .orderBy('created_at', 'desc')
      .limit(1);

    redis.set(MOTD_KEY, JSON.stringify(result));

    res.status(httpStatus.OK);
    return res.json(result);
  } catch (exception) {
    return errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};
