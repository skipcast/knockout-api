import * as subforumGetAllController from './subforum/getAll';
import * as subforumGetThreadsController from './subforum/getThreads';

import * as reportsGetAllController from './report/getAll';
import * as reportsOpenCountController from './report/getOpenCount';
import * as reportGetController from './report/get';
import * as reportCreateController from './report/create';
import * as reportResolveController from './report/resolve';

import * as postController from './postController';
import * as threadController from './threadController';
import * as authController from './authController';
import * as userController from './userController';
import * as usersController from './usersController';
import * as subforumController from './subforumController';
import * as ratingsController from './ratingsController';
import * as banController from './banController';
import * as alertController from './alertController';
import * as moderationController from './moderationController';
import * as imageController from './imageController';
import * as statsController from './statsController';
import * as eventLogController from './eventLogController';
import * as mentionsController from './mentionsController';
import * as readThreadController from './readThreadController';
import * as debugController from './debugController';
import * as userProfileController from './userProfileController';
import * as tagController from './tagController';
import * as threadAdController from './threadAdController';
import * as messageOfTheDayController from './messageOfTheDayController';
import * as postImageController from './postImageController';
import * as conversationController from './conversationController';
import * as messageController from './messageController';

export {
  subforumGetAllController,
  subforumGetThreadsController,
  reportGetController,
  reportCreateController,
  reportResolveController,
  reportsGetAllController,
  reportsOpenCountController,
  postController,
  threadController,
  authController,
  subforumController,
  userController,
  usersController,
  imageController,
  moderationController,
  alertController,
  banController,
  ratingsController,
  statsController,
  eventLogController,
  readThreadController,
  mentionsController,
  debugController,
  userProfileController,
  tagController,
  threadAdController,
  messageOfTheDayController,
  postImageController,
  conversationController,
  messageController,
};
