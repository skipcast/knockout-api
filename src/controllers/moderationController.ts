import { Request, Response } from 'express';
import httpStatus from 'http-status';
import { EventType } from 'knockout-schema';
import knex from '../services/knex';
import redis from '../services/redisClient';

import UserRetriever from '../retriever/user';
import BanRetriever from '../retriever/ban';
import ThreadRetriever from '../retriever/thread';
import fileStore from '../helpers/fileStore';
import { NODE_ENV } from '../../config/server';
import { RAID_MODE_KEY } from '../constants/adminSettings';
import errorHandler from '../services/errorHandler';
import { createEvent, removeUserAvatarBg, threadStatusEvent } from './eventLogController';
import { unBanUser } from '../helpers/user';

export const getIpsByUsername = async (req: Request, res: Response) => {
  try {
    const ipAddressResults = await knex
      .select('IpAddresses.ip_address', 'Users.username', 'Users.id as userId')
      .from('IpAddresses')
      .join('Users', 'Users.id', 'IpAddresses.user_id')
      .whereRaw('Users.username LIKE ?', [`${req.body.username}%`])
      .groupBy('IpAddresses.ip_address')
      .orderBy('IpAddresses.created_at', 'desc');

    if (!ipAddressResults[0]) {
      res.status(httpStatus.OK);
      res.json([]);
      return;
    }

    // format ip_address field for the frontend
    const userInfo = ipAddressResults.reduce((acc, val) => {
      if (!acc[val.userId]) {
        acc[val.userId] = {
          id: val.userId,
          username: val.username,
          ipAddresses: [],
        };
      }

      if (val.ip_address) {
        acc[val.userId].ipAddresses = [...acc[val.userId].ipAddresses, val.ip_address];
      }

      return acc;
    }, {});

    res.status(httpStatus.OK);
    // transform object into array
    res.json([...Object.values(userInfo)]);
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const getUsernamesByIp = async (req: Request, res: Response) => {
  try {
    const result = await knex
      .select('IpAddresses.ip_address', 'Users.username', 'Users.id as id')
      .from('IpAddresses')
      .join('Users', 'Users.id', 'IpAddresses.user_id')
      .whereRaw('IpAddresses.ip_address LIKE ?', [`${req.body.ipAddress}%`])
      .groupBy('Users.username')
      .orderBy('IpAddresses.created_at', 'desc');

    res.status(httpStatus.CREATED);
    res.json(result);
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const getOpenReports = async (req: Request, res: Response) => {
  try {
    const response = await knex('Reports')
      .select(
        'Reports.id as reportId',
        'Reports.post_id',
        'Reports.reported_by as reportedBy',
        'Reports.report_reason as reportReason',
        'reportuser.username as reportUsername',
        'Posts.id as postId',
        'Posts.content as postContent',
        'Posts.user_id as postUserId',
        'Posts.created_at as postCreatedAt',
        'Posts.updated_at as postUpdatedAt',
        'Posts.thread_id as postThreadId',
        'postuser.username as postUsername',
        'postuser.avatar_url as avatarUrl',
        'postuser.created_at as userJoinDate',
        'ban.ban_reason as banReason',
        'ban.banned_by as banBannedBy',
        'ban.created_at as banCreatedAt',
        'ban.expires_at as banExpiresAt',
        'Subforums.name as subforumName',
        knex.raw(
          '(select count (*) from Posts popos where popos.thread_id = Posts.thread_id AND popos.created_at < Posts.created_at) as olderPosts'
        ),
        knex.raw(
          '(select count (*) from Bans where Bans.user_id = Posts.user_id AND expires_at > NOW()) as postUserBans'
        )
      )
      .where('solved_by', null)
      .leftJoin('Posts', 'Posts.id', 'Reports.post_id')
      .leftJoin('Threads', 'Posts.thread_id', 'Threads.id')
      .leftJoin('Subforums', 'Threads.subforum_id', 'Subforums.id')
      .leftJoin('Users as postuser', 'Posts.user_id', 'postuser.id')
      .leftJoin('Users as reportuser', 'Reports.reported_by', 'reportuser.id')
      .leftJoin('Bans as ban', 'ban.post_id', 'Reports.post_id');

    const responseWithPage = response.map((report) => {
      report.postPage = Math.floor(report.olderPosts / 20) + 1;
      return report;
    });

    res.status(httpStatus.CREATED);
    res.json(responseWithPage);
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const closeReport = async (req: Request, res: Response) => {
  try {
    await knex('Reports')
      .where({ id: req.body.reportId })
      .update({ solved_by: req.user.id, updated_at: knex.fn.now() });

    res.status(httpStatus.CREATED);
    res.json({ message: 'Report marked as solved.' });
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const changeThreadStatus = async (req: Request, res: Response) => {
  try {
    await knex('Threads').where({ id: req.body.threadId }).update(req.body.changedData);

    threadStatusEvent(
      {
        userId: req.user.id,
        threadId: req.body.threadId,
        type: req.body.changedData,
      },
      req.app.get('io')
    );

    await new ThreadRetriever(req.body.threadId).invalidate();

    res.status(httpStatus.CREATED);
    res.json({ message: 'Thread status updated.' });
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const countOpenReportsQuery = async () => {
  const result = await knex('Reports')
    .count('id')
    .where('solved_by', null)
    .then((output) => output[0]);

  return result['count(`id`)'];
};

export const countOpenReports = async (req: Request, res: Response) => {
  try {
    const result: number = Number(await countOpenReportsQuery());

    res.status(httpStatus.OK);
    res.json({ message: result });
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const removeUserImage = async (req: Request, res: Response) => {
  try {
    if (!req.body.userId) {
      throw new Error('No user id provided.');
    }
    if (!req.body.avatar && !req.body.background) {
      throw new Error('No data requested for removal.');
    }

    const fields = {
      avatar_url: req.body.avatar ? 'none.webp' : undefined,
      background_url: req.body.background ? '' : undefined,
    };

    await knex('Users')
      .where({ id: req.body.userId })
      .update({
        ...fields,
      });

    const fileName = req.body.userId + (req.body.background ? '-bg.webp' : '.webp');
    const filePath = NODE_ENV === 'production' ? `image/${fileName}` : `avatars/${fileName}`;
    await fileStore.deleteFile(filePath);

    new UserRetriever(req.body.userId).invalidate();

    removeUserAvatarBg(
      {
        userId: req.body.userId,
        avatar: req.body.avatar,
        background: req.body.background,
        removedBy: req.user.id,
      },
      req.app.get('io')
    );

    res.status(httpStatus.OK);
    res.json({ message: 'User images updated.' });
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const removeUserProfile = async (req: Request, res: Response) => {
  try {
    if (!req.body.userId) {
      throw new Error('No user id provided.');
    }

    await knex('UserProfiles').where({ user_id: req.body.userId }).del();

    // uncache the user profile
    new UserRetriever(req.body.userId).invalidate();

    createEvent(req.user.id, EventType.USER_PROFILE_REMOVED, req.body.userId, req.app.get('io'));

    res.status(httpStatus.OK);
    res.json({ message: 'User profile updated.' });
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const getLatestUsers = async (req: Request, res: Response) => {
  try {
    const response = await knex
      .select('*')
      .from('Users')
      .orderBy('Users.created_at', 'desc')
      .limit(20);

    res.status(httpStatus.OK);
    res.json(response);
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const getDashboardData = async (req: Request, res: Response) => {
  try {
    const cacheKey = 'dashboardData';
    let dashboardData: string | object = await redis.getAsync(cacheKey);

    if (typeof dashboardData === 'string') {
      dashboardData = JSON.parse(dashboardData);
    } else {
      const newUserCount = await knex
        .count('id as count')
        .from('Users')
        .whereRaw('created_at >= now() - INTERVAL 1 DAY')
        .then((result) => result[0].count);
      const newThreadCount = await knex
        .count('id as count')
        .from('Threads')
        .whereRaw('created_at >= now() - INTERVAL 1 DAY')
        .then((result) => result[0].count);

      const newPostCount = await knex
        .count('id as count')
        .from('Posts')
        .whereRaw('created_at >= now() - INTERVAL 1 DAY')
        .then((result) => result[0].count);

      const reportsCount = await knex('Reports')
        .count('id as count')
        .where('solved_by', null)
        .then((result) => result[0].count);

      const newBanCount = await knex
        .count('id as count')
        .from('Bans')
        .whereRaw('created_at >= now() - INTERVAL 1 DAY')
        .then((result) => result[0].count);

      dashboardData = {
        threads: newThreadCount,
        users: newUserCount,
        posts: newPostCount,
        reports: reportsCount,
        bans: newBanCount,
      };

      redis.setex(cacheKey, 10, JSON.stringify(dashboardData));
    }

    res.status(httpStatus.OK);
    res.json(dashboardData);
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const getFullUserInfo = async (req: Request, res: Response) => {
  try {
    const user = await knex.select('*').from('Users').where('id', req.body.userId);

    if (!user[0]) {
      res.status(httpStatus.OK);
      res.json([]);
      return;
    }

    const bans = await knex
      .select('Bans.*', 'Users.username as bannedBy')
      .from('Bans')
      .where('user_id', req.body.userId)
      .leftJoin('Users', 'Users.id', 'Bans.banned_by');

    const ipAddresses = await knex
      .select('post_id', 'ip_address')
      .from('IpAddresses')
      .join('Users', 'Users.id', 'user_id')
      .whereRaw('user_id = ?', [req.body.userId])
      .groupBy('ip_address')
      .orderBy('created_at', 'desc');

    const result = {
      ...user[0],
      bans,
      ipAddresses,
    };

    res.status(httpStatus.OK);
    res.json(result);
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const makeBanInvalid = async (req: Request, res: Response) => {
  try {
    const originalBan = await knex('Bans').select('*').first().where({ id: req.body.banId });

    const createdAt = originalBan.created_at;
    const expiresAt = originalBan.expires_at;

    const newCreatedAt = new Date(new Date(createdAt).setFullYear(2001));
    const newExpiresAt = new Date(new Date(expiresAt).setFullYear(2001));

    await knex('Bans')
      .update({ created_at: newCreatedAt, expires_at: newExpiresAt })
      .where({ id: originalBan.id });

    await unBanUser(originalBan.user_id);

    new UserRetriever(req.body.userId).invalidate();

    new BanRetriever(originalBan.id).invalidate();

    createEvent(req.user.id, EventType.USER_UNBANNED, req.body.userId, req.app.get('io'));

    res.status(httpStatus.OK);
    res.json({ message: 'Ban year set to 2001.' });
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const setAdminSettings = async (req: Request, res: Response) => {
  try {
    const newValue = req.body.raidMode;
    const SECONDS_IN_3_DAYS = 259200;
    if (newValue !== undefined) {
      if (newValue === 'true' || newValue === true) {
        redis.setex(RAID_MODE_KEY, SECONDS_IN_3_DAYS, newValue);
      } else {
        redis.del(RAID_MODE_KEY);
      }
      return res.sendStatus(httpStatus.OK);
    }
    return res.sendStatus(httpStatus.NOT_MODIFIED);
  } catch (exception) {
    return errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const getAdminSettings = async (req: Request, res: Response) => {
  try {
    const raidMode = await redis.getAsync(RAID_MODE_KEY);
    res.send({ raidMode });
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};
