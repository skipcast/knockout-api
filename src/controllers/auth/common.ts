import querystring from 'querystring';
import jwt from 'jsonwebtoken';
import ms from 'ms';

import { Response } from 'express';
import httpStatus from 'http-status';
import datasource from '../../models/datasource';
import { JWT_SECRET as jwtSecret } from '../../../config/server';
import knex from '../../services/knex';
import redis from '../../services/redisClient';
import { RAID_MODE_KEY } from '../../constants/adminSettings';
import Scamalytics from '../../services/scamalytics';
import RoleRetriever from '../../retriever/role';

const { IpAddress, User } = datasource().models;
/**
 * Generate a JWT token with a two-week expiry time
 *
 * @param user The JWT subject
 */
function generateToken(user: { id: number; usergroup: number }) {
  const payload = {
    id: user.id,
  };
  return jwt.sign(payload, jwtSecret, {
    algorithm: 'HS256',
    expiresIn: '2 weeks',
  });
}

/**
 * Updates the user token via a cookie
 */
function updateToken(res: Response, token: string) {
  res.cookie('knockoutJwt', token, {
    maxAge: ms('2 weeks'),
    httpOnly: true,
    secure: true,
    sameSite: 'none',
  });
}

/**
 * Create a plain user without a username (pending setup)
 */
async function createUser(externalProvider: string, externalId: string) {
  const newUser = await User.create({ usergroup: 1 });

  await knex('ExternalAccounts').insert({
    user_id: newUser.id,
    provider: externalProvider,
    external_id: externalId,
  });

  return newUser;
}

/**
 * Generate and send a token for the user associated with the external provider ID,
 * creating said user if it doesn't already exist
 *
 * @param res Optional response for setting the cookie
 * and sending the final redirect
 * @param ip Optional param to log the IP of the user who is logging in
 */
async function loginWithExternalId(
  externalProvider: string,
  externalId: string,
  res: Response,
  ip?: string
) {
  let user = await knex('Users as user')
    .select(
      'user.id',
      'username',
      'avatar_url',
      'background_url',
      'usergroup',
      'role_id',
      'deleted_at'
    )
    .join('ExternalAccounts as acc', 'user.id', 'acc.user_id')
    .where('acc.provider', externalProvider)
    .where('acc.external_id', externalId)
    .first();

  if (!user) {
    const raidModeEnabled = await redis.getAsync(RAID_MODE_KEY);

    if (raidModeEnabled) {
      return res.sendStatus(httpStatus.FORBIDDEN);
    }

    if (await new Scamalytics(ip).isRisky()) {
      console.log('VPN detected', ip);
      return res.sendStatus(httpStatus.FORBIDDEN);
    }

    console.log(`Creating new user for account ${externalProvider}:${externalId}`);
    user = (await createUser(externalProvider, externalId)).dataValues;
  }

  if (user.deleted_at) {
    res.status(httpStatus.UNAUTHORIZED);
    return res.send('Invalid account.');
  }

  const token = generateToken(user);

  if (res) {
    updateToken(res, token);

    const userObject = {
      id: user.id,
      username: user.username,
      avatar_url: user.avatar_url,
      background_url: user.background_url,
      usergroup: user.usergroup,
      role: await new RoleRetriever(user.role_id).getSingleObject(),
    };

    const stringifiedUser = JSON.stringify(userObject);
    const base64User = encodeURIComponent(stringifiedUser);
    const queryString = querystring.stringify({ user: base64User });

    res.redirect(`/auth/finish?${queryString}`);
  }

  // log IP
  IpAddress.create({
    ip_address: ip,
    user_id: user.id,
  });

  return { user, token };
}

export { generateToken, updateToken, createUser, loginWithExternalId };
