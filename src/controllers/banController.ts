import { Request, Response } from 'express';

import httpStatus from 'http-status';
import { Server } from 'socket.io';
import { EventType } from 'knockout-schema';
import knex from '../services/knex';
import errorHandler from '../services/errorHandler';

import UserRetriever from '../retriever/user';
import { getBannedUserRole } from '../helpers/role';
import { createEvent } from './eventLogController';
import datasource from '../models/datasource';
import { getRoleIdsWithPermissions } from '../helpers/permissions';
import PostRetriever, { PostFlag } from '../retriever/post';

const { User } = datasource().models;

export const banUser = async (
  length: number,
  userId: number,
  reason: string,
  bannedBy: number,
  io: Server,
  postId?: number,
  accountWipe = false
) => {
  const expiresAt = new Date();

  if (length <= 0) {
    // If the ban length is 0 or less, perma ban
    expiresAt.setUTCFullYear(2100);
  } else {
    expiresAt.setUTCHours(expiresAt.getUTCHours() + length);
  }

  const banId = await knex('Bans').insert({
    user_id: userId,
    expires_at: expiresAt,
    ban_reason: reason,
    post_id: postId,
    banned_by: bannedBy,
  });

  let type: EventType = EventType.USER_BANNED;
  if (accountWipe) type = EventType.USER_WIPED;

  // if the ban is associated with a post, only broadcast
  // the ban event to users that can view the post
  let roleIds;
  if (postId) {
    const {
      thread: { subforumId },
    } = (await new PostRetriever(postId, [PostFlag.INCLUDE_THREAD]).getSingleObject()) as any;
    roleIds = await getRoleIdsWithPermissions([`subforum-${subforumId}-view`]);
  }

  createEvent(bannedBy, type, banId[0], io, {}, roleIds);

  // update user role
  const bannedUserRole = await getBannedUserRole();
  const user = await User.findOne({ where: { id: userId } });
  await user.update({ roleId: bannedUserRole.id });
  await user.save();

  new UserRetriever(userId).invalidate();

  return expiresAt;
};

export const store = async (req: Request, res: Response) => {
  try {
    const { banLength } = req.body;

    const parsedBanLength = Number(banLength);

    const expiresAt = await banUser(
      parsedBanLength,
      req.body.userId,
      req.body.banReason,
      req.user.id,
      req.app.get('io'),
      req.body.postId
    );

    res.status(httpStatus.CREATED);
    res.json({
      message: `User banned. Unban date: ${expiresAt.toUTCString()}`,
      banExpiresAt: expiresAt.getTime(),
    });
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};
