/* eslint-disable no-underscore-dangle */

import { Request, Response } from 'express';

import httpStatus from 'http-status';
import {
  imageMetadata,
  avatarGifToWebp,
  avatarToWebp,
  backgroundToWebp,
  isValidGif,
} from '../helpers/image';
import fileStore from '../helpers/fileStore';
import { NODE_ENV } from '../../config/server';
import errorHandler from '../services/errorHandler';
import UserRetriever from '../retriever/user';

// all of this should be extracted into a microservice
export const show = async (req: Request, res: Response) => {
  try {
    res.status(httpStatus.OK);
    res.setHeader('Cache-Control', 'public, max-age=120');
    res.setHeader('Expires', new Date(Date.now() + 120000).toUTCString());
    res.sendFile(`${global.__basedir}/static/avatars/${req.params.filename}`);
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const avatarUpload = async (req: Request, res: Response) => {
  try {
    if (!req?.file?.mimetype?.startsWith('image/')) {
      res.status(httpStatus.UNPROCESSABLE_ENTITY);
      res.json({ error: 'Unprocessable entity.' });
      return;
    }

    const fileName = `${req.user.id}.webp`;
    const filePath = NODE_ENV === 'production' ? `image/${fileName}` : `avatars/${fileName}`;

    const metadata = await imageMetadata(req.file.buffer);

    // validate file matching all requirements for animated avatars
    if (isValidGif(metadata)) {
      // run sharp with no resizing - important to run the validations first
      const encodedImageBuffer = await avatarGifToWebp(req.file.buffer);

      await fileStore.storeBuffer(encodedImageBuffer, filePath, 'image/webp');

      new UserRetriever(req.user.id).invalidate();

      res.status(httpStatus.CREATED);
      res.json({ message: `${req.user.id}.webp` });
      return;
    }

    // regular sharp conversion. no animation support
    const encodedImageBuffer = await avatarToWebp(req.file.buffer);

    await fileStore.storeBuffer(encodedImageBuffer, filePath, 'image/webp');

    res.status(httpStatus.CREATED);
    res.json({ message: `${req.user.id}.webp` });
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const backgroundUpload = async (req: Request, res: Response) => {
  try {
    if (!req.file || !req.file.mimetype || !req.file.mimetype.startsWith('image/')) {
      res.status(httpStatus.UNPROCESSABLE_ENTITY);
      res.json({ error: 'Unprocessable entity.' });
      return;
    }

    const fileName = `${req.user.id}-bg.webp`;
    const filePath = NODE_ENV === 'production' ? `image/${fileName}` : `avatars/${fileName}`;
    const encodedImageBuffer = await backgroundToWebp(req.file.buffer);

    await fileStore.storeBuffer(encodedImageBuffer, filePath, 'image/webp');

    new UserRetriever(req.user.id).invalidate();

    res.status(httpStatus.CREATED);
    res.json({ message: `${req.user.id}-bg.webp` });
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};
