import { Request, Response, User } from 'express';
import httpStatus from 'http-status';
import { BadRequestError, ForbiddenError } from 'routing-controllers';
import { Server } from 'socket.io';
import ms from 'ms';
import {
  CreatePostRequest,
  NotificationType,
  EventType,
  UpdatePostRequest,
  Post as PostSchema,
  THREAD_POST_LIMIT,
} from 'knockout-schema';
import knex from '../services/knex';

import responseStrategy from '../helpers/responseStrategy';
import postMentionFinder from '../helpers/postMentionFinder';
import { getCountryName } from '../helpers/geoipLookup';
import { validateAppName } from '../validations/post';
import PostRetriever, { PostFlag } from '../retriever/post';
import ThreadRetriever from '../retriever/thread';
import errorHandler from '../services/errorHandler';
import { createNotification } from './notificationController';
import { userHasPermissions } from '../helpers/user';

import { createSocketEvent } from './eventLogController';
import { getRoleIdsWithPermissions } from '../helpers/permissions';
import { threadPostRoom } from '../handlers/threadPostHandler';

const datasource = require('../models/datasource');

const { validateThreadStatus, validatePostLength } = require('../validations/post');
const { validateUserFields } = require('../validations/user');

const { Post, Rating, IpAddress } = datasource().models;

const storePostEdit = async (
  originalPostId: number,
  userId: number,
  editReason: string,
  content: string
) => {
  const { PostEdit } = datasource().models;
  const originalPost = await new PostRetriever(originalPostId, [
    PostFlag.RETRIEVE_SHALLOW,
  ]).getSingleObject();

  const createdAt = new Date(originalPost.createdAt);
  const diff = Date.now() - createdAt.getTime();

  const pastEditBufferTime = diff > ms('5 minutes');

  // if we aren't past the edit buffer time, bail out
  if (!pastEditBufferTime) {
    return;
  }

  // if this is the first update (no corresponding PostEdit records),
  // store the original post as a PostEdit record so we have access to it
  const firstEdit = !(await PostEdit.findOne({
    where: { postId: originalPost.id },
    attributes: ['id'],
  }));

  if (firstEdit) {
    await PostEdit.create({
      postId: originalPost.id,
      userId: originalPost.user,
      editReason: '',
      content: originalPost.content,
      createdAt: new Date(originalPost.createdAt),
    });
  }

  // create the post edit record
  await PostEdit.create({
    postId: originalPost.id,
    userId,
    editReason,
    content,
  });
};

export const store = async (
  body: CreatePostRequest,
  user: User,
  ipInfo: string,
  io?: Server
): Promise<PostSchema> => {
  const thread = await new ThreadRetriever(body.thread_id).getSingleObject();
  const skipPostValidations = await userHasPermissions(user.id, [
    `subforum-${thread.subforumId}-post-bypass-validations`,
  ]);

  // validations for a valid post
  if (
    !skipPostValidations &&
    (!(await validateThreadStatus(body.thread_id)) || !validatePostLength(body.content))
  ) {
    throw new BadRequestError('Failed validations.');
  }

  if (!validateUserFields(user)) {
    throw new BadRequestError('Invalid user.');
  }

  // client name logic
  let appName: string | null;

  if (body.app_name) {
    appName = validateAppName(body.app_name) ? body.app_name : null;
  } else {
    appName = null;
  }

  let post;
  let lastPostId = null;
  let automerge = false;

  // Start transaction here so we can avoid race conditions with posts
  await knex.transaction(async (trx) => {
    // automerge logic
    // if the latest post in a thread is by this
    // user, merge posts instead of creating a new one
    const lastPostQuery = await trx
      .from('Posts as po')
      .select('po.id', 'po.content', 'po.user_id')
      .where(
        knex.raw('po.created_at >= DATE_SUB(NOW(), INTERVAL 3 HOUR) AND po.thread_id = ?', [
          body.thread_id,
        ])
      )
      .limit(1)
      .orderByRaw('po.id DESC');

    // post is automerge
    if (lastPostQuery.length > 0 && lastPostQuery[0].user_id === user.id) {
      lastPostId = lastPostQuery[0].id;
      const resultingContent = `${lastPostQuery[0].content}

[b]Edited:[/b]

${body.content}`;

      if (!validatePostLength(resultingContent)) {
        throw new BadRequestError('Failed validations.');
      }

      await Post.update(
        {
          updated_at: new Date(),
          content: resultingContent,
          app_name: appName,
          transaction: trx,
        },
        { where: { id: lastPostId } }
      );
      await storePostEdit(lastPostId, user.id, 'Automerge', resultingContent);

      await new PostRetriever(lastPostId).invalidate();

      post = await trx('Posts')
        .select('id', 'user_id', 'thread_id')
        .where({ id: lastPostId })
        .first();

      automerge = true;
    } else {
      // country information logic
      const countryName = body.display_country ? await getCountryName(ipInfo) : null;

      // new post
      post = await Post.create({
        ...body,
        user_id: user.id,
        country_name: countryName,
        app_name: appName,
        transaction: trx,
      });

      // new ip address
      IpAddress.create({
        ip_address: ipInfo,
        user_id: user.id,
        post_id: post.id,
        transaction: trx,
      });
      lastPostId = post.id;

      const threadRetriever = new ThreadRetriever(post.thread_id);
      // invalidate thread object cache
      threadRetriever.invalidatePosts();

      // invalidate the thread if the thread has been locked
      if (post.thread_post_number >= THREAD_POST_LIMIT) {
        threadRetriever.invalidate();
      }
    }
  });

  const result: PostSchema = await new PostRetriever(lastPostId).getSingleObject();

  //
  // mentions logic
  //
  if (post && post.id) {
    // go through the entire value again and look for mentions :/
    const mentions = postMentionFinder(body.content);

    if (mentions && mentions.length > 0) {
      const mentionQueries = mentions.map((userId) => {
        if (userId !== user.id) {
          return createNotification(NotificationType.POST_REPLY, userId, post.id);
        }
        return undefined;
      });
      await Promise.all(mentionQueries);
    }
  }

  if (!automerge && io) {
    // send socket event to only the roles that can view this post
    const roleIds = await getRoleIdsWithPermissions([`subforum-${thread.subforumId}-view`]);
    createSocketEvent(user.id, EventType.POST_CREATED, post.id, io, {}, undefined, roleIds);
    const postData = await new PostRetriever(result.id, [
      PostFlag.RETRIEVE_SHALLOW,
      PostFlag.INCLUDE_THREAD,
    ]).getSingleObject();
    io.in(threadPostRoom(thread.id)).emit('post:new', postData);
  }

  return result;
};

export const storePost = async (req: Request, res: Response) => {
  try {
    res.status(httpStatus.CREATED);
    return res.json(
      store(
        { ...req.body, display_country: req.body.displayCountryInfo, app_name: req.body.appName },
        req.user,
        req.ipInfo,
        req.app.get('io')
      )
    );
  } catch (exception) {
    if (exception instanceof ForbiddenError) {
      return res.status(httpStatus.FORBIDDEN).json({
        error: exception.message,
      });
    }
    if (exception instanceof BadRequestError) {
      return res.status(httpStatus.BAD_REQUEST).json({
        error: exception.message,
      });
    }
    return errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const update = async (id: number, body: UpdatePostRequest, user: User, ipInfo: string) => {
  const postRetriever = new PostRetriever(id, [PostFlag.RETRIEVE_SHALLOW, PostFlag.INCLUDE_THREAD]);
  const post: any = await postRetriever.getSingleObject();
  const thread = await new ThreadRetriever(post.thread.id).getSingleObject();

  const skipPostValidations = await userHasPermissions(user.id, [
    `subforum-${thread.subforumId}-post-bypass-validations`,
  ]);

  // validations for a valid post
  if (!skipPostValidations && !validatePostLength(body.content)) {
    throw new BadRequestError('Failed validations.');
  }
  if (!validateUserFields(user)) {
    throw new BadRequestError('Invalid user.');
  }

  // client name logic
  let appName: string | null;

  if (body.app_name) {
    appName = validateAppName(body.app_name) ? body.app_name : null;
  } else {
    appName = null;
  }

  await knex('Posts').where({ id }).update({
    content: body.content,
    updated_at: knex.fn.now(),
    app_name: appName,
  });

  // create new IP corresponding to this post
  IpAddress.create({
    ip_address: ipInfo,
    post_id: id,
    user_id: user.id,
  });

  await storePostEdit(id, user.id, body.editReason, body.content);

  postRetriever.invalidate();

  return { message: 'Post updated.' };
};

export const updatePost = async (req: Request, res: Response) => {
  try {
    await update(req.body.id, { ...req.body, app_name: req.body.appName }, req.user, req.ipInfo);
    res.status(httpStatus.CREATED);
    res.json({ message: 'Post updated' });
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const withPostsAndCount = async (req: Request, res: Response) => {
  try {
    const queryResults = await knex
      .from('Posts as po')
      .select(
        'po.id as postId',
        'po.content as postContent',
        'po.user_id as postUserId',
        'po.created_at as postCreatedAt',
        'po.updated_at as postUpdatedAt',
        'po.thread_id as postThreadId',
        'us.username as userUsername',
        'us.usergroup as userUsergroup',
        'us.avatar_url as userAvatar_url',
        'us.background_url as userBackgroundUrl',
        'us.created_at as userCreated_at',
        knex.raw(
          '(select count (*) from Bans where Bans.user_id = po.user_id AND expires_at > NOW()) as postUserBans'
        )
      )
      .join('Users as us', 'us.id', 'po.user_id')
      .where(knex.raw('po.id = ?', req.params.id))
      .limit(1);

    if (queryResults.length === 0) {
      return responseStrategy.send(res, null);
    }

    const postWithUser = queryResults[0];

    const ratings = await Rating.getRatingsForPostsWithUsers([postWithUser.postId]);

    const response = {
      id: postWithUser.postId,
      content: postWithUser.postContent,
      createdAt: postWithUser.postCreatedAt,
      updatedAt: postWithUser.postUpdatedAt,
      user: {
        id: postWithUser.postUserId,
        username: postWithUser.userUsername,
        usergroup: postWithUser.userUsergroup,
        avatar_url: postWithUser.userAvatar_url,
        backgroundUrl: postWithUser.userBackgroundUrl,
        createdAt: postWithUser.userCreated_at,
        isBanned: postWithUser.postUserBans > 0,
      },
      ratings: ratings[postWithUser.postId],
    };

    return responseStrategy.send(res, response);
  } catch (error) {
    return errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, error, res);
  }
};
