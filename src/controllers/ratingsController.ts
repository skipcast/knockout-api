/* eslint-disable import/prefer-default-export */
import { Request, Response, User } from 'express';
import httpStatus from 'http-status';
import { EventType, RatePostRequest } from 'knockout-schema';
import { BadRequestError, NotFoundError } from 'routing-controllers';
import { Server } from 'socket.io';
import onDuplicateUpdate from '../helpers/onDuplicateUpdate';
import ratingList from '../helpers/ratingList.json';
import PostRetriever from '../retriever/post';
import PostRatingRetriever from '../retriever/postRating';
import errorHandler from '../services/errorHandler';
import knex from '../services/knex';
import { createSocketEvent } from './eventLogController';

export const store = async (id: number, body: RatePostRequest, user: User, io: Server) => {
  const ratingId = Object.keys(ratingList).find((key) => ratingList[key].short === body.rating);
  if (!ratingId) {
    throw new BadRequestError('Invalid rating.');
  }

  const post = await new PostRetriever(id).getSingleObject();

  if (!post) {
    throw new NotFoundError('Invalid post.');
  }
  if (post.user.id === user.id) {
    throw new BadRequestError('Rating yourself is pretty sad.');
  }

  onDuplicateUpdate(knex, 'Ratings', {
    user_id: user.id,
    post_id: id,
    rating_id: ratingId,
  });

  await new PostRatingRetriever(id).invalidate();

  createSocketEvent(
    user.id,
    EventType.RATING_CREATED,
    id,
    io,
    { rating: body.rating },
    post.user.id
  );
  return { message: 'Rated post.' };
};

export const remove = async (id: number, user: User) => {
  const post = await new PostRetriever(id).getSingleObject();

  if (!post) {
    throw new NotFoundError('Invalid post.');
  }
  if (post.user.id === user.id) {
    throw new BadRequestError('Invalid post.');
  }

  await knex('Ratings').where({ user_id: user.id, post_id: id }).delete();
  new PostRatingRetriever(id).invalidate();
  return { message: 'Unrated post.' };
};

export const storeRating = async (req: Request, res: Response) => {
  try {
    const result = store(req.body.postId, req.body.rating, req.user, req.app.get('io'));

    new PostRatingRetriever(req.body.postId).invalidate();
    res.status(httpStatus.CREATED);
    res.json(result);
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};
