import { Request, Response } from 'express';
import httpStatus from 'http-status';
import knex from '../services/knex';
import ConversationRetriever, { ConversationFlag } from '../retriever/conversation';
import errorHandler from '../services/errorHandler';

export const isUserInConversation = async (conversationId: number, userId: number) => {
  const query = await knex('ConversationUsers as cu')
    .where('cu.user_id', userId)
    .andWhere('cu.conversation_id', conversationId)
    .limit(1);

  return query.length > 0;
};

/**
 * Shows a conversation
 */
export const show = async (req: Request, res: Response) => {
  try {
    const results = await new ConversationRetriever(req.params.id).getSingleObject();
    res.status(httpStatus.OK);
    res.json(results);
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

const getConversations = async (userId: number) => {
  const query = await knex('Conversations as c')
    .select('c.id')
    .join('ConversationUsers as cu', 'cu.conversation_id', 'c.id')
    .where('cu.user_id', [userId])
    .orderBy('c.updated_at', 'desc');

  const results = await new ConversationRetriever(
    query.map((item) => item.id),
    [ConversationFlag.RETRIEVE_SHALLOW]
  ).getObjectArray();

  return results;
};

export const index = async (req: Request, res: Response) => {
  try {
    const results = await getConversations(req.user.id);

    res.status(httpStatus.OK);
    res.json(results);
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};
