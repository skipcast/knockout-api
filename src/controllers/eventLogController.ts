import { Request, Response } from 'express';
import httpStatus from 'http-status';
import { Server } from 'socket.io';
import { Event, EventType } from 'knockout-schema';
import knex from '../services/knex';
import errorHandler from '../services/errorHandler';
import EventRetriever from '../retriever/event';
import { EVENTS_ROOM } from '../handlers/eventLogHandler';
import UserRetriever from '../retriever/user';
import ThreadRetriever, { ThreadFlag } from '../retriever/thread';
import { getRoleIdsWithPermissions } from '../helpers/permissions';

export const createSocketEvent = async (
  creator: number,
  type: EventType,
  dataId: number,
  io: Server,
  content: object = {},
  targetUserId?: number,
  roleIds?: number[]
) => {
  if (!io) {
    return;
  }

  const now = new Date();
  const createdAt = now.toISOString();
  const rawEvent = {
    id: now.getTime(),
    creator,
    type,
    dataId,
    content: JSON.stringify(content),
    createdAt,
  };

  const event: Event = EventRetriever.formatRawEvent(rawEvent);
  const objectMap = await EventRetriever.getDataObjects([event]);

  event.data = objectMap[`${event.type}-${event.data.id}`];
  event.creator = await new UserRetriever(event.creator as any).getSingleObject();

  if (targetUserId) {
    io.in(`${EVENTS_ROOM}:${targetUserId}`).emit('events:new', event);
  } else if (roleIds?.length > 0) {
    io.in(roleIds.map((roleId) => `${EVENTS_ROOM}:role-${roleId}`)).emit('events:new', event);
  } else {
    io.in(EVENTS_ROOM).emit('events:new', event);
  }
};

export const createEvent = async (
  userId: number,
  type: EventType,
  dataId: number,
  io: Server,
  content: object = {},
  roleIds?: number[]
) => {
  try {
    const result = knex('Events')
      .insert({
        executed_by: userId,
        type,
        data_id: dataId,
        content: JSON.stringify(content),
      })
      .then((rows) => rows);

    createSocketEvent(userId, type, dataId, io, content, undefined, roleIds);
    return result;
  } catch (exception) {
    return { error: exception };
  }
};

export const get = async (userId: number) => {
  const events = await knex.select('id').from('Events').orderBy('created_at', 'DESC').limit(40);

  const eventObjects = await new EventRetriever(
    events.map((event) => event.id),
    [],
    { userId }
  ).getObjectArray();
  return eventObjects;
};

export const index = async (req: Request, res: Response) => {
  try {
    const eventObjects = get(req.user.id);
    res.status(httpStatus.OK);
    res.json(eventObjects);
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const threadStatusEvent = async ({ userId, threadId, type }, io: Server) => {
  let updateType: EventType = null;
  if (type.locked === true) {
    updateType = EventType.THREAD_LOCKED;
  } else if (type.locked === false) {
    updateType = EventType.THREAD_UNLOCKED;
  } else if (type.deleted_at) {
    updateType = EventType.THREAD_DELETED;
  } else if (type.deleted_at === null) {
    updateType = EventType.THREAD_RESTORED;
  } else if (type.pinned) {
    updateType = EventType.THREAD_PINNED;
  } else if (type.pinned === false) {
    updateType = EventType.THREAD_UNPINNED;
  }

  if (!updateType) {
    return { error: 'Unknown thread update event type' };
  }

  const { subforumId } = await new ThreadRetriever(threadId, [
    ThreadFlag.RETRIEVE_SHALLOW,
  ]).getSingleObject();

  const roleIds = await getRoleIdsWithPermissions([`subforum-${subforumId}-view`]);
  return createEvent(userId, updateType, threadId, io, {}, roleIds);
};

export const threadUpdateEvent = async ({ oldTitle, threadId, userId, body }, io: Server) => {
  let type: EventType;
  let content: string;

  if (body.title) {
    content = oldTitle;
    type = EventType.THREAD_RENAMED;
  } else if (body.subforum_id) {
    type = EventType.THREAD_MOVED;
  }

  let newSubforumId = body.subforum_id;
  if (!newSubforumId) {
    newSubforumId = (
      await new ThreadRetriever(threadId, [ThreadFlag.RETRIEVE_SHALLOW]).getSingleObject()
    ).subforumId;
  }

  const roleIds = await getRoleIdsWithPermissions([`subforum-${newSubforumId}-view`]);

  createEvent(userId, type, threadId, io, { oldTitle: content, newTitle: body.title }, roleIds);
};

export const removeUserAvatarBg = async ({ userId, avatar, background, removedBy }, io: Server) => {
  if (avatar) createEvent(removedBy, EventType.USER_AVATAR_REMOVED, userId, io);
  if (background) createEvent(removedBy, EventType.USER_BACKGROUND_REMOVED, userId, io);
};
