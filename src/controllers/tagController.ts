import { Request, Response } from 'express';
import httpStatus from 'http-status';

import redis from '../services/redisClient';
import knex from '../services/knex';
import errorHandler from '../services/errorHandler';

const TAGS_LIST_KEY = 'tags-list';

export const store = async (req: Request, res: Response) => {
  try {
    const { tagName } = req.body;

    await knex('Tags').insert({
      name: tagName,
    });

    redis.del(TAGS_LIST_KEY);

    res.status(httpStatus.CREATED);
    res.json({ message: 'Tag created.' });
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const index = async (req: Request, res: Response) => {
  try {
    const cachedData: string = await redis.getAsync(TAGS_LIST_KEY);

    if (cachedData) {
      res.status(httpStatus.OK);
      return res.json(JSON.parse(cachedData));
    }

    const result = await knex('Tags').select('id', 'name').where({ deleted_at: null });

    redis.setex(TAGS_LIST_KEY, 86400, JSON.stringify(result));

    res.status(httpStatus.OK);
    return res.json(result);
  } catch (exception) {
    return errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};
