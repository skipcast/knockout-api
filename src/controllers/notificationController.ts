import { NotificationType } from 'knockout-schema';
import NotificationRetriever from '../retriever/notification';
import knex from '../services/knex';

const NOTIFICATIONS_PER_PAGE = 25;

export const createNotification = async (
  type: NotificationType,
  userId: number,
  dataId: number
) => {
  await knex('Notifications')
    .insert({
      type,
      user_id: userId,
      data_id: dataId,
    })
    .onConflict('data_id')
    .merge({
      read: false,
      created_at: new Date(),
    });

  const notificationId = await knex('Notifications')
    .select('id')
    .where({
      type,
      user_id: userId,
      data_id: dataId,
    })
    .pluck('id');

  new NotificationRetriever(notificationId[0]).invalidate();
};

export const getAll = async (userId: number, page = 1) => {
  // set date cutoff to two weeks ago
  const cutoff = new Date(Date.now() - 1000 * 60 * 60 * 24 * 14);

  const notifications = await knex('Notifications')
    .select('id')
    .where('user_id', userId)
    .andWhere('created_at', '>', cutoff)
    .orderBy('created_at', 'desc')
    .limit(Number(NOTIFICATIONS_PER_PAGE))
    .offset(Number(NOTIFICATIONS_PER_PAGE) * (page - 1));

  return new NotificationRetriever(
    notifications.map((notification) => notification.id)
  ).getObjectArray();
};

export const markAsRead = async (userId: number, ids: number[]) => {
  await knex('Notifications').where('user_id', userId).whereIn('id', ids).update({ read: true });
  await new NotificationRetriever(ids).invalidate();
  return 'Marked notifications as read.';
};

export const markAllAsRead = async (userId: number) => {
  const notificationIds = (
    await knex('Notifications')
      .select('id')
      .where('user_id', userId)
      .andWhere('created_at', '>', new Date(Date.now() - 1000 * 60 * 60 * 24 * 14))
  ).map((notification) => notification.id);

  // mark all notificationIds as read
  await knex('Notifications').whereIn('id', notificationIds).update({ read: true });
  new NotificationRetriever(notificationIds).invalidate();
  return 'Marked all notifications as read.';
};
