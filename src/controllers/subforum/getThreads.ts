import { Request, Response } from 'express';
import { Op, fn, col, where } from 'sequelize';
import ResponseStrategy from '../../helpers/responseStrategy';
import ThreadRetriever, { ThreadFlag } from '../../retriever/thread';
import SubforumRetriever, { SubforumFlag } from '../../retriever/subforum';
import { THREADS_PER_SUBFORUM_PAGE } from '../../../config/server';
import datasource from '../../models/datasource';
import { getUserPermissionCodes } from '../../helpers/user';
import nsfwTagName from '../../constants/nsfwTagName';

const getThreadIds = async (
  subforumId: number,
  userId: number,
  page: number = 1,
  perPage: number = 40,
  hideNsfw: boolean = false
) => {
  const permissionCodes = await getUserPermissionCodes(userId);
  const { Tag: TagModel, Thread: ThreadModel } = datasource().models;
  const threadScopes = [
    { method: ['byUser', userId, permissionCodes] },
    { method: ['bySubforum', subforumId] },
  ];

  const whereCondition = {
    [Op.or]: [
      where(fn('FIND_IN_SET', nsfwTagName, fn('GROUP_CONCAT', col('Tags.name'))), '=', 0),
      where(fn('FIND_IN_SET', nsfwTagName, fn('GROUP_CONCAT', col('Tags.name'))), '=', null),
    ],
  };

  const query = await ThreadModel.scope(threadScopes).findAll({
    attributes: ['id'],
    limit: perPage,
    offset: (page - 1) * perPage,
    subQuery: false,
    having: hideNsfw ? whereCondition : undefined,
    include: {
      attributes: ['id'],
      model: TagModel,
      required: false,
    },
    order: [
      ['pinned', 'desc'],
      ['updated_at', 'desc'],
    ],
    group: ['Thread.id'],
  });

  return query.map((thread) => thread.id);
};

// eslint-disable-next-line import/prefer-default-export
export const getThreads = async (req: Request, res: Response) => {
  const hideNsfw = req.query.hideNsfw || false;

  const pageNum = req.params.page ? Number(req.params.page) : 1;
  const subforum = await new SubforumRetriever(req.params.id, [
    SubforumFlag.INCLUDE_TOTAL_THREADS,
  ]).getSingleObject();
  const threadCount = subforum.totalThreads;
  const threadIds = await getThreadIds(
    subforum.id,
    req.user?.id,
    req.params.page,
    Number(THREADS_PER_SUBFORUM_PAGE),
    hideNsfw
  );

  const flags = [ThreadFlag.INCLUDE_LAST_POST, ThreadFlag.INCLUDE_POST_COUNT];

  const threadRetriever = new ThreadRetriever(threadIds, flags, {
    userId: req.user?.id,
  });
  const threads = await threadRetriever.getObjectArray();

  ResponseStrategy.send(res, {
    id: subforum.id,
    name: subforum.name,
    iconId: subforum.icondId,
    createdAt: subforum.createdAt,
    updatedAt: subforum.updatedAt,
    totalThreads: threadCount,
    currentPage: pageNum,
    threads,
  });
};
