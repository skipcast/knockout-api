/* eslint-disable import/prefer-default-export */
import { Request, Response } from 'express';

import ResponseStrategy from '../../helpers/responseStrategy';
import SubforumRetriever, { SubforumFlag } from '../../retriever/subforum';

import { getViewableSubforumIdsByUser } from '../../helpers/user';

// eslint-disable-next-line import/prefer-default-export
export const getAll = async (req: Request, res: Response) => {
  const hideNsfw = req.query.hideNsfw || false;

  const flags = [
    SubforumFlag.INCLUDE_TOTAL_THREADS,
    SubforumFlag.INCLUDE_TOTAL_POSTS,
    SubforumFlag.INCLUDE_LAST_POST,
  ];

  if (hideNsfw) {
    flags.push(SubforumFlag.HIDE_NSFW);
  }

  const subforumIds = await getViewableSubforumIdsByUser(req.user?.id);

  const subforums = await new SubforumRetriever(subforumIds, flags).getObjectArray();

  const output = subforums.map((subforum) => ({
    id: subforum.id,
    name: subforum.name,
    iconId: subforum.iconId,
    createdAt: subforum.createdAt,
    updatedAt: subforum.updatedAt,
    description: subforum.description,
    icon: subforum.icon,
    totalThreads: subforum.totalThreads,
    totalPosts: subforum.totalPosts,
    lastPostId: subforum.lastPost?.id,
    lastPost: subforum.lastPost,
  }));

  const responseBody = { list: output };

  return ResponseStrategy.send(res, responseBody);
};
