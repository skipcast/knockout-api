/* eslint-disable import/prefer-default-export */

import { Request, Response } from 'express';

import httpStatus from 'http-status';
import { v4 as uuidv4 } from 'uuid';
import { POST_IMAGE_MAX_FILESIZE_BYTES } from 'knockout-schema';
import { stripExifTags, imageMetadata, isValidGif, avatarGifToWebp } from '../helpers/image';
import fileStore from '../helpers/fileStore';
import { NODE_ENV } from '../../config/server';
import errorHandler from '../services/errorHandler';

import { isLimitedUser } from '../validations/user';

export const store = async (req: Request, res: Response) => {
  try {
    if (!req.file || !req.file.mimetype || !req.file.mimetype.startsWith('image/')) {
      res.status(httpStatus.UNPROCESSABLE_ENTITY);
      res.json({ error: 'Unprocessable entity.' });
      return;
    }

    const limitedUser = await isLimitedUser(req.user.id);
    if (limitedUser) {
      res.status(httpStatus.OK);
      res.json({
        message: 'Your account must be at least a week old to upload images.',
      });
      return;
    }

    const metadata = await imageMetadata(req.file.buffer);

    const strippedBuffer = await stripExifTags(req.file.buffer);

    const fileName = `${req.user.id}-${uuidv4()}.${req.file.originalname.split('.').pop()}`;
    const filePath = NODE_ENV === 'production' ? `image/${fileName}` : `avatars/${fileName}`;

    if (isValidGif(metadata, false)) {
      // run sharp with no resizing - important to run the validations first
      const encodedImageBuffer = await avatarGifToWebp(req.file.buffer);

      await fileStore.storeBuffer(encodedImageBuffer, filePath, 'image/webp');

      res.status(httpStatus.CREATED);
      res.json({ fileName });
      return;
    }

    if (metadata.size >= POST_IMAGE_MAX_FILESIZE_BYTES) {
      throw new Error(`Image size must be below ${POST_IMAGE_MAX_FILESIZE_BYTES / 1000000} MB`);
    }

    await fileStore.storeBuffer(strippedBuffer, filePath, req.file.mimetype);

    res.status(httpStatus.CREATED);
    res.json({ fileName });
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};
