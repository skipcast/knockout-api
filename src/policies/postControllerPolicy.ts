/* eslint-disable import/prefer-default-export */

import { NextFunction, Request, Response } from 'express';
import { ForbiddenError } from 'routing-controllers';
import ms from 'ms';
import { PermissionCode } from 'knockout-schema';
import authorize from '../helpers/authorize';
import PostRetriever, { PostFlag } from '../retriever/post';
import ThreadRetriever, { ThreadFlag } from '../retriever/thread';
import { isLimitedUser } from '../validations/user';
import redis from '../services/redisClient';
import { RAID_MODE_KEY } from '../constants/adminSettings';
import datasource from '../models/datasource';
import LogEvent from '../services/logEvent';
import Scamalytics from '../services/scamalytics';
import Ripe from '../services/ripe';
import { getUserPermissionCodes, userCanViewThread } from '../helpers/user';

const { Post } = datasource().models;

const getThread = async (threadId: number) =>
  new ThreadRetriever(threadId, [ThreadFlag.RETRIEVE_SHALLOW]).getSingleObject();

export const get = async (req: Request, res: Response, next: NextFunction) => {
  const post: any = new PostRetriever(req.params.id, [PostFlag.INCLUDE_THREAD]).getSingleObject();

  const permissionCodes = getUserPermissionCodes(req.user.id);
  if (!userCanViewThread(req.user.id, await permissionCodes, (await post).thread)) {
    throw new ForbiddenError('Insufficient user permissions.');
  }

  return next();
};

export const postStore = async (req: Request, res: Response, next: NextFunction) => {
  const thread = await getThread(req.body.thread_id);
  const limitedUser = await isLimitedUser(req.user.id);

  const logEvent = new LogEvent(LogEvent.ENTITY_POST, LogEvent.ACTION_CREATE)
    .setUserId(req.user.id)
    .setProperties({ ip: req.ipInfo })
    .setSuccess(false);

  // if the user is limited:
  // - check IP
  // - if raid mode, prevent from posting
  // - if last post made within 5 mins, prevent from posting
  if (limitedUser) {
    if (await new Scamalytics(req.ipInfo).isRisky()) {
      logEvent.setMessage('VPN detected').log();
      throw new ForbiddenError('You appear to be using a proxy/VPN.');
    }

    // Do we have a ban in place for their netname or parent ASN?
    if ((await new Ripe(req.ipInfo).isBanned()) || (await new Ripe(req.ipInfo).isParentBanned())) {
      logEvent.setMessage('Banned ISP detected').log();
      throw new ForbiddenError('You appear to be using an ISP which we have banned.');
    }

    const raidModeEnabled = await redis.getAsync(RAID_MODE_KEY);

    if (raidModeEnabled) {
      throw new ForbiddenError('Our servers are under heavy load, try posting again later.');
    }

    const recentPost = await Post.findOne({
      attributes: ['created_at'],
      where: { user_id: req.user.id },
      order: [['created_at', 'DESC']],
    });

    if (recentPost) {
      const now = new Date();
      const createdAt = recentPost.dataValues.created_at;
      const diff = now.getTime() - createdAt.getTime();

      if (diff < ms('5 minutes')) {
        throw new ForbiddenError(`Please wait ${ms(ms('5 minutes') - diff)} before posting again.`);
      }
    }
  }
  const userPermissionCodes = getUserPermissionCodes(req.user.id);
  await authorize(req.user.id, [`subforum-${thread.subforumId}-post-create`]);

  if (!userCanViewThread(req.user.id, await userPermissionCodes, thread)) {
    throw new ForbiddenError('You do not have permission to post in this thread.');
  }
  logEvent.setSuccess(true).log();
  return next();
};

export const postUpdate = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  // post must belong to user, or the user must have the
  // subforum-wide post-edit permission
  const post: any = await new PostRetriever(req.params.id, [
    PostFlag.INCLUDE_THREAD,
  ]).getSingleObject();

  // user must still be able to post in the subforum (e.g. banned users editing their posts)
  const requiredCodes: PermissionCode[] = [`subforum-${post.thread.subforumId}-post-create`];

  // if the post does not belong to the user, see if user has subforum
  // post edit permission
  if (post.user.id !== req.user.id) {
    requiredCodes.push(`subforum-${post.thread.subforumId}-post-update`);
  }
  await authorize(req.user.id, requiredCodes);
  return next();
};

export const postRatingStore = async (req: Request, res: Response, next: NextFunction) => {
  const post: any = await new PostRetriever(req.params.id, [
    PostFlag.INCLUDE_THREAD,
  ]).getSingleObject();
  await authorize(req.user.id, [`subforum-${post.thread.subforumId}-post-rating-create`]);
  return next();
};
