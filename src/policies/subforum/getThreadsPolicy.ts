/* eslint-disable import/prefer-default-export */

import { NextFunction, Request, Response } from 'express';
import authorize from '../../helpers/authorize';

export const getThreads = async (req: Request, res: Response, next: NextFunction) => {
  await authorize(
    req.user?.id,
    [`subforum-${req.params.id}-view`, `subforum-${req.params.id}-view-own-threads`],
    true
  );
  return next();
};
