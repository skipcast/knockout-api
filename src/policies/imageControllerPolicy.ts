/* eslint-disable import/prefer-default-export */

import { NextFunction, Request, Response } from 'express';
import authorize from '../helpers/authorize';
import { imageMetadata } from '../helpers/image';

export const avatarUpload = async (req: Request, res: Response, next: NextFunction) => {
  const { format = '' } = req.file?.buffer ? await imageMetadata(req.file?.buffer) : {};

  // if the user is uploading a gif, make sure they have the right permission
  if (format === 'gif') {
    await authorize(req.user.id, ['user-avatar-gif-upload']);
  }
  return next();
};
