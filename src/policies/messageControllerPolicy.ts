import { NextFunction, Request, Response } from 'express';
import { ForbiddenError } from 'routing-controllers';
import { isUserInConversation } from '../controllers/conversationController';
import authorize from '../helpers/authorize';
import MessageRetriever from '../retriever/message';

export const store = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  if (req.body.conversationId) {
    const { conversationId } = req.body;
    const userInConversation = await isUserInConversation(conversationId, req.user.id);
    if (!userInConversation) {
      throw new ForbiddenError('Insufficient user permissions.');
    }
  } else {
    await authorize(req.user.id, ['message-create']);
  }
  return next();
};

export const update = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  const message = await new MessageRetriever(req.params.id).getSingleObject();
  const userInConversation = await isUserInConversation(message.conversationId, req.user.id);
  if (!userInConversation) {
    throw new ForbiddenError('Insufficient user permissions.');
  }
  return next();
};
