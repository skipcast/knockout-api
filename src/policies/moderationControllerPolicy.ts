import { NextFunction, Request, Response } from 'express';
import authorize from '../helpers/authorize';
import ThreadRetriever, { ThreadFlag } from '../retriever/thread';

export const getIpsByUsername = async (req: Request, res: Response, next: NextFunction) => {
  await authorize(req.user.id, ['ipAddress-view']);
  return next();
};

export const getUsernamesByIp = async (req: Request, res: Response, next: NextFunction) => {
  await authorize(req.user.id, ['ipAddress-view']);
  return next();
};

export const changeThreadStatus = async (req: Request, res: Response, next: NextFunction) => {
  const thread = await new ThreadRetriever(req.body.threadId, [
    ThreadFlag.RETRIEVE_SHALLOW,
  ]).getSingleObject();
  await authorize(req.user.id, [`subforum-${thread.subforumId}-thread-update`]);
  return next();
};

export const removeUserImage = async (req: Request, res: Response, next: NextFunction) => {
  await authorize(req.user.id, ['user-update']);
  return next();
};

export const removeUserProfile = async (req: Request, res: Response, next: NextFunction) => {
  await authorize(req.user.id, ['user-archive']);
  return next();
};

export const getLatestUsers = async (req: Request, res: Response, next: NextFunction) => {
  await authorize(req.user.id, ['latest-users-view']);
  return next();
};

export const getDashboardData = async (req: Request, res: Response, next: NextFunction) => {
  await authorize(req.user.id, ['dashboard-data-view']);
  return next();
};

export const getFullUserInfo = async (req: Request, res: Response, next: NextFunction) => {
  await authorize(req.user.id, ['full-user-info-view']);
  return next();
};

export const makeBanInvalid = async (req: Request, res: Response, next: NextFunction) => {
  await authorize(req.user.id, ['ban-archive']);
  return next();
};

export const getAdminSettings = async (req: Request, res: Response, next: NextFunction) => {
  await authorize(req.user.id, ['admin-settings-view']);
  return next();
};

export const setAdminSettings = async (req: Request, res: Response, next: NextFunction) => {
  await authorize(req.user.id, ['admin-settings-update']);
  return next();
};
