import { NextFunction, Request, Response } from 'express';
import { ForbiddenError } from 'routing-controllers';
import { userProfileController } from '../controllers';
import authorize from '../helpers/authorize';
import ProfileCommentRetriever from '../retriever/profileComment';

export const update = async (req: Request, res: Response, next: NextFunction) => {
  if (Number(req.params.id) !== req.user.id) throw new ForbiddenError();
  return next();
};

export const updateBackground = async (req: Request, res: Response, next: NextFunction) => {
  if (Number(req.params.id) !== req.user.id) throw new ForbiddenError();
  return next();
};

export const updateHeader = async (req: Request, res: Response, next: NextFunction) => {
  if (Number(req.params.id) !== req.user.id) throw new ForbiddenError();
  return next();
};

export const removeHeader = async (req: Request, res: Response, next: NextFunction) => {
  if (Number(req.params.id) !== req.user.id) {
    await authorize(req.user.id, ['user-update']);
  }
  return next();
};

export const createComment = async (req: Request, res: Response, next: NextFunction) => {
  await authorize(req.user.id, ['user-profile-comment-create']);
  const profile = await userProfileController.get(req.params.id);
  if (profile?.disableComments) {
    throw new ForbiddenError("Cannot comment on user's profile.");
  }
  return next();
};

export const deleteComment = async (req: Request, res: Response, next: NextFunction) => {
  const comment = await new ProfileCommentRetriever(req.params.commentId).getSingleObject();

  // user can delete their comment or a comment on their profile,
  // or if they have the corresponding permission
  if (Number(req.params.id) !== req.user.id && comment?.author?.id !== req.user.id) {
    await authorize(req.user.id, ['user-profile-comment-archive']);
  }
  return next();
};

export const wipeAccount = async (req: Request, res: Response, next: NextFunction) => {
  if (Number(req.user.id) !== Number(req.params.id)) {
    await authorize(req.user.id, ['user-archive']);
  }
  return next();
};
