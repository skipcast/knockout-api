/* eslint-disable import/prefer-default-export */

import { NextFunction, Request, Response } from 'express';
import authorize from '../../helpers/authorize';

export const post = async (req: Request, res: Response, next: NextFunction) => {
  await authorize(req.user.id, ['report-create']);
  return next();
};
