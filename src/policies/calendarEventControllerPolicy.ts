/* eslint-disable import/prefer-default-export */

import { NextFunction, Request, Response } from 'express';
import { Thread, PermissionCode } from 'knockout-schema';
import authorize from '../helpers/authorize';
import CalendarEventRetriever from '../retriever/calendarEvent';
import ThreadRetriever, { ThreadFlag } from '../retriever/thread';

const userIsThreadCreator = async (userId: number, thread: Thread) =>
  Number(thread.user) === userId;

const getCalendarEventThread = async (threadId: number) =>
  new ThreadRetriever(threadId, [ThreadFlag.RETRIEVE_SHALLOW]).getSingleObject();

export const store = async (req: Request, res: Response, next: NextFunction) => {
  const permissionCodesToAuthorize: PermissionCode[] = ['calendar-event-create'];

  // if the user is not the thread creator, they must be able to manage (update) the thread
  const thread = await getCalendarEventThread(req.body.threadId);

  if (!(await userIsThreadCreator(req.user.id, thread))) {
    permissionCodesToAuthorize.push(`subforum-${thread.subforumId}-thread-update`);
  }

  await authorize(req.user.id, permissionCodesToAuthorize);

  return next();
};

const userHasCalendarEvent = async (calendarEventId: number, userId: number) => {
  const calendarEvent = await new CalendarEventRetriever(calendarEventId).getSingleObject();
  return calendarEvent.createdBy.id === userId;
};

export const update = async (req: Request, res: Response, next: NextFunction) => {
  const permissionCodesToAuthorize: PermissionCode[] = [];

  // user can update their own calendar events; otherwise, authorize relevant permission
  if (!(await userHasCalendarEvent(req.params.id, req.user.id))) {
    permissionCodesToAuthorize.push('calendar-event-update');
  }

  // if a thread id is included in the request, validate the user can manage it
  if (req.body.threadId) {
    const thread = await getCalendarEventThread(req.body.threadId);

    if (!(await userIsThreadCreator(req.user.id, thread))) {
      permissionCodesToAuthorize.push(`subforum-${thread.subforumId}-thread-update`);
    }
  }

  await authorize(req.user.id, permissionCodesToAuthorize);

  return next();
};

export const destroy = async (req: Request, res: Response, next: NextFunction) => {
  // user can destroy their own calendar events; otherwise, authorize relevant permission
  if (!(await userHasCalendarEvent(req.params.id, req.user.id))) {
    await authorize(req.user.id, ['calendar-event-archive']);
  }
  return next();
};
