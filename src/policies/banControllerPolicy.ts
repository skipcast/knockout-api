/* eslint-disable import/prefer-default-export */

import { NextFunction, Request, Response } from 'express';
import authorize from '../helpers/authorize';

export const store = async (req: Request, res: Response, next: NextFunction) => {
  await authorize(req.user.id, ['ban-create']);
  return next();
};
