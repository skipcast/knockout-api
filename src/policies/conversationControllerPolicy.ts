/* eslint-disable import/prefer-default-export */

import { NextFunction, Request, Response } from 'express';
import { ForbiddenError } from 'routing-controllers';
import { isUserInConversation } from '../controllers/conversationController';

export const show = async (req: Request, res: Response, next: NextFunction) => {
  const userInConversation = await isUserInConversation(parseInt(req.params.id, 10), req.user.id);

  if (!userInConversation) {
    throw new ForbiddenError('Insufficient user permissions.');
  }
  return next();
};
