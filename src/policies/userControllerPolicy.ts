/* eslint-disable import/prefer-default-export */
import { NextFunction, Request, Response } from 'express';
import { ForbiddenError } from 'routing-controllers';

export const update = async (req: Request, res: Response, next: NextFunction) => {
  if (Number(req.params.id) !== req.user.id) throw new ForbiddenError();
  return next();
};
