import { NextFunction, Request, Response } from 'express';
import { PermissionCode } from 'knockout-schema';
import authorize from '../helpers/authorize';
import RuleRetriever from '../retriever/rule';

export const store = async (req: Request, res: Response, next: NextFunction) => {
  const codes: PermissionCode[] = ['rule-create'];
  if (req.params?.rulableType === 'Subforum' && req.params?.rulableId) {
    codes.push(`subforum-${req.params.rulableId}-rule-create`);
  }
  await authorize(req.user.id, codes, true);
  return next();
};

export const update = async (req: Request, res: Response, next: NextFunction) => {
  const codes: PermissionCode[] = ['rule-update'];
  const rule = await new RuleRetriever(req.params?.id)?.getSingleObject();
  if (rule?.rulableType === 'Subforum' && rule?.rulableId) {
    codes.push(`subforum-${rule.rulableId}-rule-update`);
  }
  await authorize(req.user.id, codes, true);
  return next();
};

export const destroy = async (req: Request, res: Response, next: NextFunction) => {
  const codes: PermissionCode[] = ['rule-archive'];
  const rule = await new RuleRetriever(req.params?.id)?.getSingleObject();
  if (rule?.rulableType === 'Subforum' && rule?.rulableId) {
    codes.push(`subforum-${rule.rulableId}-rule-archive`);
  }
  await authorize(req.user.id, codes, true);
  return next();
};
