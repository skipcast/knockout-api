/* eslint-disable class-methods-use-this */
import { Get, JsonController, Req, UseBefore } from 'routing-controllers';
import { OpenAPI, ResponseSchema } from 'routing-controllers-openapi';
import { Request } from 'express';
import { Event } from 'knockout-schema';
import { get } from '../controllers/eventLogController';
import { authentication } from '../middleware/auth';

@OpenAPI({ tags: ['Events'] })
@JsonController('/events')
export default class EventController {
  @Get('/')
  @UseBefore(authentication.required)
  @ResponseSchema(Event, { isArray: true })
  getEvents(@Req() request: Request): Promise<Event[]> {
    return get(request.user.id);
  }
}
