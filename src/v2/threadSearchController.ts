/* eslint-disable class-methods-use-this */

import httpStatus from 'http-status';
import { Request } from 'express';
import { JsonController, Post, Body, HttpCode, Req, UseBefore } from 'routing-controllers';
import { OpenAPI, ResponseSchema } from 'routing-controllers-openapi';
import { ThreadSearchResponse, ThreadSearchRequest } from 'knockout-schema';
import { search } from '../controllers/threadSearchController';
import { authentication } from '../middleware/auth';

@OpenAPI({ tags: ['ThreadSearch'] })
@JsonController('/threadsearch')
export default class ThreadSearchController {
  @Post('/')
  @UseBefore(authentication.optional)
  @OpenAPI({ summary: 'Search for a thread.' })
  @HttpCode(httpStatus.CREATED)
  @ResponseSchema(ThreadSearchResponse)
  async search(@Req() request: Request, @Body() body: ThreadSearchRequest) {
    return search(request, body);
  }
}
