/* eslint-disable class-methods-use-this */
import httpStatus from 'http-status';
import {
  Get,
  JsonController,
  UseBefore,
  Post,
  Body,
  Put,
  Param,
  QueryParam,
  HttpCode,
  Delete,
} from 'routing-controllers';
import { OpenAPI, ResponseSchema } from 'routing-controllers-openapi';
import { OpenAPIParam, Rule, CreateRuleRequest, UpdateRuleRequest } from 'knockout-schema';
import { authentication } from '../middleware/auth';
import { storeRule, updateRule, indexRules, destroyRule } from '../helpers/rule';
import { ruleControllerPolicy } from '../policies';
import errorHandler from '../services/errorHandler';

const { catchErrors } = errorHandler;

@OpenAPI({ tags: ['Rules'] })
@JsonController('/rules')
export default class RuleController {
  @Get('/')
  @OpenAPI({
    summary: 'View all rules',
    description:
      'If rulableType or rulableId are not provided, this will show the general site rules.',
  })
  @OpenAPIParam('rulableType', { description: 'The type of resource this rule applies to.' })
  @OpenAPIParam('rulableId', { description: 'The ID of the resource this rule applies to.' })
  @ResponseSchema(Rule, { isArray: true })
  async getRules(
    @QueryParam('rulableType') rulableType: string,
    @QueryParam('rulableId') rulableId: number
  ) {
    return indexRules({ rulableType, rulableId });
  }

  @Post('/')
  @UseBefore(...[authentication.required, catchErrors(ruleControllerPolicy.store)])
  @OpenAPI({ summary: 'Create a rule' })
  @HttpCode(httpStatus.CREATED)
  @ResponseSchema(Rule)
  async createRule(@Body() body: CreateRuleRequest) {
    return storeRule({ ...body });
  }

  @Put('/:id')
  @UseBefore(...[authentication.required, catchErrors(ruleControllerPolicy.update)])
  @OpenAPI({ summary: 'Update a rule' })
  @OpenAPIParam('id', { description: 'The id of the rule.' })
  @HttpCode(httpStatus.CREATED)
  @ResponseSchema(Rule)
  async updateRule(@Param('id') id: number, @Body() body: UpdateRuleRequest) {
    return updateRule({ ruleId: id, ...body });
  }

  @Delete('/:id')
  @UseBefore(...[authentication.required, catchErrors(ruleControllerPolicy.destroy)])
  @OpenAPI({ summary: 'Delete a rule' })
  @OpenAPIParam('id', { description: 'The id of the rule.' })
  @HttpCode(httpStatus.OK)
  async destroyRule(@Param('id') id: number) {
    return destroyRule({ ruleId: id });
  }
}
