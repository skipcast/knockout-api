/* eslint-disable class-methods-use-this */
import httpStatus from 'http-status';
import { Request, Response } from 'express';
import {
  JsonController,
  UseBefore,
  Post,
  Body,
  HttpCode,
  Req,
  Get,
  QueryParam,
  Res,
} from 'routing-controllers';
import { OpenAPI, ResponseSchema } from 'routing-controllers-openapi';
import {
  DonationCheckoutSessionResponse,
  DonationCheckoutSessionRequest,
  DonationGetUpgradeExpirationResponse,
} from 'knockout-schema';
import { authentication } from '../middleware/auth';
import StripeClient from '../services/stripeClient';
import UserRetriever, { UserFlag } from '../retriever/user';
import datasource from '../models/datasource';
import { promoteUserToGold } from '../helpers/user';

const GOLD_PRICE_PENCE = 350;

@OpenAPI({ tags: ['Donations'] })
@JsonController('/donations')
export default class DonationController {
  @Post('/checkout-session')
  @UseBefore(authentication.required)
  @OpenAPI({
    summary: `Create and return a Knockout donation Stripe checkout session for the current user. The Stripe Session ID is available in the URL parameters on successful checkout redirection in the URL parameters.`,
  })
  @HttpCode(httpStatus.CREATED)
  @ResponseSchema(DonationCheckoutSessionResponse)
  async checkout(@Req() request: Request, @Body() body: DonationCheckoutSessionRequest) {
    const stripeClient = new StripeClient();

    const successUrl = `${request.protocol}://${request.get(
      'Host'
    )}/v2/donations/sync?sessionId={CHECKOUT_SESSION_ID}&successUrl=${body.successUrl}`;

    const session = await stripeClient.donationCheckoutSession(
      request.user.id,
      successUrl,
      body.cancelUrl
    );
    return { sessionId: session.id };
  }

  @Get('/upgrade-expiration')
  @UseBefore(authentication.required)
  @OpenAPI({ summary: 'Get the expiration date of the current users donation upgrades' })
  @HttpCode(httpStatus.OK)
  @ResponseSchema(DonationGetUpgradeExpirationResponse)
  async getUpgradeExpiration(
    @Req() request: Request
  ): Promise<DonationGetUpgradeExpirationResponse> {
    const { donationUpgradeExpiresAt } = await new UserRetriever(request.user.id, [
      UserFlag.INCLUDE_DONATION_UPGRADE_EXPIRATION,
    ]).getSingleObject();
    return { expiresAt: donationUpgradeExpiresAt || null };
  }

  @Get('/sync')
  @UseBefore(authentication.required)
  @OpenAPI({
    summary: 'Syncs the users donation amount to their donation benefit expiration time',
  })
  async syncDonationAmount(
    @Req() request: Request,
    @Res() response: Response,
    @QueryParam('sessionId') sessionId: string,
    @QueryParam('successUrl') successUrl: string
  ) {
    const stripeClient = new StripeClient();

    const donationSession = await stripeClient.getCompletedDonationSession(sessionId);
    const userRetriever = new UserRetriever(request.user.id, [
      UserFlag.INCLUDE_DONATION_UPGRADE_EXPIRATION,
      UserFlag.INCLUDE_STRIPE_CUSTOMER_ID,
    ]);

    const { donationUpgradeExpiresAt, stripeCustomerId } = await userRetriever.getSingleObject();

    // bail out if donation does session not belong to the user, or is not complete (not paid, open, or expired)
    if (
      !donationSession ||
      donationSession.payment_status !== 'paid' ||
      donationSession.status !== 'complete' ||
      donationSession.customer !== stripeCustomerId
    ) {
      return response.send({ message: 'Invalid donation.' });
    }

    // do nothing if donation amount is 0 (invalid donation)
    const donationAmountPounds = donationSession.amount_subtotal;

    if (donationAmountPounds === 0) {
      return { message: 'Invalid donation.' };
    }

    // make sure that if a user tries to hit this route directly (no valid case for this),
    // the session id must have a corresponding
    // payment made in the last 15 seconds. this is to prevent the user from spamming this route
    // in the regular flow, this endpoint is hit directly after a payment is made, so regular flows
    // should pass this check
    const donationSessionPaymentIntent = await stripeClient.getSessionPaymentIntent(
      donationSession
    );
    const charge = donationSessionPaymentIntent.charges.data[0];
    const paymentDate = new Date(charge.created * 1000);
    const now = new Date();
    const secondsSincePaymentInitiated = (now.getTime() - paymentDate.getTime()) / 1000;

    if (secondsSincePaymentInitiated > 15) {
      return { message: 'Invalid donation.' };
    }

    // return immediately if donation amount is less than the gold price (minimum amount to apply upgrade)
    if (donationAmountPounds < GOLD_PRICE_PENCE) {
      return response.redirect(successUrl);
    }

    // give user floor(amount / gold price) months of donation benefits i.e.
    // add that amount of months to their donation_upgrade_expires_at, or if that date is in the past / null,
    // set the column to NOW() + x months

    // amount of months to add to donation perks
    const donationPerkMonths = Math.floor(donationAmountPounds / GOLD_PRICE_PENCE);

    // new expiration is max(now, expiration) + months
    const newDonationUpgradeExpiresAt =
      new Date(donationUpgradeExpiresAt) < now ? now : new Date(donationUpgradeExpiresAt);

    newDonationUpgradeExpiresAt.setMonth(
      newDonationUpgradeExpiresAt.getMonth() + donationPerkMonths
    );

    // promote to gold, update donation_upgrade_expires_at, and redirect to the supplied success URL
    await promoteUserToGold(request.user.id);

    const { User: UserModel } = datasource().models;

    const user = await UserModel.findOne({ where: { id: request.user.id } });
    await user.update({ donationUpgradeExpiresAt: newDonationUpgradeExpiresAt });
    await user.save();
    await userRetriever.invalidate();

    return response.redirect(successUrl);
  }
}
