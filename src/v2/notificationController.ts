/* eslint-disable class-methods-use-this */
import { Request } from 'express';
import httpStatus from 'http-status';
import { MarkNotificationsAsReadRequest, Notification } from 'knockout-schema';
import { Body, Get, JsonController, OnUndefined, Put, Req, UseBefore } from 'routing-controllers';
import { OpenAPI, ResponseSchema } from 'routing-controllers-openapi';
import { getAll, markAllAsRead, markAsRead } from '../controllers/notificationController';
import { authentication } from '../middleware/auth';

@OpenAPI({ tags: ['Notifications'] })
@JsonController('/notifications')
export default class NotificationController {
  @Get('/')
  @UseBefore(authentication.required)
  @ResponseSchema(Notification, { isArray: true })
  getNotifications(@Req() request: Request): Promise<Notification[]> {
    return getAll(request.user.id);
  }

  @Put('/')
  @UseBefore(authentication.required)
  @OnUndefined(httpStatus.OK)
  markNotificationsAsRead(@Req() request: Request, @Body() body: MarkNotificationsAsReadRequest) {
    return markAsRead(request.user.id, body.notificationIds);
  }

  @Put('/all')
  @UseBefore(authentication.required)
  @OnUndefined(httpStatus.OK)
  async markAllNotificationsAsRead(@Req() request: Request) {
    return markAllAsRead(request.user.id);
  }
}
