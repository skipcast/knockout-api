/* eslint-disable class-methods-use-this */
import httpStatus from 'http-status';
import {
  Get,
  JsonController,
  UseBefore,
  Post,
  Body,
  QueryParam,
  HttpCode,
  Req,
  Param,
  Delete,
} from 'routing-controllers';
import { OpenAPI, ResponseSchema } from 'routing-controllers-openapi';
import { Request } from 'express';
import {
  AlertsResponse,
  Alert,
  CreateAlertRequest,
  BatchDeleteRequest,
  OpenAPIParam,
} from 'knockout-schema';
import { authentication } from '../middleware/auth';
import datasource from '../models/datasource';
import { get } from '../controllers/alertController';

@OpenAPI({ tags: ['Alerts'] })
@JsonController('/alerts')
export default class AlertController {
  @Get('/:page?')
  @UseBefore(authentication.required)
  @OpenAPIParam('page', { description: 'The page of Alerts to retreive' })
  @OpenAPIParam('hideNsfw', { description: 'Whether to hide NSFW threads' })
  @HttpCode(httpStatus.OK)
  @ResponseSchema(AlertsResponse)
  async fetchAlerts(
    @QueryParam('hideNsfw') hideNsfw: number,
    @Param('page') page: number,
    @Req() request: Request
  ) {
    return get(request.user.id, page, Boolean(hideNsfw));
  }

  @Post('/')
  @UseBefore(authentication.required)
  @OpenAPI({ summary: 'Create an Alert' })
  @HttpCode(httpStatus.CREATED)
  @ResponseSchema(Alert)
  async createAlert(@Body() body: CreateAlertRequest, @Req() request: Request) {
    const { Alert: AlertModel } = datasource().models;

    await AlertModel.upsert({
      user_id: request.user.id,
      thread_id: body.threadId,
      lastPostNumber: body.lastPostNumber,
      lastSeen: new Date(),
    });

    return { message: `Created an alert for thread #${body.threadId}` };
  }

  @Delete('/:threadId')
  @UseBefore(authentication.required)
  @OpenAPI({ summary: 'Delete an Alert' })
  @OpenAPIParam('threadId', { description: 'The Thread ID of the Alert.' })
  @HttpCode(httpStatus.OK)
  async deleteAlert(@Param('threadId') threadId: number, @Req() request: Request) {
    const { Alert: AlertModel } = datasource().models;

    await AlertModel.destroy({
      where: { user_id: request.user.id, thread_id: threadId },
    });

    return { message: 'Alert deleted' };
  }

  @Post('/batchDelete')
  @UseBefore(authentication.required)
  @OpenAPI({ summary: 'Delete multiple Alerts' })
  @HttpCode(httpStatus.OK)
  async deleteAlerts(@Body() body: BatchDeleteRequest, @Req() request: Request) {
    const { Alert: AlertModel } = datasource().models;

    await AlertModel.destroy({
      where: { user_id: request.user.id, thread_id: body.threadIds || [] },
    });

    return { message: 'Alerts deleted' };
  }
}
