import { IsOptional, IsString } from 'class-validator';

export default class UpdateUserRequest {
  @IsOptional()
  @IsString()
  username?: string;

  @IsOptional()
  @IsString()
  avatarUrl?: string;

  @IsOptional()
  @IsString()
  backgroundUrl?: string;
}
