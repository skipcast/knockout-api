import { IsOptional, IsString } from 'class-validator';

export default class CheckUsernameRequest {
  @IsOptional()
  @IsString()
  username: string;
}
