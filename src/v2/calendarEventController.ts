/* eslint-disable class-methods-use-this */
import httpStatus from 'http-status';
import {
  Get,
  JsonController,
  UseBefore,
  Post,
  Body,
  Put,
  Param,
  QueryParam,
  HttpCode,
  Delete,
  Req,
  Res,
} from 'routing-controllers';
import { OpenAPI, ResponseSchema } from 'routing-controllers-openapi';
import { Op } from 'sequelize';
import { Request, Response } from 'express';
import {
  OpenAPIParam,
  CalendarEvent,
  CreateCalendarEventRequest,
  UpdateCalendarEventRequest,
  CALENDAR_EVENT_DATE_RANGE_LIMIT_DAYS,
} from 'knockout-schema';
import { authentication } from '../middleware/auth';
import errorHandler from '../services/errorHandler';
import { calendarEventControllerPolicy } from '../policies';
import datasource from '../models/datasource';
import CalendarEventRetriever from '../retriever/calendarEvent';
import calendarEventDateRangeError from '../validations/calendarEvent';

const { catchErrors } = errorHandler;

@OpenAPI({ tags: ['CalendarEvents'] })
@JsonController('/calendarEvents')
export default class CalendarEventController {
  @Get('/')
  @OpenAPI({
    summary: 'View all calendar events that start between a starting and ending date.',
    description: `Length between startDate and endDate must can not be more than ${CALENDAR_EVENT_DATE_RANGE_LIMIT_DAYS} days.`,
  })
  @OpenAPIParam('startDate', {
    description: 'Calendar Events that start at or after this date.',
    required: true,
  })
  @OpenAPIParam('endDate', {
    description: 'Calendar Events that start before this date.',
    required: true,
  })
  @ResponseSchema(CalendarEvent, { isArray: true })
  async getCalendarEvents(
    @QueryParam('startDate', { required: true }) startDateIsoString: string,
    @QueryParam('endDate', { required: true }) endDateIsoString: string,
    @Res() response: Response
  ) {
    const { CalendarEvent: CalendarEventModel } = datasource().models;

    const startDate = Date.parse(startDateIsoString);
    const endDate = Date.parse(endDateIsoString);

    const dateRangeError = calendarEventDateRangeError(startDate, endDate);

    if (dateRangeError) {
      return response.status(httpStatus.BAD_REQUEST).send({
        message: dateRangeError,
      });
    }

    const calendarEventIds = await CalendarEventModel.findAll({
      attributes: ['id'],
      where: {
        startsAt: {
          [Op.between]: [startDate, endDate],
        },
      },
      order: [['created_at', 'DESC']],
    }).then((events) => events.map((event) => event.id));

    return new CalendarEventRetriever(calendarEventIds).getObjectArray();
  }

  @Post('/')
  @UseBefore(...[authentication.required, catchErrors(calendarEventControllerPolicy.store)])
  @OpenAPI({ summary: 'Create a Calendar Event' })
  @HttpCode(httpStatus.CREATED)
  @ResponseSchema(CalendarEvent)
  async createCalendarEvent(
    @Body() body: CreateCalendarEventRequest,
    @Req() request: Request,
    @Res() response: Response
  ) {
    const { CalendarEvent: CalendarEventModel } = datasource().models;

    const startsAtDate = Date.parse(body.startsAt);
    const endsAtDate = Date.parse(body.endsAt);

    const dateRangeError = calendarEventDateRangeError(startsAtDate, endsAtDate);

    if (dateRangeError) {
      return response.status(httpStatus.BAD_REQUEST).send({
        message: dateRangeError,
      });
    }

    const calendarEvent = await CalendarEventModel.create({
      createdBy: request.user.id,
      title: body.title,
      description: body.description,
      threadId: body.threadId,
      startsAt: startsAtDate,
      endsAt: endsAtDate,
    });

    return new CalendarEventRetriever(calendarEvent.id).getSingleObject();
  }

  @Put('/:id')
  @UseBefore(...[authentication.required, catchErrors(calendarEventControllerPolicy.update)])
  @OpenAPI({ summary: 'Update a Calendar Event' })
  @OpenAPIParam('id', { description: 'The id of the Calendar Event.' })
  @HttpCode(httpStatus.OK)
  @ResponseSchema(CalendarEvent)
  async updateCalendarEvent(
    @Param('id') id: number,
    @Body() body: UpdateCalendarEventRequest,
    @Res() response: Response
  ) {
    const { CalendarEvent: CalendarEventModel } = datasource().models;

    const retriever = new CalendarEventRetriever(id);
    const {
      title: oldTitle,
      description: oldDescription,
      thread: oldThread,
      startsAt: oldStartsAt,
      endsAt: oldEndsAt,
    } = await retriever.getSingleObject();

    const startsAtDate = Date.parse(body.startsAt || oldStartsAt);
    const endsAtDate = Date.parse(body.endsAt || oldEndsAt);

    if (body.startsAt || body.endsAt) {
      const dateRangeError = calendarEventDateRangeError(startsAtDate, endsAtDate);

      if (dateRangeError) {
        return response.status(httpStatus.BAD_REQUEST).send({
          message: dateRangeError,
        });
      }
    }

    const calendarEventRecord = await CalendarEventModel.findOne({ where: { id } });

    await calendarEventRecord.update({
      title: body.title || oldTitle,
      description: body.description || oldDescription,
      threadId: body.threadId || oldThread.id,
      startsAt: startsAtDate,
      endsAt: endsAtDate,
    });
    await calendarEventRecord.save();

    await retriever.invalidate();
    return retriever.getSingleObject();
  }

  @Delete('/:id')
  @UseBefore(...[authentication.required, catchErrors(calendarEventControllerPolicy.destroy)])
  @OpenAPI({ summary: 'Delete a Calendar Event' })
  @OpenAPIParam('id', { description: 'The id of the Calendar Event.' })
  @HttpCode(httpStatus.OK)
  async destroyCalendarEvent(@Param('id') id: number) {
    const { CalendarEvent: CalendarEventModel } = datasource().models;
    await CalendarEventModel.destroy({ where: { id } });
    return { message: 'Calendar Event deleted' };
  }
}
