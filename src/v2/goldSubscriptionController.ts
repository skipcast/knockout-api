/* eslint-disable class-methods-use-this */
import httpStatus from 'http-status';
import { Request } from 'express';
import {
  Get,
  JsonController,
  UseBefore,
  Post,
  Body,
  HttpCode,
  Delete,
  Req,
  BadRequestError,
} from 'routing-controllers';
import { OpenAPI, ResponseSchema } from 'routing-controllers-openapi';
import {
  GoldSubscriptionCheckoutResponse,
  GoldSubscriptionCheckoutRequest,
  GoldSubscription,
} from 'knockout-schema';
import { authentication } from '../middleware/auth';
import StripeClient from '../services/stripeClient';

@OpenAPI({ tags: ['GoldSubscriptions'] })
@JsonController('/gold-subscriptions')
export default class GoldSubscriptionController {
  @Post('/checkout-session')
  @UseBefore(authentication.required)
  @OpenAPI({
    summary:
      'Create and return a Knockout Gold Subscription Stripe checkout session for the current user',
  })
  @HttpCode(httpStatus.CREATED)
  @ResponseSchema(GoldSubscriptionCheckoutResponse)
  async checkout(@Req() request: Request, @Body() body: GoldSubscriptionCheckoutRequest) {
    const stripeClient = new StripeClient();

    const existingSubscription = await stripeClient.findGoldSubscription(request.user.id);
    if (existingSubscription) {
      throw new BadRequestError(
        `Knockout Gold Subscription already exists for user ${request.user.id}`
      );
    }

    const session = await stripeClient.goldCheckoutSession(
      request.user.id,
      body.successUrl,
      body.cancelUrl
    );
    return { sessionId: session.id };
  }

  @Get('/')
  @UseBefore(authentication.required)
  @OpenAPI({
    summary: 'Get the current Knockout Gold Subscription for the current user',
  })
  @HttpCode(httpStatus.OK)
  @ResponseSchema(GoldSubscription)
  async getSubscription(@Req() request: Request) {
    const existingSubscription = await new StripeClient().findGoldSubscription(request.user.id);
    if (!existingSubscription) {
      return { status: 'inactive' };
    }
    return existingSubscription;
  }

  @Delete('/')
  @UseBefore(authentication.required)
  @OpenAPI({ summary: 'Cancel the Knockout Gold Subscription for the current user' })
  @ResponseSchema(GoldSubscription)
  async cancel(@Req() request: Request) {
    const subscription = await new StripeClient().cancelGoldSubscription(request.user.id);
    if (!subscription) {
      throw new BadRequestError(
        `Knockout Gold Subscription does not exist for user ${request.user.id}`
      );
    }

    return subscription;
  }
}
