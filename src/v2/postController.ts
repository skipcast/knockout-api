/* eslint-disable class-methods-use-this */
import httpStatus from 'http-status';
import {
  Get,
  JsonController,
  UseBefore,
  Post,
  Body,
  Put,
  Param,
  HttpCode,
  Req,
  Delete,
} from 'routing-controllers';
import { OpenAPI, ResponseSchema } from 'routing-controllers-openapi';
import { Request } from 'express';
import {
  OpenAPIParam,
  CreatePostRequest,
  UpdatePostRequest,
  RatePostRequest,
  Post as PostSchema,
} from 'knockout-schema';
import { authentication } from '../middleware/auth';
import PostRetriever from '../retriever/post';
import { rateLimiterUserMiddleware } from '../middleware/rateLimit';
import { postController, ratingsController } from '../controllers';
import errorHandler from '../services/errorHandler';
import { postControllerPolicy } from '../policies';

const { catchErrors } = errorHandler;

@OpenAPI({ tags: ['Posts'] })
@JsonController('/posts')
export default class PostController {
  @Get('/:id')
  @UseBefore(...[authentication.required, catchErrors(postControllerPolicy.get)])
  @OpenAPI({ summary: 'Get a post' })
  @OpenAPIParam('id', { description: 'The id of the post.' })
  @ResponseSchema(PostSchema)
  async getPost(@Param('id') id: number): Promise<PostSchema> {
    return new PostRetriever(id).getSingleObject();
  }

  @Post('/')
  @UseBefore(
    ...[
      authentication.required,
      rateLimiterUserMiddleware,
      catchErrors(postControllerPolicy.postStore),
    ]
  )
  @HttpCode(httpStatus.CREATED)
  @OpenAPI({ summary: 'Create a post' })
  @ResponseSchema(PostSchema)
  createPost(@Body() body: CreatePostRequest, @Req() request: Request): Promise<PostSchema> {
    return postController.store(body, request.user, request.ipInfo, request.app.get('io'));
  }

  @Put('/:id')
  @UseBefore(...[authentication.required, catchErrors(postControllerPolicy.postUpdate)])
  @OpenAPI({ summary: 'Update a post' })
  @OpenAPIParam('id', { description: 'The id of the post.' })
  async updatePost(
    @Param('id') id: number,
    @Body() body: UpdatePostRequest,
    @Req() request: Request
  ) {
    return postController.update(id, body, request.user, request.ipInfo);
  }

  @Put('/:id/ratings')
  @UseBefore(...[authentication.required, catchErrors(postControllerPolicy.postRatingStore)])
  @OpenAPI({ summary: 'Rate a post' })
  @OpenAPIParam('id', { description: 'The id of the post.' })
  async ratePost(@Param('id') id: number, @Body() body: RatePostRequest, @Req() request: Request) {
    return ratingsController.store(id, body, request.user, request.app.get('io'));
  }

  @Delete('/:id/ratings')
  @UseBefore(authentication.required)
  @OpenAPI({ summary: "Remove a post's rating" })
  @OpenAPIParam('id', { description: 'The id of the post.' })
  async unratePost(@Param('id') id: number, @Req() request: Request) {
    return ratingsController.remove(id, request.user);
  }
}
