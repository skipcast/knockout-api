/* eslint-disable class-methods-use-this */
import { Request, Response } from 'express';
import httpStatus from 'http-status';
import {
  Get,
  JsonController,
  UseBefore,
  Req,
  Res,
  Post,
  Body,
  Put,
  Param,
  HttpCode,
  QueryParam,
} from 'routing-controllers';
import { OpenAPI, ResponseSchema } from 'routing-controllers-openapi';
import {
  CreateThreadRequest,
  NewThread,
  Thread,
  ThreadWithLastPost,
  ThreadWithPosts,
  ThreadWithRecentPosts,
  UpdateThreadRequest,
  UpdateThreadTagsRequest,
  ThreadMetadata,
  OpenAPIParam,
} from 'knockout-schema';
import { threadController } from '../controllers';
import { authentication } from '../middleware/auth';
import { rateLimiterUserMiddleware } from '../middleware/rateLimit';
import { threadControllerPolicy } from '../policies';
import errorHandler from '../services/errorHandler';
import ThreadRetriever, { ThreadFlag } from '../retriever/thread';
import { getGuestUserRole } from '../helpers/role';
import RoleRetriever, { RoleFlag } from '../retriever/role';

const { catchErrors } = errorHandler;

@OpenAPI({ tags: ['Threads'] })
@JsonController('/threads')
export default class ThreadController {
  @Get('/latest')
  @UseBefore(authentication.optional)
  @ResponseSchema(ThreadWithLastPost, { isArray: true })
  @OpenAPIParam('excludedSubforums', {
    description: "A string with subforum ids separated by ',' that should be excluded",
    example: '1,2,3',
  })
  async getLatestThreads(
    @Req() request: Request,
    @QueryParam('excludedSubforums') excludedSubforums: string
  ) {
    return threadController.latest(request, excludedSubforums?.split(/,/) || []);
  }

  @Get('/popular')
  @UseBefore(authentication.optional)
  @ResponseSchema(ThreadWithRecentPosts, { isArray: true })
  @OpenAPIParam('excludedSubforums', {
    description: "A string with subforum ids separated by ',' that should be excluded",
    example: '1,2,3',
  })
  async getPopularThreads(
    @Req() request: Request,
    @QueryParam('excludedSubforums') excludedSubforums: string
  ) {
    return threadController.popular(request, excludedSubforums?.split(/,/) || []);
  }

  @Get('/:id/metadata')
  @OpenAPI({ summary: 'View metadata for a thread' })
  @OpenAPIParam('id', { description: 'The id of the thread.' })
  @ResponseSchema(ThreadMetadata)
  async getMetadata(@Param('id') id: number, @Res() response: Response) {
    const threadFlags = [
      ThreadFlag.RETRIEVE_SHALLOW,
      ThreadFlag.INCLUDE_SUBFORUM,
      ThreadFlag.INCLUDE_USER,
    ];
    const thread: any = await new ThreadRetriever(id, threadFlags).getSingleObject();
    const guestUserRoleRecord = await getGuestUserRole();
    const guestUserRole = await new RoleRetriever(guestUserRoleRecord.id, [
      RoleFlag.INCLUDE_PERMISSION_CODES,
    ]).getSingleObject();
    const { permissionCodes } = guestUserRole;

    // if the thread is deleted or is not publicly visibly, return
    if (
      thread.deletedAt !== null ||
      !permissionCodes.includes(`subforum-${thread.subforumId}-view`)
    ) {
      return response.status(httpStatus.FORBIDDEN).send({});
    }

    return {
      title: thread.title,
      iconId: thread.iconId,
      subforumName: thread.subforum.name,
      updatedAt: thread.updatedAt,
      createdAt: thread.createdAt,
      username: thread.user.username,
    };
  }

  @Get('/:id/:page?')
  @UseBefore(...[authentication.optional, catchErrors(threadControllerPolicy.getPostsAndCount)])
  @OpenAPIParam('id', { description: 'The id of the thread.' })
  @OpenAPIParam('page', { description: 'The page of the thread.' })
  @ResponseSchema(ThreadWithPosts)
  getThreadPosts(@Req() request: Request) {
    return threadController.getPostsAndCount(request);
  }

  @Post('/')
  @UseBefore(
    ...[
      authentication.required,
      rateLimiterUserMiddleware,
      catchErrors(threadControllerPolicy.store),
    ]
  )
  @OpenAPI({ summary: 'Create a thread' })
  @HttpCode(httpStatus.CREATED)
  @ResponseSchema(NewThread)
  async createThread(
    @Req() request: Request,
    @Res() response: Response,
    @Body() body: CreateThreadRequest
  ) {
    /* eslint-disable @typescript-eslint/naming-convention */
    const { icon_id, user_id, subforum_id, background_url, background_type, ...result } =
      await threadController.store(request, response, body);
    /* eslint-enable @typescript-eslint/naming-convention */

    return {
      ...result,
      iconId: icon_id,
      userId: user_id,
      subforumId: subforum_id,
      backgroundUrl: background_url,
      backgroundType: background_type,
    };
  }

  @Put('/:id')
  @UseBefore(...[authentication.required, catchErrors(threadControllerPolicy.update)])
  @OpenAPI({ summary: 'Update a thread' })
  @OpenAPIParam('id', { description: 'The id of the thread.' })
  @ResponseSchema(Thread)
  updateThread(
    @Param('id') id: number,
    @Req() request: Request,
    @Body() body: UpdateThreadRequest
  ) {
    return threadController.update(id, body, request.user, request.app.get('io'));
  }

  @Put('/:id/tags')
  @UseBefore(...[authentication.required, catchErrors(threadControllerPolicy.updateTags)])
  @OpenAPIParam('id', { description: 'The id of the thread.' })
  async updateThreadTags(
    @Param('id') id: number,
    @Req() request: Request,
    @Body() body: UpdateThreadTagsRequest
  ) {
    await threadController.updateTags(id, body);
    return { message: 'Tags updated.' };
  }
}
