import { GoldSubscription } from 'knockout-schema';
import Stripe from 'stripe';

import {
  STRIPE_DONATION_PRICE_ID,
  STRIPE_GOLD_MEMBER_PRICE_ID,
  STRIPE_SECRET_KEY,
} from '../../config/server';

import datasource from '../models/datasource';
import UserRetriever, { UserFlag } from '../retriever/user';
import redis from './redisClient';

export const AVAILABLE_PRODUCT_TYPES = ['Gold Member'];

const USER_KNOCKOUT_GOLD_PREFIX = 'knockout-gold-user-';
const USER_KNOCKOUT_GOLD_TTL_SECONDS = 86400;

/**
 * A wrapper around Stripe to handle common flows, such as creating Stripe customers
 * and creating checkout sessions for Knockout Gold.
 */
export default class StripeClient {
  /**
   * The authenticated Stripe instance associated with this StripeClient
   */
  private stripeInstance: Stripe;

  /**
   * Initializes our Stripe interface along with an authenticated Stripe client.
   */
  constructor() {
    try {
      this.stripeInstance = new Stripe(STRIPE_SECRET_KEY, {
        apiVersion: '2020-08-27',
      });
    } catch (exception) {
      this.stripeInstance = null;
      console.error(`[stripe-api] Could not initialize Stripe client: ${exception.message}`);
    }
  }

  /**
   * Finds the current Knockout Gold subscription for a user
   *
   * @param userId
   * @returns the associated Knockout Gold Stripe Subscription, or null if no sub is found
   */
  public async findGoldSubscription(userId: number): Promise<GoldSubscription> {
    // if the user does not have a stripe customer ID, return null
    const userRetriever = new UserRetriever(userId, [UserFlag.INCLUDE_STRIPE_CUSTOMER_ID]);
    const user = await userRetriever.getSingleObject();
    const { stripeCustomerId } = user;

    if (!stripeCustomerId) {
      return null;
    }

    // if we already have a cached subscription, return it
    const cachedSubscription = await redis.getAsync(StripeClient.subscriptionCacheKey(userId));
    const parsedSubscription = cachedSubscription ? JSON.parse(cachedSubscription) : null;

    if (parsedSubscription) {
      return parsedSubscription;
    }

    // otherwise, get the subscription for the user
    try {
      const userGoldMemberSubscriptions = await this.stripeInstance.subscriptions.list({
        customer: stripeCustomerId,
        price: STRIPE_GOLD_MEMBER_PRICE_ID,
      });

      const formattedSubscription = StripeClient.formattedSubscription(
        userGoldMemberSubscriptions.data[0]
      );

      redis.setex(
        StripeClient.subscriptionCacheKey(userId),
        USER_KNOCKOUT_GOLD_TTL_SECONDS,
        JSON.stringify(formattedSubscription)
      );

      return formattedSubscription;
    } catch (exception) {
      console.error(`[stripe-api] Error finding gold membership for user with ID ${userId}:`);
      console.error(exception.message);
      return null;
    }
  }

  /**
   * Creates a Stripe Checkout Session for purchasing a Knockout Gold subscription for a user
   * Does not create a new session of user already has an active Knockout Gold subscription
   *
   * @param userId the user ID we are creating this checkout session for
   * @param successUrl the URL to redirect to on successful payment
   * @param cancelUrl the URL to redirect to when the user cancels the operation
   * @returns the corresponding Stripe Checkout Session, or null if the user has Knockout Gold already
   */
  public async goldCheckoutSession(
    userId: number,
    successUrl: string,
    cancelUrl: string
  ): Promise<Stripe.Checkout.Session> {
    const stripeCustomerid = await this.findOrCreateStripeCustomerId(userId);
    const currentSubscription = await this.findGoldSubscription(userId);

    // make sure we cant get a duplicate checkout session if the user is already gold
    if (currentSubscription) {
      return null;
    }

    try {
      const session = await this.stripeInstance.checkout.sessions.create({
        mode: 'subscription',
        payment_method_types: ['card'],
        customer: stripeCustomerid,
        line_items: [
          {
            price: STRIPE_GOLD_MEMBER_PRICE_ID,
            quantity: 1,
          },
        ],
        metadata: {
          name: `Knockout Gold checkout session for User ${userId}`,
        },
        success_url: successUrl,
        cancel_url: cancelUrl,
      });

      return session;
    } catch (exception) {
      console.error(
        `[stripe-api] Error creating Knockout Gold checkout session for user with ID ${userId}`
      );
      console.error(exception.message);
      return null;
    }
  }

  /**
   * Creates a Stripe Checkout Session for a user donating a fixed amount
   *
   * @param userId the user ID we are creating this checkout session for
   * @param successUrl the URL to redirect to on successful payment
   * @param cancelUrl the URL to redirect to when the user cancels the operation
   * @param amountCentsUsd: the amount in cents USD the checkout session's donation will be for
   * @returns the corresponding Stripe Checkout Session, or null if the session could not be generated
   */
  public async donationCheckoutSession(
    userId: number,
    successUrl: string,
    cancelUrl: string
  ): Promise<Stripe.Checkout.Session> {
    const stripeCustomerid = await this.findOrCreateStripeCustomerId(userId);

    try {
      const session = await this.stripeInstance.checkout.sessions.create({
        mode: 'payment',
        payment_method_types: ['card'],
        customer: stripeCustomerid,
        line_items: [
          {
            price: STRIPE_DONATION_PRICE_ID,
            quantity: 1,
          },
        ],
        metadata: {
          name: `Knockout donation checkout session for User ${userId}`,
          type: 'donation',
        },
        success_url: successUrl,
        cancel_url: cancelUrl,
        submit_type: 'donate',
      });

      return session;
    } catch (exception) {
      console.error(
        `[stripe-api] Error creating Knockout donation checkout session for user with ID ${userId}`
      );
      console.error(exception.message);
      return null;
    }
  }

  public async getCompletedDonationSession(
    donationSessionId: string
  ): Promise<Stripe.Checkout.Session> {
    try {
      const session = await this.stripeInstance.checkout.sessions.retrieve(donationSessionId);
      return session;
    } catch (exception) {
      console.error(
        `[stripe-api] Error retrieving donation session data for session ${donationSessionId}`
      );
      console.error(exception.message);
      return null;
    }
  }

  public async getSessionPaymentIntent(
    donationSession: Stripe.Checkout.Session
  ): Promise<Stripe.PaymentIntent> {
    try {
      const paymentIntent = await this.stripeInstance.paymentIntents.retrieve(
        String(donationSession.payment_intent)
      );
      return paymentIntent;
    } catch (exception) {
      console.error(
        `[stripe-api] Error retrieving payment intent for session ${donationSession.id}`
      );
      console.error(exception.message);
      return null;
    }
  }

  /**
   * Deletes the Stripe Customer associated with a User. Does nothing if the user
   * has no associated Stripe Customer ID
   *
   * @param userId the user we want to delete the Stripe Customer for
   * @returns true if the deletion was a success or if the user has no stripe customer; false otherwise
   */
  public async deleteStripeCustomer(userId: number): Promise<boolean> {
    const userRetriever = new UserRetriever(userId, [UserFlag.INCLUDE_STRIPE_CUSTOMER_ID]);
    const user = await userRetriever.getSingleObject();
    const { stripeCustomerId } = user;

    // if the user does not have an associated stripe customer, return
    if (!stripeCustomerId) {
      return true;
    }

    try {
      const deletionResponse = await this.stripeInstance.customers.del(stripeCustomerId);

      if (deletionResponse.deleted) {
        // update user and bust user cache
        const { User: UserModel } = datasource().models;

        const userRecord = await UserModel.findOne({ where: { id: userId } });
        await userRecord.update({ stripeCustomerId });
        await userRecord.save();

        await userRetriever.invalidate();

        return true;
      }
      return false;
    } catch (exception) {
      console.error(`[stripe-api] Error deleting Stripe Customer for user with ID ${userId}`);
      console.error(exception.message);
      return null;
    }
  }

  /**
   * Cancels a user's current Knockout Gold subscription, if they have one
   *
   * @param userId the user who we want to cancel Knockout Gold for
   * @returns the cancelled Stripe Subscription, or null if no sub was found
   */
  public async cancelGoldSubscription(userId: number): Promise<GoldSubscription> {
    const subscription = await this.findGoldSubscription(userId);
    if (!subscription) {
      return null;
    }

    try {
      // cancel the subscription amd bust the subscription cache for the user
      const cancelledSubscription = await this.stripeInstance.subscriptions.del(
        subscription.stripeId
      );

      redis.del(StripeClient.subscriptionCacheKey(userId));
      return StripeClient.formattedSubscription(cancelledSubscription);
    } catch (exception) {
      console.error(
        `[stripe-api] Error cancelling Knockout Gold subscription for user with ID ${userId}`
      );
      console.error(exception.message);
      return null;
    }
  }

  /**
   * Finds the corresponding Stripe Customer ID for a user,
   * or creates one if it does not exist already.
   *
   * @param userId the user ID to fetch the Stripe customer ID for
   * @returns the Stripe customer ID of the user
   */
  private async findOrCreateStripeCustomerId(userId: number): Promise<string> {
    // if the user already has a stripe customer id, return it
    const userRetriever = new UserRetriever(userId, [UserFlag.INCLUDE_STRIPE_CUSTOMER_ID]);
    const user = await userRetriever.getSingleObject();
    let { stripeCustomerId } = user;

    if (stripeCustomerId) {
      return stripeCustomerId;
    }

    try {
      // otherwise create a new customer and return its id
      const newStripeCustomer = await this.stripeInstance.customers.create({
        description: `Knockout User ${userId}`,
        metadata: {
          knockoutUserId: userId,
        },
      });

      stripeCustomerId = newStripeCustomer['id'];

      // update user and bust user cache
      const { User: UserModel } = datasource().models;

      const userRecord = await UserModel.findOne({ where: { id: userId } });
      await userRecord.update({ stripeCustomerId });
      await userRecord.save();
      await userRetriever.invalidate();

      return stripeCustomerId;
    } catch (exception) {
      console.error(`[stripe-api] Error creating stripe customer for user with ID ${userId}:`);
      console.error(exception.message);
      return null;
    }
  }

  /**
   * Takes a Stripe.Subscription object and returns a GoldSubscription
   *
   * @param stripeSubscription the raw Stripe Subscription
   * @returns the GoldSubscription object corresponding to the Stripe Subscription,
   *          or null if the stripe sub is null
   */
  private static formattedSubscription(stripeSubscription?: Stripe.Subscription): GoldSubscription {
    if (!stripeSubscription) {
      return null;
    }

    const {
      id,
      start_date: startDate,
      status,
      cancel_at: cancelAt,
      current_period_end: currentPeriodEnd,
    } = stripeSubscription;
    const startedAt = new Date(startDate * 1000).toISOString();
    const canceledAt = cancelAt ? new Date(cancelAt * 1000).toISOString() : '';
    const nextPaymentAt = currentPeriodEnd ? new Date(currentPeriodEnd * 1000).toISOString() : '';
    return {
      stripeId: id,
      status,
      startedAt,
      canceledAt,
      nextPaymentAt,
    };
  }

  /**
   * The Knockout Gold subscription cache key for a user
   *
   * @param userId the ID of the User to get KO subscription info for
   * @returns the cache key
   */
  private static subscriptionCacheKey(userId: number) {
    return `${USER_KNOCKOUT_GOLD_PREFIX}-${userId}`;
  }
}
