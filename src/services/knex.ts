import knex from 'knex';
import config from '../../config/config';

const { host, port, username, password, database, pool } = config[process.env.NODE_ENV];

const knexClient = knex({
  client: 'mysql2',
  connection: {
    host,
    port,
    user: username,
    password,
    database,
    charset: 'utf8mb4',
    pool: {
      ...pool,
      createTimeoutMillis: 3000,
      acquireTimeoutMillis: 30000,
      idleTimeoutMillis: 30000,
      reapIntervalMillis: 1000,
      createRetryIntervalMillis: 100,
      propagateCreateError: false,
    },
  },
});

// New export
export default knexClient;

// Backwards-compatible export
module.exports.knex = knexClient;
