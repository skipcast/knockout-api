import knex from '../knex';
import logUserDeletionError from './logUserDeletionError';

export default async (userId: number) => {
  try {
    await knex('ProfileComments').where('author', userId).update({
      content: 'This profile comment is from a deleted user and is no longer directly available.',
    });
  } catch (e) {
    logUserDeletionError('ProfileComments', userId, e.message);
  }
};
