import DELETED_USER_ID from '../../constants/deletedUserId';
import knex from '../knex';
import logUserDeletionError from './logUserDeletionError';

export const wipePosts = async (userId: number) => {
  try {
    await knex('Posts').where('user_id', userId).update({
      content: "This post's content has been removed by a moderator.",
    });
  } catch (e) {
    logUserDeletionError('Posts', userId, e.message);
  }
};

export default async (userId: number) => {
  try {
    await knex('Posts').where('user_id', userId).update({
      user_id: DELETED_USER_ID,
      country_name: null,
      app_name: null,
    });
  } catch (e) {
    logUserDeletionError('Posts', userId, e.message);
  }
};
