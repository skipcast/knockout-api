import DELETED_USER_ID from '../../constants/deletedUserId';
import knex from '../knex';
import logUserDeletionError from './logUserDeletionError';

export const wipePostEdits = async (userId: number) => {
  try {
    await knex('PostEdits').where('user_id', userId).update({
      content: "This post edit's content has been removed by a moderator.",
      edit_reason: null,
    });
  } catch (e) {
    logUserDeletionError('PostEdits', userId, e.message);
  }
};

export default async (userId: number) => {
  try {
    await knex('PostEdits').where('user_id', userId).update({
      user_id: DELETED_USER_ID,
      content: null,
      edit_reason: null,
    });
  } catch (e) {
    logUserDeletionError('PostEdits', userId, e.message);
  }
};
