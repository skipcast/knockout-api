import knex from '../knex';
import logUserDeletionError from './logUserDeletionError';

export default async (userId: number) => {
  try {
    await knex('ReadThreads').where('user_id', userId).del();
  } catch (e) {
    logUserDeletionError('ReadThreads', userId, e.message);
  }
};
