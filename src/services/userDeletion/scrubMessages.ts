import knex from '../knex';
import logUserDeletionError from './logUserDeletionError';

export default async (userId: number) => {
  try {
    await knex('Messages').where('user_id', userId).update({
      content: 'This message is from a deleted user and is no longer available.',
    });
  } catch (e) {
    logUserDeletionError('Messages', userId, e.message);
  }
};
