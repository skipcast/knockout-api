import scrubExternalAccount from './scrubExternalAccount';
import deleteIpAddresses from './deleteIpAddresses';
import scrubMessages from './scrubMessages';
import scrubPosts from './scrubPosts';
import scrubPostEdits from './scrubPostEdits';
import scrubProfileComments from './scrubProfileComments';
import scrubUser from './scrubUser';
import scrubUserProfiles from './scrubUserProfiles';
import deleteAlerts from './deleteAlerts';
import deleteReadThreads from './deleteReadThreads';
import invalidateCache from './invalidateCache';

/** *
 * deleteUser
 *
 * given a user id, scrubs* (does not delete) all data associated with them
 *
 * * we "scrub" the data, as in nullifying / randomizing all data associated
 * with the user. we do this as opposed to deleting due to:
 *
 * - data consistency (ex: foreign keys on Users might not correspond to a row if the user was deleted)
 * - reporting ability (we want to know when and how often users are deleted and why)
 * - frontend compatibilites (ex: trying to get a user from the user_id in a
 *   response from another API call, related to first point above)
 *
 * tables with user_id as a foriegn key:
 *
 * - Alert
 * - Ban*
 * - ConversationUser*
 * - ExternalAccount
 * - IpAddress
 * - Message
 * - Post
 * - PostEdit
 * - ProfileComment (as `user_profile` and `author`)
 * - Rating*
 * - ReadThread
 * - Thread
 * - UserProfile
 *
 * we go through each* of these tables and scrub all data,
 * so we keep the row but lose all information
 *
 * * we don't scrub these because these tables contain only
 * * internally relevant data, such as join tables or
 * * tables used for internal logkeeping (e.g. bans and ban reasons)
 */
export default async (userId: number) => {
  await scrubExternalAccount(userId);
  await deleteIpAddresses(userId);
  await scrubMessages(userId);
  await scrubPosts(userId);
  await scrubPostEdits(userId);
  await scrubProfileComments(userId);
  await scrubUserProfiles(userId);
  await deleteAlerts(userId);
  await deleteReadThreads(userId);
  await scrubUser(userId);
  await invalidateCache(userId);
};
