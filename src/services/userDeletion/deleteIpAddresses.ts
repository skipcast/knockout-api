import knex from '../knex';
import logUserDeletionError from './logUserDeletionError';

export default async (userId: number) => {
  try {
    await knex('IpAddresses').where('user_id', userId).del();
  } catch (e) {
    logUserDeletionError('IpAddresses', userId, e.message);
  }
};
