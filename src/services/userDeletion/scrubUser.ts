import { getBannedUserRole } from '../../helpers/role';
import knex from '../knex';
import logUserDeletionError from './logUserDeletionError';

export default async (userId: number) => {
  try {
    const bannedUserRole = await getBannedUserRole();
    await knex('Users').where('id', userId).update({
      username: 'DELETED_USER',
      usergroup: 1,
      email: null,
      avatar_url: '',
      background_url: '',
      external_type: null,
      external_id: null,
      title: null,
      role_id: bannedUserRole.id,
    });
  } catch (e) {
    logUserDeletionError('Users', userId, e.message);
  }
};
