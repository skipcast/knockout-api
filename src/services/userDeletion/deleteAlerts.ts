import knex from '../knex';
import logUserDeletionError from './logUserDeletionError';

export default async (userId: number) => {
  try {
    await knex('Alerts').where('user_id', userId).del();
  } catch (e) {
    logUserDeletionError('Alerts', userId, e.message);
  }
};
