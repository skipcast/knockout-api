import knex from '../knex';
import logUserDeletionError from './logUserDeletionError';

export default async (userId: number) => {
  try {
    await knex('Ratings').where('user_id', userId).delete();
  } catch (e) {
    logUserDeletionError('Ratings', userId, e.message, 'delete');
  }
};
