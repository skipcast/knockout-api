import knex from '../knex';
import logUserDeletionError from './logUserDeletionError';

export default async (userId: number) => {
  try {
    await knex('Threads').where('user_id', userId).update({
      background_url: null,
      background_type: null,
    });
  } catch (e) {
    logUserDeletionError('Threads', userId, e.message);
  }
};
