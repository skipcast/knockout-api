import knex from '../knex';
import logUserDeletionError from './logUserDeletionError';

export default async (userId: number) => {
  try {
    await knex('UserProfiles').where('user_id', userId).update({
      heading_text: null,
      personal_site: null,
      background_url: null,
      background_type: null,
      steam: null,
      discord: null,
      github: null,
      youtube: null,
      twitter: null,
      twitch: null,
      gitlab: null,
      tumblr: null,
      header: null,
    });
  } catch (e) {
    logUserDeletionError('UserProfiles', userId, e.message);
  }
};
