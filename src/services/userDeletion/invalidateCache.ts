import datasource from '../../models/datasource';
import MessageRetriever from '../../retriever/message';
import PostRetriever from '../../retriever/post';
import ProfileCommentRetriever from '../../retriever/profileComment';
import UserRetriever from '../../retriever/user';
import ProfileRetriever from '../../retriever/userProfile';
import logUserDeletionError from './logUserDeletionError';

// clears the cache for all outward facing models after user deletion
export default async (userId: number) => {
  try {
    const { Message, Post, ProfileComment } = datasource().models;

    // messages
    const messageIds = (
      await Message.findAll({
        attributes: ['id'],
        where: { user_id: userId },
      })
    ).map((message) => message.id);

    // posts
    const postIds = (
      await Post.findAll({
        attributes: ['id'],
        where: { user_id: userId },
      })
    ).map((post) => post.id);

    // profile comments
    const profileCommentIds = (
      await ProfileComment.findAll({
        attributes: ['id'],
        where: { author: userId },
      })
    ).map((comment) => comment.id);

    // invalidate caches
    await new MessageRetriever(messageIds).invalidate();
    await new PostRetriever(postIds).invalidate();
    await new ProfileCommentRetriever(profileCommentIds).invalidate();
    await new UserRetriever(userId).invalidate();
    await new ProfileRetriever(userId).invalidate();
  } catch (e) {
    logUserDeletionError('cache', userId, e.message);
  }
};
