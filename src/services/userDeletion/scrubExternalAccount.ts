import knex from '../knex';
import logUserDeletionError from './logUserDeletionError';

export default async (userId: number) => {
  try {
    await knex('ExternalAccounts').where('user_id', userId).update({
      provider: '',
      external_id: '',
    });
  } catch (e) {
    logUserDeletionError('ExternalAccounts', userId, e.message);
  }
};
