export default (resource: string, userId: number, message: string, action: string = 'scrub') => {
  console.log(`[user-deletion] Could not ${action} ${resource} for user ${userId}: ${message}`);
};
