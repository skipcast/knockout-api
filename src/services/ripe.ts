import axios from 'axios';
import httpAdapter from 'axios/lib/adapters/http';
import redis from './redisClient';
import knex from './knex';

export default class Ripe {
  private ip: string;

  constructor(ip) {
    axios.defaults.adapter = httpAdapter;
    this.ip = ip;
  }

  getCacheKey() {
    return `ripe-whois-${this.ip}`;
  }

  public async setQueryCache(data) {
    await redis.setAsync(this.getCacheKey(), JSON.stringify(data));
  }

  public async queryCache() {
    const response = await redis.getAsync(this.getCacheKey());
    return JSON.parse(response);
  }

  public async queryAPI() {
    try {
      const response = await axios({
        method: 'get',
        baseURL: 'https://stat.ripe.net/',
        url: `/data/whois/data.json`,
        params: {
          resource: this.ip,
        },
      });
      return response.data;
    } catch (err) {
      return null;
    }
  }

  public async query() {
    const cacheResults = await this.queryCache();
    if (cacheResults !== null) return cacheResults;
    const apiResults = await this.queryAPI();
    return apiResults;
  }

  public async isBanned() {
    try {
      const result = await this.query();
      const netname = result.data.records[0]
        .filter((record) => record.key === 'netname')
        .shift().value;
      const results = await knex.from('IspBans').pluck('id').where('netname', netname);
      const output = typeof results[0] !== 'undefined';
      await this.setQueryCache(result);
      return output;
    } catch (err) {
      return false;
    }
  }

  public async isParentBanned() {
    try {
      const result = await this.query();
      const origin = result.data.irr_records[0]
        .filter((record) => record.key === 'origin')
        .shift().value;
      const results = await knex.from('IspBans').pluck('id').where('asn', origin);
      const output = typeof results[0] !== 'undefined';
      await this.setQueryCache(result);
      return output;
    } catch (err) {
      return false;
    }
  }
}
