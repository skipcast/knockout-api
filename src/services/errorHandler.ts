import { Request, Response, NextFunction, RequestHandler } from 'express';
import { BadRequestError, HttpError } from 'routing-controllers';
import ResponseStrategy from '../helpers/responseStrategy';

const env = process.env.NODE_ENV || 'development';

const respondWithError = (
  statusCode: number,
  exception: Error,
  res: Response,
  message?: string
) => {
  if (res.headersSent) {
    return;
  }
  res.status(statusCode);
  res.json({
    message,
    error: exception.message,
    stackTrace: env === 'development' ? exception.stack : undefined,
  });
};

/*
  Catch Errors Handler

  With async/await, you need some way to catch errors
  Instead of using try{} catch(e) {} in each controller, we wrap the function in
  catchErrors(), catch and errors they throw, and pass it along to our express middleware with next()
*/
const catchErrors = (fn: RequestHandler) =>
  function (req: Request, res: Response, next: NextFunction) {
    fn(req, res, next).catch((error) => {
      if ((!res.headersSent && error instanceof BadRequestError) || error instanceof HttpError) {
        return respondWithError(error.httpCode, error, res, error.message);
      }
      return next();
    });
  };

// Always remember to exec this after all routes on the app
const catchHandler = (app) => {
  app.use((_req, res) => {
    if (!res.headersSent) {
      ResponseStrategy.send(res, null, null);
    }
  });

  app.use((err, _req, res) => {
    if (!res.headersSent) {
      ResponseStrategy.send(res, err, null);
    }
  });
};

export default {
  catchErrors,
  catchHandler,
  respondWithError,
};
