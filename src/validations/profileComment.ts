import { PROFILE_COMMENT_CHARACTER_LIMIT } from 'knockout-schema';
import validateLength from './validateLength';

const validateProfileCommentLength = (comment: string) =>
  validateLength(comment, PROFILE_COMMENT_CHARACTER_LIMIT);

export default validateProfileCommentLength;
