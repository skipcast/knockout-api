import { User } from 'express';
import { RoleCode } from '../helpers/permissions';
import UserRetriever from '../retriever/user';
import knex from '../services/knex';
import datasource from '../models/datasource';
import { getLimitedUserRole } from '../helpers/role';

export const LIMITED_USER_POST_COUNT = 15;

const { User: UserModel } = datasource().models;

const validateUserFields = (user: User) => {
  if (!user) {
    return false;
  }
  if (!user.username || !user.id || !user.avatar_url || !user.usergroup) {
    console.error('user validation failed.');
    console.error('user.username', user.username);
    console.error('user.id', user.id);
    console.error('user.avatar_url', user.avatar_url);
    console.error('user.usergroup', user.usergroup);
    return false;
  }
  if (user.username === '') {
    console.error('user validation failed.');
    console.error('user.username', user.username);
    return false;
  }
  if (user.username.length < 3) {
    console.error('user validation failed.');
    console.error('user.username is ', user.username);
    return false;
  }
  return true;
};

const isLimitedUser = async (userId: number): Promise<boolean> => {
  // users under a week old should be considered suspicious under certain conditions
  // this validator is used in conjunction with a VPN/datacenter IP check to prevent alts
  const userRetriever = new UserRetriever(userId);
  const user = await userRetriever.getSingleObject();

  if (!user) return true;

  // if the user's role is present and not limited, no need to check further
  // since all newly created users have the limited-user role,
  // this check will be ran every time the function is invoked
  // for them and we will update their role if needed
  if (user.role?.code && user.role?.code !== RoleCode.LIMITED_USER) {
    return false;
  }

  // check if user is limited
  const now = new Date();
  const cutoff = new Date(user.createdAt);
  const grandfather = new Date('7 January 2020');
  cutoff.setDate(cutoff.getDate() + 7);
  const limited =
    now.getTime() < cutoff.getTime() ||
    (user.posts < LIMITED_USER_POST_COUNT && grandfather.getTime() < cutoff.getTime());

  const userRecord = await UserModel.findOne({ where: { id: userId } });
  let newRole;

  // if the user is not limited,
  // bump the role to a regular user role so we dont have to check next time
  //
  // otherwise, if the user didn't have a role and should be limited,
  // bump them to a limited user
  if (!limited) {
    newRole = await knex('Roles').select('id').where('code', RoleCode.BASIC_USER).first();
  } else if (!userRecord?.roleId) {
    newRole = await getLimitedUserRole();
  }

  if (newRole) {
    userRecord.update({ roleId: newRole.id });
    userRecord.save();

    await userRetriever.invalidate();
  }

  return limited;
};

export { validateUserFields, isLimitedUser };
