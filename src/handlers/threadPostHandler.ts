import { getUserPermissionCodes, userCanViewThread } from '../helpers/user';
import ThreadRetriever from '../retriever/thread';

const threadPostRoomPrefix = 'threads';

export const threadPostRoom = (id) => `${threadPostRoomPrefix}:${id}:posts`;

export default async (socket) => {
  const joinThreadPosts = async (id: number) => {
    if (socket.request.user?.id) {
      const permissionCodes = await getUserPermissionCodes(socket.request.user.id);
      const thread = await new ThreadRetriever(id).getSingleObject();

      if (!thread) {
        return;
      }

      const canViewThread = userCanViewThread(socket.request.user.id, permissionCodes, thread);
      if (canViewThread) {
        socket.join(threadPostRoom(id));
      }
    }
  };

  const joinAllThreadPosts = async (ids: number[]) => ids?.map(joinThreadPosts);

  const leaveThreadPosts = async (id) => {
    if (socket.request.user?.id) {
      socket.leave(threadPostRoom(id));
    }
  };

  socket.on('threadPosts:joinAll', joinAllThreadPosts);
  socket.on('threadPosts:join', joinThreadPosts);
  socket.on('threadPosts:leave', leaveThreadPosts);
};
