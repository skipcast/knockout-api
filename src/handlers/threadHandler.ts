import { Server } from 'socket.io';
import redis from '../services/redisClient';
// eslint-disable-next-line import/no-cycle
import ThreadRetriever, { ThreadFlag } from '../retriever/thread';

const threadViewerPrefix = 'thread-viewers';
const threadRoomPrefix = 'thread';
const viewerCacheLifetime = 360;

const sessionPrefix = 'session';
const sessionCacheLifetime = 86400;

const POPULAR_THREAD_VIEWS_KEY = 'popular-threads-views';

const getRoomKey = (id) => `${threadRoomPrefix}-${id}`;
const getViewerKey = (id) => `${threadViewerPrefix}-${id}`;

export const getMemberCountKey = (id) => `${getViewerKey(id)}-members`;
export const getGuestCountKey = (id) => `${getViewerKey(id)}-guests`;
export const getPopularThreadViewsKey = (subforumId) => `${POPULAR_THREAD_VIEWS_KEY}-${subforumId}`;

export default async (io: Server, socket) => {
  const getSessionKey = (session, id) => `${sessionPrefix}-${session}-${id}`;
  const getSessionCount = async (session, id) => redis.getAsync(getSessionKey(session, id));

  const expireAndEmit = async (key, room) => {
    redis.expire(key, viewerCacheLifetime);
    const count = Number(await redis.getAsync(key));
    io.in(room).emit(`thread:${key.split('-').pop()}`, count);
  };

  const decrement = async (key) => {
    const value = await redis.decrAsync(key);
    if (value < 0) {
      await redis.setAsync(key, 0);
    }
  };

  const updatePopularThreads = async (id) => {
    const totalViewers =
      Number(await redis.getAsync(getMemberCountKey(id))) +
      Number(await redis.getAsync(getGuestCountKey(id)));

    const threadRetriever = new ThreadRetriever(id, [
      ThreadFlag.RETRIEVE_SHALLOW,
      ThreadFlag.EXCLUDE_READ_THREADS,
    ]);

    const { subforumId } = await threadRetriever.getSingleObject();

    const cacheKey = getPopularThreadViewsKey(subforumId);

    await redis.zaddAsync(cacheKey, totalViewers, id);
    redis.zremrangebyrankAsync(cacheKey, 0, -21);
  };

  const joinThread = async (id) => {
    if (!id) return;
    await socket.join(getRoomKey(id));

    await redis.incrAsync(getSessionKey(socket.request.session, id));
    if (Number(await getSessionCount(socket.request.session, id)) <= 1) {
      const isNotInRoom = socket.request.user?.id
        ? await redis.saddAsync(getViewerKey(id), socket.request.user?.id)
        : true;

      if (isNotInRoom) {
        if (socket.request.user?.id) {
          await redis.incrAsync(getMemberCountKey(id));
          expireAndEmit(getMemberCountKey(id), getRoomKey(id));
        } else {
          await redis.incrAsync(getGuestCountKey(id));
          expireAndEmit(getGuestCountKey(id), getRoomKey(id));
        }
        updatePopularThreads(id);

        redis.expire(getViewerKey(id), viewerCacheLifetime);
      }
      redis.expire(getSessionKey(socket.request.session, id), sessionCacheLifetime);
    }
  };

  const leaveThread = async (id) => {
    if (!id) return;
    socket.leave(getRoomKey(id));

    await redis.decrAsync(getSessionKey(socket.request.session, id));
    if (Number(await getSessionCount(socket.request.session, id)) > 0) return;

    const wasInRoom = socket.request.user?.id
      ? await redis.sremAsync(getViewerKey(id), socket.request.user?.id)
      : true;

    if (wasInRoom) {
      if (socket.request.user?.id) {
        await decrement(getMemberCountKey(id));
        expireAndEmit(getMemberCountKey(id), getRoomKey(id));
      } else {
        await decrement(getGuestCountKey(id));
        expireAndEmit(getGuestCountKey(id), getRoomKey(id));
      }
    }

    updatePopularThreads(id);
    redis.expire(getViewerKey(id), viewerCacheLifetime);
  };

  const leaveAllThreads = async () => {
    socket.rooms.forEach((room: string) => {
      if (room.startsWith(threadRoomPrefix)) {
        const thread = room.split('-')[1];
        leaveThread(thread);
      }
    });
  };

  socket.on('thread:join', joinThread);
  socket.on('thread:leave', leaveThread);
  socket.on('disconnecting', leaveAllThreads);
};

export const getViewers = async (id: number) => redis.smembersAsync(getViewerKey(id));
