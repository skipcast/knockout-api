export const EVENTS_ROOM = 'events';

const eventsRoleRoom = (user) => `${EVENTS_ROOM}:role-${user.role_id}`;

const eventsUserRoom = (user) => `${EVENTS_ROOM}:${user.id}`;

export default async (socket) => {
  const joinEvents = async () => {
    if (socket.request.user?.id) {
      // Join the events room for general events
      socket.join(EVENTS_ROOM);

      // Join the events room corresponding to the user role ID
      socket.join(eventsRoleRoom(socket.request.user));

      // Join the events room corresponding to the specific user
      socket.join(eventsUserRoom(socket.request.user));
    }
  };

  const leaveEvents = async () => {
    if (socket.request.user?.id) {
      socket.leave(EVENTS_ROOM);
      socket.leave(eventsRoleRoom(socket.request.user));
      socket.leave(eventsUserRoom(socket.request.user));
    }
  };

  socket.on('events:join', joinEvents);
  socket.on('events:leave', leaveEvents);
};
