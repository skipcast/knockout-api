'use strict';

import { lorem, date } from 'faker';

const THREADS_PER_SUBFORUM = 40;
const ICON_ID_COUNT = 30;

const randomInt = (max, min = 1) => {
  return Math.floor(Math.random() * (max - 1) + min);
}

const threadAttributes = (title, iconId, userId, subforumId, createdAt, locked, pinned) => {
  return {
    title,
    icon_id: iconId,
    user_id: userId,
    subforum_id: subforumId,
    created_at: createdAt,
    updated_at: createdAt,
    locked,
    pinned,
    background_url: '',
    background_type: ''
  }
}

const randomThreads = (subforumIds, userIds) => {
  const threads = subforumIds.map((subforumId) => {
    return Array.from({ length: THREADS_PER_SUBFORUM }).map(() => {
      const userId = userIds[randomInt(userIds.length)];
      // 1% chance to lock
      const isLocked = Math.random < 0.01;
      // 0.5% chance to pin
      const isPinned = Math.random() < 0.005;
      return threadAttributes(
        lorem.sentence(randomInt(7, 4)),
        randomInt(ICON_ID_COUNT),
        userId,
        subforumId,
        date.recent(30),
        isLocked,
        isPinned
      );
    })
  })

  return [].concat(...threads);
}

export async function up(queryInterface, Sequelize) {
  const subforumIds = (await queryInterface.sequelize.query(
    `SELECT id FROM Subforums`,
    { type: Sequelize.QueryTypes.SELECT }
  )).map((record) => record.id);

  const userIds = (await queryInterface.sequelize.query(
    `SELECT id FROM Users`,
    { type: Sequelize.QueryTypes.SELECT }
  )).map((record) => record.id);

  return queryInterface.bulkInsert('Threads', randomThreads(subforumIds, userIds), {});
}

export function down(queryInterface, Sequelize) {
  return queryInterface.bulkDelete('Threads', null, {});
}
