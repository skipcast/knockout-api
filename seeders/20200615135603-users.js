'use strict';

import { internet } from 'faker';

const STANDARD_USER_COUNT = 5000;
const GOLD_MEMBER_USER_COUNT = 5000;
const MOD_USER_COUNT = 5;
const ADMIN_COUNT = 2;

const userAttributes = (username, email, usergroup) => {
  return {
    username,
    email,
    avatar_url: '',
    background_url: '',
    usergroup,
    external_type: 'Google',
    external_id: 'GoogleID',
    created_at: new Date(),
    updated_at: new Date()
  }
}

const standardUsers = Array.from({ length: STANDARD_USER_COUNT }).map(() => 
  userAttributes(internet.userName(), internet.email(), 1)
);

const goldMemberUsers = Array.from({ length: GOLD_MEMBER_USER_COUNT }).map(() => 
  userAttributes(internet.userName(), internet.email(), 2)
);

const moderatorUsers = Array.from({ length: MOD_USER_COUNT }).map(() => 
  userAttributes(internet.userName(), internet.email(), 3)
);

const adminUsers = Array.from({ length: ADMIN_COUNT }).map(() => 
  userAttributes(internet.userName(), internet.email(), 4)
);

const randomUserArray = [].concat.apply([], [
  standardUsers, 
  goldMemberUsers, 
  moderatorUsers, 
  adminUsers
])

export function up(queryInterface, Sequelize) {
  return queryInterface.bulkInsert('Users', randomUserArray);
}
export function down(queryInterface, Sequelize) {
  return queryInterface.bulkDelete('Users', null, {});
}
