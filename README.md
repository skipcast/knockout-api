# knockout-api

## Express API for the Knockout Forums

This is the API for the Knockout Forums.

It's written in Typescript, uses Express to serve web requests, MariaDB for data storage, and Redis for in-memory storage.

For the official frontend, see the [Knockout Web Client.](https://gitlab.com/knockout-community/knockout-front/)

## Dependencies

- Node 16.15.0
- yarn
- Docker
- Docker Compose

## Local Development

3. Run a `yarn install`
4. Create a `.env` file in the root directory with the contents of `.env.example`
5. Run `docker-compose up -d`
6. Run `yarn sequelize db:migrate` to create the tables
7. Run `yarn sequelize db:seed:all` to create sample data
8. Run `yarn start`
9. Open `http://localhost:3000/thread` on your browser to test that the API is working

## Deployment

Deployments are handled automatically via Gitlab CI/CD.

When the `master` branch is updated, a script is ran on each web server to deploy the newest API.

# License

See LICENSE file.

# Contributing to knockout-api

All new routes should be created in `src/v2`.

1. Make a feature branch targeting qa
2. Do work, push it to your branch
3. Write tests
4. Create a pull request
